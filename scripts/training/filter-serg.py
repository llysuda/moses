# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 17:29:06 2014

@author: lly
"""

import sys
import gzip
import codecs
import multiprocessing as mp
from multiprocessing import Pool as ProcessPool

MAX_SIZE = 5

class Hole(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

class Node (object):
    def __init__(self, sid, word, pos, fid):
        self.sid = sid
        self.word = word
        self.pos = pos
        self.fid = fid
    
class Deptree (object):
    def __init__ (self, line):
        self.nodes = []
        tokens = line.split()
        for i, token in enumerate(tokens):
            word, pos, fid, rel = token.split('|')
            if fid == "-1":
                fid = "9999"
            node = Node(i,word, pos, int(fid))
            self.nodes.append(node)
    def size(self):
        return len(self.nodes)
    
    def get_heads(self, start, end):
        heads = []
        for i in range(start, end+1):
            if self.nodes[i].fid < start or self.nodes[i].fid > end:
                heads.append(self.nodes[i])
        return heads
    def get_span_fid(self, start, end):
        for i in range(start, end+1):
            if self.nodes[i].fid < start or self.nodes[i].fid > end:
                return self.nodes[i].fid
        return -1
        
    def get_heads_str(self, start, end):
        heads = self.get_heads(start,end)
        return "_".join([n.pos for n in heads])
    
    def valid(self, start, end):
        nodes = {}
        external_nodes = {}
        
        for i in range(start, end+1):
            sid = i
            eid = self.nodes[i].fid
            if eid < 0 or eid >= len(self.nodes):
                external_nodes[eid] = 1
            nodes[sid] = 1
            nodes[eid] = 1

        if len(nodes) > 1 + end-start+1:
            return False
    
        for i in range(start):
            sid = i;
            eid = self.nodes[i].fid;
            if sid in nodes:
                external_nodes[sid] = 1
            if eid in nodes:
                external_nodes[eid] = 1

        for i in range(end+1, len(self.nodes)):
            sid = i
            eid = self.nodes[i].fid
            if sid in nodes:
                external_nodes[sid] = 1
            if eid in nodes:
                external_nodes[eid] = 1;
        if len(external_nodes) > 2:
            return False
        return True

    def isInDependent(self,start,end, startHole, endHole):
        for i in range(start, startHole):
            fid = self.nodes[i].fid
            if fid >= startHole and fid <= endHole:
                return False
        for i in range(endHole+1, end+1):
            fid = self.nodes[i].fid
            if fid >= startHole and fid <= endHole:
                return False
        return True

    def getDepth(self, start, end, startHole, endHole):
        minDepth = 9999
        for i in range(startHole, endHole+1):
            depth = 1
            fid = self.nodes[i].fid
            while fid >= start and fid <= end:
                depth += 1
                fid = self.nodes[fid].fid
            if depth < minDepth:
                minDepth = depth
        return minDepth


def get_lex_serg(start, end, tree):
    items = []
    for i in range(start, end+1):
        fid = tree.nodes[i].fid
        if fid < start or fid > end:
            fid = -1
        else:
            fid = fid-start
        items.append(tree.nodes[i].word+":"+str(fid))
    return " ".join(items)

def add_to_rules(start, end, rules):
    if start not in rules:
        rules[start] = {}
    if end not in rules[start]:
        rules[start][end] = 1

def get_hier_serg(start, end, holes, tree):
    curr_pos = start
    hindex = 0
    items = []
    word_count = 0
    new_pos = {}
    while curr_pos <= end:
        is_hole = False
        if hindex < len(holes) and curr_pos == holes[hindex].start:
            is_hole = True
        if is_hole:
            for i in range(curr_pos, holes[hindex].end+1):
                new_pos[i] = word_count
            #label = tree.get_heads_str(curr_pos, holes[hindex].end)
            #items.append(label)
            curr_pos = holes[hindex].end
            hindex += 1
        else:
            #items.append(tree.nodes[curr_pos].word)
            new_pos[curr_pos] = word_count
        curr_pos += 1
        word_count += 1
    #
    curr_pos = start
    hindex = 0
    while curr_pos <= end:
        is_hole = False
        if hindex < len(holes) and curr_pos == holes[hindex].start:
            is_hole = True
        if is_hole:
            fid = tree.get_span_fid(curr_pos, holes[hindex].end)
            if fid < 0:
                print >> sys.stderr, "no span fid"
                sys.exit(1)
            if fid < start or fid > end:
                fid = -1
            else:
                fid = new_pos[fid]
            label = tree.get_heads_str(curr_pos, holes[hindex].end)
            items.append("["+label+":"+str(fid)+"][X]")
            curr_pos = holes[hindex].end
            hindex += 1
        else:
            fid = tree.nodes[curr_pos].fid
            if fid < start or fid > end:
                fid = -1
            else:
                fid = new_pos[fid]
            items.append(tree.nodes[curr_pos].word+":"+str(fid))
        curr_pos += 1
    
    return " ".join(items)

def add_hier_rules(start, end, rules, serg, holes, init_s, wcount, tree):
    for s in range(init_s, end+1):
        for e in range(s, end+1):
            if not tree.valid(s, e):
                continue
            if not tree.isInDependent(start, end, s,e) or tree.getDepth(start,end,s,e) > 2:
                continue
            if s == start and e == end:
                continue
            if s not in rules or e not in rules[s]:
                continue
            #holes.append(Hole(s,e))
            new_wcount = wcount - (e-s+1)

            if new_wcount < 1:
                continue
            
            holes.append(Hole(s,e))
            if new_wcount + len(holes) <= MAX_SIZE:
                serg[get_hier_serg(start, end, holes, tree)] = 1
            #init_s = e + 1
            add_hier_rules(start, end, rules, serg, holes, e+1, new_wcount, tree)
            holes.pop()
    
def collect_serg_from_line(line):
    line = line.strip()
    serg = {}
    rules = {}
    tree = Deptree(line)
    max_size = min(20,tree.size())
    for size in range(1,max_size+1):
        for start in range(tree.size()-size+1):
            end = start + size - 1
            if not tree.valid(start, end):
                continue
            serg[get_lex_serg(start, end, tree)] = 1
            if end-start+1 <= MAX_SIZE:
                add_to_rules(start, end, rules)
            if start == end:
                continue
            holes = []
            add_hier_rules(start, end, rules, serg, holes, start, end-start+1, tree)
    print >> sys.stderr, ".",
    return serg

def collect_serg_from_input(input):
    serg = {}
    f = codecs.open(input,'r', encoding='utf-8')
    pool = ProcessPool(2*mp.cpu_count())
    results = pool.map(collect_serg_from_line, f.readlines())
    pool.close()
    pool.join()
    #count = 0
    #for line in f:
    #    count += 1
    #    #if count % 100 == 0:
    #    print >> sys.stderr, ".",
    #    line = line.strip()
    #    serg.update(collect_serg_from_line(line))
    f.close()
    print >> sys.stderr, ""
    for ret in results:
        serg.update(ret)
    return serg

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print >> sys.stderr, "python input phrase-table [out-table]"
        sys.exit(1)
    input = sys.argv[1];
    phrase_table = sys.argv[2]
    output = sys.argv[3]
    
    print >> sys.stderr, "collecting......"
    sergs = collect_serg_from_input(input)
    
    print >> sys.stderr, "filtering......"
    fin, fout = None, None
    if phrase_table.endswith(".gz"):
        fin = codecs.getreader('utf-8')(gzip.open(phrase_table,'r'))
        fout = codecs.getreader('utf-8')(gzip.open(output, 'w'))
    else:
        fin = codecs.open(phrase_table,'r',encoding='utf-8')
        fout = codecs.open(output, 'w',encoding='utf-8')
    line_count = 0
    for line in fin:
        line_count += 1
        if line_count % 10000000 == 0:
            print >> sys.stderr, str(line_count)+"...",
        src, tgt, dummy = line.strip().split(" ||| ", 2)
        src = " ".join(src.split()[:-1])
        if src in sergs:
            fout.write(line.encode('utf-8'))
    fin.close()
    fout.close()
    print >> sys.stderr, ""

