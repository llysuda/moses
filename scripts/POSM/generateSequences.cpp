#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <set>
#include <cstdlib>

using namespace std;


int stringToInteger(string s)
{

	istringstream buffer(s);
	int some_int;
	buffer >> some_int;
	return some_int;
}

void loadInput(const char * fileName, vector <string> & input)
{

	ifstream sr (fileName);
	char* tmp;
	
	if(sr.is_open())
	{
		while(! sr.eof() )
		{
						
			tmp= new char[5000];
			sr.getline (tmp,5000);
			input.push_back(tmp);
			//cout<<tmp<<input.size()<<endl;
			delete [] tmp;
		}
	
		sr.close();
	}
	else
	{
		cout<<"Unable to read "<<fileName<<endl;
		exit(1);
	}

}

void getWords(string inp, vector <string> & currInput)
{
	currInput.clear();

	int a=0;
	a = inp.find(' ', inp.length()-1);

	if( a == -1)
		inp.append(" ");

	a=0;
	int b=0;

	for (int j=0; j<inp.length(); j++)
	{
			
		a=inp.find(' ',b);
	
		if(a != -1)
		{	
			currInput.push_back(inp.substr(b,a-b));
			
			b=a+1;
			j=b;
		}
		else
		{
			j=inp.length();
		}
	
	}

}


string getTranslation(int index, vector < pair <string , vector <int> > > & gCepts , vector <string> & currF , map <string,int> & singletons)
{

	string translation = "";

	vector <int> fSide = gCepts[index].second;
	vector <int> :: iterator iter;

	for (iter = fSide.begin(); iter != fSide.end(); iter++)
	{
		if (iter != fSide.begin())
		 translation += "^_^";
	
		translation+= currF[*iter];
	}

	if(singletons.find(translation)==singletons.end())
	{
		return "_TRANS_" + gCepts[index].first + "_TO_" + translation + " ";	
	}
	
	else
	{

		return "_TRANS_SLF_ ";
	}
}



int closestGap(map <int,string> gap,int j1, int & gp)
{

	int dist=1172;
	int value=-1;
	int temp=0;
	gp=0;
	int opGap=0;

	map <int,string> :: iterator iter;

		iter=gap.end();
		
		do
		{
			iter--;
			//cout<<"Trapped "<<iter->first<<endl;

		   	if(iter->first==j1 and iter->second=="Unfilled")
			{
				opGap++;
				gp = opGap;
				return j1;
			}
		
		   	if(iter->second =="Unfilled")
		   	{
				opGap++;
				temp = iter->first - j1;

				if(temp<0)
			 	temp=temp * -1;
			
				if(dist>temp && iter->first < j1)
				{
					dist=temp;
					value=iter->first;
					gp=opGap;
				}
		  	}
			 

		}
		while(iter!=gap.begin());

	//cout<<"Out"<<endl;
	return value;
}


void generateStory(vector <pair <string , vector <int> > > & gCepts, set <int> & targetNullWords, vector<string> & currF, map <string,int> & singletons)
{

	int fl = 0;
	int i = 0;   // Current English string position
	int j = 0; // Current French Position
	int N = gCepts.size(); // Total number of English words
	int k = 0; // Number of already generate French words
	int E = 0; // Position after most rightward French word generate so far
	int j1 = 0;  // Next french translation;
	int Li =0; // Links of word i 
	int Lj=0; // Links of word j
	map <int,int > generated;
	map <int,string> gap;
	map <int,int> :: iterator  iter;
	int gp=0;
	//vector <string> iterator :: iterF;

	while (targetNullWords.find(j) != targetNullWords.end())
	{
		cout<<"_INS_"<<currF[j]<<" ";
		generated[j]=-1;  // This word is generated -1 means unlinked ...
		j=j+1;
	}
		
	while (i < gCepts.size() && gCepts[i].second.size() == 0)
	{
		cout<<"_DEL_"<<gCepts[i].first<<" ";
		i=i+1;
	}

	E=j; // Update the position of most rightward French word

	while (i<N)
	{

		//cout<<"I am sending to the link "<<i<<" with 0 "<<endl;
		//j1 = getLink(i,0,Li,k);
		
		Li = gCepts[i].second.size();
		j1 = gCepts[i].second[k];

		//cout<<"i = "<<i<<" j1 = "<<j1<<"  j = "<<j<<" E = "<<E<<endl;

		if(j<j1) // reordering needed ...
		{
			iter = generated.find(j);
			if( iter == generated.end()) // fj is not generated ...
			{
				cout<<"_INS_GAP_ ";
				gap[j] = "Unfilled";
			}
		
			if (j==E)
			{
				j=j1;
			}
			else
			{
				cout<<"_JMP_FWD_ ";
				j=E;
			}

		}

		if(j1<j)
		{
			iter = generated.find(j);
			if(j<E && iter == generated.end()) // fj is not generated ...
			{

				cout<<"_INS_GAP_ ";
				gap[j]="Unfilled";
			}	

			j=closestGap(gap,j1,gp);
			//cout<<j<<endl;
			cout<<"_JMP_BCK_"<<gp<<" ";
			
			if(j==j1)
				gap[j]="Filled";
	
		}

		if(j<j1)
		{
				cout<<"_INS_GAP_ ";
				gap[j] = "Unfilled";
				j=j1;
		}

		if(k==0)
		{
		  cout<<getTranslation(i, gCepts,currF,singletons);
		}  
		else
		{
		  //cout<<"_CONT_CEPT_ ";
		}
		generated[j]=i;
		j=j+1;
		k=k+1;
		
		while(targetNullWords.find(j) != targetNullWords.end()) // fj is unlinked word ...
		{
			//cout<<"Came here"<<j<<k<<endl;
			cout<<"_INS_"<<currF[j]<<" ";
			generated[j]=-1;  // This word is generated -1 means unlinked ...
			j=j+1;
		}
		
		if(E<j)
		  E=j;
		//cout<<" Li "<<Li<<endl;
		if(k==Li)
		{
			i=i+1;
			k=0;
			
				while(i < gCepts.size() && gCepts[i].second.size() == 0)  // ei is unliked word ...
				{
					cout<<"_DEL_"<<gCepts[i].first<<" ";
					i=i+1;

				}	

		}
		
	}

	cout<<endl;
}



void ceptsInGenerativeStoryFormat(vector < pair < set <int> , set <int> > > & ceptsInPhrase , vector < pair < string , vector <int> > > & gCepts , set <int> & sourceNullWords, vector <string> & currE)
{

	gCepts.clear();
	set <int> eSide;
	set <int> fSide;
	std::set <int> :: iterator iter;
	string english;
	vector <int> germanIndex;
	int engIndex = 0;
	int prev;
	int curr;
	set <int> engDone;

	
	for (int i = 0; i< ceptsInPhrase.size(); i++)
	{
		english = "";
		germanIndex.clear();
		fSide = ceptsInPhrase[i].first;
		eSide = ceptsInPhrase[i].second;	
		
		
		while(engIndex < *eSide.begin())
		{
		 // cout<<engIndex<<" "<<*eSide.begin()<<endl;
		  
		 while(engDone.find(engIndex) != engDone.end())
	 	  engIndex++;		

		  while(sourceNullWords.find(engIndex) != sourceNullWords.end())
		  {
			english = currE[engIndex];
			engIndex++;
			gCepts.push_back(make_pair (english , germanIndex));
			english = "";
		  }
		}

		for (iter = eSide.begin(); iter != eSide.end(); iter++)
		{
		   curr = *iter;
	
		   if(iter != eSide.begin())
		   {
			english += "^_^";
			
			if (prev == curr-1)
			{
			  prev++;
			  engIndex++;
			}
			else
			   engDone.insert(curr);
		   }
		   else
		   {	
			prev = curr;
			//engIndex++;
			engIndex = prev+1;		     
		   }
			english +=currE[curr];

		}

		for (iter = fSide.begin(); iter != fSide.end(); iter++)
		{
			germanIndex.push_back(*iter);				
		}	    

		gCepts.push_back(make_pair (english , germanIndex));
	//	cout<<engIndex<<endl;
	
	}	

	english = "";
	germanIndex.clear();
	
	//for (int i = 0; i< currE.size(); i++)
	// cout<<i<<" "<<currE[i]<<endl;	

   while(engIndex < currE.size())
   {
	// cout<<engIndex<<" "<<currE.size()-1<<endl;
	while(engDone.find(engIndex) != engDone.end())
	 engIndex++;
	
	while(sourceNullWords.find(engIndex) != sourceNullWords.end())
	{
		english = currE[engIndex];
		//cout<<"Here "<<engIndex<<english<<" "<<germanIndex.size()<<endl;
		engIndex++;
		gCepts.push_back(make_pair (english , germanIndex));
		english = "";
	}
    }	
	
}

void printCepts(vector < pair < string , vector <int> > > & gCepts , vector <string> & currF)
{

	string eSide;
	vector <int> fSide;
	
	for (int i = 0; i < gCepts.size(); i++)
	{

		fSide = gCepts[i].second;
		eSide = gCepts[i].first;

		cerr<<eSide;
		cerr<<" <---> ";

		for (int j = 0; j < fSide.size(); j++)
		{
			cerr<<currF[fSide[j]]<<" ";
		}
			    	
		cerr<<endl;
	}
			
}

void getMeCepts ( pair <int,int> & eSide , pair <int,int> & fSide , vector<vector <int> > & tS , vector<vector <int> > & sT)
{
	set <int> :: iterator iter;

  int startE = eSide.first;
  int endE = eSide.second;
  //int fz = fSide.second-fSide.first+1;
  vector <int> t;

  for (int i = eSide.first; i <= eSide.second; i++) {
    if (tS[i].size() > 0) {
	  t = tS[i];
	  for (int j = 0; j < t.size(); j++) {
		fSide.first = std::min(fSide.first,t[j]);
		fSide.second = std::max(fSide.second,t[j]);
	  }
    }
  }

  for (int i = fSide.first; i <= fSide.second; i++) {
    if (sT[i].size() > 0) {
      t = sT[i];
      for (int j = 0 ; j<t.size(); j++) {
        eSide.first = std::min(eSide.first,t[j]);
        eSide.second = std::max(eSide.second,t[j]);
      }
    }
  }

  if (eSide.second > endE || eSide.first < startE) {
    getMeCepts(eSide,fSide,tS,sT);
  }
}

int setMin(const set<int> & nums)
{
	int min = * nums.begin();
	set<int>::const_iterator iter;
	for(iter = nums.begin(); iter != nums.end(); iter++) {
		if (min > *iter) {
		  min = *iter;
		}
	}
	return min;
}

int setMax(const set<int> & nums)
{
	int max = * nums.begin();
	set<int>::const_iterator iter;
	for(iter = nums.begin(); iter != nums.end(); iter++) {
		if (max < *iter) {
		  max = *iter;
		}
	}
	return max;
}

bool overlap( const pair < set <int> , set <int> >& pair1, const pair < set <int> , set <int> >& pair2) 
{
	int startF1 = setMin(pair1.first);
	int endF1 = setMax(pair1.first);
	int startF2 = setMin(pair2.first);
	int endF2 = setMax(pair2.first);
	
	if (!( startF1 > endF2 || startF2 > endF1)) {
		return true;
	}
	
	int startE1 = setMin(pair1.second);
	int endE1 = setMax(pair1.second);
	int startE2 = setMin(pair2.second);
	int endE2 = setMax(pair2.second);
	
	if (!( startE1 > endE2 || startE2 > endE1)) {
		return true;
	}
	
	return false;
}

void constructCepts(vector < pair < set <int> , set <int> > > & ceptsInPhrase, set <int> & sourceNullWords, set <int> & targetNullWords, vector <string> & alignment, int eSize, int fSize)
{
		ceptsInPhrase.clear();
		sourceNullWords.clear();
		targetNullWords.clear();		

		vector <int> align;
		
		std::vector <vector <int> > sT;
		std::vector <vector <int> > tS;
		std::set <int> eSide;
		std::set <int> fSide;
		std::pair <int, int> eSideSpan;
        std::pair <int, int> fSideSpan;
		std::set <int> :: iterator iter;
		std :: map <int , vector <int> > :: iterator iter2;
		std :: pair < set <int> , set <int> > cept;
		int src;
		int tgt;
		ceptsInPhrase.clear();
		
		for (int i = 0; i< fSize; i++)
		{
			vector<int> dummy;
			sT.push_back(dummy);
		}

		for (int i = 0; i< eSize; i++)
		{
			vector<int> dummy;
			tS.push_back(dummy);
		}
		
		for (int j=0; j<alignment.size(); j+=2)
		{
		  align.push_back(stringToInteger(alignment[j+1]));
		  align.push_back(stringToInteger(alignment[j]));	
		}

		for (int i = 0;  i < align.size(); i+=2)
		{
			src = align[i];
			tgt = align[i+1];
			tS[tgt].push_back(src);
			sT[src].push_back(tgt);
		}

		for (int i = 0; i< fSize; i++)
		{
			if (sT[i].size() == 0)
			{
				targetNullWords.insert(i);
			}
		}

		for (int i = 0; i< eSize; i++)
		{
			if (tS[i].size() == 0)
			{
				sourceNullWords.insert(i);
			}
		}

		int preEndE = -1;
		while (preEndE < eSize-1)
		{
			int startE = preEndE + 1;
			if (tS[startE].size() == 0) {
				preEndE++;
				continue;
			}
			
			eSide.clear();
			fSide.clear();
			eSideSpan = make_pair(startE,startE);
	        fSideSpan = make_pair(fSize+1, -1);
			//cerr << "getMeCepts" << endl;
			getMeCepts(eSideSpan, fSideSpan, tS , sT);
			//cerr << "end getMeCepts" << endl;
			//cerr << preEndE << ":" << fSideSpan.first << "|" << fSideSpan.second << "|" << eSideSpan.first << "|" << eSideSpan.second << "<---->";
			
			for (int i = eSideSpan.first; i <= eSideSpan.second; i++) {
			  eSide.insert(i);
			  iter = sourceNullWords.find(i);
			  if (tS[i].size() == 0 && iter != sourceNullWords.end()) {
				sourceNullWords.erase(iter);
			  }
			}
			for (int i = fSideSpan.first; i <= fSideSpan.second; i++) {
			  fSide.insert(i);
			  iter = targetNullWords.find(i);
			  if (sT[i].size() == 0 && iter != targetNullWords.end()) {
				targetNullWords.erase(iter);
			  }
			}

			cept = make_pair (fSide , eSide);
			vector < int > cache;
			for (int i = ceptsInPhrase.size()-1; i>= 0;  i--) {
			  if (overlap(ceptsInPhrase[i],cept)) {
			    cache.push_back(i);
			  }
			}
			for (int i = 0; i < cache.size(); i++) {
			  ceptsInPhrase.erase(ceptsInPhrase.begin()+cache[i]);
			} 
			ceptsInPhrase.push_back(cept);
			
			preEndE = eSideSpan.second;
		}

}

int main(int argc, char * argv[])
{

	vector <string> e;
	vector <string> f;
	vector <string> a;
	vector <string> singletons;
	map <string,int> sTons;
	vector < pair < set <int> , set <int> > > ceptsInPhrase;
	vector < pair < string , vector <int> > > gCepts; 
	
	set <int> sourceNullWords;
	set <int> targetNullWords;
	
	vector <string> currE;
	vector <string> currF;
	vector <string> currA;

	loadInput(argv[4],singletons);

	for(int i=0; i<singletons.size(); i++)
	  sTons[singletons[i]]=i;

	loadInput(argv[1],e);
	loadInput(argv[2],f);
	loadInput(argv[3],a);
	
	ifstream efile(argv[1]);
	ifstream ffile(argv[2]);
	ifstream afile(argv[3]);

	if(!efile.is_open()) {
        	cout << argv[1] << " could not be opened." << endl;
	}
	else if(!ffile.is_open()) {
        	cout << argv[2] << " could not be opened." << endl;
	}
	else if(!afile.is_open()) {
        	cout << argv[3] << " could not be opened." << endl;
	}
	else{

		std::string eString;
		std::string fString;
		std::string aString;

		int i = 0;
		while(getline(efile,eString) && getline(ffile,fString) && getline(afile,aString)){
			//cerr << i << endl;	
			getWords(eString,currE);
			getWords(fString,currF);
			getWords(aString,currA);
			constructCepts(ceptsInPhrase, sourceNullWords , targetNullWords, currA , currE.size(), currF.size());
			//cerr<<"CC done"<<endl;
			ceptsInGenerativeStoryFormat(ceptsInPhrase , gCepts , sourceNullWords, currE);
			//cerr<<"format done"<<endl;	
			//printCepts(gCepts, currF);
			generateStory(gCepts, targetNullWords ,currF,sTons);
			//cerr<<"story done"<<endl;

			i++;
			if(i % 100 == 0) { 
				cerr<<"!";
			}		
		}
	}
/*
	for (int i=0; i<a.size()-1; i++)
	{
		
	
		getWords(e[i],currE);
		getWords(f[i],currF);
		getWords(a[i],currA);
		
		constructCepts(ceptsInPhrase, sourceNullWords , targetNullWords, currA , currE.size(), currF.size());
		cerr<<"CC done"<<endl;
		ceptsInGenerativeStoryFormat(ceptsInPhrase , gCepts , sourceNullWords, currE);
		cout<<"format done"<<endl;	
		// printCepts(gCepts, currF);
		generateStory(gCepts, targetNullWords ,currF,sTons);

		
	}	
*/

	cerr << "\n";
	return 0;
	
}
