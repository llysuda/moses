#!/usr/bin/perl -w

use warnings;
use strict;

while (<>) {
  chomp;
  my @tokens = split /\s+/;
  for my $token(@tokens) {
    my ($s, $t) = split /\-/,$token;
    print STDOUT "$t $s ";
  }
  print STDOUT "\n";
}
