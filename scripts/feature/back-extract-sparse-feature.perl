#!/usr/bin/perl -w

use strict;
use warnings;
use Getopt::Long;

my $MAX_LENGTH = 7;
my ($SRC,$TGT,$ALIGN,$EXTRACT,$SRC_CLASS,$PHRASE_TABLE);
my $HIER = 0;
my $OVER_WRITE = 0;
my $TCOUNT = 4;
GetOptions(
	's=s' => \$SRC,
	't=s' => \$TGT,
	'a=s' => \$ALIGN,
	'e=s' => \$EXTRACT,
	'hier' => \$HIER,
	'l=i' => \$MAX_LENGTH,
	'overwrite' => \$OVER_WRITE,
	'sc=s' => \$SRC_CLASS,
	'threads=i' => \$TCOUNT,
	'pt=s' => \$PHRASE_TABLE
) or die "param error\n";

print STDERR "begin extract features...\n";
# extract feature into file $EXTARCT
if ($OVER_WRITE or (not -e "$EXTRACT.sorted")) {
	if ($HIER) {
		my $EXTARCTER = "/home/lly/plateform/moses/bin/feature-extract-rules";
		my $CMD = "$EXTARCTER $TGT $SRC $ALIGN $EXTRACT --OnlyDirect";
		system($CMD);
	} else {
		my $EXTRACTER = "/home/lly/plateform/moses/bin/feature-extract";
		my $CMD = "$EXTRACTER $TGT $SRC $ALIGN $EXTRACT $MAX_LENGTH";
		system($CMD);
	}
} else {
	print STDERR "re-use $EXTRACT\n";
}
print STDERR "end extract features\n";
print STDERR "begin sort...\n";
 # sort
my $EXTRACT_SORT = "$EXTRACT.sorted";
if ($OVER_WRITE or (not -e $EXTRACT_SORT)) {
	system("LC_ALL=C sort -T . < $EXTRACT > $EXTRACT_SORT");
	`rm $EXTRACT`;
} else {
	print STDERR "re-use $EXTRACT_SORT\n";
}
print STDERR "end sort\n";


# load src class file
my %CLASS;
print STDERR "load source class file...\n";
my %WORD_CLASS;
open I,"<",$SRC_CLASS or die "open $SRC_CLASS error\n";
while (<I>) {
	chomp;
	my ($word,$class) = split /\s+/;
	$WORD_CLASS{$word} = $class;
	$CLASS{$class} = 1;
}
close I;
print STDERR "end load\n";
my $CCOUNT = keys %CLASS;

#die "$CCOUNT\n";

# compress with the same source and target

print STDERR "begin compress and consolidate...\n";
if (not -e $PHRASE_TABLE) {
	system("gunzip $PHRASE_TABLE.gz");
}
#if ($PHRASE_TABLE =~ /^(.+)\.gz$/) {
#	$PHRASE_TABLE = $1;
#}

open PT,"<", $PHRASE_TABLE or die "open $PHRASE_TABLE error\n";
open I, "<", $EXTRACT_SORT or die "open $EXTRACT_SORT error\n";
my $LAST_SRC = "";
my $LAST_TGT = "";
my @FEATURES = ();
my $COUNT = 0;
my $PCOUNT = 1;
my $ptline = <PT>;
chomp $ptline;
while (<I>) {
	if ($COUNT%100000 == 0) { print STDERR "." };
	chomp;
	my ($src,$tgt,$feature) = split /\s+\|\|\|\s+/;
	if ($src eq "" or $tgt eq "" or $feature eq "") {
		die "empty element $CCOUNT: $_\n";
	}
	
	if ($src eq $LAST_SRC and $tgt eq $LAST_TGT) {
		push @FEATURES, split /\s+/,$feature;
	} else {
		
		outone();
		
		$LAST_SRC = $src;
		$LAST_TGT = $tgt;
		@FEATURES = ();
		@FEATURES = split /\s+/,$feature;
	}
	$COUNT++;
}

outone();

close I;
close PT;
print STDERR "end compress\n";

print STDERR "gzip...\n";
`gzip $EXTRACT_SORT`;
`gzip $PHRASE_TABLE`;

print STDERR "end!!!\n";

sub outone {
	if (not $LAST_SRC eq "") {

		my ($ptsrc,$pttgt,$rest) = split /\s+\|\|\|\s+/,$ptline,3;
		
		if ($ptsrc eq $LAST_SRC and $pttgt eq $LAST_TGT) {
			@FEATURES = GetFeature(@FEATURES);
			#chop($FEATURES);
			if (($#FEATURES+1)%2 == 1) {
				die "format error $COUNT: @FEATURES\n";
			}
			
			print "$ptline ||| @FEATURES\n";
			
			while ($ptline = <PT>) {
				chomp($ptline);
				$PCOUNT++;
				my ($ptsrc,$pttgt,$rest) = split /\s+\|\|\|\s+/,$ptline,3;
		
				if ($ptsrc eq $LAST_SRC and $pttgt eq $LAST_TGT) {
					print "$ptline ||| @FEATURES\n";
				} else {
					last;
				}
			}
			
		} else {
			#print STDERR "$ptsrc\n$LAST_SRC\n$pttgt\n$LAST_TGT\n";
			die "line does not match:\n$PCOUNT:$ptline\n$COUNT:$LAST_SRC ||| $LAST_TGT ||| @FEATURES\n";
		}
	} else {
		print STDERR "empty $COUNT\n";
	}
}

sub GetFeature {
	my @out = ();
	#my $string = shift;
	my %FEATURE_VALUE;
	#my @tokens = split /\s+/,$string;
	for my $token (@_) {
		$token =~ /^([A-Za-z0-9]+):(\S+)$/ or die "pattern error $token\n";
		my $flag = $1;
		my $word = $2;
		my $class = 0;
		if ($WORD_CLASS{$word}) {
			$class = $WORD_CLASS{$word}+1;
		}
		
		my $feature = "select_$flag+$class";
		#if ($FEATURE_VALUE{$feature}) {
			#$FEATURE_VALUE{$feature}++;
		#} else {
			$FEATURE_VALUE{$feature} = 1;
#		}
	}
	
	while (my ($key,$value)=each %FEATURE_VALUE) {
		#my $float = $value/($CCOUNT+1);
		push @out, $key; 
		push @out, $value;
	}
	
	#chop($out);
	return @out;
}
