#!/usr/bin/perl -w

use strict;
use warnings;
use Getopt::Long;


my ($EXTRACT,$SRC_CLASS,$PHRASE_TABLE);
my $HIER = 0;
GetOptions(
	'e=s' => \$EXTRACT,
	'hier' => \$HIER,
	'sc=s' => \$SRC_CLASS,
	'pt=s' => \$PHRASE_TABLE
) or die "param error\n";


# load src class file
my %CLASS;
print STDERR "load source class file...\n";
my %WORD_CLASS;
open I,"<",$SRC_CLASS or die "open $SRC_CLASS error\n";
while (<I>) {
	chomp;
	my ($word,$class) = split /\s+/;
	$WORD_CLASS{$word} = $class;
	$CLASS{$class} = 1;
}
close I;
print STDERR "end load\n";
my $CCOUNT = keys %CLASS;

#die "$CCOUNT\n";

# compress with the same source and target

print STDERR "begin compress and consolidate...\n";
open (PT, "gunzip -c $PHRASE_TABLE.gz |") or die "open $PHRASE_TABLE.gz error\n";
open (I, "gunzip -c $EXTRACT.sorted.gz |") or die "open $EXTRACT.sorted.gz error\n";
my $LAST_SRC = "";
my $LAST_TGT = "";
my @COLLECTION = ();

my $COUNT = 0;
my $PCOUNT = 0;
#my $ptline = <PT>;
#chomp $ptline;
while (<I>) {
	if ($COUNT%100000 == 0) { print STDERR "." };
	chomp;
	my ($src,$tgt,$align,$feature) = split /\s+\|\|\|\s+/;
	if ($src eq "" or $tgt eq "" or $align eq "" or $feature eq "") {
		die "empty element $CCOUNT: $_\n";
	}
	
	if ($src eq $LAST_SRC and $tgt eq $LAST_TGT) {
		my $added = 0;
		for my $col (@COLLECTION) {
			my ($a,$f) = @{$col};
			if (checkalign($src,$a,$align)) {
				$f .= $feature." ";
				${$col}[1] = $f;
				$added = 1;
			}
		}
		if (not $added) {
			push @COLLECTION,[$align,$feature];
		}
	} else {
		
		outSameSrcTgt();
		
		$LAST_SRC = $src;
		$LAST_TGT = $tgt;
		@COLLECTION = ();
		push @COLLECTION,[$align,$feature];
	}
	$COUNT++;
}

outSameSrcTgt();

close I;
close PT;
print STDERR "end compress\n";

#print STDERR "gzip...\n";
#`gzip $EXTRACT_SORT`;
#`gzip $PHRASE_TABLE`;

print STDERR "end!!!\n";

sub checkalign {
	
	my ($src,$a,$b) = @_;
	
	if (not $a or not $b) {
		die "$src $a $b\n";
	}
	
	if (not $HIER) {
		return 1;
	}
	
	if ($a eq $b) {
		return 1;
	}
	#return 1;
	
	my @ALIGN = @{parsealign($a)};
	my @LALIGN = @{parsealign($b)};
	my @srcw = split /\s+/,$src;
	for my $i (0..($#srcw-1)) {
		if ($srcw[$i] =~ /^\[.+\]$/) {
			if ((not $#{$ALIGN[$i]} == 0) or (not $#{$LALIGN[$i]}==0) or (not $ALIGN[$i][0] == $LALIGN[$i][0])) {
				return 0;
			}
		}
	}
	return 1;
}


#sub check {
	#my ($src,$tgt,$align) = (shift,shift,shift);
	#if (not $src eq $LAST_SRC) {
		#return 0;
	#}
	#if (not $tgt eq $LAST_TGT) {
		#return 0;
	#}
	#if (not $HIER) {
		#return 1;
	#}
	
	#if ($align eq $LAST_ALIGN) {
		#return 1;
	#}
	##return 1;
	
	#my @ALIGN = @{parsealign($align)};
	#my @LALIGN = @{parsealign($LAST_ALIGN)};
	#my @srcw = split /\s+/,$src;
	#for my $i (0..($#srcw-1)) {
		#if ($srcw[$i] =~ /^\[.+\]$/) {
			#if ((not $#{$ALIGN[$i]} == 0) or (not $#{$LALIGN[$i]}==0) or (not $ALIGN[$i][0] == $LALIGN[$i][0])) {
				#return 0;
			#}
		#}
	#}
	#return 1;
#}

sub parsealign {
	my $align = shift;
	my @tokens = split /\s+/,$align;
	my @ALIGN;
	for my $tok (@tokens) {
		my ($sid,$tid) = split /-/,$tok;
		if (not defined $ALIGN[$sid]) {
			$ALIGN[$sid] = [];
		}
		push @{$ALIGN[$sid]},$tid;
	}
	return \@ALIGN;
}

sub outSameSrcTgt {
	if (not $LAST_SRC eq "") {
		
		for my $col (@COLLECTION) {
			my ($align,$feature) = @{$col};
			$PCOUNT++;
			my $ptline = <PT>;
			chomp $ptline;
			my ($ptsrc,$pttgt,$score,$ptalign,$rest) = split /\s+\|\|\|\s+/,$ptline,5;
			
			if ($ptsrc eq $LAST_SRC and $pttgt eq $LAST_TGT and checkalign($ptsrc,$ptalign,$align)) {
				my @FEATURES = GetFeature(split/\s+/,$feature);
				#chop($FEATURES);
				if (($#FEATURES+1)%2 == 1) {
					die "format error $COUNT: @FEATURES\n";
				}
				
				print "$ptline ||| @FEATURES\n";
				
				#while ($ptline = <PT>) {
				#	chomp($ptline);
				#	$PCOUNT++;
				#	my ($ptsrc,$pttgt,$rest) = split /\s+\|\|\|\s+/,$ptline,3;
			
				#	if ($ptsrc eq $LAST_SRC and $pttgt eq $LAST_TGT) {
				#		print "$ptline ||| @FEATURES\n";
				#	} else {
				#		last;
				#	}
				#}
				
			} else {
				#print STDERR "$ptsrc\n$LAST_SRC\n$pttgt\n$LAST_TGT\n";
				die "line does not match:\n$PCOUNT\n$ptline\n$COUNT:$LAST_SRC ||| $LAST_TGT ||| $align ||| $feature\n";
			}
		}
		
	} else {
		print STDERR "empty $COUNT\n";
	}
}

sub GetFeature {
	my @out = ();
	#my $string = shift;
	my %FEATURE_VALUE;
	#my @tokens = split /\s+/,$string;
	for my $token (@_) {
		$token =~ /^([A-Za-z0-9]+):(\S+)$/ or die "pattern error $token\n";
		my $flag = $1;
		my $word = $2;
		my $class = 0;
		if ($WORD_CLASS{$word}) {
			$class = $WORD_CLASS{$word}+1;
		}
		
		my $feature = "select_$flag+$class";
		#if ($FEATURE_VALUE{$feature}) {
			#$FEATURE_VALUE{$feature}++;
		#} else {
			$FEATURE_VALUE{$feature} = 1;
#		}
	}
	
	while (my ($key,$value)=each %FEATURE_VALUE) {
		#my $float = $value/($CCOUNT+1);
		push @out, $key; 
		push @out, $value;
	}
	
	#chop($out);
	return @out;
}
