#!/usr/bin/perl -w

use strict;
use warnings;

my ($ppl, $src, $tgt) = @ARGV;

my $wc = `wc -l $src`;
chomp $wc;
$wc =~ /(\d+)/;
my $NUM = $1;

print STDERR "collect line num... $NUM\n";
open P,"<",$ppl or die "open $ppl error\n";
my $count = 1;
my %DICT;
while ($count < $NUM) {
	<P>; # sent line
	<P>; # stat line
	my $pline = <P>; # score line
	<P>; #nothing
	
	chomp $pline;
	
	if (not $pline =~ /ppl=\s*(\d*\.*\d*)/) {
		die "cannot extract ppl: $pline\n";
	}
	
	$DICT{$count} = $1;
	
	$count++;
}
close P;

print STDERR "\t sort\n";
$count = 0;
my @index;
foreach my $key ( sort { $DICT{$a} <=> $DICT{$b} } keys %DICT ) {
     #my $value = $DICT{$key};
     if ($count < $NUM/10.0) {
		push @index,$key;
		$count++;
	 } else {
		last;
	 }
     #do something with ($key, $value)
}
@index = sort {$a<=>$b} @index;

print STDERR "filter training corpus... ($#index+1)\n";

$count = 1;
open S,"<",$src or die "open $ppl error\n";
open T,"<",$tgt or die "open $ppl error\n";

open SO,">","$ppl.src" or die "open $ppl.src error\n";
open TO,">","$ppl.tgt" or die "open $ppl.tgt error\n";

$count = 1;
for my $i (@index) {
	
	while ($count < $i) {
		<S>;
		<T>;
		$count++;
	}
	my $src = <S>;
	my $tgt = <T>;
	print SO $src;
	print TO $tgt;
	$count++;
	#die "$i";
	#print STDERR "$count ";
}

close S;
close T;
close SO;
close TO;
