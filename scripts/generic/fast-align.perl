#!/usr/bin/perl -w

use strict;
use warnings;

sub RunFork($);
sub systemCheck($);

my $MOSESSCRIPTDIR="~/plateform/moses/scripts";
my $MOSESBIN="~/plateform/moses/bin";
my $FASTALIGNER="~/plateform/cdec/word-aligner/fast_align";

if ($#ARGV < 4) {
  die "usage: program corpus f e align rootdir";
}

my ($corpus,$f,$e,$align,$rootdir) = @ARGV;

$corpus = "$rootdir/$corpus";
my $modeldir = "$rootdir/model-fast";
`mkdir $modeldir`;

`paste -d'\t' $corpus.$f $corpus.$e | sed 's/\t/ \|\|\| /' > $corpus.$f-$e`;

my $forward = "nohup nice $FASTALIGNER -i $corpus.$f-$e -d -o -v  > $modeldir/forward.align 2>>err.fw.fast";
my $backward = "nohup nice $FASTALIGNER -i $corpus.$f-$e -d -o -v -r > $modeldir/backward.align 2>>err.bw.fast";


my @children;
my $fw = RunFork($forward);
push(@children, $fw);
my $bw = RunFork($backward);
push(@children, $bw);

foreach (@children) {
  waitpid($_, 0);
}

`nohup nice $MOSESSCRIPTDIR/ems/support/symmetrize-fast-align.perl $modeldir/forward.align $modeldir/backward.align $corpus.$f $corpus.$e $modeldir/aligned $align $MOSESBIN/symal`;

sub RunFork($)
{
  my $cmd = shift;

  my $pid = fork();
  
  if ($pid == 0)
  { # child
    print STDERR $cmd;
    systemCheck($cmd);
    exit();
  }
  return $pid;
}

sub systemCheck($)
{
  my $cmd = shift;
  my $retVal = system($cmd);
  if ($retVal != 0)
  {
    exit(1);
  }
}

