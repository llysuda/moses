/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef G2s_HOLECOLLECTION_H_INCLUDED_
#define G2s_HOLECOLLECTION_H_INCLUDED_

#include <vector>

#include "G2sHole.h"

namespace MosesG2S
{

class G2sHoleCollection
{
protected:
  HoleList m_holes;
  std::vector<G2sHole*> m_sortedSourceHoles;
  GraphRange nids;
//  std::vector<int> m_sourceHoleStartPoints;
//  std::vector<int> m_sourceHoleEndPoints;
//  std::vector<int> m_scope;
//  std::vector<int> m_sourcePhraseStart;
//  std::vector<int> m_sourcePhraseEnd;

public:
  G2sHoleCollection(const GraphRange& ids) {
  	nids = ids;
  }

  const HoleList &GetHoles() const {
    return m_holes;
  }

  HoleList &GetHoles() {
    return m_holes;
  }

  std::vector<G2sHole*> &GetSortedSourceHoles() {
    return m_sortedSourceHoles;
  }

  void Add(int startT, int endT, const GraphRange& snids, int sentLen);

  void RemoveLast();

  bool OverlapSource(const G2sHole &targetHole) const {
    HoleList::const_iterator iter;
    for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
      const G2sHole &currHole = *iter;
      if (currHole.OverlapS(targetHole))
        return true;
    }
    return false;
  }

//  bool ConsecSource(const G2sHole &sourceHole) const {
//    HoleList::const_iterator iter;
//    for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
//      const G2sHole &currHole = *iter;
//      if (currHole.Neighbor(sourceHole, 0))
//        return true;
//    }
//    return false;
//  }

  bool ConsecTarget(const G2sHole &sourceHole) const {
		HoleList::const_iterator iter;
		for (iter = m_holes.begin(); iter != m_holes.end(); ++iter) {
			const G2sHole &currHole = *iter;
			if (currHole.NeighborT(sourceHole))
				return true;
		}
		return false;
	}

  // Determine the scope that would result from adding the given hole.
//  int Scope(const G2sHole &proposedHole) const;

  void SortSourceHoles();

};

}

#endif
