/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef G2SHOLE_H_INCLUDED_
#define G2SHOLE_H_INCLUDED_

#include <cassert>
#include <string>
#include <vector>
#include <algorithm>

#include "typedef.h"
#include "GraphBitmap.h"

namespace MosesG2S
{

class G2sHole
{
protected:
  GraphBitmap m_bitmap;
  GraphRange m_source;
  std::vector<int> m_target, m_pos;
  std::vector<std::string> m_label;

public:

  G2sHole(int sentLen)
    : m_target(2)
		, m_pos(2)
    , m_label(2)
    , m_bitmap(sentLen){
  }

  G2sHole(const G2sHole &copy)
    : m_source(copy.m_source)
    , m_target(copy.m_target)
  	, m_pos(copy.m_pos)
    , m_label(copy.m_label)
    , m_bitmap(copy.m_bitmap) {
  }

  G2sHole(const GraphRange& snids, int startT, int endT, int sentLen)
    : m_target(2)
  	, m_pos(2)
    , m_label(2)
    , m_bitmap(sentLen){
    m_source = snids;
    m_target[0] = startT;
    m_target[1] = endT;
    m_bitmap.SetValue(m_source, true);
  }

  void SetPos(int pos, int direction) {
  	m_pos[direction] = pos;
  }

  int GetPos(int direction) const {
  	return m_pos[direction];
  }

  int GetStartT() const {
    return m_target[0];
  }

  int GetEndT() const {
    return m_target[1];
  }

  const GraphRange& GetSource() const {
  	return m_source;
  }

  int GetSourceMin() const {
  	return *(std::min_element(m_source.begin(), m_source.end()));
  }

  int GetSourceMax() const {
		return *(std::max_element(m_source.begin(), m_source.end()));
	}

  int GetTargetSize() const {
    return m_target[1] - m_target[0] + 1;
  }

  void SetLabel(const std::string &label, size_t direction) {
    m_label[direction] = label;
  }

  const std::string &GetLabel(size_t direction) const {
    return m_label[direction];
  }

  bool OverlapT(const G2sHole &otherHole) const {
    return ! ( otherHole.GetEndT()   < GetStartT() ||
               otherHole.GetStartT() > GetEndT() );
  }

  bool OverlapS(const G2sHole &otherHole) const {
//    BUIset source(m_source.begin(), m_source.end());
    return m_bitmap.Overlap(otherHole.GetSource());
  }

  bool NeighborT(const G2sHole &otherHole) const {
    return ( otherHole.GetEndT()+1 == GetStartT() ||
             otherHole.GetStartT() == GetEndT()+1 );
  }
};

typedef std::list<G2sHole> HoleList;

class HoleSourceOrderer
{
public:
  bool operator()(const G2sHole* holeA, const G2sHole* holeB) const {
    assert(holeA->GetSourceMin() != holeB->GetSourceMin());
    return holeA->GetSourceMin() < holeB->GetSourceMin();
  }
};

}
#endif
