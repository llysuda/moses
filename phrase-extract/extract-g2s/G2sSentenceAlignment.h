/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef G2S_SENTENCE_ALIGNMENT_H_INCLUDED_
#define G2S_SENTENCE_ALIGNMENT_H_INCLUDED_

#include <string>
#include <vector>
#include <set>
#include <map>

#include "MultiDiGraph.h"
#include "G2sExtractionOptions.h"

namespace MosesG2S
{

class G2sSentenceAlignment
{
	TermCacheType termCache;
	NTermCacheType ntermCahce;

public:
  std::vector<std::string> target;
  std::vector<int> targetFid;
  std::vector<std::string> targetPos;
  std::vector<std::string> targetLabel;

  MultiDiGraph* source;
  std::vector<int> alignedCountS;
  std::vector<std::vector<int> > alignedToT;
  int sentenceID;
  std::string weightString;

  virtual ~G2sSentenceAlignment();

  virtual bool processTargetSentence(const char *, int, bool boundaryRules);

  virtual bool processSourceSentence(const char *, int, const G2sExtractionOptions& options);

  bool create(char targetString[], char sourceString[],
              char alignmentString[], char weightString[], int sentenceID, const G2sExtractionOptions& options);

  void get_sorted_words(const GraphRange& nids, std::vector<std::pair<std::string, int> >&, bool sort);
  std::vector<std::string> get_structure(const GraphRange& nids, bool incLabel) const;
  std::string get_structure_string(const GraphRange&, bool incLabel);
  std::string get_target_struct(int start, int end) const;
  std::string GetBetweenLink(const GraphRange&, const GraphRange&, bool) const;

  std::string GetTargetLabel(int startT, int endT) const;
  std::string GetSourceLabel(const GraphRange& nids) const;
  std::vector<std::string> SortWordsWithHoles(const GraphRange& nids,
  												std::map<int, int>& indexWord, bool incLabel, bool sort);
  std::vector<std::string> get_structure(const std::vector<GraphRange >&, bool incLabel) const;
  std::vector<int> get_unalignedF(const GraphRange& nids, int order, bool isSDTU);

//  std::vector<std::string> get_nt_structure_string(
//          const GraphRange& nids,
//              const std::vector<GraphRange >& holes,
//              const std::map<int, int>& indexWord,
//              bool incLabel, bool relaxNT);

  std::vector<std::string> get_nt_structure(
          const std::vector<GraphRange >& symbols,
          bool incLabel, bool relaxNT);

private:


  std::string ConcatLabels(const EdgeMap& labels, bool incLabel) const;

  void CollectLabels(const BUIImap& I2J,
  		const std::set<int>& except,
  		const EdgeMap& edges,
			EdgeMap& ret) const;

};

}


#endif
