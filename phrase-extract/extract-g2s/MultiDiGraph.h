/*
 * DepNgramGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef EXTRACT_G2S_DepNgramGraph_H_
#define EXTRACT_G2S_DepNgramGraph_H_

#include <string>
#include <vector>

#include "typedef.h"

namespace MosesG2S
{

struct Node
{
	int id;
	std::string word;
	std::string pos;
	int fid;
};

class MultiDiGraph
{
  typedef std::vector<const struct Node*> NodeVec;
private:
	NodeVec nodes;
	EdgeVec succ;
	EdgeVec pred;
	ConnectCacheColl cct;
public:
	MultiDiGraph(size_t size) {
	  nodes.resize(size, NULL);
	  succ.resize(size);
	  pred.resize(size);
	  cct.resize(size);
	}

	~MultiDiGraph(){
      NodeVec::iterator iter;
      while (nodes.begin() != nodes.end()) {
        iter = nodes.begin();
        nodes.erase(iter);
        delete *iter;
      }
	}

	void add_node(int id, const struct Node* attr);
	void add_edge(int u, int v, const std::string& label);
	bool is_connected(const GraphRange& nids);
	bool between_connected(const BUIset& g1, const GraphRange& g2) const;
	bool between_connected(const GraphRange& g1, const GraphRange& g2) const;
//	const MultiDiGraph* get_subgraph(const std::vector<int>& nids) const;
	BUIset get_neighbors(const GraphRange& nids) const;

	void print_graph() const;

	int size() const {
		return nodes.size();
	}

	std::string get_word(int nid) const {
		return nodes[nid]->word;
	}
	std::string get_pos(int nid) const {
        return nodes[nid]->pos;
    }
	int get_fid(int nid) const {
        return nodes[nid]->fid;
    }

	bool has_succ(int nid) const {
		return succ[nid].size() > 0;
	}

	bool has_pred(int nid) const {
		return pred[nid].size() > 0;
	}

	const EdgeMap& get_succ_edges(int nid) const {
		return succ[nid];
	}

	const EdgeMap& get_pred_edges(int nid) const {
		return pred[nid];
	}

private:
	void DepthFirst(int curr_nid,
			BUIset& old_nids,
			const BUIset& all_nids) const;
	bool is_connected(const BUIset& nids);
};

} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
