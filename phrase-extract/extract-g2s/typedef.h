#pragma once
#ifndef G2STYPEDEF_H_INCLUDED_
#define G2STYPEDEF_H_INCLUDED_

#include <vector>
#include <boost/functional/hash.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

namespace MosesG2S
{


typedef std::vector<int> GraphRange;
typedef boost::unordered_set<GraphRange, boost::hash<GraphRange > > BUVIset;
typedef boost::unordered_set<int, boost::hash<int> > BUIset;
typedef boost::unordered_map<int, int, boost::hash<int> > BUIImap;
typedef std::set<std::string> LabelSet;
typedef boost::unordered_map<int, LabelSet, boost::hash<int> > EdgeMap;
typedef std::vector< EdgeMap > EdgeVec;

typedef boost::unordered_map<GraphRange, std::string, boost::hash<GraphRange> > TermCacheType;
typedef boost::unordered_map<GraphRange, std::string, boost::hash<GraphRange> > NTermCache;
typedef boost::unordered_map<GraphRange, NTermCache, boost::hash<GraphRange> > NTermCacheType;

typedef boost::unordered_map<GraphRange, bool, boost::hash<GraphRange> > ConnectCacheType;
typedef std::vector< ConnectCacheType > ConnectCacheColl;

typedef boost::unordered_map<std::string, std::string, boost::hash<std::string> > ClassMap;

};

#endif
