/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef EXTRACTEDG2sRULE_H_INCLUDED_
#define EXTRACTEDG2sRULE_H_INCLUDED_

#include <string>
#include <iostream>
#include <sstream>
#include <map>

#include "typedef.h"

namespace MosesG2S
{
// sentence-level collection of rules
class ExtractedG2sRule
{
  friend std::ostream& operator<<(std::ostream &, const ExtractedG2sRule &);

public:
  std::string source;
  std::string target;
  std::string alignment;
  std::string alignmentInv;
  std::string orientation;
  std::string orientationForward;
  std::string features;

  std::string reoSource;
  std::string reoTarget;

  int startT;
  int endT;
  GraphRange snids;
  GraphRange fnids;
  float count;
  double pcfgScore;

  std::map<size_t, std::pair<size_t, size_t> > m_ntLengths;

  ExtractedG2sRule(int sT, int eT, const GraphRange& sn, const GraphRange& fn)
    : source()
    , target()
    , alignment()
    , alignmentInv()
    , orientation()
    , orientationForward()
    , startT(sT)
    , endT(eT)
    , snids(sn)
    , fnids(fn)
    , count(0)
    , pcfgScore(0.0)
    , reoSource(), reoTarget() {
  }

  void SetSpanLength(size_t sourcePos, size_t sourceLength, size_t targetLength) {
    m_ntLengths[sourcePos] = std::pair<size_t, size_t>(sourceLength, targetLength);
  }

  void SetFeatures(const std::string& features) {
    this->features = features;
  }

  void OutputNTLengths(std::ostream &out) const;
  void OutputNTLengths(std::ostringstream &out) const;
};

}
#endif
