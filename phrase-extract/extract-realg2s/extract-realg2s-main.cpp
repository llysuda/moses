/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2009 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifdef WIN32
// Include Visual Leak Detector
//#include <vld.h>
#endif

#include "../extract-g2s/typedef.h"
#include "../extract-g2s/ExtractedG2sRule.h"
#include "../extract-g2s/G2sHole.h"
#include "../extract-g2s/G2sHoleCollection.h"
#include "../extract-g2s/G2sExist.h"
#include "SafeGetline.h"
#include "../extract-g2s/G2sSentenceAlignment.h"
#include "../extract-g2s/MultiDiGraph.h"
#include "tables-core.h"
#include "XmlTree.h"
#include "InputFileStream.h"
#include "OutputFileStream.h"
#include "../extract-g2s/G2sExtractionOptions.h"
#include "../extract-g2s/GraphBitmap.h"

#define LINE_MAX_LENGTH 500000

using namespace std;
using namespace MosesG2S;

namespace MosesRealG2S {
  typedef vector< int > LabelIndex;
  typedef map< int, int > WordIndex;

class ExtractTask
{
private:
  G2sSentenceAlignment &m_sentence;
  const G2sExtractionOptions &m_options;
  Moses::OutputFileStream& m_extractFile;
  Moses::OutputFileStream& m_extractFileInv;

  vector< ExtractedG2sRule > m_extractedRules;
  bool isBinary(int, int) const;

  // main functions
  void extractRules();
  void addRuleToCollection(ExtractedG2sRule &rule);
  void consolidateRules();
  void writeRulesToFile();

  // subs
  void addRule( int, int, const GraphRange&, int, G2sExist &ruleExist);
  void addHieroRule( int startT, int endT, const GraphRange& nids
                     , G2sExist &ruleExist, G2sHoleCollection &holeColl, int numHoles, int initStartF, int wordCountT, int wordCountS
                     , const GraphBitmap& nidSet, GraphBitmap& subtractId);
  void saveHieroPhrase( int startT, int endT, const GraphRange& nids, const GraphRange& fnids
                        , G2sHoleCollection &holeColl, LabelIndex &labelIndex, int countS);
  string saveTargetHieroPhrase(  int startT, int endT, const GraphRange& nids
                                 , WordIndex &indexT, G2sHoleCollection &holeColl, const LabelIndex &labelIndex, double &logPCFGScore, int countS);
  string saveSourceHieroPhrase( int startT, int endT, const GraphRange& nids
                                , G2sHoleCollection &holeColl, const LabelIndex &labelIndex, const WordIndex &indexS
																, const vector<string>& structStrs);
  vector<string> preprocessSourceHieroPhrase( int startT, int endT, const GraphRange& nids
                                    , WordIndex &indexS, G2sHoleCollection &holeColl, const LabelIndex &labelIndex);
  void saveHieroAlignment(  int startT, int endT, const GraphRange& nids
                            , const WordIndex &indexS, const WordIndex &indexT, G2sHoleCollection &holeColl, ExtractedG2sRule &rule);
  void saveAllHieroPhrases( int startT, int endT, const GraphRange& nids, const GraphRange& fnids, G2sHoleCollection &holeColl, int countS);

  inline string IntToString( int i ) {
    stringstream out;
    out << i;
    return out.str();
  }

public:
  ExtractTask(G2sSentenceAlignment &sentence, const G2sExtractionOptions &options, Moses::OutputFileStream &extractFile,
      Moses::OutputFileStream &extractFileInv):
    m_sentence(sentence),
    m_options(options),
    m_extractFile(extractFile),
    m_extractFileInv(extractFileInv){}
  void Run();

};

// stats for glue grammar and unknown word label probabilities
void collectWordLabelCounts(G2sSentenceAlignment &sentence );
void writeGlueGrammar(const string &, G2sExtractionOptions &options, set< string > &targetLabelCollection, map< string, int > &targetTopLabelCollection);
void writeUnknownWordLabel(const string &);
};

using namespace MosesRealG2S;

int main(int argc, char* argv[])
{
  cerr << "extract-rules, written by Philipp Koehn\n"
       << "rule extraction from an aligned parallel corpus\n";

  G2sExtractionOptions options;
  int sentenceOffset = 0;
#ifdef WITH_THREADS
  int thread_count = 1;
#endif
  if (argc < 5) {
    cerr << "syntax: extract-rules corpus.target corpus.source corpus.align extract ["

         << " --GlueGrammar FILE"
         << " | --UnknownWordLabel FILE"
         << " | --OnlyDirect"
         << " | --OutputNTLengths"
         << " | --MaxSpan[" << options.maxSpan << "]"
         << " | --MinHoleTarget[" << options.minHoleTarget << "]"
         << " | --MinHoleSource[" << options.minHoleSource << "]"
         << " | --MinWords[" << options.minWords << "]"
         << " | --MaxSymbolsTarget[" << options.maxSymbolsTarget << "]"
         << " | --MaxSymbolsSource[" << options.maxSymbolsSource << "]"
         << " | --MaxNonTerm[" << options.maxNonTerm << "]"
         << " | --MaxScope[" << options.maxScope << "]"
         << " | --SourceSyntax | --TargetSyntax"
         << " | --AllowOnlyUnalignedWords | --DisallowNonTermConsecTarget |--NonTermConsecSource |  --NoNonTermFirstWord | --NoFractionalCounting"
         << " | --UnpairedExtractFormat"
         << " | --ConditionOnTargetLHS ]"
         << " | --BoundaryRules[" << options.boundaryRules << "]";

    exit(1);
  }
  char* &fileNameT = argv[1];
  char* &fileNameS = argv[2];
  char* &fileNameA = argv[3];
  string fileNameGlueGrammar;
  string fileNameUnknownWordLabel;
  string fileNameExtract = string(argv[4]);

  int optionInd = 5;
//  options.sibling = true;

  for(int i=optionInd; i<argc; i++) {
    // maximum span length
    if (strcmp(argv[i],"--MaxSpan") == 0) {
      options.maxSpan = atoi(argv[++i]);
      if (options.maxSpan < 1) {
        cerr << "extract error: --maxSpan should be at least 1" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i],"--MinHoleTarget") == 0) {
      options.minHoleTarget = atoi(argv[++i]);
      if (options.minHoleTarget < 1) {
        cerr << "extract error: --minHoleTarget should be at least 1" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i],"--MinHoleSource") == 0) {
      options.minHoleSource = atoi(argv[++i]);
      if (options.minHoleSource < 1) {
        cerr << "extract error: --minHoleSource should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MaxSymbolsTarget") == 0) {
      options.maxSymbolsTarget = atoi(argv[++i]);
      if (options.maxSymbolsTarget < 1) {
        cerr << "extract error: --MaxSymbolsTarget should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MaxSymbolsSource") == 0) {
      options.maxSymbolsSource = atoi(argv[++i]);
      if (options.maxSymbolsSource < 1) {
        cerr << "extract error: --MaxSymbolsSource should be at least 1" << endl;
        exit(1);
      }
    }
    // minimum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MinWords") == 0) {
      options.minWords = atoi(argv[++i]);
      if (options.minWords < 0) {
        cerr << "extract error: --MinWords should be at least 0" << endl;
        exit(1);
      }
    }
    // maximum number of non-terminals
    else if (strcmp(argv[i],"--MaxNonTerm") == 0) {
      options.maxNonTerm = atoi(argv[++i]);
//      if (options.maxNonTerm < 1) {
//        cerr << "extract error: --MaxNonTerm should be at least 1" << endl;
//        exit(1);
//      }
    }
    // maximum scope (see Hopkins and Langmead (2010))
    else if (strcmp(argv[i],"--MaxScope") == 0) {
      options.maxScope = atoi(argv[++i]);
      if (options.maxScope < 0) {
        cerr << "extract error: --MaxScope should be at least 0" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i], "--GZOutput") == 0) {
      options.gzOutput = true;
    }
    // allow consecutive non-terminals (X Y | X Y)
    else if (strcmp(argv[i],"--TargetSyntax") == 0) {
      options.targetSyntax = true;
    } else if (strcmp(argv[i],"--SourceSyntax") == 0) {
      options.sourceSyntax = true;
    } else if (strcmp(argv[i],"--AllowOnlyUnalignedWords") == 0) {
      options.requireAlignedWord = false;
    } else if (strcmp(argv[i],"--DisallowNonTermConsecTarget") == 0) {
      options.nonTermConsecTarget = false;
    } else if (strcmp(argv[i],"--NonTermConsecSource") == 0) {
      options.nonTermConsecSource = true;
    } else if (strcmp(argv[i],"--NonTermFirstWord") == 0) {
      options.nonTermFirstWord = true;
    } else if (strcmp(argv[i],"--OnlyOutputSpanInfo") == 0) {
      options.onlyOutputSpanInfo = true;
    } else if (strcmp(argv[i],"--OnlyDirect") == 0) {
      options.onlyDirectFlag = true;
    } else if (strcmp(argv[i],"--GlueGrammar") == 0) {
      options.glueGrammarFlag = true;
      if (++i >= argc) {
        cerr << "ERROR: Option --GlueGrammar requires a file name" << endl;
        exit(0);
      }
      fileNameGlueGrammar = string(argv[i]);
      cerr << "creating glue grammar in '" << fileNameGlueGrammar << "'" << endl;
    } else if (strcmp(argv[i],"--UnknownWordLabel") == 0) {
      options.unknownWordLabelFlag = true;
      if (++i >= argc) {
        cerr << "ERROR: Option --UnknownWordLabel requires a file name" << endl;
        exit(0);
      }
      fileNameUnknownWordLabel = string(argv[i]);
      cerr << "creating unknown word labels in '" << fileNameUnknownWordLabel << "'" << endl;
    }
    // TODO: this should be a useful option
    //else if (strcmp(argv[i],"--ZipFiles") == 0) {
    //  zipFiles = true;
    //}
    // if an source phrase is paired with two target phrases, then count(t|s) = 0.5
    else if (strcmp(argv[i],"--FractionalCounting") == 0) {
      options.fractionalCounting = true;
    } else if (strcmp(argv[i],"--PCFG") == 0) {
      options.pcfgScore = true;
    } else if (strcmp(argv[i],"--OutputNTLengths") == 0) {
      options.outputNTLengths = true;
    } else if (strcmp(argv[i],"--UnpairedExtractFormat") == 0) {
      options.unpairedExtractFormat = true;
    } else if (strcmp(argv[i],"--ConditionOnTargetLHS") == 0) {
      options.conditionOnTargetLhs = true;
    } else if (strcmp(argv[i],"-threads") == 0 ||
               strcmp(argv[i],"--threads") == 0 ||
               strcmp(argv[i],"--Threads") == 0) {
#ifdef WITH_THREADS
      thread_count = atoi(argv[++i]);
#else
      cerr << "thread support not compiled in." << '\n';
      exit(1);
#endif
    } else if (strcmp(argv[i], "--SentenceOffset") == 0) {
      if (i+1 >= argc || argv[i+1][0] < '0' || argv[i+1][0] > '9') {
        cerr << "extract: syntax error, used switch --SentenceOffset without a number" << endl;
        exit(1);
      }
      sentenceOffset = atoi(argv[++i]);
    } else if (strcmp(argv[i],"--BoundaryRules") == 0) {
      options.boundaryRules = true;
    } else if (strcmp(argv[i],"--Sibling") == 0) {
      options.sibling = true;
    } else if (strcmp(argv[i],"--OnlyTree") == 0) {
      options.onlyTree = true;
    } else {
      cerr << "extract: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  cerr << "extracting hierarchical rules" << endl;

  // open input files
  Moses::InputFileStream tFile(fileNameT);
  Moses::InputFileStream sFile(fileNameS);
  Moses::InputFileStream aFile(fileNameA);

  istream *tFileP = &tFile;
  istream *sFileP = &sFile;
  istream *aFileP = &aFile;

  // open output files
  string fileNameExtractInv = fileNameExtract + ".inv" + (options.gzOutput?".gz":"");
  Moses::OutputFileStream extractFile;
  Moses::OutputFileStream extractFileInv;
  extractFile.Open((fileNameExtract  + (options.gzOutput?".gz":"")).c_str());
  if (!options.onlyDirectFlag)
    extractFileInv.Open(fileNameExtractInv.c_str());



  // stats on labels for glue grammar and unknown word label probabilities
  set< string > targetLabelCollection, sourceLabelCollection;
  map< string, int > targetTopLabelCollection, sourceTopLabelCollection;

  // loop through all sentence pairs
  size_t i=sentenceOffset;
  while(true) {
    i++;
    if (i%1000 == 0) cerr << i << " " << flush;

    char targetString[LINE_MAX_LENGTH];
    char sourceString[LINE_MAX_LENGTH];
    char alignmentString[LINE_MAX_LENGTH];
    SAFE_GETLINE((*tFileP), targetString, LINE_MAX_LENGTH, '\n', __FILE__);
    if (tFileP->eof()) break;
    SAFE_GETLINE((*sFileP), sourceString, LINE_MAX_LENGTH, '\n', __FILE__);
    SAFE_GETLINE((*aFileP), alignmentString, LINE_MAX_LENGTH, '\n', __FILE__);

    G2sSentenceAlignment sentence;
    //az: output src, tgt, and alingment line
    if (options.onlyOutputSpanInfo) {
      cout << "LOG: SRC: " << sourceString << endl;
      cout << "LOG: TGT: " << targetString << endl;
      cout << "LOG: ALT: " << alignmentString << endl;
      cout << "LOG: PHRASES_BEGIN:" << endl;
    }

    if (sentence.create(targetString, sourceString, alignmentString,"", i, options)) {
      ExtractTask *task = new ExtractTask(sentence, options, extractFile, extractFileInv);
      task->Run();
      delete task;
    }
    if (options.onlyOutputSpanInfo) cout << "LOG: PHRASES_END:" << endl; //az: mark end of phrases
  }

  tFile.Close();
  sFile.Close();
  aFile.Close();
  // only close if we actually opened it
  if (!options.onlyOutputSpanInfo) {
    extractFile.Close();
    if (!options.onlyDirectFlag) extractFileInv.Close();
  }


  if (options.glueGrammarFlag)
    writeGlueGrammar(fileNameGlueGrammar, options, targetLabelCollection, targetTopLabelCollection);

  if (options.unknownWordLabelFlag)
    writeUnknownWordLabel(fileNameUnknownWordLabel);
}


namespace MosesRealG2S {

void ExtractTask::Run()
{
  extractRules();
  consolidateRules();
  writeRulesToFile();
  m_extractedRules.clear();
}

void ExtractTask::extractRules()
{
  int countT = m_sentence.target.size();
  int countS = m_sentence.source->size();


  // phrase repository for creating hiero phrases
  G2sExist ruleExist(countT);

  GraphBitmap nidSet(countS);
  GraphBitmap subtractId(countS);

  // check alignments for target phrase startT...endT
  for(int lengthT=1; lengthT <= m_options.maxSpan && lengthT <= countT;
      lengthT++) {
    for(int startT=0; startT < countT-(lengthT-1); startT++) {

      // that's nice to have
      int endT = startT + lengthT - 1;

      // if there is target side syntax, there has to be a node
//      if (m_options.targetSyntax && !m_sentence.targetTree.HasNode(startT,endT))
//        continue;

      BUIset usedF;
      vector< int > usedFAlign = m_sentence.alignedCountS;
      for(int ei=startT; ei<=endT; ei++) {
        for(size_t i=0; i<m_sentence.alignedToT[ei].size(); i++) {
          int fi = m_sentence.alignedToT[ei][i];
          usedF.insert(fi);
          usedFAlign[ fi ]--;
        }
      }

      int usedF_size = (int)usedF.size();
      if (usedF_size == 0 || usedF_size > m_options.maxSpan)
          continue;

      // make sure usedF does not align to outside
      bool out_of_bounds = false;
      for(BUIset::const_iterator iter = usedF.begin(); iter != usedF.end(); ++iter) {
      	if (usedFAlign[*iter] > 0) {
      		out_of_bounds = true;
      		break;
      	}
      }
      if (out_of_bounds)
      	continue;

			// make sure usedF does not aligned to outside

      vector< GraphRange > queue;
      queue.reserve(10000);
      BUVIset seen;
      GraphRange initGR(GraphRange(usedF.begin(), usedF.end()));
      std::sort(initGR.begin(), initGR.end());
      queue.push_back(initGR);

      while (queue.size()>0) {
        GraphRange base = queue.back();
        queue.pop_back();

        if (seen.find(base) != seen.end())
            continue;
        seen.insert(base);

        for(size_t i = 1; i < base.size(); i++)
          if (base[i] <= base[i-1]) {
            cerr << "error!" << endl;
            exit(0);
          }


        int base_size = (int)base.size();
        int startS = *base.begin();
        int endS = *base.rbegin();

        bool isConnected = m_sentence.source->is_connected(base);

        if (isConnected) {

          if (endT-startT < m_options.maxSpan && base_size <= m_options.maxSpan) {
            ruleExist.Add(startT, endT, base, countS);
          }


          if (endT-startT < m_options.maxSymbolsTarget && base_size <= m_options.maxSymbolsSource)
            addRule(startT,endT, base, countS, ruleExist);

          int initStartT = m_options.nonTermFirstWord ? startT : startT + 1;
          G2sHoleCollection holeColl(base); // empty hole collection
          nidSet.SetValue(false);
          nidSet.SetValue(base, true);
          addHieroRule(startT, endT, base,
                       ruleExist, holeColl, 0, initStartT,
                       endT-startT+1, base_size, nidSet, subtractId);
        }
        // grow source with unaligned words
        if (base_size >= m_options.maxSpan)
            continue;
        GraphRange unalignedF_set = m_sentence.get_unalignedF(base, 1, true);

        for(GraphRange::const_iterator iter = unalignedF_set.begin(); iter != unalignedF_set.end(); ++iter) {
          GraphRange nids(base);
          size_t ni = 0;
          for (; ni < base.size(); ni++) {
            if (*iter < base[ni]) {
              nids.insert(nids.begin()+ni,*iter);
              break;
            }
          }
          if (ni == base.size())
            nids.push_back(*iter);

          queue.push_back(nids);
        }
      }
    } // end startT
  } // end length
}



vector<string> ExtractTask::preprocessSourceHieroPhrase( int startT, int endT, const GraphRange& remains
    , WordIndex &indexS, G2sHoleCollection &holeColl, const LabelIndex &labelIndex)
{
  vector<G2sHole*>::iterator iterHoleList = holeColl.GetSortedSourceHoles().begin();
  assert(iterHoleList != holeColl.GetSortedSourceHoles().end());

  int holeCount = 0;
  int holeTotal = holeColl.GetSortedSourceHoles().size();
//  for(; iterHoleList != holeColl.GetSortedSourceHoles().end(); ++iterHoleList) {
//  	G2sHole &hole = *iterHoleList;
//		int labelI = labelIndex[ 2+holeCount+holeTotal ];
//		const GraphRange& hs = hole.GetSource();
//		string label = "X"; //useSyntaxNT ? m_sentence.GetSourceLabel(hole.GetSource()) : "X";
//		hole.SetLabel(label, 0);
//		hole.SetPos(remains.size()+holeCount, 0);
//		++holeCount;
//  }

  std::vector<GraphRange > symbols;

  int count = 0;

  for (int i = 0; i < remains.size(); i++) {
    int id = remains[i];
    bool isHole = false;
    if (iterHoleList != holeColl.GetSortedSourceHoles().end()) {
      const G2sHole &hole = **iterHoleList;
      isHole = (hole.GetSourceMin() < id);
    }

    if (isHole) {
      G2sHole &hole = **iterHoleList;
      symbols.push_back(hole.GetSource());
      int labelI = labelIndex[ 2+holeCount+holeTotal ];
      string label = "X"; //useSyntaxNT ? m_sentence.GetSourceLabel(hole.GetSource()) : "X";
      hole.SetLabel(label, 0);
      hole.SetPos(count, 0);
      ++holeCount;
      ++count;
      ++iterHoleList;
    }
    GraphRange dummy;
    dummy.push_back(id);
    symbols.push_back(dummy);
    indexS[id] = count;
    ++count;
  }
  while (iterHoleList != holeColl.GetSortedSourceHoles().end()) {
    G2sHole &hole = **iterHoleList;
    symbols.push_back(hole.GetSource());
    int labelI = labelIndex[ 2+holeCount+holeTotal ];
    string label = "X"; //useSyntaxNT ? m_sentence.GetSourceLabel(hole.GetSource()) : "X";
    hole.SetLabel(label, 0);
    hole.SetPos(count, 0);
    ++iterHoleList;
    ++count;
    ++holeCount;
  }

  // for return
  vector<string> structStrs = m_sentence.get_nt_structure(symbols, false, false);
  return structStrs;
}

string ExtractTask::saveTargetHieroPhrase( int startT, int endT, const GraphRange& nids
    , WordIndex &indexT, G2sHoleCollection &holeColl, const LabelIndex &labelIndex, double &logPCFGScore
    , int countS)
{
  HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
  assert(iterHoleList != holeColl.GetHoles().end());

  string out = "";
  int outPos = 0;
  int holeCount = 0;
  for(int currPos = startT; currPos <= endT; currPos++) {
    bool isHole = false;
    if (iterHoleList != holeColl.GetHoles().end()) {
      const G2sHole &hole = *iterHoleList;
      isHole = hole.GetStartT() == currPos;
    }

    if (isHole) {
      G2sHole &hole = *iterHoleList;

      const string &sourceLabel = hole.GetLabel(0);
      assert(sourceLabel != "");

      int labelI = labelIndex[ 2+holeCount ];
      string targetLabel;
      if (m_options.targetSyntax) {
        targetLabel = m_sentence.GetTargetLabel(startT, endT);
      } else {
        targetLabel = "X";
      }

      hole.SetLabel(targetLabel, 1);

//      if (!m_options.onlyLastNT) {
        if (m_options.unpairedExtractFormat) {
          out += "[" + targetLabel + "] ";
        } else {
          out += "[" + sourceLabel + "][" + targetLabel + "] ";
        }
//      }

      currPos = hole.GetEndT();
      hole.SetPos(outPos, 1);
      ++iterHoleList;
      holeCount++;
    } else {
      indexT[currPos] = outPos;
      out += m_sentence.target[currPos] + " ";
    }

    outPos++;
  }

  assert(iterHoleList == holeColl.GetHoles().end());
  return out.erase(out.size()-1);
}

string ExtractTask::saveSourceHieroPhrase( int startT, int endT, const GraphRange& nids
    , G2sHoleCollection &holeColl, const LabelIndex &labelIndex, const WordIndex& indexS
		, const vector<string>& structStrs)
{
  vector<G2sHole*>::iterator iterHoleList = holeColl.GetSortedSourceHoles().begin();
  assert(iterHoleList != holeColl.GetSortedSourceHoles().end());

  string out = "";
  int holeCount = 0;
  int holeTotal = holeColl.GetSortedSourceHoles().size();
  std::vector<GraphRange > symbols;

  int count = 0;
  for (int i = 0; i < nids.size(); i++) {
    int id = nids[i];
    bool isHole = false;
    if (iterHoleList != holeColl.GetSortedSourceHoles().end()) {
      const G2sHole &hole = **iterHoleList;
      isHole = (hole.GetSourceMin() < id);
    }

    if (isHole) {
      G2sHole &hole = **iterHoleList;
      const string &sourceLabel = hole.GetLabel(0);
      const string &targetLabel = hole.GetLabel(1);
      if (m_options.unpairedExtractFormat) {
        out += "[" + sourceLabel + "] ";
      } else {
        out += "[" + sourceLabel + "][" + targetLabel + "] ";
      }
      ++iterHoleList;
    }
    out += m_sentence.source->get_word(id) += " ";
  }
  while (iterHoleList != holeColl.GetSortedSourceHoles().end()) {
    G2sHole &hole = **iterHoleList;
    const string &sourceLabel = hole.GetLabel(0);
    const string &targetLabel = hole.GetLabel(1);
    if (m_options.unpairedExtractFormat) {
      out += "[" + sourceLabel + "] ";
    } else {
      out += "[" + sourceLabel + "][" + targetLabel + "] ";
    }
    ++iterHoleList;
  }

  return out.erase(out.size()-1);
}

void ExtractTask::saveHieroAlignment( int startT, int endT, const GraphRange& nids
                                      , const WordIndex &indexS, const WordIndex &indexT, G2sHoleCollection &holeColl, ExtractedG2sRule &rule)
{
  // print alignment of words
  for(int ti=startT; ti<=endT; ti++) {
    WordIndex::const_iterator p = indexT.find(ti);
    if (p != indexT.end()) { // does word still exist?
      for(unsigned int i=0; i<m_sentence.alignedToT[ti].size(); i++) {
        int si = m_sentence.alignedToT[ti][i];
        std::string sourceSymbolIndex = IntToString(indexS.find(si)->second);
        std::string targetSymbolIndex = IntToString(p->second);
        rule.alignment      += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
        if (! m_options.onlyDirectFlag)
          rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";
      }
    }
  }

//  if (!m_options.onlyLastNT) {
    // print alignment of non terminals
    HoleList::const_iterator iterHole;
    for (iterHole = holeColl.GetHoles().begin(); iterHole != holeColl.GetHoles().end(); ++iterHole) {
      const G2sHole &hole = *iterHole;

      std::string sourceSymbolIndex = IntToString(hole.GetPos(0));
      std::string targetSymbolIndex = IntToString(hole.GetPos(1));
      rule.alignment      += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
      if (!m_options.onlyDirectFlag)
        rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";

      rule.SetSpanLength(hole.GetPos(0), hole.GetSource().size(), hole.GetTargetSize() ) ;

    }
//  }

  rule.alignment.erase(rule.alignment.size()-1);
  if (!m_options.onlyDirectFlag) {
    rule.alignmentInv.erase(rule.alignmentInv.size()-1);
  }
}

void ExtractTask::saveHieroPhrase( int startT, int endT, const GraphRange& nids, const GraphRange& fnids
                                   , G2sHoleCollection &holeColl, LabelIndex &labelIndex, int countS)
{
  WordIndex indexS, indexT; // to keep track of word positions in rule

  ExtractedG2sRule rule( startT, endT, nids, fnids);
  // phrase labels
  string targetLabel;
  if (m_options.targetSyntax) {
    targetLabel = m_sentence.GetTargetLabel(startT, endT);
  } else {
    targetLabel = "X";
  }


  string sourceLabel = m_options.sourceSyntax ?
                       m_sentence.GetSourceLabel(nids) : "X";

  // create non-terms on the source side
  vector<string> structStrs = preprocessSourceHieroPhrase(startT, endT, nids, indexS, holeColl, labelIndex);

//  if (m_options.onlyLastNT) {
//      targetLabel = holeColl.GetHoles().front().GetLabel(0);
//    }

	double logPCFGScore = 0.0f;
	rule.target = saveTargetHieroPhrase(startT, endT, nids, indexT, holeColl, labelIndex, logPCFGScore, countS)
								+ " [" + targetLabel + "]";

  // source
  rule.source = saveSourceHieroPhrase(startT, endT, nids, holeColl, labelIndex, indexS, structStrs);
  rule.source += " [" + sourceLabel + "@" + structStrs[0] + "]";

  // alignment
  saveHieroAlignment(startT, endT, nids, indexS, indexT, holeColl, rule);

  addRuleToCollection( rule );
}

void ExtractTask::saveAllHieroPhrases( int startT, int endT, const GraphRange& nids, const GraphRange& fnids, G2sHoleCollection &holeColl, int countS)
{
  LabelIndex labelIndex,labelCount;

  // number of target head labels
  int numLabels = 1;//m_options.targetSyntax ? m_sentence.targetTree.GetNodes(startT,endT).size() : 1;
  labelCount.push_back(numLabels);
  labelIndex.push_back(0);

  // number of source head labels
  numLabels =  1;//m_options.sourceSyntax ? m_sentence.sourceTree.GetNodes(startS,endS).size() : 1;
  labelCount.push_back(numLabels);
  labelIndex.push_back(0);

  // number of target hole labels
  for( HoleList::const_iterator hole = holeColl.GetHoles().begin();
       hole != holeColl.GetHoles().end(); hole++ ) {
    int numLabels =  1;//m_options.targetSyntax ? m_sentence.targetTree.GetNodes(hole->GetStart(1),hole->GetEnd(1)).size() : 1 ;
    labelCount.push_back(numLabels);
    labelIndex.push_back(0);
  }

  // number of source hole labels
  holeColl.SortSourceHoles();
  for( vector<G2sHole*>::iterator i = holeColl.GetSortedSourceHoles().begin();
       i != holeColl.GetSortedSourceHoles().end(); i++ ) {
    const G2sHole &hole = **i;
    int numLabels =  1;//m_options.sourceSyntax ? m_sentence.sourceTree.GetNodes(hole.GetStart(0),hole.GetEnd(0)).size() : 1 ;
    labelCount.push_back(numLabels);
    labelIndex.push_back(0);
  }

  // loop through the holes
  bool done = false;
  while(!done) {
    saveHieroPhrase( startT, endT, nids, fnids, holeColl, labelIndex, countS );
    for(unsigned int i=0; i<labelIndex.size(); i++) {
      labelIndex[i]++;
      if(labelIndex[i] == labelCount[i]) {
        labelIndex[i] = 0;
        if (i == labelIndex.size()-1)
          done = true;
      } else {
        break;
      }
    }
  }
}

// this function is called recursively
// it pokes a new hole into the phrase pair, and then calls itself for more holes
void ExtractTask::addHieroRule( int startT, int endT, const GraphRange& nids
                                , G2sExist &ruleExist, G2sHoleCollection &holeColl
                                , int numHoles, int initStartT, int wordCountT, int wordCountS,
                                const GraphBitmap& nidSet, GraphBitmap& subtractId)
{
  // done, if already the maximum number of non-terminals in phrase pair
  if (numHoles >= m_options.maxNonTerm)
    return;

  int sentLen = m_sentence.source->size();
  // find a hole...
  for (int startHoleT = initStartT; startHoleT <= endT; ++startHoleT) {
    for (int endHoleT = startHoleT+(m_options.minHoleTarget-1); endHoleT <= endT; ++endHoleT) {
      // if last non-terminal, enforce word count limit
      if (numHoles == m_options.maxNonTerm-1 && wordCountT - (endHoleT-startT+1) + (numHoles+1) > m_options.maxSymbolsTarget)
        continue;

      // determine the number of remaining target words
      const int newWordCountT = wordCountT - (endHoleT-startHoleT+1);

      // always enforce min word count limit
      if (newWordCountT < m_options.minWords)
        continue;

      // except the whole span
      if (startHoleT == startT && endHoleT == endT)
        continue;

      // does a phrase cover this target span?
      // if it does, then there should be a list of mapped source phrases
      // (multiple possible due to unaligned words)
      const HoleList &sourceHoles = ruleExist.GetSourceHoles(startHoleT, endHoleT);

      // loop over sub phrase pairs
      HoleList::const_iterator iterSourceHoles;
      for (iterSourceHoles = sourceHoles.begin(); iterSourceHoles != sourceHoles.end(); ++iterSourceHoles) {
        const G2sHole &sourceHole = *iterSourceHoles;

        const int sourceHoleSize = sourceHole.GetSource().size();

        // enforce minimum hole size
        if (sourceHoleSize < m_options.minHoleSource)
          continue;

        // determine the number of remaining source words
        const int newWordCountS = wordCountS - sourceHoleSize;

        // if last non-terminal, enforce word count limit
        if (numHoles == m_options.maxNonTerm-1 && newWordCountS + (numHoles+1) > m_options.maxSymbolsSource)
          continue;

        // enforce min word count limit
        if (newWordCountS < m_options.minWords)
          continue;

        // hole must be subphrase of the source phrase
        // (may be violated if subphrase contains additional unaligned source word)
        bool violate = false;
        const GraphRange& sourceHoleSource = sourceHole.GetSource();
        if (!nidSet.SubRange(sourceHoleSource))
          continue;

        // make sure source side does not overlap with another hole
        if (holeColl.OverlapSource(sourceHole))
          continue;

        // require that at least one aligned word is left (unless there are no words at all)
        if (m_options.requireAlignedWord && (newWordCountS > 0 || newWordCountT > 0)) {
          HoleList::const_iterator iterHoleList = holeColl.GetHoles().begin();
          bool foundAlignedWord = false;
          // loop through all word positions
          for(int pos = startT; pos <= endT && !foundAlignedWord; pos++) {
            // new hole? moving on...
            if (pos == startHoleT) {
              pos = endHoleT;
            }
            // covered by hole? moving on...
            else if (iterHoleList != holeColl.GetHoles().end() && iterHoleList->GetStartT() == pos) {
              pos = iterHoleList->GetEndT();
              ++iterHoleList;
            }
            // covered by word? check if it is aligned
            else {
              if (m_sentence.alignedToT[pos].size() > 0)
                foundAlignedWord = true;
            }
          }
          if (!foundAlignedWord)
            continue;
        }

        // update list of holes in this phrase pair
        holeColl.Add(startHoleT, endHoleT, sourceHoleSource, sentLen);
        // now some checks that disallow this phrase pair, but not further recursion
        bool allowablePhrase = true;

        // maximum words count violation?
        if (newWordCountS + (numHoles+1) > m_options.maxSymbolsSource)
          allowablePhrase = false;

        if (newWordCountT + (numHoles+1) > m_options.maxSymbolsTarget)
          allowablePhrase = false;

        subtractId.SetValue(false);
        // remaining nodes should be connected
        HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
        for(; iterHoleList != holeColl.GetHoles().end(); ++iterHoleList) {
        	G2sHole &hole = *iterHoleList;
      		const GraphRange& hs = hole.GetSource();
      		subtractId.SetValue(hs, true);
        }
        GraphRange range = nidSet.Subtract(subtractId);
////        GraphRange range(remain.begin(), remain.end());
////        std::sort(range.begin(), range.end());
//        if (!m_sentence.source->is_connected(range))
//        	allowablePhrase = false;

        // passed all checks...
        if (allowablePhrase) {
          saveAllHieroPhrases(startT, endT, range, nids, holeColl, wordCountS);
        }

        // recursively search for next hole
        int nextInitStartT = m_options.nonTermConsecTarget ? endHoleT + 1 : endHoleT + 2;
        addHieroRule(startT, endT, nids
                     , ruleExist, holeColl, numHoles + 1, nextInitStartT
                     , newWordCountT, newWordCountS, nidSet, subtractId);

        holeColl.RemoveLast();
      }
    }
  }
}

void ExtractTask::addRule( int startT, int endT, const GraphRange& nids, int countS, G2sExist &ruleExist)
{
  // contains only <s> or </s>. Don't output
//  if (m_options.boundaryRules
//      && (   (startS == 0         && endS == 0)
//             || (startS == countS-1  && endS == countS-1))) {
//    return;
//  }

  if (m_options.onlyOutputSpanInfo) {
    cout << nids.size() << " " << startT << " " << endT << endl;
    return;
  }

  ExtractedG2sRule rule(startT, endT, nids, nids);

  // phrase labels
  string targetLabel,sourceLabel;
  if (m_options.targetSyntax && m_options.conditionOnTargetLhs) {
    sourceLabel = targetLabel = m_sentence.GetTargetLabel(startT,endT);
  } else {
    sourceLabel = m_options.sourceSyntax ?
                  m_sentence.GetSourceLabel(nids) : "X";

    if (m_options.targetSyntax) {
      targetLabel = m_sentence.GetTargetLabel(startT,endT);
    } else {
      targetLabel = "X";
    }

    if (m_options.conditionOnTargetLhs) {
        sourceLabel = targetLabel;
    }
  }

  //
  vector<pair<string,int> > wordsF;
	m_sentence.get_sorted_words(nids,wordsF,false);
	GraphRange sorted_nids;
	for(size_t i = 0; i < wordsF.size(); i++) {
		sorted_nids.push_back(wordsF[i].second);
	}
	string structStr = m_sentence.get_structure_string(sorted_nids, false);

  // source
  rule.source = "";
  for(int fi=0; fi<(int)wordsF.size(); fi++)
    rule.source += wordsF[fi].first + " ";

  rule.source += "[" + sourceLabel + "@" + structStr + "]";

  // target
  rule.target = "";
  for(int ti=startT; ti<=endT; ti++)
    rule.target += m_sentence.target[ti] + " ";

  rule.target += "[" + targetLabel + "]";


  map<int,int> alignMap;
	for(int i = 0; i < (int)wordsF.size(); i++) {
		alignMap[wordsF[i].second] = i;
	}
  // alignment
  for(int ti=startT; ti<=endT; ti++) {
    for(unsigned int i=0; i<m_sentence.alignedToT[ti].size(); i++) {
      int si = m_sentence.alignedToT[ti][i];
      std::string sourceSymbolIndex = IntToString(alignMap[si]);
      std::string targetSymbolIndex = IntToString(ti-startT);
      rule.alignment += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
      if (!m_options.onlyDirectFlag)
        rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";
    }
  }

  rule.alignment.erase(rule.alignment.size()-1);
  if (!m_options.onlyDirectFlag)
    rule.alignmentInv.erase(rule.alignmentInv.size()-1);

  addRuleToCollection( rule );
}

void ExtractTask::addRuleToCollection( ExtractedG2sRule &newRule )
{

  // no double-counting of identical rules from overlapping spans
  if (!m_options.duplicateRules) {
    vector<ExtractedG2sRule>::const_iterator rule;
    for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      if (rule->source.compare( newRule.source ) == 0 &&
          rule->target.compare( newRule.target ) == 0 &&
          !(rule->endT < newRule.startT || rule->startT > newRule.endT)) { // overlapping
        return;
      }
    }
  }
  m_extractedRules.push_back( newRule );
}

void ExtractTask::consolidateRules()
{
  typedef vector<ExtractedG2sRule>::iterator R;
  map<int, map<int, map<vector<int>,int > > > spanCount;

  // compute number of rules per span
  if (m_options.fractionalCounting) {
    for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      spanCount[ rule->startT ][ rule->endT ][ rule->fnids ]++;
    }
  }

  // compute fractional counts
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    rule->count =    1.0/(float) (m_options.fractionalCounting ? spanCount[ rule->startT ][ rule->endT ][ rule->fnids ] : 1.0 );
  }

  // consolidate counts
  map<std::string, map< std::string, map< std::string, float> > > consolidatedCount;
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] += rule->count;
  }

  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    float count = consolidatedCount[ rule->source ][ rule->target][ rule->alignment ];
    rule->count = count;
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] = 0;
  }
}

void ExtractTask::writeRulesToFile()
{
  vector<ExtractedG2sRule>::const_iterator rule;
  ostringstream out;
  ostringstream outInv;
  ostringstream outOri;
  ostringstream outOrd;
  for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    if (rule->count == 0)
      continue;

    out << rule->source << " ||| "
        << rule->target << " ||| "
        << rule->alignment << " ||| "
        << rule->count << " ||| ";
    if (m_options.outputNTLengths) {
      rule->OutputNTLengths(out);
    }
    if (m_options.pcfgScore) {
      out << " ||| " << rule->pcfgScore;
    }
    out << "\n";

    if (!m_options.onlyDirectFlag) {
      outInv << rule->target << " ||| "
             << rule->source << " ||| "
             << rule->alignmentInv << " ||| "
             << rule->count << "\n";
    }

  }
    m_extractFile << out.str();
    m_extractFileInv << outInv.str();
}

void writeGlueGrammar( const string & fileName, G2sExtractionOptions &options, set< string > &targetLabelCollection, map< string, int > &targetTopLabelCollection )
{
  ofstream grammarFile;
  grammarFile.open(fileName.c_str());
  if (!options.targetSyntax) {
    grammarFile << "<s> [X] ||| <s> [S] ||| 1 ||| ||| 0" << endl
                << "[X][S] </s> [X] ||| [X][S] </s> [S] ||| 1 ||| 0-0 ||| 0" << endl
                << "[X][S] [X][X] [X] ||| [X][S] [X][X] [S] ||| 2.718 ||| 0-0 1-1 ||| 0" << endl;
  } else {
    // chose a top label that is not already a label
    string topLabel = "QQQQQQ";
    for( unsigned int i=1; i<=topLabel.length(); i++) {
      if(targetLabelCollection.find( topLabel.substr(0,i) ) == targetLabelCollection.end() ) {
        topLabel = topLabel.substr(0,i);
        break;
      }
    }
    // basic rules
    grammarFile << "<s> [X] ||| <s> [" << topLabel << "] ||| 1  ||| " << endl
                << "[X][" << topLabel << "] </s> [X] ||| [X][" << topLabel << "] </s> [" << topLabel << "] ||| 1 ||| 0-0 " << endl;

    // top rules
    for( map<string,int>::const_iterator i =  targetTopLabelCollection.begin();
         i !=  targetTopLabelCollection.end(); i++ ) {
      grammarFile << "<s> [X][" << i->first << "] </s> [X] ||| <s> [X][" << i->first << "] </s> [" << topLabel << "] ||| 1 ||| 1-1" << endl;
    }

    // glue rules
    for( set<string>::const_iterator i =  targetLabelCollection.begin();
         i !=  targetLabelCollection.end(); i++ ) {
      grammarFile << "[X][" << topLabel << "] [X][" << *i << "] [X] ||| [X][" << topLabel << "] [X][" << *i << "] [" << topLabel << "] ||| 2.718 ||| 0-0 1-1" << endl;
    }
    grammarFile << "[X][" << topLabel << "] [X][X] [X] ||| [X][" << topLabel << "] [X][X] [" << topLabel << "] ||| 2.718 |||  0-0 1-1 " << endl; // glue rule for unknown word...
  }
  grammarFile.close();
}

// collect counts for labels for each word
// ( labels of singleton words are used to estimate
//   distribution oflabels for unknown words )

map<string,int> wordCount;
map<string,string> wordLabel;
void collectWordLabelCounts( G2sSentenceAlignment &sentence )
{
//  int countT = sentence.target.size();
//  for(int ti=0; ti < countT; ti++) {
//    string word = sentence.target[ ti ];
//    const vector< SyntaxNode* >& labels = sentence.targetTree.GetNodes(ti,ti);
//    if (labels.size() > 0) {
//      wordCount[ word ]++;
//      wordLabel[ word ] = labels[0]->GetLabel();
//    }
//  }
}

void writeUnknownWordLabel(const string & fileName)
{
  ofstream outFile;
  outFile.open(fileName.c_str());
  typedef map<string,int>::const_iterator I;

  map<string,int> count;
  int total = 0;
  for(I word = wordCount.begin(); word != wordCount.end(); word++) {
    // only consider singletons
    if (word->second == 1) {
      count[ wordLabel[ word->first ] ]++;
      total++;
    }
  }

  for(I pos = count.begin(); pos != count.end(); pos++) {
    double ratio = ((double) pos->second / (double) total);
    if (ratio > 0.03)
      outFile << pos->first << " " << ratio << endl;
  }

  outFile.close();
}

inline std::vector<std::string> Tokenize(const std::string& str,
    const std::string& delimiters = " \t")
{
  std::vector<std::string> tokens;
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }

  return tokens;
}

};
