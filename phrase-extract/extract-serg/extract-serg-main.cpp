/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2009 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifdef WIN32
// Include Visual Leak Detector
//#include <vld.h>
#endif

#include "ExtractedSERGRule.h"
#include "SERGHole.h"
#include "SERGHoleCollection.h"
#include "SERGExist.h"
#include "SafeGetline.h"
#include "SERGSentenceAlignment.h"
#include "tables-core.h"
#include "InputFileStream.h"
#include "OutputFileStream.h"
#include "SERGExtractionOptions.h"

#define LINE_MAX_LENGTH 500000

using namespace std;
using namespace Moses::SERG;

typedef vector< int > LabelIndex;
typedef map< int, int > WordIndex;

class ExtractTask
{
private:
  SERGSentenceAlignment &m_sentence;
  const SERGExtractionOptions &m_options;
  Moses::OutputFileStream& m_extractFile;
  Moses::OutputFileStream& m_extractFileInv;

  vector< ExtractedSERGRule > m_extractedRules;

  // main functions
  void extractRules();
  void addRuleToCollection(ExtractedSERGRule &rule);
  void consolidateRules();
  void writeRulesToFile();

  // subs
  string extractContextFeatures(int startS, int endS, int startT, int endT, const SERGHoleCollection &holeColl);
  string extractSourcePatternFeatures(int startS, int endS, const SERGHoleCollection &holeColl);
  string extractSourceSegFeatures(int startS, int endS);
  bool ruleDepthLimit(int, int, SERGHoleCollection &holeColl);
  void addRule( int, int, int, int, int, SERGExist &ruleExist);
  /*void addHieroRuleComplete( int startT, int endT, int startS, int endS
                       , SERGExist &ruleExist, vector<char> &ntypes, int numHoles, int initFlagS
                       , int wordCountT, int wordCountS, const vector<pair<pair<int, int>, char> >&segments
                       ,const vector<char>& types);
  void constructHieroRuleComplete( int startT, int endT, int startS, int endS
                         , SERGExist &ruleExist, vector<char> &ntypes, int numHoles, int initFlagS
                         , int wordCountT, int wordCountS, const vector<pair<pair<int, int>, char> >&segments
                         , const vector<int>& holeIndex);*/
  void addHieroRule( int startT, int endT, int startS, int endS
                     , SERGExist &ruleExist, SERGHoleCollection &holeColl, int numHoles, int initStartS
                     , int wordCountT, int wordCountS
                     , const vector<pair<pair<int, int>, char> >&segments);
  void saveHieroPhrase( int startT, int endT, int startS, int endS
                        , SERGHoleCollection &holeColl, LabelIndex &labelIndex, int countS);
  string saveTargetHieroPhrase(  int startT, int endT, int startS, int endS
                                 , WordIndex &indexT, SERGHoleCollection &holeColl, const LabelIndex &labelIndex, double &logPCFGScore, int countS);
  string saveSourceHieroPhrase( int startT, int endT, int startS, int endS
                                , SERGHoleCollection &holeColl, const LabelIndex &labelIndex, vector<string>& fids);
  void preprocessSourceHieroPhrase( int startT, int endT, int startS, int endS
                                    , WordIndex &indexS, SERGHoleCollection &holeColl, const LabelIndex &labelIndex);
  void saveHieroAlignment(  int startT, int endT, int startS, int endS
                            , const WordIndex &indexS, const WordIndex &indexT, SERGHoleCollection &holeColl, ExtractedSERGRule &rule);
  void saveAllHieroPhrases( int startT, int endT, int startS, int endS, SERGHoleCollection &holeColl, int countS);

  inline string IntToString( int i ) {
    stringstream out;
    out << i;
    return out.str();
  }

public:
  ExtractTask(SERGSentenceAlignment &sentence, const SERGExtractionOptions &options, Moses::OutputFileStream &extractFile, Moses::OutputFileStream &extractFileInv):
    m_sentence(sentence),
    m_options(options),
    m_extractFile(extractFile),
    m_extractFileInv(extractFileInv) {}
  void Run();

};

// stats for glue grammar and unknown word label probabilities
void collectWordLabelCounts(SERGSentenceAlignment &sentence );
void writeGlueGrammar(const string &, SERGExtractionOptions &options, set< string > &targetLabelCollection, map< string, int > &targetTopLabelCollection);
void writeUnknownWordLabel(const string &);


int main(int argc, char* argv[])
{
  cerr << "extract-serg, written by Liangyou Li\n"
       << "rule extraction from an aligned parallel corpus\n";

  SERGExtractionOptions options;
  int sentenceOffset = 0;
#ifdef WITH_THREADS
  int thread_count = 1;
#endif
  if (argc < 5) {
    cerr << "syntax: extract-rules corpus.target corpus.source corpus.align extract ["

         << " --GlueGrammar FILE"
         << " | --UnknownWordLabel FILE"
         << " | --OnlyDirect"
         << " | --OutputNTLengths"
         << " | --MaxSpan[" << options.maxSpan << "]"
         << " | --MinHoleTarget[" << options.minHoleTarget << "]"
         << " | --MinHoleSource[" << options.minHoleSource << "]"
         << " | --MinWords[" << options.minWords << "]"
         << " | --MaxSymbolsTarget[" << options.maxSymbolsTarget << "]"
         << " | --MaxSymbolsSource[" << options.maxSymbolsSource << "]"
         << " | --MaxNonTerm[" << options.maxNonTerm << "]"
         << " | --MaxScope[" << options.maxScope << "]"
         << " | --SourceSyntax | --TargetSyntax"
         << " | --AllowOnlyUnalignedWords | --DisallowNonTermConsecTarget |--NonTermConsecSource |  --NoNonTermFirstWord | --NoFractionalCounting"
         << " | --UnpairedExtractFormat"
         << " | --ConditionOnTargetLHS ]"
         << " | --BoundaryRules[" << options.boundaryRules << "]";

    exit(1);
  }
  char* &fileNameT = argv[1];
  char* &fileNameS = argv[2];
  char* &fileNameA = argv[3];
  string fileNameGlueGrammar;
  string fileNameUnknownWordLabel;
  string fileNameExtract = string(argv[4]);

  int optionInd = 5;

  for(int i=optionInd; i<argc; i++) {
    // maximum span length
    if (strcmp(argv[i],"--MaxSpan") == 0) {
      options.maxSpan = atoi(argv[++i]);
      if (options.maxSpan < 1) {
        cerr << "extract error: --maxSpan should be at least 1" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i],"--MinHoleTarget") == 0) {
      options.minHoleTarget = atoi(argv[++i]);
      if (options.minHoleTarget < 1) {
        cerr << "extract error: --minHoleTarget should be at least 1" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i],"--MinHoleSource") == 0) {
      options.minHoleSource = atoi(argv[++i]);
      if (options.minHoleSource < 1) {
        cerr << "extract error: --minHoleSource should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MaxSymbolsTarget") == 0) {
      options.maxSymbolsTarget = atoi(argv[++i]);
      if (options.maxSymbolsTarget < 1) {
        cerr << "extract error: --MaxSymbolsTarget should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MaxSymbolsSource") == 0) {
      options.maxSymbolsSource = atoi(argv[++i]);
      if (options.maxSymbolsSource < 1) {
        cerr << "extract error: --MaxSymbolsSource should be at least 1" << endl;
        exit(1);
      }
    }
    // minimum number of words in hierarchical phrase
    else if (strcmp(argv[i],"--MinWords") == 0) {
      options.minWords = atoi(argv[++i]);
      if (options.minWords < 0) {
        cerr << "extract error: --MinWords should be at least 0" << endl;
        exit(1);
      }
    }
    // maximum number of non-terminals
    else if (strcmp(argv[i],"--MaxNonTerm") == 0) {
      options.maxNonTerm = atoi(argv[++i]);
      if (options.maxNonTerm < 1) {
        cerr << "extract error: --MaxNonTerm should be at least 1" << endl;
        exit(1);
      }
    }
    // maximum scope (see Hopkins and Langmead (2010))
    else if (strcmp(argv[i],"--MaxScope") == 0) {
      options.maxScope = atoi(argv[++i]);
      if (options.maxScope < 0) {
        cerr << "extract error: --MaxScope should be at least 0" << endl;
        exit(1);
      }
    } else if (strcmp(argv[i], "--GZOutput") == 0) {
      options.gzOutput = true;
    }
    // allow consecutive non-terminals (X Y | X Y)
    else if (strcmp(argv[i],"--TargetSyntax") == 0) {
      options.targetSyntax = true;
    } else if (strcmp(argv[i],"--SourceSyntax") == 0) {
      options.sourceSyntax = true;
    } else if (strcmp(argv[i],"--AllowOnlyUnalignedWords") == 0) {
      options.requireAlignedWord = false;
    } else if (strcmp(argv[i],"--DisallowNonTermConsecTarget") == 0) {
      options.nonTermConsecTarget = false;
    } else if (strcmp(argv[i],"--NonTermConsecSource") == 0) {
      options.nonTermConsecSource = true;
    } else if (strcmp(argv[i],"--NoNonTermFirstWord") == 0) {
      options.nonTermFirstWord = false;
    } else if (strcmp(argv[i],"--OnlyOutputSpanInfo") == 0) {
      options.onlyOutputSpanInfo = true;
    } else if (strcmp(argv[i],"--OnlyDirect") == 0) {
      options.onlyDirectFlag = true;
    } else if (strcmp(argv[i],"--GlueGrammar") == 0) {
      options.glueGrammarFlag = true;
      if (++i >= argc) {
        cerr << "ERROR: Option --GlueGrammar requires a file name" << endl;
        exit(0);
      }
      fileNameGlueGrammar = string(argv[i]);
      cerr << "creating glue grammar in '" << fileNameGlueGrammar << "'" << endl;
    } else if (strcmp(argv[i],"--UnknownWordLabel") == 0) {
      options.unknownWordLabelFlag = true;
      if (++i >= argc) {
        cerr << "ERROR: Option --UnknownWordLabel requires a file name" << endl;
        exit(0);
      }
      fileNameUnknownWordLabel = string(argv[i]);
      cerr << "creating unknown word labels in '" << fileNameUnknownWordLabel << "'" << endl;
    }
    // TODO: this should be a useful option
    //else if (strcmp(argv[i],"--ZipFiles") == 0) {
    //  zipFiles = true;
    //}
    // if an source phrase is paired with two target phrases, then count(t|s) = 0.5
    else if (strcmp(argv[i],"--NoFractionalCounting") == 0) {
      options.fractionalCounting = false;
    } else if (strcmp(argv[i],"--PCFG") == 0) {
      options.pcfgScore = true;
    } else if (strcmp(argv[i],"--OutputNTLengths") == 0) {
      options.outputNTLengths = true;
    } else if (strcmp(argv[i],"--UnpairedExtractFormat") == 0) {
      options.unpairedExtractFormat = true;
    } else if (strcmp(argv[i],"--ConditionOnTargetLHS") == 0) {
      options.conditionOnTargetLhs = true;
    } else if (strcmp(argv[i],"-threads") == 0 ||
               strcmp(argv[i],"--threads") == 0 ||
               strcmp(argv[i],"--Threads") == 0) {
#ifdef WITH_THREADS
      thread_count = atoi(argv[++i]);
#else
      cerr << "thread support not compiled in." << '\n';
      exit(1);
#endif
    } else if (strcmp(argv[i], "--SentenceOffset") == 0) {
      if (i+1 >= argc || argv[i+1][0] < '0' || argv[i+1][0] > '9') {
        cerr << "extract: syntax error, used switch --SentenceOffset without a number" << endl;
        exit(1);
      }
      sentenceOffset = atoi(argv[++i]);
    } else if (strcmp(argv[i],"--BoundaryRules") == 0) {
      options.boundaryRules = true;
    } else if (strcmp(argv[i],"--MaxRuleDepth") == 0) {
    	options.maxRuleDepth = atoi(argv[++i]);
    } else if (strcmp(argv[i],"--OnlyHDHole") == 0) {
    	options.onlyHDHole = true;
    } else if (strcmp(argv[i],"--RelaxDep") == 0) {
    	options.relaxDep = true;
    } else if (strcmp(argv[i],"--NTBoundary") == 0) {
    	options.ntBoundary = true;
    }else if (strcmp(argv[i],"--RequireComplete") == 0) {
    	options.requireComplete = true;
    } else if (strcmp(argv[i],"--IgnorePhraseDep") == 0) {
    	options.ignorePhraseDep = true;
    } else if (strcmp(argv[i],"--MaxCompleteSegments") == 0) {
    	options.maxCompleteSegments = atoi(argv[++i]);
    } else if (strcmp(argv[i],"--UniqueLabelSet") == 0) {
    	options.uniqueLabelSet = true;
    } else if (strcmp(argv[i],"--UseConnection") == 0) {
    	options.useConnection = true;
    } else if (strcmp(argv[i],"--NoDep") == 0) {
    	options.noDep = true;
    } else if (strcmp(argv[i],"--IncludeWordLabel") == 0) {
    	options.includeWordLabel = true;
    } else if (strcmp(argv[i],"--LabelWithExternalEdge") == 0) {
    	options.labelWithExternalEdge = true;
    } else if (strcmp(argv[i],"--ContextAwareLabel") == 0) {
    	options.contextAwareLabel = true;
    	options.useConnection = true;
    	options.conditionOnTargetLhs = true;
    	//options.labelWithExternalEdge = true;
    } else if (strcmp(argv[i],"--SingleNonTerm") == 0) {
    	options.singleNonTerm = true;
    	//options.useConnection = true;
    } else if (strcmp(argv[i],"--AllowMoreExternalNodes") == 0) {
    	options.allowMoreExternalNodes = true;
    } else if (strcmp(argv[i],"--PartialChildLabel") == 0) {
    	options.partialChildLabel = true;
    } else if (strcmp(argv[i],"--ContextFeatures") == 0) {
      options.contextFeatures = true;
    } else if (strcmp(argv[i],"--Flabel") == 0) {
      options.flabel = true;
    } else if (strcmp(argv[i],"--SourceSegFeatures") == 0) {
      options.sourceSegFeatures = true;
    } else if (strcmp(argv[i],"--SourcePatternFeatures") == 0) {
      options.sourcePatternFeatures = true;
    } else if (strcmp(argv[i],"--BoundaryRules") == 0) {
      options.boundaryRules = true;
    } else {
      cerr << "extract: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  cerr << "extracting SERG rules" << endl;

  // open input files
  Moses::InputFileStream tFile(fileNameT);
  Moses::InputFileStream sFile(fileNameS);
  Moses::InputFileStream aFile(fileNameA);

  istream *tFileP = &tFile;
  istream *sFileP = &sFile;
  istream *aFileP = &aFile;

  // open output files
  string fileNameExtractInv = fileNameExtract + ".inv" + (options.gzOutput?".gz":"");
  Moses::OutputFileStream extractFile;
  Moses::OutputFileStream extractFileInv;
  extractFile.Open((fileNameExtract  + (options.gzOutput?".gz":"")).c_str());
  if (!options.onlyDirectFlag)
    extractFileInv.Open(fileNameExtractInv.c_str());


  // stats on labels for glue grammar and unknown word label probabilities
  set< string > targetLabelCollection, sourceLabelCollection;
  map< string, int > targetTopLabelCollection, sourceTopLabelCollection;

  // loop through all sentence pairs
  size_t i=sentenceOffset;
  while(true) {
    i++;
    if (i%1000 == 0) cerr << i << " " << flush;
    char targetString[LINE_MAX_LENGTH];
    char sourceString[LINE_MAX_LENGTH];
    char alignmentString[LINE_MAX_LENGTH];
    SAFE_GETLINE((*tFileP), targetString, LINE_MAX_LENGTH, '\n', __FILE__);
    if (tFileP->eof()) break;
    SAFE_GETLINE((*sFileP), sourceString, LINE_MAX_LENGTH, '\n', __FILE__);
    SAFE_GETLINE((*aFileP), alignmentString, LINE_MAX_LENGTH, '\n', __FILE__);

    SERGSentenceAlignment sentence;
    //az: output src, tgt, and alingment line
    if (options.onlyOutputSpanInfo) {
      cout << "LOG: SRC: " << sourceString << endl;
      cout << "LOG: TGT: " << targetString << endl;
      cout << "LOG: ALT: " << alignmentString << endl;
      cout << "LOG: PHRASES_BEGIN:" << endl;
    }

    if (sentence.create(targetString, sourceString, alignmentString,i, options)) {
      if (options.unknownWordLabelFlag) {
        collectWordLabelCounts(sentence);
      }
      ExtractTask *task = new ExtractTask(sentence, options, extractFile, extractFileInv);
      task->Run();
      delete task;
    }
    if (options.onlyOutputSpanInfo) cout << "LOG: PHRASES_END:" << endl; //az: mark end of phrases
    //break;
  }

  tFile.Close();
  sFile.Close();
  aFile.Close();
  // only close if we actually opened it
  if (!options.onlyOutputSpanInfo) {
    extractFile.Close();
    if (!options.onlyDirectFlag) extractFileInv.Close();
  }

  if (options.glueGrammarFlag)
    writeGlueGrammar(fileNameGlueGrammar, options, targetLabelCollection, targetTopLabelCollection);

  if (options.unknownWordLabelFlag)
    writeUnknownWordLabel(fileNameUnknownWordLabel);
}

void ExtractTask::Run()
{
  extractRules();
  if (!m_options.sourceSegFeatures && !m_options.sourcePatternFeatures) {
    consolidateRules();
  }
  writeRulesToFile();
  m_extractedRules.clear();
}

void ExtractTask::extractRules()
{
  int countT = m_sentence.target.size();
  int countS = m_sentence.source.size();

  // phrase repository for creating hiero phrases
  SERGExist ruleExist(countS);

  // check alignments for source phrase startS...endS
  for(int lengthS=1; lengthS <= countS && lengthS <= m_options.maxSpan; lengthS++) {
    for(int startS=0; startS < countS-(lengthS-1); startS++) {

      // that's nice to have
      int endS = startS + lengthS - 1;

      // check if it's valid
      // connected and max 2 external node
      if ((!m_options.useConnection && !m_options.allowMoreExternalNodes  && !m_sentence.source.valid(startS, endS))
      		|| ((m_options.useConnection || m_options.allowMoreExternalNodes) && !m_sentence.source.IsConnected(startS, endS)))
      	continue;

      bool isComplete = false;
      if (m_options.requireComplete)
      	isComplete = m_sentence.source.isComplete(startS, endS);
			if (lengthS > 1 && m_options.requireComplete && !isComplete)
				continue;
			vector<pair<pair<int, int>, char> > segments;
			if (m_options.requireComplete) {
				segments = m_sentence.source.GetHDSegment(startS, endS);
			}

      // find find aligned target words
      // first: find minimum and maximum target word
      int minT = 9999;
      int maxT = -1;
      vector< int > usedT = m_sentence.alignedCountT;
      for(int si=startS; si<=endS; si++) {
        for(unsigned int i=0; i<m_sentence.alignedSoT[si].size(); i++) {
          int ti = m_sentence.alignedSoT[si][i];
          if (ti<minT) {
            minT = ti;
          }
          if (ti>maxT) {
            maxT = ti;
          }
          usedT[ ti ]--;
        }
      }

      // unaligned phrases are not allowed
      if( maxT == -1 )
        continue;

      // source phrase has to be within limits
      if( maxT-minT >= m_options.maxSpan )
        continue;

      // check if target words are aligned to out of bound target words
      bool out_of_bounds = false;
      for(int ti=minT; ti<=maxT && !out_of_bounds; ti++)
        if (usedT[ti]>0) {
          out_of_bounds = true;
        }

      // if out of bound, you gotta go
      if (out_of_bounds)
        continue;

      if (m_options.sourceSegFeatures) {
        ExtractedSERGRule rule(0, 0, startS, endS);
        rule.source = "";
        for(int si=startS; si<=endS; si++) {
            rule.source += m_sentence.source.GetEdge(si).GetWord() + " ";
         }
        string sourceLabel = m_sentence.source.GetLabel(startS, endS, 0);
        rule.source += "[" + sourceLabel + "]";

        rule.target = "";
        for(int si=startS; si<=endS; si++) {
            rule.target += m_sentence.source.GetEdge(si).GetPos() + " ";
         }
        rule.target += "[" + sourceLabel + "]";

        rule.features = extractSourceSegFeatures(startS, endS);
        rule.count = 1.0;
        addRuleToCollection(rule);
        continue;
      }

      if (m_options.contextAwareLabel)
      	m_sentence.source.InitContextAwareLabel(m_options, startS, endS);

      // done with all the checks, lets go over all consistent phrase pairs
      // start point of source phrase may retreat over unaligned
      for(int startT=minT;
          (startT>=0 &&
           startT>maxT - m_options.maxSpan &&// within length limit
           (startT==minT || m_sentence.alignedCountT[startT]==0)); // unaligned
          startT--) {
        // end point of source phrase may advance over unaligned
        for(int endT=maxT;
            (endT<countT &&
            endT<startT + m_options.maxSpan &&// within length limit
             (endT==maxT || m_sentence.alignedCountT[endT]==0)); // unaligned
            endT++) {
          // if there is source side syntax, there has to be a node
          //if (m_options.sourceSyntax && !m_sentence.sourceTree.HasNode(startS,endS))
          //  continue;

          // TODO: loop over all source and target syntax labels

        	//if (lengthS == 5 && startS == 45)
        	//	cout << "here" << endl;

          // if within length limits, add as fully-lexical phrase pair
          if (endT-startT < m_options.maxSymbolsTarget && endS-startS < m_options.maxSymbolsSource) {
            addRule(startT,endT,startS,endS, countS, ruleExist);

          } /*else {
						// take note that this is a valid phrase alignment
						Span tspan = m_sentence.GetDepSpan(startS, endS);
						if(tspan.first <= tspan.second)
							ruleExist.Add(tspan.first, tspan.second, startS, endS, true);
          }*/

          // extract hierarchical rules
          //cout << "exist: " << startS << " " << endS << " " << startT << " " << endT << endl;
          ruleExist.Add(startT, endT, startS, endS);

          // are rules not allowed to start non-terminals?
          int initStartS = m_options.nonTermFirstWord ? startS : startS + 1;

          SERGHoleCollection holeColl(startS, endS); // empty hole collection
          /*map<int,int> sourceHolePos, targetHolePos;
          map<int,int> targetPos;
          for(int i=startT; i<=endT; i++) {
          	targetPos[i] = m_sentence.alignedToS[i].size();;
          }*/
          if (lengthS > 1) {
          	if (m_options.requireComplete && segments.size() > m_options.maxCompleteSegments)
          		continue;
						addHieroRule(startT, endT, startS, endS,
												 ruleExist, holeColl, 0, initStartS,
												 endT-startT+1, endS-startS+1, segments);
          }

          //if (lengthS == 5 && startS == 45)
          //	return;
        }
      }
    }
  }
}

void ExtractTask::preprocessSourceHieroPhrase( int startT, int endT, int startS, int endS
    , WordIndex &indexS, SERGHoleCollection &holeColl, const LabelIndex &labelIndex)
{
	HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
  assert(iterHoleList != holeColl.GetHoles().end());

  int outPos = 0;
  int holeCount = 0;
  //int holeTotal = holeColl.GetHoles().size();
  for(int currPos = startS; currPos <= endS; currPos++) {
    bool isHole = false;
    if (iterHoleList != holeColl.GetHoles().end()) {
      const SERGHole &hole = *iterHoleList;
      isHole = hole.GetStart(0) == currPos;
    }

    if (isHole) {
      SERGHole &hole = *iterHoleList;

      int labelI = labelIndex[ 2+holeCount ];
      string label = m_sentence.source.GetLabels(currPos, hole.GetEnd(0))[labelI];//m_options.sourceSyntax ?
                     //m_sentence.sourceTree.GetNodes(currPos,hole.GetEnd(0))[ labelI ]->GetLabel() : "X";
      hole.SetLabel(label, 0);

      currPos = hole.GetEnd(0);
      hole.SetPos(outPos, 0);
      ++iterHoleList;
      ++holeCount;
    } else {
      indexS[currPos] = outPos;
    }

    outPos++;
  }

  assert(iterHoleList == holeColl.GetHoles().end());
}

string ExtractTask::saveTargetHieroPhrase( int startT, int endT, int startS, int endS
    , WordIndex &indexT, SERGHoleCollection &holeColl, const LabelIndex &labelIndex, double &logPCFGScore
    , int countS)
{
  vector<SERGHole*>::iterator iterHoleList = holeColl.GetSortedTargetHoles().begin();
  assert(iterHoleList != holeColl.GetSortedTargetHoles().end());

  string out = "";
  int outPos = 0;
  int holeCount = 0;
  for(int currPos = startT; currPos <= endT; currPos++) {
    bool isHole = false;
    if (iterHoleList != holeColl.GetSortedTargetHoles().end()) {
      const SERGHole &hole = **iterHoleList;
      isHole = hole.GetStart(1) == currPos;
    }

    if (isHole) {
      SERGHole &hole = **iterHoleList;

      const string &sourceLabel = hole.GetLabel(0);
      assert(sourceLabel != "");

      //int labelI = labelIndex[ 2+holeCount ];
      string targetLabel;
      if (m_options.targetSyntax) {
        targetLabel = "X";//m_sentence.targetTree.GetNodes(currPos,hole.GetEnd(1))[labelI]->GetLabel();
      } else if (m_options.boundaryRules && (startS == 0 || endS == countS - 1)) {
        targetLabel = "S";
      } else {
        targetLabel = "X";
      }

      hole.SetLabel(targetLabel, 1);

      if (m_options.unpairedExtractFormat) {
        out += "[" + targetLabel + "] ";
      } else {
        out += "[" + sourceLabel + "][" + targetLabel + "] ";
      }

      /*if (m_options.pcfgScore) {
        double score = m_sentence.targetTree.GetNodes(currPos,hole.GetEnd(1))[labelI]->GetPcfgScore();
        logPCFGScore -= score;
      }*/

      currPos = hole.GetEnd(1);
      hole.SetPos(outPos, 1);
      ++iterHoleList;
      holeCount++;
    } else {
      indexT[currPos] = outPos;
      out += m_sentence.target[currPos] + " ";
    }

    outPos++;
  }

  assert(iterHoleList == holeColl.GetSortedTargetHoles().end());
  return out.erase(out.size()-1);
}

string ExtractTask::saveSourceHieroPhrase( int startT, int endT, int startS, int endS
    , SERGHoleCollection &holeColl, const LabelIndex &labelIndex, vector<string>& fids)
{
  HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
  assert(iterHoleList != holeColl.GetHoles().end());

  //
  map<int,int> newPos;
  int wordCount = 0;
  for(int currPos = startS; currPos <= endS; currPos++) {
  	bool isHole = false;
		if (iterHoleList != holeColl.GetHoles().end()) {
			const SERGHole &hole = *iterHoleList;
			isHole = hole.GetStart(0) == currPos;
		}

		if (isHole) {
			SERGHole &hole = *iterHoleList;
			for(int i = currPos; i <= hole.GetEnd(0); i++)
				newPos[i] = wordCount;
			currPos = hole.GetEnd(0);
			++iterHoleList;
		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
  }
  assert(iterHoleList == holeColl.GetHoles().end());

//
  iterHoleList = holeColl.GetHoles().begin();
  string out = "";
  int outPos = 0;
  int holeCount = 0;
  for(int currPos = startS; currPos <= endS; currPos++) {
    bool isHole = false;
    if (iterHoleList != holeColl.GetHoles().end()) {
      const SERGHole &hole = *iterHoleList;
      isHole = hole.GetStart(0) == currPos;
    }

    if (isHole) {
      SERGHole &hole = *iterHoleList;
      int labelI = labelIndex[2+holeCount];
      string label = m_sentence.source.GetLabels(currPos, hole.GetEnd(0))[labelI];
      int fid = m_sentence.source.GetSpanFid(currPos, hole.GetEnd(0));
      if (fid < 0) {
      	cerr << "no fid specified" << endl;
      	assert(false);
      }
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];

      const string &targetLabel = hole.GetLabel(1);
      assert(targetLabel != "");

      //hole.SetLabel(label + ":" + IntToString(fid),0);
      fids.push_back(IntToString(fid));

      const string &sourceLabel =  hole.GetLabel(0);
      if (m_options.unpairedExtractFormat) {
        out += "[" + sourceLabel + "] ";
      } else {
        out += "[" + sourceLabel + "][" + targetLabel + "] ";
      }

      currPos = hole.GetEnd(0);
      hole.SetPos(outPos, 0);
      ++iterHoleList;
      ++holeCount;
    } else {
    	int fid = m_sentence.source.GetEdge(currPos).GetFatherId();
    	if (fid < startS || fid > endS)
    		fid = -1;
    	else
    		fid = newPos[fid];
    	fids.push_back(IntToString(fid));
      out += m_sentence.source.GetEdge(currPos).GetWord() + " ";
    }

    outPos++;
  }

  assert(iterHoleList == holeColl.GetHoles().end());
  return out.erase(out.size()-1);
}

void ExtractTask::saveHieroAlignment( int startT, int endT, int startS, int endS
                                      , const WordIndex &indexS, const WordIndex &indexT, SERGHoleCollection &holeColl, ExtractedSERGRule &rule)
{
  // print alignment of words
  for(int ti=startT; ti<=endT; ti++) {
    WordIndex::const_iterator p = indexT.find(ti);
    if (p != indexT.end()) { // does word still exist?
      for(unsigned int i=0; i<m_sentence.alignedToS[ti].size(); i++) {
        int si = m_sentence.alignedToS[ti][i];
        std::string sourceSymbolIndex = IntToString(indexS.find(si)->second);
        std::string targetSymbolIndex = IntToString(p->second);
        rule.alignment      += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
        if (! m_options.onlyDirectFlag)
          rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";
      }
    }
  }

  // print alignment of non terminals
  HoleList::const_iterator iterHole;
  for (iterHole = holeColl.GetHoles().begin(); iterHole != holeColl.GetHoles().end(); ++iterHole) {
    const SERGHole &hole = *iterHole;

    std::string sourceSymbolIndex = IntToString(hole.GetPos(0));
    std::string targetSymbolIndex = IntToString(hole.GetPos(1));
    rule.alignment      += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
    if (!m_options.onlyDirectFlag)
      rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";

    rule.SetSpanLength(hole.GetPos(0), hole.GetSize(0), hole.GetSize(1) ) ;

  }

  rule.alignment.erase(rule.alignment.size()-1);
  if (!m_options.onlyDirectFlag) {
    rule.alignmentInv.erase(rule.alignmentInv.size()-1);
  }
}

void ExtractTask::saveHieroPhrase( int startT, int endT, int startS, int endS
                                   , SERGHoleCollection &holeColl, LabelIndex &labelIndex, int countS)
{
  WordIndex indexS, indexT; // to keep track of word positions in rule

  ExtractedSERGRule rule( startT, endT, startS, endS );

  // phrase labels
  string targetLabel;
  if (m_options.targetSyntax) {
    targetLabel = "X";//m_sentence.targetTree.GetNodes(startT,endT)[labelIndex[0] ]->GetLabel();
  } else if (m_options.boundaryRules && (startS == 0 || endS == countS - 1)) {
    targetLabel = "S";
  } else {
    targetLabel = "X";
  }

  string sourceLabel = m_sentence.source.GetLabels(startS, endS)[ labelIndex[1] ];//m_options.sourceSyntax ?
                       //m_sentence.sourceTree.GetNodes(startS,endS)[ labelIndex[1] ]->GetLabel() : "X";

  // create non-terms on the source side
  preprocessSourceHieroPhrase(startT, endT, startS, endS, indexS, holeColl, labelIndex);

  // target
  /*if (m_options.pcfgScore) {
    double logPCFGScore = m_sentence.targetTree.GetNodes(startT,endT)[labelIndex[0]]->GetPcfgScore();
    rule.target = saveTargetHieroPhrase(startT, endT, startS, endS, indexT, holeColl, labelIndex, logPCFGScore, countS)
                  + " [" + targetLabel + "]";
    rule.pcfgScore = std::exp(logPCFGScore);
  } else {*/
    double logPCFGScore = 0.0f;
    rule.target = saveTargetHieroPhrase(startT, endT, startS, endS, indexT, holeColl, labelIndex, logPCFGScore, countS)
                  + " [" + targetLabel + "]";
  //}

  // source
  vector<string> fids;
  rule.source = saveSourceHieroPhrase(startT, endT, startS, endS, holeColl, labelIndex, fids);
  if (m_options.conditionOnTargetLhs) {
  	sourceLabel = targetLabel;
    //rule.source += " [" + targetLabel + "]";
  } //else {
  if (!m_options.noDep) {
  	sourceLabel += "+";
  	if (m_options.relaxDep) {
  		for (size_t f = 0; f < fids.size(); f++) {
  			if (fids[f] == "-1")
  				sourceLabel += IntToString(f) + ":";
			}
  	} else {
			for (size_t f = 0; f < fids.size(); f++) {
				sourceLabel += fids[f] + ":";
			}
  	}
  	sourceLabel = sourceLabel.erase(sourceLabel.size()-1);
  }
    rule.source += " [" + sourceLabel + "]";
 // }

  // alignment
  saveHieroAlignment(startT, endT, startS, endS, indexS, indexT, holeColl, rule);

  if (m_options.contextFeatures) {
    string ctxt = extractContextFeatures(startS, endS, startT, endT, holeColl);
    rule.SetFeatures(ctxt);
  }

  if (m_options.sourcePatternFeatures) {
    string feats = extractSourcePatternFeatures(startS, endS, holeColl);
    rule.SetFeatures(feats);

    rule.target = "";
    for(int si=startS; si<=endS; si++) {
        rule.target += m_sentence.source.GetEdge(si).GetWord() + " ";
    }
    rule.target.erase(rule.target.size()-1);

    rule.alignment = "";
    for(int si=startS; si<=endS; si++) {
        rule.alignment += m_sentence.source.GetEdge(si).GetPos() + " ";
    }
    rule.alignment.erase(rule.alignment.size()-1);

    rule.count = 1.0;
  }

  addRuleToCollection( rule );
}

void ExtractTask::saveAllHieroPhrases( int startT, int endT, int startS, int endS, SERGHoleCollection &holeColl, int countS)
{

/*	if (m_options.onlyOutputSpanInfo) {
		if (startS == 45 && endS == 49) {
			cout << startS <<" " << endS << " holes: ";
			for( HoleList::const_iterator hole = holeColl.GetHoles().begin();
					 hole != holeColl.GetHoles().end(); hole++ ) {
				cout << hole->GetStart(0) << "-" << hole->GetEnd(0) << "<>" << hole->GetStart(1) << "-" << hole->GetEnd(1) << " ";
			}
			cout << endl;
		}
	}*/

  LabelIndex labelIndex,labelCount;

  // number of target head labels
  int numLabels = 1;//m_options.targetSyntax ? m_sentence.targetTree.GetNodes(startT,endT).size() : 1;
  labelCount.push_back(numLabels);
  labelIndex.push_back(0);

  // number of source head labels
  numLabels =  m_sentence.source.GetLabels(startS,endS).size();//m_options.sourceSyntax ? m_sentence.sourceTree.GetNodes(startS,endS).size() : 1;

  if (m_options.contextAwareLabel)
  	numLabels = 1;

  if (numLabels == 0) {
  	cerr << "non-term num error " << startS << " " << endS << endl;
  	assert(false);
  }

  labelCount.push_back(numLabels);
  labelIndex.push_back(0);

  // number of source hole labels
  for( HoleList::const_iterator hole = holeColl.GetHoles().begin();
       hole != holeColl.GetHoles().end(); hole++ ) {

    int numLabels =  m_sentence.source.GetLabels(hole->GetStart(0),hole->GetEnd(0)).size();//m_options.targetSyntax ? m_sentence.targetTree.GetNodes(hole->GetStart(1),hole->GetEnd(1)).size() : 1 ;

    if (numLabels == 0) {
			cerr << "non-term num error in hole " << hole->GetStart(0) << " " << hole->GetEnd(0) << endl;
			assert(false);
		}

    labelCount.push_back(numLabels);
    labelIndex.push_back(0);
  }

  // number of target hole labels
  holeColl.SortTargetHoles();
  for( vector<SERGHole*>::iterator i = holeColl.GetSortedTargetHoles().begin();
       i != holeColl.GetSortedTargetHoles().end(); i++ ) {
    //const SERGHole &hole = **i;
    int numLabels =  1;//m_options.sourceSyntax ? m_sentence.sourceTree.GetNodes(hole.GetStart(0),hole.GetEnd(0)).size() : 1 ;
    labelCount.push_back(numLabels);
    labelIndex.push_back(0);
  }

  // loop through the holes
  bool done = false;
  while(!done) {
    saveHieroPhrase( startT, endT, startS, endS, holeColl, labelIndex, countS );
    for(unsigned int i=0; i<labelIndex.size(); i++) {
      labelIndex[i]++;
      if(labelIndex[i] == labelCount[i]) {
        labelIndex[i] = 0;
        if (i == labelIndex.size()-1)
          done = true;
      } else {
        break;
      }
    }
  }
}

// this function is called recursively
// it pokes a new hole into the phrase pair, and then calls itself for more holes
/*void ExtractTask::addHieroRuleComplete( int startT, int endT, int startS, int endS
                                , SERGExist &ruleExist, vector<char> &ntypes
                                , int numHoles, int initFlagS, int wordCountT, int wordCountS
                                , const vector<pair<pair<int, int>, char> >&segments, const vector<char>& types)
{
  // done, if already the maximum number of non-terminals in phrase pair
  //if (numHoles >= m_options.maxNonTerm)
  //  return;

  // find a hole...
  //int sc = segCount;
	// flag sequence,: head leaf internal
  for (int startFlagS = initFlagS; startFlagS < types.size(); ++startFlagS) {
  	char ntype = types[startFlagS];
  	ntypes.push_back(ntype);

  	vector<int> holeCount, holeIndex;
  	vector<pair<pair<int, int>, char> >::const_iterator iter;
  	for (iter = segments.begin(); iter != segments.end(); iter++) {
  		pair<int,int> span = iter->first;
  		char stype = iter->second;

  		if (std::find(ntypes.begin(), ntypes.end(), stype) == ntypes.end())
  			continue;

  		int startHoleS = span.first, endHoleS = span.second;
  		const HoleList &targetHoles = ruleExist.GetTargetHoles(startHoleS, endHoleS);
  		if (targetHoles.size() == 0)
  			continue;
  		holeCount.push_back(targetHoles.size());
  		holeIndex.push_back(0);
  	}

	// loop through the holes
		bool done = false;
		while(!done) {
			constructHieroRuleComplete(startT, endT, startS, endS, ruleExist, ntypes, numHoles
																 , initFlagS, wordCountT, wordCountS, segments, holeIndex);
			for(unsigned int i=0; i<holeIndex.size(); i++) {
				holeIndex[i]++;
				if(holeIndex[i] == holeCount[i]) {
					holeIndex[i] = 0;
					if (i == holeIndex.size()-1)
						done = true;
				} else {
					break;
				}
			}
		}

		addHieroRuleComplete(startT, endT, startS, endS, ruleExist, ntypes, numHoles, startFlagS+1, wordCountT, wordCountS, segments, types);
		ntypes.pop_back();
		addHieroRuleComplete(startT, endT, startS, endS, ruleExist, ntypes, numHoles, startFlagS+1, wordCountT, wordCountS, segments, types);
  }
}


void ExtractTask::constructHieroRuleComplete( int startT, int endT, int startS, int endS
                        , SERGExist &ruleExist, vector<char> &ntypes, int numHoles, int initFlagS
                        , int wordCountT, int wordCountS, const vector<pair<pair<int, int>, char> >&segments
                        , const vector<int>& holeIndex)
{

	if (holeIndex.size() == 0)
		return;

	int newCountS = wordCountS;
	SERGHoleCollection holeColl(startS, endS);
	vector<pair<pair<int, int>, char> >::const_iterator iter;
	int count = 0;
	for (iter = segments.begin(); iter != segments.end(); iter++) {
		pair<int,int> span = iter->first;
		char stype = iter->second;

		if (std::find(ntypes.begin(), ntypes.end(), stype) == ntypes.end())
			continue;

		int startHoleS = span.first, endHoleS = span.second;
		const HoleList &targetHoles = ruleExist.GetTargetHoles(startHoleS, endHoleS);
		if (targetHoles.size() == 0)
			continue;

		HoleList::const_iterator iterHole = targetHoles.begin();
		std::advance(iterHole, holeIndex[count]);
		const SERGHole &targetHole = *iterHole;
		count++;

		if (startT > targetHole.GetStart(1) || endT <  targetHole.GetEnd(1))
		  return;
		// make sure target side does not overlap with another hole
		if (holeColl.OverlapTarget(targetHole))
			return;

		newCountS -= (endS-startS);
		holeColl.Add(targetHole.GetStart(1), targetHole.GetEnd(1), startHoleS, endHoleS, targetHole.isdep);
	}

	if (newCountS > m_options.maxSymbolsSource)
		return;
	//if (holeColl.GetHoles().size() == 0)
	//	return;
	saveAllHieroPhrases(startT, endT, startS, endS, holeColl, wordCountS);

}*/

// this function is called recursively
// it pokes a new hole into the phrase pair, and then calls itself for more holes
void ExtractTask::addHieroRule( int startT, int endT, int startS, int endS
                                , SERGExist &ruleExist, SERGHoleCollection &holeColl
                                , int numHoles, int initStartS, int wordCountT, int wordCountS
                                , const vector<pair<pair<int,int>, char> >& segments)
{
  // done, if already the maximum number of non-terminals in phrase pair
  if (numHoles >= m_options.maxNonTerm)
    return;

  // find a hole...
  //int sc = segCount;
  for (int startHoleS = initStartS; startHoleS <= endS; ++startHoleS) {

  	if (m_options.requireComplete) {
  		bool found = false;
  		for(size_t si = 0; si < segments.size(); si++) {
  			if (segments[si].first.first >= startHoleS) {
  				startHoleS = segments[si].first.first;
  				found = true;
  				break;
  			}
  		}
  		if (!found) {
  			continue;
  		}
  	}

    for (int endHoleS = startHoleS + m_options.minHoleSource - 1; endHoleS <= endS; ++endHoleS) {

    	if (m_options.requireComplete) {
				bool found = false;
				for(size_t si = 0; si < segments.size(); si++) {
					if (segments[si].first.second >= endHoleS) {
						endHoleS = segments[si].first.second;
						found = true;
						break;
					}
				}
				if (!found) {
					continue;
				}
			}

      // if last non-terminal, enforce word count limit
      if (numHoles == m_options.maxNonTerm-1 && wordCountS - (endHoleS-startHoleS+1) + (numHoles+1) > m_options.maxSymbolsSource)
        continue;

    	if(m_options.useConnection && !m_sentence.source.valid(startS, endS, startHoleS, endHoleS))
    		continue;

      //if (ruleExist.GetTargetHoles(startHoleS, endHoleS).size() == 0)
      //	continue;

    	if (m_options.requireComplete  && ! (m_sentence.source.isHeadHole(startS, endS, startHoleS, endHoleS)
    			  || m_sentence.source.isComplete(startHoleS, endHoleS)))
    		continue;

      // determine the number of remaining target words
      const int newWordCountS = wordCountS - (endHoleS-startHoleS+1);

      // always enforce min word count limit
      if (newWordCountS < m_options.minWords)
        continue;

      // except the whole span
      if (startHoleS == startS && endHoleS == endS)
        continue;

      // does a phrase cover this target span?
      // if it does, then there should be a list of mapped source phrases
      // (multiple possible due to unaligned words)
      const HoleList &targetHoles = ruleExist.GetTargetHoles(startHoleS, endHoleS);

      /*if (targetHoles.size() > 0) {
      	for(int i = startS; i <= endS; i++) {
      			sourceHolePos[i] = 1;
      	}
      }*/

      // loop over sub phrase pairs
      HoleList::const_iterator iterTargetHoles;
      for (iterTargetHoles = targetHoles.begin(); iterTargetHoles != targetHoles.end(); ++iterTargetHoles) {
        const SERGHole &targetHole = *iterTargetHoles;

        const int targetHoleSize = targetHole.GetEnd(1)-targetHole.GetStart(1)+1;

        // enforce minimum hole size
        if (targetHoleSize < m_options.minHoleTarget)
          continue;

        // determine the number of remaining source words
        const int newWordCountT = wordCountT - targetHoleSize;

        // if last non-terminal, enforce word count limit
        if (numHoles == m_options.maxNonTerm-1 && newWordCountT + (numHoles+1) > m_options.maxSymbolsTarget)
          continue;

        // enforce min word count limit
        if (newWordCountT < m_options.minWords)
          continue;

        // hole must be subphrase of the source phrase
        // (may be violated if subphrase contains additional unaligned source word)
        if (startT > targetHole.GetStart(1) || endT <  targetHole.GetEnd(1))
          continue;

        // make sure target side does not overlap with another hole
        if (holeColl.OverlapTarget(targetHole))
          continue;

        // if consecutive non-terminals are not allowed, also check for source
        if (!m_options.nonTermConsecTarget && holeColl.ConsecTarget(targetHole) )
          continue;

        // check that rule scope would not exceed limit if sourceHole
        // were added
        //if (holeColl.Scope(targetHole) > m_options.maxScope)
        //  continue;

        // require that at least one aligned word is left (unless there are no words at all)
        if (m_options.requireAlignedWord && (newWordCountS > 0 || newWordCountT > 0)) {
          HoleList::const_iterator iterHoleList = holeColl.GetHoles().begin();
          bool foundAlignedWord = false;
          // loop through all word positions
          for(int pos = startS; pos <= endS && !foundAlignedWord; pos++) {
            // new hole? moving on...
            if (pos == startHoleS) {
              pos = endHoleS;
            }
            // covered by hole? moving on...
            else if (iterHoleList != holeColl.GetHoles().end() && iterHoleList->GetStart(0) == pos) {
              pos = iterHoleList->GetEnd(0);
              ++iterHoleList;
            }
            // covered by word? check if it is aligned
            else {
              if (m_sentence.alignedSoT[pos].size() > 0)
                foundAlignedWord = true;
            }
          }
          if (!foundAlignedWord)
            continue;
        }

        // update list of holes in this phrase pair
        holeColl.Add(targetHole.GetStart(1), targetHole.GetEnd(1), startHoleS, endHoleS, targetHole.isdep);
        // now some checks that disallow this phrase pair, but not further recursion
        bool allowablePhrase = true;

        // maximum words count violation?
        if (newWordCountS + (numHoles+1) > m_options.maxSymbolsSource)
          allowablePhrase = false;

        if (newWordCountT + (numHoles+1) > m_options.maxSymbolsTarget)
          allowablePhrase = false;
        // check lexical word alignment consistency

        //if (!ruleDepthLimit(startS, endS, holeColl))
        //	allowablePhrase = false;

        // passed all checks...
        if (allowablePhrase)
          saveAllHieroPhrases(startT, endT, startS, endS, holeColl, wordCountS);

        // recursively search for next hole
        int nextInitStartS = m_options.nonTermConsecSource ? endHoleS + 1 : endHoleS + 2;
        addHieroRule(startT, endT, startS, endS
                     , ruleExist, holeColl, numHoles + 1, nextInitStartS
                     , newWordCountT, newWordCountS, segments);

        holeColl.RemoveLast();
      }

    }
  }
}

bool ExtractTask::ruleDepthLimit(int startS, int endS, SERGHoleCollection &holeColl)
{

	HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
	assert(iterHoleList != holeColl.GetHoles().end());

	//
	map<int,int> newPos;
	int wordCount = 0;
	for(int currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holeColl.GetHoles().end()) {
			const SERGHole &hole = *iterHoleList;
			isHole = hole.GetStart(0) == currPos;
		}

		if (isHole) {
			SERGHole &hole = *iterHoleList;
			for(int i = currPos; i <= hole.GetEnd(0); i++)
				newPos[i] = wordCount;
			currPos = hole.GetEnd(0);
			++iterHoleList;
		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
	}

	iterHoleList = holeColl.GetHoles().begin();
	string out = "";
	int outPos = 0;
	int holeCount = 0;
	vector<int> fids;
	for(int currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holeColl.GetHoles().end()) {
			const SERGHole &hole = *iterHoleList;
			isHole = hole.GetStart(0) == currPos;
		}

		if (isHole) {
			SERGHole &hole = *iterHoleList;
			int fid = m_sentence.source.GetSpanFid(currPos, hole.GetEnd(0));
			if (fid < 0) {
				cerr << "no fid specified" << endl;
				assert(false);
			}
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];
			fids.push_back(fid);

			currPos = hole.GetEnd(0);
			hole.SetPos(outPos, 0);
			++iterHoleList;
			++holeCount;
		} else {
			int fid = (int)m_sentence.source.GetEdge(currPos).GetFatherId();
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];
			fids.push_back(fid);
		}

		outPos++;
	}

	assert(iterHoleList == holeColl.GetHoles().end());

	for(size_t i = 0; i < fids.size(); i++) {
		int depth = 1;
		int fid = fids[i];
		while (fid >= 0 || fid < fids.size()) {
			depth++;
			fid = fids[fid];
		}
		if (depth > m_options.maxRuleDepth)
			return false;
	}

	return true;

  /*HoleList::iterator iterHoleList = holeColl.GetHoles().begin();
  assert(iterHoleList != holeColl.GetHoles().end());

  int depth = 0;
  for(int currPos = startS; currPos <= endS; currPos++) {
    bool isHole = false;
    if (iterHoleList != holeColl.GetHoles().end()) {
      const SERGHole &hole = *iterHoleList;
      isHole = hole.GetStart(0) == currPos;
    }

    if (isHole) {
      SERGHole &hole = *iterHoleList;
      for (int i = currPos; i <= hole.GetEnd(0); i++) {
      	if (m_sentence.source.GetDepth(startS, endS, currPos) > m_options.maxRuleDepth) {
      		int fid = m_sentence.source.GetSpanFid(i,i);
      		if (fid < currPos || fid > hole.GetEnd(0))
      	   return false;
      	}
      }
      currPos = hole.GetEnd(0);
      ++iterHoleList;
    } else {
    	if (m_sentence.source.GetDepth(startS, endS, currPos) > m_options.maxRuleDepth) {
    		return false;
    	}
    }
  }
  assert(iterHoleList == holeColl.GetHoles().end());
  return true;*/
}

void ExtractTask::addRule( int startT, int endT, int startS, int endS, int countS, SERGExist &ruleExist)
{
  // contains only <s> or </s>. Don't output
  if (m_options.boundaryRules
      && (   (startS == 0         && endS == 0)
             || (startS == countS-1  && endS == countS-1))) {
    return;
  }

  if (m_options.onlyOutputSpanInfo) {
    cout << startS << " " << endS << " " << startT << " " << endT << endl;
    return;
  }

  ExtractedSERGRule rule(startT, endT, startS, endS);

  // phrase labels
  string targetLabel,sourceLabel;
  if (m_options.targetSyntax && m_options.conditionOnTargetLhs) {
    sourceLabel = targetLabel = "X";//m_sentence.targetTree.GetNodes(startT,endT)[0]->GetLabel();
  } else {
    sourceLabel = m_sentence.source.GetLabel(startS, endS, 0);//m_options.sourceSyntax ?
                  //m_sentence.sourceTree.GetNodes(startS,endS)[0]->GetLabel() : "X";

    if (m_options.targetSyntax) {
      targetLabel = "X";//m_sentence.targetTree.GetNodes(startT,endT)[0]->GetLabel();
    } else if (m_options.boundaryRules && (startS == 0 || endS == countS - 1)) {
      targetLabel = "S";
    } else {
      targetLabel = "X";
    }
  }

  if (m_options.conditionOnTargetLhs) {
  	sourceLabel = targetLabel;
  }

  // source
  rule.source = "";
  for(int si=startS; si<=endS; si++) {
		rule.source += m_sentence.source.GetEdge(si).GetWord() + " ";
	}
  if (!m_options.noDep) {
		sourceLabel += "+";
		if (m_options.ignorePhraseDep) {
			if(m_options.relaxDep)
				sourceLabel += "0";
			else
				sourceLabel += "-1";
		} else {
			if (m_options.relaxDep) {
				for(int si=startS; si<=endS; si++) {
					int fid = (int)m_sentence.source.GetEdge(si).GetFatherId();
					if(fid < startS || fid > endS) {
						sourceLabel += IntToString(si-startS) + ":";
					}
				}
				sourceLabel = sourceLabel.erase(sourceLabel.size()-1);
			} else {
				for(int si=startS; si<=endS; si++) {
					int fid = (int)m_sentence.source.GetEdge(si).GetFatherId();
					if(fid < startS || fid > endS) {
						fid = -1;
					} else {
						fid = fid-startS;
					}
					sourceLabel += IntToString(fid) + ":";
				}
				sourceLabel = sourceLabel.erase(sourceLabel.size()-1);
			}
		}
  }

  rule.source += "[" + sourceLabel + "]";

  // target
  rule.target = "";
  for(int ti=startT; ti<=endT; ti++)
    rule.target += m_sentence.target[ti] + " ";
  rule.target += "[" + targetLabel + "]";

 /* if (m_options.pcfgScore) {
    double logPCFGScore = m_sentence.targetTree.GetNodes(startT,endT)[0]->GetPcfgScore();
    rule.pcfgScore = std::exp(logPCFGScore);
  }*/

  // alignment
  for(int ti=startT; ti<=endT; ti++) {
    for(unsigned int i=0; i<m_sentence.alignedToS[ti].size(); i++) {
      int si = m_sentence.alignedToS[ti][i];
      std::string sourceSymbolIndex = IntToString(si-startS);
      std::string targetSymbolIndex = IntToString(ti-startT);
      rule.alignment += sourceSymbolIndex + "-" + targetSymbolIndex + " ";
      if (!m_options.onlyDirectFlag)
        rule.alignmentInv += targetSymbolIndex + "-" + sourceSymbolIndex + " ";
    }
  }

  rule.alignment.erase(rule.alignment.size()-1);
  if (!m_options.onlyDirectFlag)
    rule.alignmentInv.erase(rule.alignmentInv.size()-1);

  // extract context features for phrasal rule
  if (m_options.contextFeatures) {
    string ctxt = extractContextFeatures(startS, endS, startT, endT, SERGHoleCollection(startS, endS));
    rule.SetFeatures(ctxt);
  }

  if (m_options.sourcePatternFeatures) {
    string feats = extractSourcePatternFeatures(startS, endS,SERGHoleCollection(startS, endS));
    rule.SetFeatures(feats);

    rule.target = "";
    for(int si=startS; si<=endS; si++) {
        rule.target += m_sentence.source.GetEdge(si).GetWord() + " ";
    }
    rule.target.erase(rule.target.size()-1);

    rule.alignment = "";
    for(int si=startS; si<=endS; si++) {
        rule.alignment += m_sentence.source.GetEdge(si).GetPos() + " ";
    }
    rule.alignment.erase(rule.alignment.size()-1);

    rule.count = 1.0;
  }

  addRuleToCollection( rule );
}

string ExtractTask::extractSourceSegFeatures(int startS, int endS)
{
  vector<string> features;
  int size = m_sentence.source.size();

  // source sequence context feature
  string prew="SOS", nextw="EOS", prep="SOS", nextp="EOS";
  if (startS > 0) {
    prew = m_sentence.source.GetEdge(startS-1).GetWord();
    prep = m_sentence.source.GetEdge(startS-1).GetPos();
  }
  if (endS < size-1) {
    nextw = m_sentence.source.GetEdge(endS+1).GetWord();
    nextp = m_sentence.source.GetEdge(endS+1).GetPos();
  }
  features.push_back("SRC_PRE_LEX_"+prew);
  features.push_back("SRC_PRE_POS_"+prep);
  features.push_back("SRC_NXT_LEX_"+nextw);
  features.push_back("SRC_NXT_POS_"+nextp);

  // source dep feature
  int fid = m_sentence.source.GetSpanFid(startS, endS);
  vector<int> kids = m_sentence.source.GetSpanKidsIndex(startS, endS);
  vector<int> sibs = m_sentence.source.GetSpanSiblingIndex(startS, endS);
  ///// father node
  if (fid < 0 || fid >= size) {
    features.push_back("SRC_PAR_LEX_ROOT");
    features.push_back("SRC_PAR_POS_ROOT");
  } else {
    features.push_back("SRC_PAR_LEX_"+m_sentence.source.GetEdge(fid).GetWord());
    features.push_back("SRC_PAR_POS_"+m_sentence.source.GetEdge(fid).GetPos());
  }
  

  for(size_t i = 0; i < kids.size(); i++) {
    features.push_back("SRC_KID_LEX_" + m_sentence.source.GetEdge(kids[i]).GetWord());
    features.push_back("SRC_KID_POS_" + m_sentence.source.GetEdge(kids[i]).GetPos());
  }

  if (features.size() == 0)
    return "";

  string ret = features[0];
  for(size_t i = 1; i < features.size(); i++) {
    ret += " " + features[i];
  }
  return ret;
}

string ExtractTask::extractSourcePatternFeatures(int startS, int endS, const SERGHoleCollection &holeColl)
{
  vector<string> features;
  int size = m_sentence.source.size();

  // source sequence context feature
  string prew="SOS", nextw="EOS", prep="SOS", nextp="EOS";
  if (startS > 0) {
    prew = m_sentence.source.GetEdge(startS-1).GetWord();
    prep = m_sentence.source.GetEdge(startS-1).GetPos();
  }
  if (endS < size-1) {
    nextw = m_sentence.source.GetEdge(endS+1).GetWord();
    nextp = m_sentence.source.GetEdge(endS+1).GetPos();
  }
  features.push_back("SRC_PRE_LEX_"+prew);
  features.push_back("SRC_PRE_POS_"+prep);
  features.push_back("SRC_NXT_LEX_"+nextw);
  features.push_back("SRC_NXT_POS_"+nextp);

  //features.push_back("SRC_FST_LEX_"+m_sentence.source.GetEdge(startS).GetWord());
  //features.push_back("SRC_FST_POS_"+m_sentence.source.GetEdge(startS).GetPos());
  //features.push_back("SRC_LST_LEX_"+m_sentence.source.GetEdge(endS).GetWord());
  //features.push_back("SRC_LST_POS_"+m_sentence.source.GetEdge(endS).GetPos());

  // source and target hole feature
  const HoleList& holes = holeColl.GetHoles();
  HoleList::const_iterator iter = holes.begin();
  if (iter != holes.end()) {
    int count = 1;
    for (; iter != holes.end(); ++iter) {
      int hstart = iter->GetStart(0), hend = iter->GetEnd(0);
      // source
      string prefix = "SRC_X"+IntToString(count)+"_";
      features.push_back(prefix+"FST_LEX_" + m_sentence.source.GetEdge(hstart).GetWord());
      features.push_back(prefix+"FST_POS_" + m_sentence.source.GetEdge(hstart).GetPos());
      features.push_back(prefix+"LST_LEX_" + m_sentence.source.GetEdge(hend).GetWord());
      features.push_back(prefix+"LST_POS_" + m_sentence.source.GetEdge(hend).GetPos());
      int len = hend-hstart+1;
      features.push_back(prefix+"LEN_" + IntToString(len));
      // target
      //prefix = "TGT_X"+IntToString(count)+"_";
      //hstart = iter->GetStart(1), hend = iter->GetEnd(1);
      //features.push_back(prefix+"FST_LEX_" + m_sentence.target[hstart]);
      //features.push_back(prefix+"LST_LEX_" + m_sentence.target[hend]);
      //len = hend-hstart+1;
      //features.push_back(prefix+"LEN_" + IntToString(len));
      count++;
    }
  }

  // source dep feature
  int fid = m_sentence.source.GetSpanFid(startS, endS);
  vector<int> kids = m_sentence.source.GetSpanKidsIndex(startS, endS);
  vector<int> sibs = m_sentence.source.GetSpanSiblingIndex(startS, endS);
  ///// father node
  if (fid < 0 || fid >= size) {
    features.push_back("SRC_PAR_LEX_ROOT");
    features.push_back("SRC_PAR_POS_ROOT");
  } else {
    features.push_back("SRC_PAR_LEX_"+m_sentence.source.GetEdge(fid).GetWord());
    features.push_back("SRC_PAR_POS_"+m_sentence.source.GetEdge(fid).GetPos());
  }
  ///// kids
  for(size_t i = 0; i < kids.size(); i++) {
    features.push_back("SRC_KID_LEX_" + m_sentence.source.GetEdge(kids[i]).GetWord());
    features.push_back("SRC_KID_POS_" + m_sentence.source.GetEdge(kids[i]).GetPos());
  }
  ///// siblings
  for(size_t i = 0; i < sibs.size(); i++) {
    features.push_back("SRC_SIB_LEX_" + m_sentence.source.GetEdge(sibs[i]).GetWord());
    features.push_back("SRC_SIB_POS_" + m_sentence.source.GetEdge(sibs[i]).GetPos());
  }


  if (features.size() == 0)
    return "";

  string ret = features[0];
  for(size_t i = 1; i < features.size(); i++) {
    ret += " " + features[i];
  }
  return ret;
}

string ExtractTask::extractContextFeatures(int startS, int endS, int startT, int endT, const SERGHoleCollection &holeColl)
{
  vector<string> features;
  int size = m_sentence.source.size();

  // source sequence context feature
  string prew="SOS", nextw="EOS", prep="SOS", nextp="EOS";
  if (startS > 0) {
    prew = m_sentence.source.GetEdge(startS-1).GetWord();
    prep = m_sentence.source.GetEdge(startS-1).GetPos();
  }
  if (endS < size-1) {
    nextw = m_sentence.source.GetEdge(endS+1).GetWord();
    nextp = m_sentence.source.GetEdge(endS+1).GetPos();
  }
  features.push_back("SRC_PRE_LEX_"+prew);
  features.push_back("SRC_PRE_POS_"+prep);
  features.push_back("SRC_NXT_LEX_"+nextw);
  features.push_back("SRC_NXT_POS_"+nextp);

  features.push_back("SRC_FST_LEX_"+m_sentence.source.GetEdge(startS).GetWord());
  features.push_back("SRC_FST_POS_"+m_sentence.source.GetEdge(startS).GetPos());
  features.push_back("SRC_LST_LEX_"+m_sentence.source.GetEdge(endS).GetWord());
  features.push_back("SRC_LST_POS_"+m_sentence.source.GetEdge(endS).GetPos());

  // source and target hole feature
  const HoleList& holes = holeColl.GetHoles();
  HoleList::const_iterator iter = holes.begin();
  if (iter != holes.end()) {
    int count = 1;
    for (; iter != holes.end(); ++iter) {
      int hstart = iter->GetStart(0), hend = iter->GetEnd(0);
      // source
      string prefix = "SRC_X"+IntToString(count)+"_";
      features.push_back(prefix+"FST_LEX_" + m_sentence.source.GetEdge(hstart).GetWord());
      features.push_back(prefix+"FST_POS_" + m_sentence.source.GetEdge(hstart).GetPos());
      features.push_back(prefix+"LST_LEX_" + m_sentence.source.GetEdge(hend).GetWord());
      features.push_back(prefix+"LST_POS_" + m_sentence.source.GetEdge(hend).GetPos());
      int len = hend-hstart+1;
      features.push_back(prefix+"LEN_" + IntToString(len));
      // target
      prefix = "TGT_X"+IntToString(count)+"_";
      hstart = iter->GetStart(1), hend = iter->GetEnd(1);
      features.push_back(prefix+"FST_LEX_" + m_sentence.target[hstart]);
      features.push_back(prefix+"LST_LEX_" + m_sentence.target[hend]);
      len = hend-hstart+1;
      features.push_back(prefix+"LEN_" + IntToString(len));
      count++;
    }
  }

  // source dep feature
  int fid = m_sentence.source.GetSpanFid(startS, endS);
  vector<int> kids = m_sentence.source.GetSpanKidsIndex(startS, endS);
  vector<int> sibs = m_sentence.source.GetSpanSiblingIndex(startS, endS);
  ///// father node
  if (fid < 0 || fid >= size) {
    features.push_back("SRC_PAR_LEX_ROOT");
    features.push_back("SRC_PAR_POS_ROOT");
  } else {
    features.push_back("SRC_PAR_LEX_"+m_sentence.source.GetEdge(fid).GetWord());
    features.push_back("SRC_PAR_POS_"+m_sentence.source.GetEdge(fid).GetPos());
  }
  ///// kids
  for(size_t i = 0; i < kids.size(); i++) {
    features.push_back("SRC_KID_LEX_" + m_sentence.source.GetEdge(kids[i]).GetWord());
    features.push_back("SRC_KID_POS_" + m_sentence.source.GetEdge(kids[i]).GetPos());
  }
  ///// siblings
  for(size_t i = 0; i < sibs.size(); i++) {
    features.push_back("SRC_SIB_LEX_" + m_sentence.source.GetEdge(sibs[i]).GetWord());
    features.push_back("SRC_SIB_POS_" + m_sentence.source.GetEdge(sibs[i]).GetPos());
  }


  if (features.size() == 0)
    return "";

  string ret = features[0];
  for(size_t i = 1; i < features.size(); i++) {
    ret += " " + features[i];
  }
  return ret;
}

void ExtractTask::addRuleToCollection( ExtractedSERGRule &newRule )
{

  // no double-counting of identical rules from overlapping spans
  if (!m_options.duplicateRules) {
    vector<ExtractedSERGRule>::const_iterator rule;
    for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      if (rule->source.compare( newRule.source ) == 0 &&
          rule->target.compare( newRule.target ) == 0 &&
          !(rule->endT < newRule.startT || rule->startT > newRule.endT)) { // overlapping
        return;
      }
    }
  }
  m_extractedRules.push_back( newRule );
}

void ExtractTask::consolidateRules()
{
  typedef vector<ExtractedSERGRule>::iterator R;
  map<int, map<int, map<int, map<int,int> > > > spanCount;

  // compute number of rules per span
  if (m_options.fractionalCounting) {
    for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      spanCount[ rule->startT ][ rule->endT ][ rule->startS ][ rule->endS ]++;
    }
  }

  // compute fractional counts
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    rule->count =    1.0/(float) (m_options.fractionalCounting ? spanCount[ rule->startT ][ rule->endT ][ rule->startS ][ rule->endS ] : 1.0 );
  }

  // consolidate counts
  map<std::string, map< std::string, map< std::string, float> > > consolidatedCount;
  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] += rule->count;
  }

  for(R rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    float count = consolidatedCount[ rule->source ][ rule->target][ rule->alignment ];
    rule->count = count;
    consolidatedCount[ rule->source ][ rule->target][ rule->alignment ] = 0;
  }
}

void ExtractTask::writeRulesToFile()
{
  vector<ExtractedSERGRule>::const_iterator rule;
  ostringstream out;
  ostringstream outInv;
  for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    if (rule->count == 0)
      continue;

    out << rule->source << " ||| "
        << rule->target << " ||| "
        << rule->alignment << " ||| "
        << rule->count << " ||| ";
    if (m_options.outputNTLengths) {
      rule->OutputNTLengths(out);
    }
    if (m_options.pcfgScore) {
      out << " ||| " << rule->pcfgScore;
    }
    if (m_options.sourceSegFeatures || m_options.contextFeatures || m_options.sourcePatternFeatures) {
      out << " ||| " << rule->features;
    }
    out << "\n";

    if (!m_options.onlyDirectFlag) {
      outInv << rule->target << " ||| "
             << rule->source << " ||| "
             << rule->alignmentInv << " ||| "
             << rule->count << "\n";
    }
  }
  m_extractFile << out.str();
  m_extractFileInv << outInv.str();
}

void writeGlueGrammar( const string & fileName, SERGExtractionOptions &options, set< string > &targetLabelCollection, map< string, int > &targetTopLabelCollection )
{
  ofstream grammarFile;
  grammarFile.open(fileName.c_str());
  if (!options.targetSyntax) {
    grammarFile << "<s> [X] ||| <s> [S] ||| 1 ||| ||| 0" << endl
                << "[X][S] </s> [X] ||| [X][S] </s> [S] ||| 1 ||| 0-0 ||| 0" << endl
                << "[X][S] [X][X] [X] ||| [X][S] [X][X] [S] ||| 2.718 ||| 0-0 1-1 ||| 0" << endl;
  } else {
    // chose a top label that is not already a label
    string topLabel = "QQQQQQ";
    for( unsigned int i=1; i<=topLabel.length(); i++) {
      if(targetLabelCollection.find( topLabel.substr(0,i) ) == targetLabelCollection.end() ) {
        topLabel = topLabel.substr(0,i);
        break;
      }
    }
    // basic rules
    grammarFile << "<s> [X] ||| <s> [" << topLabel << "] ||| 1  ||| " << endl
                << "[X][" << topLabel << "] </s> [X] ||| [X][" << topLabel << "] </s> [" << topLabel << "] ||| 1 ||| 0-0 " << endl;

    // top rules
    for( map<string,int>::const_iterator i =  targetTopLabelCollection.begin();
         i !=  targetTopLabelCollection.end(); i++ ) {
      grammarFile << "<s> [X][" << i->first << "] </s> [X] ||| <s> [X][" << i->first << "] </s> [" << topLabel << "] ||| 1 ||| 1-1" << endl;
    }

    // glue rules
    for( set<string>::const_iterator i =  targetLabelCollection.begin();
         i !=  targetLabelCollection.end(); i++ ) {
      grammarFile << "[X][" << topLabel << "] [X][" << *i << "] [X] ||| [X][" << topLabel << "] [X][" << *i << "] [" << topLabel << "] ||| 2.718 ||| 0-0 1-1" << endl;
    }
    grammarFile << "[X][" << topLabel << "] [X][X] [X] ||| [X][" << topLabel << "] [X][X] [" << topLabel << "] ||| 2.718 |||  0-0 1-1 " << endl; // glue rule for unknown word...
  }
  grammarFile.close();
}

// collect counts for labels for each word
// ( labels of singleton words are used to estimate
//   distribution oflabels for unknown words )

map<string,int> wordCount;
map<string,string> wordLabel;
void collectWordLabelCounts( SERGSentenceAlignment &sentence )
{
  /*int countT = sentence.target.size();
  for(int ti=0; ti < countT; ti++) {
    string &word = sentence.target[ ti ];
    const vector< SyntaxNode* >& labels = sentence.targetTree.GetNodes(ti,ti);
    if (labels.size() > 0) {
      wordCount[ word ]++;
      wordLabel[ word ] = labels[0]->GetLabel();
    }
  }*/
}

void writeUnknownWordLabel(const string & fileName)
{
  ofstream outFile;
  outFile.open(fileName.c_str());
  typedef map<string,int>::const_iterator I;

  map<string,int> count;
  int total = 0;
  for(I word = wordCount.begin(); word != wordCount.end(); word++) {
    // only consider singletons
    if (word->second == 1) {
      count[ wordLabel[ word->first ] ]++;
      total++;
    }
  }

  for(I pos = count.begin(); pos != count.end(); pos++) {
    double ratio = ((double) pos->second / (double) total);
    if (ratio > 0.03)
      outFile << pos->first << " " << ratio << endl;
  }

  outFile.close();
}
