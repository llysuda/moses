/*
 * Edge.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "Edge.h"
//#include <string>
//#include <vector>
#include "Edge.h"

using namespace std;

namespace Moses {
namespace SERG {


Edge::Edge(int id, string word, string pos, int fid)
: m_id (id)
, m_word (word)
, m_pos (pos)
, m_fatherId (fid)
, m_consistent (true)
, m_isLeaf(true)
, m_headSpan(9999,9999)
, m_startNodeID(id)
, m_endNodeID(fid) {

}

Edge::Edge(const Edge & edge) {
  m_id = edge.GetId();
  m_word = edge.GetWord();
  m_pos = edge.GetPos();
  m_fatherId = edge.GetFatherId();
  m_consistent = edge.IsConsistent();
  m_isLeaf = edge.IsLeaf();
  m_headSpan = edge.GetHeadSpan();
  m_relation = edge.GetRelation();
  m_startNodeID = edge.GetStartNodeID();
  m_endNodeID = edge.GetEndNodeID();
}

Edge::~Edge() {
  // TODO Auto-generated destructor stub
}

void Edge::SetHeadSpan(const Span & span) {
  //if (span.first < 0) return;
  m_headSpan = span;
}

} /* namespace SERG */
} /* namespace Moses */
