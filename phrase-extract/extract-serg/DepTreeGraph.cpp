/*
 * DepTreeGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "DepTreeGraph.h"
#include "tables-core.h"
#include "moses/Util.h"
#include <set>


using namespace std;
using namespace Moses;
//using namespace MosesTraining;

//vector<string> tokenize( const char* input );
//vector<string> tokenize( const char* input, const char separator );

vector<string> tokenize( const char* input, const char separator )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);
    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

namespace Moses {
namespace SERG {

void DepTreeGraph::Init(const string & line, const SERGExtractionOptions& options) {
  vector<string> tokens = tokenize(line.c_str());
	for(size_t i = 0; i < tokens.size(); i++) {
	  vector<string> edgeAttrib = tokenize(tokens[i].c_str(),'|');
	  if (edgeAttrib.size() < 3) {
	  	cerr << "error source format !" << endl;
	  	exit(1);
	  }
	  int fid = atoi(edgeAttrib[2].c_str());
	  if (fid < 0) {
	  	fid = 9999;
	  	m_rootId.push_back(i);
	  }
	  Edge edge(i, edgeAttrib[0], edgeAttrib[1], fid);
	  if (edgeAttrib.size() > 3) {
	  	edge.SetRelation(edgeAttrib[3]);
	  }
	  m_edges.push_back(edge);
	}

	// set leaf flag, startNodeID and endNodeID
	for (size_t i = 0; i < m_edges.size(); i++) {
		size_t j = m_edges[i].GetFatherId();
		if (j < m_edges.size())
		  m_edges[j].SetLeaf(false);
	}

  CheckSequence();

  InitLabel(options);
}

DepTreeGraph::DepTreeGraph()
  : m_rootId (0)
  , m_edges() {
}

DepTreeGraph::DepTreeGraph(string line, const SERGExtractionOptions& options) {

  Init(line, options);
}
void DepTreeGraph::CheckSequence() const {
	int id = 0;
  for (size_t i = 0; i < m_edges.size(); i++){
    assert(id == m_edges[i].GetId());
    id++;
  }
}

void DepTreeGraph::PrintAnnotation() const
{
  cerr << "headspan  ";
  for (size_t i = 0; i < m_edges.size(); i++) {
    Span span = m_edges[i].GetHeadSpan();
    cerr << "[" << span.first << "," << span.second << "] ";
  }
  cerr << endl;
  cerr << "leaf  ";
  for (size_t i = 0; i < m_edges.size(); i++) {
    cerr << m_edges[i].IsLeaf() << " ";
  }
  cerr << endl;
  cerr << "consistent  ";
  for (size_t i = 0; i < m_edges.size(); i++) {
    cerr << m_edges[i].IsConsistent() << " ";
  }
  cerr << endl;
}

void DepTreeGraph::Annotate (const vector<int> & alignedCountS, const vector<vector<int> > & alignedToT) {

	for (size_t idx=0; idx < m_edges.size(); idx++) {
		int min = alignedCountS.size() +1, max = -1;
		vector<int> usedF = alignedCountS;
		for (size_t i = 0; i < alignedToT[idx].size(); i++) {
			int position = alignedToT[idx][i];
			max = std::max(max, position);
			min = std::min(min, position);
			usedF[position]--;
		}

		for (size_t i = min; i <= max; i++) {
			if (usedF[i] > 0) {
				m_edges[idx].SetConsistent(false);
				break;
			}
		}

		if (max >= min) {
			Span hspan(min,max);
			m_edges[idx].SetHeadSpan(hspan);
		}
	}

}

vector<int> DepTreeGraph::GetPostOrder(int headId) const {
  vector<int> ret;
  for (size_t i = 0; i < m_edges.size(); i++) {
    if (m_edges[i].GetFatherId() == headId) {
      vector<int> kidsOrder = GetPostOrder(m_edges[i].GetId());
      for (size_t j = 0; j < kidsOrder.size(); i++) {
        ret.push_back(kidsOrder[j]);
      }
    }
  }
  ret.push_back(headId);
  return ret;
}

/*Span DepTreeGraph::GetSourceSpan(size_t index) const {
  Span retSpan(index,index);
  vector<size_t> indexes = GetSubtreeIndexes(index);
  if (indexes.size() > 1) {
    for (size_t i = 0; i < indexes.size(); i++) {
      if (indexes[i] != index) {
        Span span = GetSourceSpan(indexes[i]);
        retSpan.first = std::min(retSpan.first, span.first);
        retSpan.second = std::max(retSpan.second, span.second);
      }
    }
  }
  return retSpan;
}

vector<const Edge &> DepTreeGraph::GetSubtreeNodes(size_t index) const{
  vector<const Edge &> ret;
  vector<size_t> indexes = GetSubtreeIndexes(index);
  for (size_t i = 0; i < indexes.size(); i++) {
    ret.push_back(m_edges[indexes[i]]);
  }
  return ret;
}

vector<size_t> DepTreeGraph::GetSubtreeIndexes(size_t index) const {
  vector<size_t> ret;
  for (size_t i = 0; i < m_edges.size(); i++) {
    if (m_edges[i].GetFatherId() == m_edges[index].GetId())
      ret.push_back(i);
    else if (i == index)
      ret.push_back(index);
  }
  return ret;
}

vector<size_t> DepTreeGraph::GetLeftChildrenIndexes(size_t index) const {
  vector<size_t> ret;
  for (size_t i = 0; i < index; i++) {
    if (m_edges[i].GetFatherId() == m_edges[index].GetId())
      ret.push_back(i);
  }
  return ret;
}

vector<size_t> DepTreeGraph::GetRightChildrenIndexes(size_t index) const {
  vector<size_t> ret;
  for (size_t i = index+1; i < m_edges.size(); i++) {
    if (m_edges[i].GetFatherId() == m_edges[index].GetId())
      ret.push_back(i);
  }
  return ret;
}
*/
DepTreeGraph::~DepTreeGraph() {
  // TODO Auto-generated destructor stub
}

string DepTreeGraph::GetFactorString() const
{
  string ret = "";
  for (size_t i = 0; i < m_edges.size(); i++) {
    ret += m_edges[i].GetWord() + "|" + m_edges[i].GetPos() + "|" + SPrint<int>(m_edges[i].GetFatherId()) + "|" + m_edges[i].GetRelation();
    ret += " ";
  }
  return ret.erase(ret.size()-1);
}

void DepTreeGraph::SetFatherId(int nid, int fid)
{
  m_edges[nid].SetFatherId(fid);
}

bool DepTreeGraph::IsConnected(int start, int end) const {
	map<int, int> nodes;

	for(int i = start; i <= end; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();

		assert(sid == i && eid == m_edges[i].GetFatherId());

		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	return true;
}

bool DepTreeGraph::valid(int start, int end, int startHole, int endHole) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<int, int> nodes;
	map<int, int> external_nodes;

	for(int i = startHole; i <= endHole; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();

		if (eid > endHole || eid < startHole) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + endHole-startHole+1)
		return false;
	//

	for(int i = start; i < startHole; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(int i = endHole+1; i <= end; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}

std::map<int,int> DepTreeGraph::GetExternalNodes(int start, int end, int startHole, int endHole) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<int, int> nodes;
	map<int, int> external_nodes;

	for(int i = startHole; i <= endHole; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();

		if (eid > endHole || eid < startHole) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	//if (nodes.size() > 1 + endHole-startHole+1)
	//	return false;
	//

	for(int i = start; i < startHole; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(int i = endHole+1; i <= end; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}

bool DepTreeGraph::valid(int start, int end) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<int, int> nodes;
	map<int, int> external_nodes;

	for(int i = start; i <= end; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();

		if (eid >= m_edges.size() || eid < 0) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	//

	for(int i = 0; i < start; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(int i = end+1; i < m_edges.size(); i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}

map<int, int> DepTreeGraph::GetExternalNodes(int start, int end) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<int, int> nodes;
	map<int, int> external_nodes;

	for(int i = start; i <= end; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();

		if (eid >= m_edges.size() || eid < 0) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}

	for(int i = 0; i < start; i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(int i = end+1; i < m_edges.size(); i++) {
		int sid = m_edges[i].GetStartNodeID();
		int eid = m_edges[i].GetEndNodeID();
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}

void DepTreeGraph::InitContextAwareLabel(const SERGExtractionOptions& options, int startS, int endS)
{
	m_labels.clear();
	for(int start = 0; start <=endS; start++) {
		m_labels.push_back(vector<vector<string> >());
		for(int end=start; end <=endS; end++) {
			if (start >= startS && IsConnected(start, end)) {
				vector<string> labels = GetLabels(startS, endS, start, end);
				m_labels[start].push_back(labels);
			} else {
				m_labels[start].push_back(vector<string>());
			}
		}
	}
	//m_labels[startS][endS-startS].push_back("X");
}

void DepTreeGraph::InitLabelWithExternalEdge(const SERGExtractionOptions& options) {
	m_labels.clear();
	for(size_t start = 0; start < m_edges.size(); start++) {
		m_labels.push_back(vector<vector<string> >());
		for(size_t end=start; end < m_edges.size(); end++) {
			m_labels[start].push_back(vector<string>());
			if (!valid(start, end)) continue;
			map<int,int> externalNodes = GetExternalNodes(start,end);
			if (externalNodes.size() == 1) {
				vector<string> labels;
				for(size_t i = start; i <= end; i++) {
					const Edge& edge = m_edges[i];
					size_t fid = edge.GetFatherId();
					if(fid < start || fid > end) {
						labels.push_back(edge.GetPos());
					}
				}
				string ret = labels[0];
				//map<string, int> used;
				for(size_t i = 1; i < labels.size(); i++) {
					//if (used.find(labels[i]) != used.end())
					//	continue;
					ret += "_" + labels[i];
					//used[labels[i]] = 1;
				}

				m_labels[start][end-start].push_back(ret);
			} else {

				int fid = GetSpanFid(start, end);

				if (fid < 0) {
					cerr << "span fid error " << start << " " << end << endl;
					assert(false);
				}

				map<int,int>::iterator iter = externalNodes.begin();
				int s = iter->first;
				iter++;
				int e = iter->first;

				if (fid != s && fid != e) {
					cerr << "external nodes error " << start << " " << end << endl;
					assert(false);
				}

				if (fid == s) {
					s = e;
					e = fid;
				}

				vector<string> labels;
				int step = s;
				while (m_edges[step].GetEndNodeID() != e) {
						labels.push_back(m_edges[step].GetPos());
						step = m_edges[step].GetFatherId();
				}
				labels.push_back(m_edges[step].GetPos());

				string ret = labels[0];
				if (labels.size() > 1)
					ret += "_" + labels[labels.size()-1];

				m_labels[start][end-start].push_back(ret);
			}

		}
	}

}

void DepTreeGraph::InitLabel(const SERGExtractionOptions& options) {
	if (options.labelWithExternalEdge) {
		InitLabelWithExternalEdge(options);
		return;
	}
	m_labels.clear();
	for(size_t start = 0; start < m_edges.size(); start++) {
		m_labels.push_back(vector<vector<string> >());
		for(size_t end=start; end < m_edges.size(); end++) {
			m_labels[start].push_back(vector<string>());
			vector<string> labels, wlabels;
				for(size_t i = start; i <= end; i++) {
					const Edge& edge = m_edges[i];
					size_t fid = edge.GetFatherId();
					if(fid < start || fid > end) {
						labels.push_back(edge.GetPos());
						wlabels.push_back(edge.GetWord());
					}
				}
				string ret = labels[0];
				string wret = wlabels[0];
				if (options.ntBoundary) {
					if (labels.size() > 1) {
						ret += "_" + labels[labels.size()-1];
						wret += "_" + wlabels[wlabels.size()-1];
					}
				} else if (options.uniqueLabelSet) {
					set<string> labelSet(labels.begin(), labels.end());
					set<string> wlabelSet(wlabels.begin(), wlabels.end());
					set<string>::iterator iter = labelSet.begin();
					set<string>::iterator witer = wlabelSet.begin();
					ret = *iter;
					wret = *witer;
					iter++;
					witer++;
					for(;iter != labelSet.end(); iter++)
						ret += "_" + *iter;
					for(;witer != wlabelSet.end(); witer++)
						wret += "_" + *witer;
				}
				else {
					for(size_t i = 1; i < labels.size(); i++) {
						ret += "_" + labels[i];
					}
					for(size_t i = 1; i < wlabels.size(); i++) {
						wret += "_" + wlabels[i];
					}
				}

				if (options.singleNonTerm) {
					ret = "SNT";
				}

				if (options.partialChildLabel && hasPartialChild(start,end))
					ret += "_HPC";

				if (options.flabel) {
				  if (labels.size() == 1) {
				    ret = labels[0];
				  } else {
				    int fid = GetSpanFid(start,end);
				    if (fid < 0 || fid >= m_edges.size())
				      ret = "ROOOT";
				    else
				      ret = m_edges[fid].GetPos()+"_SS";
				  }
				}
				m_labels[start][end-start].push_back(ret);
				if (options.includeWordLabel)
					m_labels[start][end-start].push_back(wret);
		}
	}

}

bool DepTreeGraph::hasPartialChild(int start, int end) const
{
	bool hasChild = false;
	set<int> heads;
	for(int i = start; i <= end; i++) {
		int fid = m_edges[i].GetFatherId();
		if(fid < start || fid > end) {
			heads.insert(i);
		}
	}
	assert(heads.size() > 0);
	for(int i = start; i <= end; i++) {
		int fid = m_edges[i].GetFatherId();
		if (heads.find(fid) != heads.end())
			hasChild = true;
	}

	if (!hasChild)
		return false;

	for (int i = 0; i < start; i++) {
		int fid = m_edges[i].GetFatherId();
		if (heads.find(fid) != heads.end())
			return true;
	}
	for (int i = end+1; i < m_edges.size(); i++) {
		int fid = m_edges[i].GetFatherId();
		if (heads.find(fid) != heads.end())
			return true;
	}
	return false;
}

std::vector<std::string> DepTreeGraph::GetLabels(int start, int end, int startHole, int endHole) const
{
		vector<string> ret;
		map<int,int> externalNodes = GetExternalNodes(start,end, startHole, endHole);
		if (externalNodes.size() == 1) {
			vector<string> labels;
			for(int i = startHole; i <= endHole; i++) {
				const Edge& edge = m_edges[i];
				int fid = edge.GetFatherId();
				if(fid < startHole || fid > endHole) {
					labels.push_back(edge.GetPos());
				}
			}
			assert(labels.size() > 0);
			//string label = labels[0];
			map<string,int> used;
			for(size_t i = 0; i < labels.size(); i++) {
				if (used.find(labels[i]) == used.end())
				//label += "_" + labels[i];
					ret.push_back(labels[i]);
				used[labels[i]] = 1;
			}
		} else if (externalNodes.size() == 2) {

			int fid = GetSpanFid(startHole, endHole);

			if (fid < 0) {
				cerr << "span fid error " << start << " " << end << endl;
				assert(false);
			}

			map<int,int>::iterator iter = externalNodes.begin();
			int s = iter->first;
			iter++;
			int e = iter->first;

			if (fid != s && fid != e) {
				cerr << "external nodes error " << start << " " << end << endl;
				assert(false);
			}

			if (fid == s) {
				s = e;
				e = fid;
			}

			vector<string> labels;
			int step = s;
			while (m_edges[step].GetEndNodeID() != e) {
					labels.push_back(m_edges[step].GetPos());
					step = m_edges[step].GetFatherId();
			}
			labels.push_back(m_edges[step].GetPos());

			string label = labels[0];
			if (labels.size() > 1)
				label += "_" + labels[labels.size()-1];

			ret.push_back(label);
		}
		return ret;
}

int DepTreeGraph::GetSpanFid(int start, int end) const {
	set<int> fids;
	for(int i = start; i <= end; i++) {
		int fid = (int)m_edges[i].GetFatherId();
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size() > 1)
	  return -1;
	assert(fids.size() == 1);
	return *(fids.begin());
}

/*int DepTreeGraph::GetDepth(int start, int end, int pos) const
{
	int depth = 1;
	int fid = m_edges[pos].GetFatherId();
	while (fid >= start && fid <= end) {
		depth++;
		fid = m_edges[fid].GetFatherId();
	}
	return depth;
}

int DepTreeGraph::GetDepth(int start, int end, int startHole, int endHole) const
{
	int minDepth = 999;
	for (int pos = startHole; pos <= endHole; pos++) {
		int depth = GetDepth(start, end, pos);
		if (depth < minDepth)
			minDepth = depth;
	}
	return minDepth;
}

bool DepTreeGraph::isInDependent(int start, int end, int startHole, int endHole) const {
	bool hasHead = false;
	bool hasChild = false;
	for(int i = startHole; i <= endHole; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid < start || fid > end)
			hasHead = true;
		if (fid >= start && fid <= end)
			hasChild = true;
	}
	if (hasHead && hasChild)
		return false;
	if (hasHead && !hasChild)
		return true;
	//
	for (int i = start; i< startHole; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid >= startHole && fid <= endHole)
				return false;
	}
	for (int i = endHole+1; i <= end; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid >= startHole && fid <= endHole)
				return false;
	}
	return true;
}*/

bool DepTreeGraph::isComplete(int start, int end) const {
	for (int i = 0; i< start; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid >= start && fid <= end)
			return false;
	}
	for (int i = end+1; i < m_edges.size(); i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid >= start && fid <= end)
			return false;
	}
	return true;
}

bool DepTreeGraph::isHeadHole(int start, int end, int startHole, int endHole) const
{
	if (endHole-startHole+1 > 1)
		return false;
	for (int i = startHole; i <= endHole; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid >= start && fid <= end) {
			return false;
		}
	}
	return true;
}

/*bool DepTreeGraph::isDependentHole(int start, int end, int startHole, int endHole) const
{
	if (!isComplete(startHole, endHole))
		return false;
	int fid = GetSpanFid(startHole, endHole);
	if (fid < start || fid > end)
		return true;
	int ffid = m_edges[fid].GetFatherId();
	if (ffid < start || ffid > end)
		return true;
	return false;
}*/

std::pair<int,int> DepTreeGraph::GetTreeSpan(int headid) const
{
	pair<int, int> span(headid, headid);
	for(size_t i = 0; i < m_edges.size(); i++) {
		int fid = m_edges[i].GetFatherId();
	  if (fid == headid) {
	  	pair<int, int> subspan = GetTreeSpan((int)i);
			span.first = std::min(subspan.first, span.first);
			span.second = std::max(subspan.second, span.second);
	  }
	}
	return span;
}

std::vector<std::pair<std::pair<int,int>, char> > DepTreeGraph::GetHDSegment(int start, int end) const
{
	vector<pair<pair<int,int>, char> > ret;
	vector<int> fids;
	for (int i = start; i <= end; i++) {
		int fid = m_edges[i].GetFatherId();
		if (fid < start || fid > end) {
			ret.push_back(make_pair(make_pair(i,i), 'H'));
			continue;
		}
		int ffid = m_edges[fid].GetFatherId();
		if (ffid < start || ffid > end) {
			pair<int,int> span = GetTreeSpan(i);

			assert(span.first >= start && span.first <= end);
			assert(span.second >= start && span.second <= end);

			if (span.first == span.second)
				ret.push_back(make_pair(span, 'L'));
			else
				ret.push_back(make_pair(span, 'I'));
			continue;
		}
	}

	return ret;
}

std::vector<int> DepTreeGraph::GetSpanKidsIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_edges.size();
  for (int i = 0; i < start; i++) {
    int fid = m_edges[i].GetFatherId();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_edges[i].GetFatherId();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  return ret;
}

std::vector<int> DepTreeGraph::GetSpanSiblingIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_edges.size();
  int rfid = GetSpanFid(start,end);
  for (int i = 0; i < start; i++) {
    int fid = m_edges[i].GetFatherId();
    if (fid == rfid)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_edges[i].GetFatherId();
    if (fid == rfid)
      ret.push_back(i);
  }
  return ret;
}

} /* namespace SERG */
} /* namespace Moses */
