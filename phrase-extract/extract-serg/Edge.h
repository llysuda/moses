#pragma once

//#ifndef SERG_V2_NODE_H_
//#define SERG_V2_NODE_H_

#include <string>
#include <vector>
#include <map>

namespace Moses {

namespace SERG {

typedef std::pair<size_t,size_t> Span;

class Edge {

public:
  Edge(int id, std::string word, std::string pos, int fid);
  Edge(const Edge & edge);

  virtual ~Edge();
  int GetId() const { return m_id;}
  int GetFatherId() const { return m_fatherId;}
  std::string GetWord() const { return m_word;}
  std::string GetPos() const { return m_pos;}
  std::string GetRelation() const { return m_relation;}
  Span GetHeadSpan() const { return m_headSpan;}
  bool IsLeaf() const { return m_isLeaf;}
  bool IsConsistent() const { return m_consistent;}
  int GetStartNodeID() const {return m_startNodeID;}
  int GetEndNodeID() const {return m_endNodeID;}


  void SetHeadSpan(const Span & span);
  void SetLeaf(bool flag) { m_isLeaf = flag;}
  void SetWord(std::string word) { m_word = word; }
  void SetPos(std::string pos) { m_pos = pos; }
  void SetConsistent(bool flag) { m_consistent = flag;}
  void SetRelation(std::string relation) {m_relation = relation;}
  void SetFatherId(int fid) {m_fatherId = fid;}
  void SetNodeID(int start, int end) {
	  m_startNodeID = start;
	  m_endNodeID = end;
  }
private:
  std::string m_word;
  std::string m_pos;
  std::string m_relation;
  int m_id;
  int m_fatherId;
  Span m_headSpan;
  bool m_consistent;
  bool m_isLeaf;
  int m_startNodeID;
  int m_endNodeID;
};

} /* namespace HDR */
} /* namespace Moses */
//#endif /* NODE_H_ */
