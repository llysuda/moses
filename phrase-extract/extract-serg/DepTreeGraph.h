/*
 * DepTreeGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef EXTRACT_SERG_DEPTREEGRAPH_H_
#define EXTRACT_SERG_DEPTREEGRAPH_H_

#include <string>
#include <vector>
#include "Edge.h"
#include "SERGExtractionOptions.h"

namespace Moses
{
namespace SERG
{

class DepTreeGraph
{
private:
  std::vector<Edge> m_edges;
  std::vector<int> m_rootId;
  std::vector< std::vector< std::vector<std::string> > > m_labels;

public:
  DepTreeGraph();
  DepTreeGraph(std::string line, const SERGExtractionOptions& options);
  virtual ~DepTreeGraph();
  void InitLabelWithExternalEdge(const SERGExtractionOptions& options);

  void Init(const std::string & line, const SERGExtractionOptions& options);
  size_t size() const {return m_edges.size();}
  std::vector<int> GetPostOrder(int headId) const;
  void Annotate (const std::vector<int> & alignedCountS, const std::vector<std::vector<int> > & alignedToT);
  std::vector<int> GetRootId() const { return m_rootId;}
  const Edge& GetEdge(int index) const { return m_edges[index]; }
  bool IsLeaf(int index) { return m_edges[index].IsLeaf();}
  std::string GetWord(int index) const { return m_edges[index].GetWord();}
  //std::vector<size_t> GetSubtreeIndexes(size_t index) const;
  //std::vector<const Edge &> GetSubtreeNodes(size_t index) const;
  //Span GetSourceSpan(size_t index) const;
  void CheckSequence() const;
  std::string GetFactorString() const;
  //std::vector<size_t> GetLeftChildrenIndexes(size_t index) const;
  //std::vector<size_t> GetRightChildrenIndexes(size_t index) const;
  void SetFatherId(int nid, int fid);
  void PrintAnnotation() const;

  bool valid(int start, int end) const;
  bool hasPartialChild(int start, int end) const;
  void InitLabel(const SERGExtractionOptions& options);
  const std::vector<std::string>& GetLabels(int start, int end) const {
  	return m_labels[start][end-start];
  }
  std::string GetLabel(int start, int end, int index) const {
  	return m_labels[start][end-start][index];
  }

  //const std::vector<std::string>& GetLabels(int start, int end, int startHole, int endHole);
	std::vector<std::string> GetLabels(int start, int end, int startHole, int endHole) const;
	std::map<int,int> GetExternalNodes(int start, int end, int startHole, int endHole) const;
	void InitContextAwareLabel(const SERGExtractionOptions& options, int startS, int endS);

  int GetSpanFid(int start, int end) const;
  //int GetDepth(int start, int end, int pos) const;
  //int GetDepth(int start, int end, int startHole, int endHole) const;
  //bool isInDependent(int start, int end, int startHole, int endHole) const;
  bool isComplete(int start, int end) const;
  bool isHeadHole(int start, int end, int startHole, int endHole) const;
  //bool isDependentHole(int start, int end, int startHole, int endHole) const;
  std::vector<std::pair<std::pair<int,int>, char> > GetHDSegment(int start, int end) const;
  std::pair<int,int> GetTreeSpan(int headid) const;
  bool IsConnected(int start, int end) const;
  bool valid(int start, int end, int startHole, int endHole) const;
  std::map<int, int> GetExternalNodes(int start, int end) const;

  // for extracting features
  std::vector<int> GetSpanKidsIndex(int start, int end) const;
  std::vector<int> GetSpanSiblingIndex(int start, int end) const;

};

} /* namespace HDR */
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
