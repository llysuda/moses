#pragma once
/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

/* Created by Rohit Gupta, CDAC, Mumbai, India on 18 July, 2012*/

#include <string>
#include <vector>

namespace Moses
{
namespace PG2S
{

enum REO_MODEL_TYPE {REO_MSD, REO_MSLR, REO_MONO, REO_MSLRO};
enum REO_POS {LEFT, RIGHT, DLEFT, DRIGHT, UNKNOWN, OVERLAP};

class PG2SExtractionOptions
{
  bool wordModel;
  REO_MODEL_TYPE wordType;
  bool phraseModel;
  REO_MODEL_TYPE phraseType;
  bool hierModel;
  REO_MODEL_TYPE hierType;
  bool allModelsOutputFlag;
  bool orientationFlag;
  bool translationFlag;

public:
  const int maxPhraseLength;
  bool sibling;
  bool onlyTree;
private:
  bool includeSentenceIdFlag; //include sentence id in extract file
  bool onlyOutputSpanInfo;
  bool gzOutput;
  std::string instanceWeightsFile; //weights for each sentence
  int unalignedOrder;
  bool incLabel;
  bool targetDep;
  bool onlySyntaxTarget;
  bool sort;
  bool noStruct;
//  bool noBigram;
  bool sDTU;
  int maxSpan;
  int normUnalign;
  bool useLinkType;
//  bool reverseBigram;
public:
  std::vector<std::string> placeholders;

  PG2SExtractionOptions(const int initmaxPhraseLength):
    maxPhraseLength(initmaxPhraseLength),
    includeSentenceIdFlag(false),
    onlyOutputSpanInfo(false),
    gzOutput(false),
    unalignedOrder(2),
    incLabel{false},
    targetDep(false),
    onlySyntaxTarget(false),
    sort(false),
    noStruct(false),
//    noBigram(false),
    sDTU(false),
    maxSpan(10),
    normUnalign(true),
    useLinkType(false),
//    reverseBigram(false),
    wordModel(false),
    wordType(REO_MSD),
    phraseModel(false),
    phraseType(REO_MSD),
    hierModel(false),
    hierType(REO_MSD),
    allModelsOutputFlag(false),
    orientationFlag(false),
    translationFlag(true),
    sibling(false), onlyTree(false) {}

  //functions for initialization of options
  void initAllModelsOutputFlag(const bool initallModelsOutputFlag) {
    allModelsOutputFlag=initallModelsOutputFlag;
  }
  void initOrientationFlag(const bool initorientationFlag) {
    orientationFlag=initorientationFlag;
  }
  void initTranslationFlag(const bool inittranslationFlag) {
    translationFlag=inittranslationFlag;
  }
  void initIncludeSentenceIdFlag(const bool initincludeSentenceIdFlag) {
    includeSentenceIdFlag=initincludeSentenceIdFlag;
  }
  void initOnlyOutputSpanInfo(const bool initonlyOutputSpanInfo) {
    onlyOutputSpanInfo= initonlyOutputSpanInfo;
  }
  void initGzOutput (const bool initgzOutput) {
    gzOutput= initgzOutput;
  }
  void initInstanceWeightsFile(const char* initInstanceWeightsFile) {
    instanceWeightsFile = std::string(initInstanceWeightsFile);
  }
  void initUnalignedOrder(int order) {
  	unalignedOrder = order;
  }
  void initIncLabel(bool label) {
  	incLabel = label;
  }
  void initTargetDep(bool td) {
		targetDep = td;
	}
  void initOnlySyntaxTarget(bool value) {
  	onlySyntaxTarget = value;
  }
  void initSort(bool value) {
  	sort = value;
  }
  void initNoStruct(bool value) {
  	noStruct = value;
  }
//  void initNoBigram(bool value) {
//  	noBigram = value;
//  }

  void initSDTU(bool value) {
  	sDTU = value;
  }

  void initMaxSpan(bool value) {
  	maxSpan = value;
  }
  void initNormUnalign(bool value) {
  	normUnalign = value;
  }
  void initUseLinkType(bool value) {
  	useLinkType = value;
  }
//  void initReverseBigram(bool value) {
//  	reverseBigram = value;
//  }

  void initWordModel(const bool initwordModel) {
    wordModel=initwordModel;
  }
  void initWordType(REO_MODEL_TYPE initwordType ) {
    wordType=initwordType;
  }
  void initPhraseModel(const bool initphraseModel ) {
    phraseModel=initphraseModel;
  }
  void initPhraseType(REO_MODEL_TYPE initphraseType) {
    phraseType=initphraseType;
  }
  void initHierModel(const bool inithierModel) {
    hierModel=inithierModel;
  }
  void initHierType(REO_MODEL_TYPE inithierType) {
    hierType=inithierType;
  }

  // functions for getting values
  bool isAllModelsOutputFlag() const {
    return allModelsOutputFlag;
  }
  bool isTranslationFlag() const {
    return translationFlag;
  }
  bool isIncludeSentenceIdFlag() const {
    return includeSentenceIdFlag;
  }
  bool isOnlyOutputSpanInfo() const {
    return onlyOutputSpanInfo;
  }
  bool isGzOutput () const {
    return gzOutput;
  }
  std::string getInstanceWeightsFile() const {
    return instanceWeightsFile;
  }
  int getUnalignedOrder() const {
  	return unalignedOrder;
  }

  bool isIncLabel() const {
  	return incLabel;
  }

  bool isTargetDep() const {
  	return targetDep;
  }

  bool isOnlySyntaxTarget() const {
  	return onlySyntaxTarget;
  }

  bool isSort() const {
  	return sort;
  }

  bool isNoStruct() const {
  	return noStruct;
  }
//  bool isNoBigram() const {
//  	return noBigram;
//  }
  bool isSDTU() const {
  	return sDTU;
  }
  bool GetMaxSpan() const {
  	return maxSpan;
  }
  bool IsNormUnalign() const {
  	return normUnalign;
  }
  bool isUseLinkType() const {
  	return useLinkType;
  }

//  bool isReverseBigram() const {
//  	return reverseBigram;
//  }

  bool isWordModel() const {
    return wordModel;
  }
  REO_MODEL_TYPE isWordType() const {
    return wordType;
  }
  bool isPhraseModel() const {
    return phraseModel;
  }
  REO_MODEL_TYPE isPhraseType() const {
    return phraseType;
  }
  bool isHierModel() const {
    return hierModel;
  }
  REO_MODEL_TYPE isHierType() const {
    return hierType;
  }
  bool isOrientationFlag() const {
    return orientationFlag;
  }
};

}
}

