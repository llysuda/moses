/*
 * DepNgramGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef EXTRACT_PG2S_DepNgramGraph_H_
#define EXTRACT_PG2S_DepNgramGraph_H_

#include <string>
#include <vector>
#include <set>
#include <map>

namespace Moses
{
namespace PG2S
{

struct Node
{
	int id;
	std::string word;
	std::string pos;
	int fid;
};

class MultiDiGraph
{
private:
	std::map<int, const struct Node*> nodes;
	std::map<int, std::map<int, std::set<std::string> > > succ;
	std::map<int, std::map<int, std::set<std::string> > > pred;
public:
	MultiDiGraph() {};
	virtual ~MultiDiGraph(){
		std::map<int, const struct Node*>::iterator iter = nodes.begin();
		for (; iter != nodes.end(); iter++) {
			const Node* node = iter->second;
			delete node;
		}
	}

	void add_node(int id, const struct Node* attr);
	void add_edge(int u, int v, const std::string& label);
	bool is_connected(const std::set<int>& nids) const;
	const MultiDiGraph* get_subgraph(const std::set<int>& nids) const;
	std::set<int> get_neighbors(const std::set<int>& nids) const;

	void print_graph() const;

	int size() const {
		return nodes.size();
	}

	void clear() {
		nodes.clear();
		succ.clear();
		pred.clear();
	}

	std::string get_word(int nid) const {
		return nodes.find(nid)->second->word;
	}

	bool has_succ(int nid) const {
		return succ.find(nid) != succ.end();
	}

	const std::map<int, std::set<std::string> > get_succ_edges(int nid) const {
		return succ.find(nid)->second;
	}

private:
	void DepthFirst(int curr_nid,
			std::set<int>& old_nids,
			const std::set<int>& all_nids) const;
};

} /* namespace HDR */
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
