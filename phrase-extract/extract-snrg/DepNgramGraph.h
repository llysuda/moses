/*
 * DepNgramGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef EXTRACT_SNRG_DepNgramGraph_H_
#define EXTRACT_SNRG_DepNgramGraph_H_

#include <string>
#include <vector>
#include <set>

#include "GraphNode.h"
#include "SNRGExtractionOptions.h"

namespace Moses
{
namespace SNRG
{

class DepNgramGraph
{
private:
  std::vector<GraphNode> m_nodes;
  std::vector<int> m_rootId;
  std::vector< std::vector< std::vector<std::string> > > m_labels;

public:
  DepNgramGraph();
  DepNgramGraph(std::string line, const SNRGExtractionOptions& options);
  virtual ~DepNgramGraph();

  void AddNode(const GraphNode& node) {
    m_nodes.push_back(node);
  }

  void Init(const std::string & line, const SNRGExtractionOptions& options);
  void Annotate (const std::vector<int> & alignedCountS, const std::vector<std::vector<int> > & alignedToT);
  size_t size() const {return m_nodes.size();}
  std::vector<int> GetPostOrder(int headId) const;
  std::vector<int> GetRootId() const { return m_rootId;}
  const GraphNode& GetNode(int index) const { return m_nodes[index]; }
  bool IsLeaf(int index) { return m_nodes[index].IsLeaf();}
  std::string GetWord(int index) const { return m_nodes[index].GetWord();}

  void CheckSequence() const;
  std::string GetFactorString() const;
  void SetFatherId(int nid, int fid);
  void PrintAnnotation() const;

  void InitLabel(const SNRGExtractionOptions& options);
  const std::vector<std::string>& GetLabels(int start, int end) const {
  	return m_labels[start][end-start];
  }
  std::string GetLabel(int start, int end, int index) const {
  	return m_labels[start][end-start][index];
  }

//  std::vector<std::string> GetLabels(int start, int end, int startHole, int endHole) const;
  std::pair<int,int> GetTreeSpan(int headid) const;
  int GetSpanFid(int start, int end) const;

  // for extracting features
  std::vector<int> GetSpanKidsIndex(int start, int end) const;
  std::vector<int> GetSpanSiblingIndex(int start, int end) const;

  bool BigramConnected(int im1,int i) const;
  bool NextIsOlder(int im1, int i) const;
  int GetLevel(int index) const;
  std::vector<Edge> GetSpanIncomings(int start, int end) const;
  bool IsConnected(int start, int end) const;
//  bool IsConnected(const std::set<int>& nodes) const;
//  void DepthFirst(const std::set<int>& nodes, int current, std::set<int>& retNodes) const;
  void DepthFirst(int start, int end, int current, std::set<int>& nodes) const;

  bool IsMSTLeaf(int start, int end) const;
  bool HasMST(int start, int end) const;
  bool HasMST(int start, int end, const std::vector<std::pair<int,int> >& holes) const;
  bool HasMST(int start, int end, int startHoleS, int endHoleS) const;
  bool IsDirectedConnected(int rootNode, const std::set<int>& nodes) const;
  void DirectedDepthFirst(const std::set<int>& nodes, int current, std::set<int>& retNodes) const;

};

} /* namespace HDR */
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
