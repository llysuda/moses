#pragma once

//#ifndef SERG_V2_GraphNode_H_
//#define SERG_V2_GraphNode_H_

#include <string>
#include <vector>
#include <map>
#include <algorithm>

namespace Moses {

namespace SNRG {

typedef std::pair<size_t,size_t> Span;

class Edge{
public:
  int incoming;
  std::string relation;

  Edge(int id, const std::string& rel) {
    incoming = id;
    relation = rel;
  }

  Edge(const Edge& copy) {
    incoming = copy.incoming;
    relation = copy.relation;
  }

};

struct EdgeSetSortor{
  bool operator()(const Edge& ea, const Edge& eb) const {
      if( ea.incoming == eb.incoming)
        return ea.relation < eb.relation;
      return ea.incoming < eb.incoming;
  }
};

class GraphNode {

public:
  GraphNode(int id, std::string word, std::string pos, int fid, const std::string& rel)
  : m_id (id)
  , m_word (word)
  , m_pos (pos)
  , m_fid (fid)
  , m_leaf (true)
  , m_rel(rel) {
    if(fid >= 0) {
      Edge e(fid, rel);
      m_incomings.push_back(e);
    }
  }

  GraphNode(const GraphNode & node){
    m_id = node.m_id;
    m_word = node.m_word;
    m_pos = node.m_pos;
    m_fid = node.m_fid;
    m_leaf = node.m_leaf;
    m_incomings = node.m_incomings;
    m_outgoings = node.m_outgoings;
    m_rel = node.m_rel;
    //m_inrels = node.m_inrels;
  }

  virtual ~GraphNode() {}

  int GetId() const { return m_id;}
  int GetFid() const { return m_fid; }
  std::string GetRel() const { return m_rel; }
  std::string GetWord() const { return m_word;}
  std::string GetPos() const { return m_pos;}
  const Edge& GetIncoming(int index) const { return m_incomings[index]; }
  int GetOutgoing(int index) const { return m_outgoings[index]; }
  //std::string GetInrel(int index) const { return m_incomings[index].relation; }
  const std::vector<Edge>& GetIncomings() const { return m_incomings; }
  const std::vector<int>& GetOutgoings() const { return m_outgoings; }
  //const std::vector<std::string>& GetInrels() const { return m_inrels; }
  bool IsLeaf() const { return m_leaf; }

  bool Connected(int index) const {
//    if (std::find(m_incomings.begin(), m_incomings.end(), index) != m_incomings.end())
//      return true;
    if (std::find(m_outgoings.begin(), m_outgoings.end(), index) != m_outgoings.end())
      return true;
    return false;
  }

  void SetId(int id) {m_id = id;}
  void SetFid(int fid) { m_fid = fid; };
  void SetWord(std::string word) { m_word = word; }
  void SetPos(std::string pos) { m_pos = pos; }
  void SetLeaf(bool leaf) { m_leaf = leaf; }
  void AddIncoming(int inID, const std::string& rel) {
    Edge e(inID, rel);
    m_incomings.push_back(e);
  }
  void AddOutgoing(int outID) { m_outgoings.push_back(outID); }
  void UpdateOutgoingEdges(const std::map<int,int>& posMap) {
    std::vector<int> edges = m_outgoings;
    m_outgoings.clear();
    std::map<int,int>::const_iterator iter;
    for (size_t i = 0; i < edges.size(); i++) {
      int id = edges[i];
      iter = posMap.find(id);
      if (iter != posMap.end()) {
        m_outgoings.push_back(iter->second);
      }
    }
  }
  //void AddInrel(const std::string& rel) { m_inrels.push_back(rel); }
//  void Sort() {
//    std::sort(m_incomings.begin(), m_incomings.end(), EdgeIdOrder);
//    std::sort(m_outgoings.begin(), m_outgoings.end());
//  }

private:
  std::string m_word;
  std::string m_pos;
  std::string m_rel;
  bool m_leaf;
  //std::vector<Edge> m_inrels;
  int m_id;
  int m_fid;
  std::vector<Edge> m_incomings;
  std::vector<int> m_outgoings;
};

} /* namespace HDR */
} /* namespace Moses */
//#endif /* GraphNode_H_ */
