/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2010 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef SNRGExist_H_INCLUDED_
#define SNRGExist_H_INCLUDED_

#include <vector>

#include "SNRGHole.h"

namespace Moses
{
namespace SNRG
{

// reposity of extracted phrase pairs
// which are potential holes in larger phrase pairs
class SNRGExist
{
protected:
  std::vector< std::vector<HoleList> > m_phraseExist;
  // indexed by source pos. and source length
  // maps to list of holes where <int, int> are target pos

public:
  SNRGExist(size_t size)
    :m_phraseExist(size) {
    // size is the length of the source sentence
    for (size_t pos = 0; pos < size; ++pos) {
      // create empty hole lists
      std::vector<HoleList> &endVec = m_phraseExist[pos];
      endVec.resize(size - pos);
    }
  }

  void Add(int startT, int endT, int startS, int endS, bool isdep=false) {
    m_phraseExist[startS][endS - startS].push_back(SNRGHole(startS, endS, startT, endT, isdep));
  }

  const HoleList &GetTargetHoles(int startS, int endS) const {
    const HoleList &targetHoles = m_phraseExist[startS][endS - startS];
    return targetHoles;
  }

};

}
}

#endif
