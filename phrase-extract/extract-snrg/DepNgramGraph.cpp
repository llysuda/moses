/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "tables-core.h"
#include "moses/Util.h"
#include <set>
#include "DepNgramGraph.h"
#include <algorithm>


using namespace std;
using namespace Moses;
//using namespace MosesTraining;

//vector<string> tokenize( const char* input );
//vector<string> tokenize( const char* input, const char separator );

vector<string> tokenize( const char* input, const char separator )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);
    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

namespace Moses {
namespace SNRG {

bool DepNgramGraph::IsMSTLeaf(int start, int end) const
{
  // if not incoming edges, return false
  int i = start;
  for (; i <= end; i++) {
    if (m_nodes[i].GetIncomings().size() > 0)
      break;
  }
  if (i > end) return false;

  // in other nodes, at most one node has no incoming edges
  int count = 0;
  set<int> restNodes;
  int rootNode = -1;

  for (i = 0; i < start; i++) {
    restNodes.insert(i);
    if (m_nodes[i].GetIncomings().size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }
  for (i = end+1; i < m_nodes.size(); i++) {
    restNodes.insert(i);
    if (m_nodes[i].GetIncomings().size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }
  // at last other nodes are connected and
  if (count == 1) return IsDirectedConnected(rootNode, restNodes);
  for (set<int>::const_iterator iter = restNodes.begin();
      iter != restNodes.end(); ++iter) {
    int id = *iter;
    if (IsDirectedConnected(id, restNodes))
        return true;
  }
  return false;
}

bool DepNgramGraph::HasMST(int start, int end) const
{
  // at most one node has no incoming edges
  int count = 0;
  set<int> restNodes;
  int rootNode = -1;
  int i = 0;
  for (i = start; i <= end; i++) {
    restNodes.insert(i);
    if (m_nodes[i].GetIncomings().size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }

  // at last other nodes are connected and
  if (count == 1) return IsDirectedConnected(rootNode, restNodes);
  for (set<int>::const_iterator iter = restNodes.begin();
      iter != restNodes.end(); ++iter) {
    int id = *iter;
    if (IsDirectedConnected(id, restNodes))
        return true;
  }
  return false;
}

bool DepNgramGraph::HasMST(int start, int end, int startHoleS, int endHoleS) const
{
  for(int i = start; i < startHoleS; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= startHoleS && fid <= endHoleS)
      return true;
  }
  for(int i = startHoleS; i <= endHoleS; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= start && fid <= end && fid < startHoleS && fid > endHoleS)
      return true;
  }
  for(int i = endHoleS+1; i <= end; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= startHoleS && fid <= endHoleS)
      return true;
  }
  return false;
}

bool DepNgramGraph::HasMST(int start, int end, const std::vector<std::pair<int,int> >& holes) const
{
  // get index map
  map<int, int> posMap;
  int holeCount = 0;
  int wordCount = 0;
  bool isHole = false;
  for (int curPos = start; curPos <= end; curPos++) {
    if (holes.size() > holeCount && holes[holeCount].first == curPos) {
      isHole = true;
    }
    if (isHole) {
      for(int pos = curPos; pos <= holes[holeCount].second; pos++)
        posMap[pos] = wordCount;
      curPos = holes[holeCount].second;
      holeCount++;
      isHole = false;
    } else {
      posMap[curPos] = wordCount;
    }
    wordCount++;
  }
  // create new graph with new index map
  DepNgramGraph dng;
  holeCount = 0;
  wordCount = 0;
  isHole = false;
  for (int curPos = start; curPos <= end; curPos++) {
    if (holes.size() > holeCount && holes[holeCount].first == curPos) {
      isHole = true;
    }
    if (isHole) {
      GraphNode node(wordCount, "", "", -1, "");
      set<int> outgoing;
      map<int,int>::const_iterator iter;
      for(int pos = curPos; pos <= holes[holeCount].second; pos++) {
        const vector<int>& e = m_nodes[pos].GetOutgoings();
        for (size_t i = 0; i < e.size(); i++) {
          iter = posMap.find(e[i]);
          if (iter != posMap.end()) {
            outgoing.insert(iter->second);
          }
        }
      }
      for(set<int>::const_iterator iter2 = outgoing.begin();
          iter2 != outgoing.end(); ++iter2)
        node.AddOutgoing(*iter2);
      dng.AddNode(node);
      //
      curPos = holes[holeCount].second;
      holeCount++;
      isHole = false;
    } else {
      GraphNode node = m_nodes[curPos];
      node.UpdateOutgoingEdges(posMap);
      node.SetId(wordCount);
      dng.AddNode(node);
    }
    wordCount++;
  }

  return dng.HasMST(0, dng.size()-1);
}

bool DepNgramGraph::IsDirectedConnected(int rootNode, const std::set<int>& nodes) const
{
  set<int> retNodes;
  retNodes.insert(rootNode);
  DirectedDepthFirst(nodes, rootNode, retNodes);
  return retNodes.size() == nodes.size();
}

void DepNgramGraph::DirectedDepthFirst(const std::set<int>& nodes, int current, std::set<int>& retNodes) const
{
  vector<int> outgoings = m_nodes[current].GetOutgoings();
//  vector<Edge> incomings = m_nodes[current].GetIncomings();

  set<int> adjacent;
  for(size_t i = 0; i < outgoings.size(); i++) {
    int id = outgoings[i];
    if (nodes.find(id) != nodes.end() && retNodes.find(id) == retNodes.end()) {
      adjacent.insert(id);
    }
  }

  for(set<int>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
    retNodes.insert(*iter);
    DirectedDepthFirst(nodes, *iter, retNodes);
  }
}

void DepNgramGraph::Init(const string & line, const SNRGExtractionOptions& options) {
  vector<string> tokens = tokenize(line.c_str());
  vector< vector<int> > children;
  children.resize(tokens.size());

  for(size_t i = 0; i < tokens.size(); i++) {
    vector<string> nodeAttrib = tokenize(tokens[i].c_str(),'|');
    if (nodeAttrib.size() < 3) {
      cerr << "error source format !" << endl;
      exit(1);
    }
    int fid = atoi(nodeAttrib[2].c_str());
    if (fid < 0) {
      //fid = 9999;
      m_rootId.push_back(i);
    }

    if (fid >= 0)
      children[fid].push_back(i);

    string  rel = nodeAttrib[3];
    if (options.useType)
      rel = "DP";
    GraphNode node(i, nodeAttrib[0], nodeAttrib[1], fid, rel);
    m_nodes.push_back(node);
  }

  // set leaf flag, startNodeID and endNodeID
  for (int i = 0; i < m_nodes.size(); i++) {
      int j = m_nodes[i].GetFid();
      if (j >= 0 && j < m_nodes.size()) {
        m_nodes[j].SetLeaf(false);
        m_nodes[j].AddOutgoing(i);
      }
  }

  // add ngram edges
//  if (!options.sibling) {
//    if (!options.onlyTree) {
//      for(int i = 1; i < m_nodes.size(); i++) {
//        //if (!BigramConnected(i-1,i)) {
//        int uid = i;
//        int nid = i-1;
//
//        m_nodes[uid].AddOutgoing(nid);
//        m_nodes[nid].AddIncoming(uid, "BG");
//      }
//    }
//  } else {
    // add links between siblings
    if (!options.onlyTree) {
      if (options.sibling) {
        for(size_t i = 0; i < children.size(); i++) {
          for (size_t j = 1; j < children[i].size(); j++) {
            int uid = children[i][j];
            int nid = children[i][j-1];
            m_nodes[uid].AddOutgoing(nid);
            m_nodes[nid].AddIncoming(uid, "SG");
          }
        }
      } else {
        for(int i = 1; i < m_nodes.size(); i++) {
          int uid = i;
          int nid = i-1;
          m_nodes[uid].AddOutgoing(nid);
          m_nodes[nid].AddIncoming(uid, "BG");
        }
      }
    }
//  }

  CheckSequence();

  InitLabel(options);
}

bool DepNgramGraph::BigramConnected(int im1,int i) const {
  return m_nodes[im1].Connected(i) || m_nodes[i].Connected(im1);
}

bool DepNgramGraph::NextIsOlder(int im1, int i) const {
  int level_im1 = GetLevel(im1);
  int level_i = GetLevel(i);
  if (level_im1<=level_i)
    return false;
  return true;
}

int DepNgramGraph::GetLevel(int index) const {
  int ret = 0;
  while (m_nodes[index].GetFid() >= 0) {
    index = m_nodes[index].GetFid();
    ret++;
  }
  return ret;
}

void DepNgramGraph::Annotate (const vector<int> & alignedCountS, const vector<vector<int> > & alignedToT) {

//    for (size_t idx=0; idx < m_nodes.size(); idx++) {
//        int min = alignedCountS.size() +1, max = -1;
//        vector<int> usedF = alignedCountS;
//        for (size_t i = 0; i < alignedToT[idx].size(); i++) {
//            int position = alignedToT[idx][i];
//            max = std::max(max, position);
//            min = std::min(min, position);
//            usedF[position]--;
//        }
//
//        for (size_t i = min; i <= max; i++) {
//            if (usedF[i] > 0) {
//                m_nodes[idx].SetConsistent(false);
//                break;
//            }
//        }
//
//        if (max >= min) {
//            Span hspan(min,max);
//            m_edges[idx].SetHeadSpan(hspan);
//        }
//    }

}

DepNgramGraph::DepNgramGraph()
  : m_rootId (0)
  , m_nodes() {
}

DepNgramGraph::DepNgramGraph(string line, const SNRGExtractionOptions& options) {

  Init(line, options);
}
void DepNgramGraph::CheckSequence() const {
	int id = 0;
  for (size_t i = 0; i < m_nodes.size(); i++){
    assert(id == m_nodes[i].GetId());
    id++;
  }
}

void DepNgramGraph::PrintAnnotation() const
{
//  cerr << "headspan  ";
//  for (size_t i = 0; i < m_nodes.size(); i++) {
//    Span span = m_nodes[i].GetHeadSpan();
//    cerr << "[" << span.first << "," << span.second << "] ";
//  }
//  cerr << endl;
//  cerr << "leaf  ";
//  for (size_t i = 0; i < m_nodes.size(); i++) {
//    cerr << m_nodes[i].IsLeaf() << " ";
//  }
//  cerr << endl;
//  cerr << "consistent  ";
//  for (size_t i = 0; i < m_nodes.size(); i++) {
//    cerr << m_nodes[i].IsConsistent() << " ";
//  }
//  cerr << endl;
}

vector<int> DepNgramGraph::GetPostOrder(int headId) const {
  vector<int> ret;
  for (size_t i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFid() == headId) {
      vector<int> kidsOrder = GetPostOrder(m_nodes[i].GetId());
      for (size_t j = 0; j < kidsOrder.size(); i++) {
        ret.push_back(kidsOrder[j]);
      }
    }
  }
  ret.push_back(headId);
  return ret;
}

DepNgramGraph::~DepNgramGraph() {
  // TODO Auto-generated destructor stub
}

string DepNgramGraph::GetFactorString() const
{
  string ret = "";
  for (size_t i = 0; i < m_nodes.size(); i++) {
    ret += m_nodes[i].GetWord() + "|" + m_nodes[i].GetPos() + "|" + SPrint<int>(m_nodes[i].GetFid()) + "|" + m_nodes[i].GetIncoming(0).relation;
    ret += " ";
  }
  return ret.erase(ret.size()-1);
}

void DepNgramGraph::SetFatherId(int nid, int fid)
{
  m_nodes[nid].SetFid(fid);
}


void DepNgramGraph::InitLabel(const SNRGExtractionOptions& options) {

	m_labels.clear();
	for(size_t start = 0; start < m_nodes.size(); start++) {
		m_labels.push_back(vector<vector<string> >());
		for(size_t end=start; end < m_nodes.size(); end++) {
			m_labels[start].push_back(vector<string>());
			vector<string> labels;
			if (options.boundaryNT) {
			  // using boundary pos
			  labels.push_back(m_nodes[start].GetPos());
			  if (end > start)
			    labels.push_back(m_nodes[end].GetPos());
			} else if (options.graphNT) {
			  // using nodes which have incomings outside
			  for(size_t i = start; i <= end; i++) {
                  const GraphNode& node = m_nodes[i];
                  const vector<Edge>& incomings = node.GetIncomings();
                  const vector<int>& outgoings = node.GetOutgoings();

                  set<int> edgeIDs(outgoings.begin(), outgoings.end());
                  for(size_t ii = 0; ii < incomings.size(); ii++)
                    edgeIDs.insert(incomings[ii].incoming);


                  for (set<int>::iterator iter = edgeIDs.begin(); iter != edgeIDs.end(); iter++) {
                    if (*iter < start || *iter > end) {
                      labels.push_back(node.GetPos());
                      break;
                    }
                  }
              }
			  if (labels.size() == 0) {
			    labels.push_back("XX");
			  }
			} else {
			  // use nodes which are heads in dep tree
              for(size_t i = start; i <= end; i++) {
                  const GraphNode& node = m_nodes[i];
                  int fid = node.GetFid();
                  if(fid < (int)start || fid > (int)end) {
                      labels.push_back(node.GetPos());
                  }
              }
			}

            string ret = labels[0];

            for(size_t i = 1; i < labels.size(); i++) {
                ret += "_" + labels[i];
            }

            if (options.singleNT) {
              ret = "SNT";
            }

            m_labels[start][end-start].push_back(ret);
		}
	}

}


//std::vector<std::string> DepNgramGraph::GetLabels(int start, int end, int startHole, int endHole) const
//{
//		vector<string> ret;
//		return ret;
//}

int DepNgramGraph::GetSpanFid(int start, int end) const {
	set<int> fids;
	for(int i = start; i <= end; i++) {
		int fid = (int)m_nodes[i].GetFid();
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size() > 1)
	  return -1;
	assert(fids.size() == 1);
	return *(fids.begin());
}

vector<Edge> DepNgramGraph::GetSpanIncomings(int start, int end) const {
    set<Edge, EdgeSetSortor> incomings;
    for(int i = start; i <= end; i++) {
        const vector<Edge>& ic = m_nodes[i].GetIncomings();
        for(int j = 0; j < ic.size(); j++) {
          if (ic[j].incoming < start || ic[j].incoming > end)
            incomings.insert(ic[j]);
        }
    }
    vector<Edge> ret(incomings.begin(), incomings.end());
//    std::sort(ret.begin(),ret.end(), SORT::EdgeOrder);
    return ret;
}


/*bool DepNgramGraph::isDependentHole(int start, int end, int startHole, int endHole) const
{
	if (!isComplete(startHole, endHole))
		return false;
	int fid = GetSpanFid(startHole, endHole);
	if (fid < start || fid > end)
		return true;
	int ffid = m_nodes[fid].GetFid();
	if (ffid < start || ffid > end)
		return true;
	return false;
}*/

std::pair<int,int> DepNgramGraph::GetTreeSpan(int headid) const
{
	pair<int, int> span(headid, headid);
	for(size_t i = 0; i < m_nodes.size(); i++) {
		int fid = m_nodes[i].GetFid();
	  if (fid == headid) {
	  	pair<int, int> subspan = GetTreeSpan((int)i);
			span.first = std::min(subspan.first, span.first);
			span.second = std::max(subspan.second, span.second);
	  }
	}
	return span;
}


std::vector<int> DepNgramGraph::GetSpanKidsIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_nodes.size();
  for (int i = 0; i < start; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid >= start && fid <= end)
      ret.push_back(i);
  }
  return ret;
}

std::vector<int> DepNgramGraph::GetSpanSiblingIndex(int start, int end) const
{
  vector<int> ret;
  int size = m_nodes.size();
  int rfid = GetSpanFid(start,end);
  for (int i = 0; i < start; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid == rfid)
      ret.push_back(i);
  }
  for (int i = end+1; i < size; i++) {
    int fid = m_nodes[i].GetFid();
    if (fid == rfid)
      ret.push_back(i);
  }
  return ret;
}

void DepNgramGraph::DepthFirst(int start, int end, int current, set<int>& nodes) const {
  vector<int> outgoings = m_nodes[current].GetOutgoings();
  vector<Edge> incomings = m_nodes[current].GetIncomings();

  set<int> adjacent;
  for(size_t i = 0; i < outgoings.size(); i++) {
    int id = outgoings[i];
    if (id >= start && id <= end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }
  for (size_t i = 0; i < incomings.size(); i++) {
    int id = incomings[i].incoming;
    if (id >= start && id <= end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }

  for(set<int>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
    nodes.insert(*iter);
    DepthFirst(start, end, *iter, nodes);
  }
}

bool DepNgramGraph::IsConnected(int start, int end) const {
  set<int> nodes;
  nodes.insert(start);
  DepthFirst(start, end, start, nodes);
  return nodes.size() == end-start+1;
}

//bool DepNgramGraph::IsConnected(const std::set<int>& nodes) const
//{
//  set<int> retNodes;
//  retNodes.insert(*nodes.begin());
//  DepthFirst(nodes, *nodes.begin(), retNodes);
//  return retNodes.size() == nodes.size();
//}
//
//void DepNgramGraph::DepthFirst(const std::set<int>& nodes, int current, std::set<int>& retNodes) const
//{
//  vector<int> outgoings = m_nodes[current].GetOutgoings();
//  vector<Edge> incomings = m_nodes[current].GetIncomings();
//
//  set<int> adjacent;
//  for(size_t i = 0; i < outgoings.size(); i++) {
//    int id = outgoings[i];
//    if (nodes.find(id) != nodes.end() && retNodes.find(id) == retNodes.end()) {
//      adjacent.insert(id);
//    }
//  }
//  for (size_t i = 0; i < incomings.size(); i++) {
//    int id = incomings[i].incoming;
//    if (nodes.find(id) != nodes.end() && retNodes.find(id) == retNodes.end()) {
//      adjacent.insert(id);
//    }
//  }
//
//  for(set<int>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
//    retNodes.insert(*iter);
//    DepthFirst(nodes, *iter, retNodes);
//  }
//}


} /* namespace SNRG */
} /* namespace Moses */
