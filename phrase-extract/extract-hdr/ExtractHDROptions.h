/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2011 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef EXTRACT_HDR_OPTIONS_H_
#define EXTRACT_HDR_OPTIONS_H_

namespace Moses
{
namespace HDR
{

class ExtractHDROptions{

public:
  ExtractHDROptions()
    : gzOutput(false)
    , sentenceOffset(0)
    , unpairedExtractFormat(false)
    , maxPhraseLength(10)
    , includeSentenceID (false)
    , relaxLimit(false)
    , extractPhraseRule(false)
    , duplicateRules (true)
    , factionalCount (false)
    , subFragment (false)
    , subPhrase (false)
    //, useRelation (false)
    , alignConsistent (false)
    , delAlign(false)
    , maxGlueDepth(20)
    , glueGrammar(false)
	, floatRule(false)
    , label (false)
{}

public:
  void initGzOutput(bool flag){ gzOutput = flag;}
  void initLabel(bool flag){ label = flag;}
  void initIncludeSentenceIdFlag(bool flag) { includeSentenceID = flag;}
  void initSentenceOffset(int offset) { sentenceOffset = offset;}
  void initMaxPhraseLength(int maxLength) { maxPhraseLength = maxLength;}
  //void initSubtractFragment(bool flag) { subtractFragment = flag;}
  void initSubFragment(bool flag) { subFragment = flag;}
  void initSubPhrase(bool flag) { subPhrase = flag;}
  void initPhraseRule(bool flag) { extractPhraseRule = flag;}
  //void initUseRelation(bool flag) { useRelation = flag;}
  void initAlignConsistent(bool flag) { alignConsistent = flag;}
  void initDelAlign(bool flag) { delAlign = flag;}
  void initFactionalCount(bool flag) { factionalCount = flag;}
  void initMaxGlueDpeth(int value) { maxGlueDepth = value; }
  void initGlueGrammar(bool flag) { glueGrammar = flag; }
  void initFloatRule(bool flag) { floatRule = flag; }

  int GetSentenceOffset() const { return sentenceOffset;}
  bool isGzOutput() const { return gzOutput;}
  bool RelaxLimit() const { return relaxLimit;}
  bool ExtractPhraseRule() const { return extractPhraseRule;}
  int MaxPhraseLength() const { return maxPhraseLength;}
  bool DuplicateRules() const { return duplicateRules;}
  bool FactionalCount() const { return factionalCount;}
  bool SubFragment() const { return subFragment;}
  //bool SubtractFragment() const { return subtractFragment;}
  bool SubPhrase() const { return subPhrase;}
  //bool UseRelation() const { return useRelation;}
  bool AlignConsistent() const { return alignConsistent;}
  bool DelAlign() const { return delAlign;}
  int MaxGlueDepth() const { return maxGlueDepth;}
  bool GlueGrammar() const { return glueGrammar;}
  bool FloatRule() const { return floatRule;}
  bool Label() const { return label;}

private:
  int sentenceOffset;
  int maxGlueDepth;
  bool unpairedExtractFormat;
  bool gzOutput;
  int maxPhraseLength;
  bool includeSentenceID;
  bool relaxLimit;
  bool extractPhraseRule;
  bool duplicateRules;
  bool factionalCount;
  bool subFragment;
  bool subPhrase;
  //bool subtractFragment;
  //bool useRelation;
  bool alignConsistent;
  bool delAlign;
  bool glueGrammar;
  bool floatRule;
  bool label;

};

}  // namespace GHKM
}  // namespace Moses

#endif
