/*
 * ExtractedHDRRule.h
 *
 *  Created on: Dec 5, 2013
 *      Author: lly
 */

#ifndef EXTRACTEDHDRRULE_H_
#define EXTRACTEDHDRRULE_H_

namespace Moses
{
namespace HDR
{

class ExtractedHDRRule {
public:
  std::string source;
  std::string target;
  std::string alignment;
  std::string alignmentInv;

  float count;
  int startS;
  int endS;
  int startT;
  int endT;
  int headID;

  ExtractedHDRRule(int sT, int eT, int sS, int eS, int hid)
    : source()
    , target()
    , alignment()
    , alignmentInv()
    , startT(sT)
    , endT(eT)
    , startS(sS)
    , endS(eS)
    , headID(hid)
    , count(0) {
    }

};

}
}


#endif /* EXTRACTEDHDRRULE_H_ */
