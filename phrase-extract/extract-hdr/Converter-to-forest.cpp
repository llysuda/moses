/*
 * Converter.cpp
 *
 *  Created on: Dec 12, 2013
 *      Author: lly
 */

#include "Converter.h"
#include "Converter-to-forest.h"
#include "moses/Util.h"
#include "../InputFileStream.h"
#include "../OutputFileStream.h"
#include <iostream>
#include "SafeGetline.h"
#include "DependencyTree.h"

using namespace std;
using namespace Moses::HDR;

namespace Moses {
namespace HDR {

bool ConverterForest::useLabel = false;

ConverterForest::ConverterForest(int sentID, const string & line) : m_sentID (sentID){
  m_source.Init(line);

  for (int i = 0; i < m_source.Size(); i++) {
    vector<vector<string> > dummy;
    for (int j = 0; j < m_source.Size(); j++) {
      vector<string> dummy2;
      dummy.push_back(dummy2);
    }
    m_sourceLabels.push_back(dummy);
  }
}

ConverterForest::~ConverterForest() {
  // TODO Auto-generated destructor stub
}

void ConverterForest::Process () {
    vector<int> rootid = m_source.GetRootId();
    //ConstTree ctree =
    int depth = 0;
    for(size_t i = 0; i < rootid.size(); i++) {
      //cerr << rootid[i] << endl;
      ProcessSubtree(rootid[i],depth);
    }
}

void ConverterForest::ProcessSubtree(int hid, int& depth) {
  Span span = m_source.GetSourceSpan(hid);

  if (m_source.GetNode(hid).IsLeaf()) {
      if (m_source.GetNode(hid).GetFatherId() < 0) {
        ProcessHead(hid);
      } else {
        ProcessLeaf(hid,0);
      }
      return;
  }

  vector<int> ids = m_source.GetSubtreeIndexes(hid);

  int hindex = -1;
  for (size_t i = 0; i < ids.size(); i++) {
    if (m_source.GetNode(ids[i]).GetId() == hid) {
        hindex = i;
        break;
    }
  }

  //string gluelabel = "G"+SPrint<int>(depth);
  for (size_t i = 0; i < ids.size(); i++) {
    Node n = m_source.GetNode(ids[i]);
    if (n.GetId() == hid) {
        ProcessHead(hid);
       // m_sourceLabels[hid][hid].push_back(gluelabel);
    } else if (n.IsLeaf()) {
        ProcessLeaf(n.GetId(),i-hindex);
        //m_sourceLabels[n.GetId()][n.GetId()].push_back(gluelabel);
    } else {
        depth++;
        ProcessInternal(n.GetId(),i-hindex, depth);

        //Span span = m_source.GetSourceSpan(n.GetId());
       // m_sourceLabels[span.first][span.second].push_back(gluelabel);
    }
  }

string label = m_source.GetNode(hid).GetPos()+":0";
if (useLabel) {
  label = m_source.GetNode(hid).GetPos()+":H0";
}
  for (int i = hindex; i >= 0; i--) {
    for (int j = hindex; j<ids.size(); j++) {
      if (j > i && j-i+1!=ids.size()) {
        int min = hid, max = hid;
        for (int k = i; k <= j; k++) {
          if (k != hindex) {
            Span sp = m_source.GetSourceSpan(ids[k]);
            min = std::min(min, sp.first);
            max = std::max(max, sp.second);
          }
        }


	//string label = m_source.GetNode(hid).GetPos();
        m_sourceLabels[min][max].push_back(label);

        int position = hindex-i;
        m_sourceLabels[min][max].push_back("H" + SPrint<int>(position));

      }
    }
  }

/*  for (int start = 0; start < ids.size(); start++) {
      for (int end = start+1; end < ids.size(); end++) {
          if (hindex >= start && hindex <= end) break;

          int min = span.second, max = span.first;
          for (int k = start; k <= end; k++) {
              Span sp = m_source.GetSourceSpan(ids[k]);
              min = std::min(min, sp.first);
              max = std::max(max, sp.second);
          }
          string label = "XF:I"+SPrint<int>(end-hindex);
          m_sourceLabels[min][max].push_back(label);
      }
  }*/
}

void ConverterForest::ProcessInternal(int id, int position, int& depth) {

  Span span = m_source.GetSourceSpan(id);

  int flag = position/std::abs(position);
  for (int i = 1; i <= std::abs(position); i++) {
    string llabel = m_source.GetNode(id).GetWord()+":"+SPrint<int>(flag*i);
    string hlabel = m_source.GetNode(id).GetPos()+":"+SPrint<int>(flag*i);
    if (useLabel) {
      llabel = m_source.GetNode(id).GetWord()+":I"+SPrint<int>(flag*i);
      hlabel = m_source.GetNode(id).GetPos()+":I"+SPrint<int>(flag*i);
    }
  //string llabel = m_source.GetNode(id).GetWord();
  //string hlabel = m_source.GetNode(id).GetPos();
    //if (i < std::abs(position)) {
    //  llabel = m_source.GetNode(id).GetWord()+":"+m_source.GetNode(id).GetRelation()+":I"+SPrint<int>(flag*i);
    //  hlabel = m_source.GetNode(id).GetPos()+":"+m_source.GetNode(id).GetRelation()+":I"+SPrint<int>(flag*i);
    //}

    m_sourceLabels[span.first][span.second].push_back(llabel);
    m_sourceLabels[span.first][span.second].push_back(hlabel);
  }

  ProcessSubtree(id, depth);
}

void ConverterForest::ProcessLeaf(int id,int position) {
  Node n = m_source.GetNode(id);
  if (n.IsOpenClass()) {
    int flag = position/std::abs(position);
    for (int i = 1; i <= std::abs(position); i++) {
      string label = n.GetPos()+":"+SPrint<int>(flag*i);
      if (useLabel) {
        label = n.GetPos()+":L"+SPrint<int>(flag*i);
      }
	  //string label = n.GetPos();
      //if (i < std::abs(position)) {
      //  label = n.GetPos()+":"+n.GetRelation()+":L"+SPrint<int>(flag*i);
      //}
      m_sourceLabels[id][id].push_back(label);
    }
  }
}

void ConverterForest::ProcessHead(int hid) {
  string label = m_source.GetNode(hid).GetPos()+":0";
  if (useLabel) {
    label = m_source.GetNode(hid).GetPos()+":H0";
  }
  //string label = m_source.GetNode(hid).GetPos();
  m_sourceLabels[hid][hid].push_back(label);
  //m_sourceLabels[hid][hid].push_back(label);

  vector<int> idx = m_source.GetLeftChildrenIndexes(hid);
  Span span = m_source.GetSourceSpan(hid);
  int position = idx.size();
  label = "H" + SPrint<int>(position);
  m_sourceLabels[span.first][span.second].push_back(label);
}

string ConverterForest::SPrintForest() const {
  string out = "<tree label=\"TOP\">";
  for (size_t i = 0; i < m_source.Size(); i++) {
    out += " " + m_source.GetWord(i);
  }
  for (size_t i = 0; i < m_sourceLabels.size(); i++) {
    for (size_t j = 0; j < m_sourceLabels[i].size(); j++) {
      if (m_sourceLabels[i][j].size() > 0) {
        string spanStr = SPrint<int>(i)+"-"+SPrint<int>(j);
        for (size_t k = 0 ; k < m_sourceLabels[i][j].size(); k++) {
          out += " <tree label=\""+m_sourceLabels[i][j][k]+"\" span=\""+spanStr+"\"> </tree>";
        }
      }
    }
  }
  out += " </tree>";
  return out;
}

} /* namespace HDR */
} /* namespace Moses */


int main(int argc, char* argv[])
{
  cerr  << "converter-forest-hdr v1.0, written by Liangyou Li\n"
            << "convert from dep line to cons forest with XML\n";

  if (argc < 3) {
    cerr << "syntax: converter-forest-hdr input output --Label";
    exit(1);
  }

  for(int i=3; i<argc; i++)
  {
    if (strcmp(argv[i],"--Label") == 0) {
      cerr << "use label" << endl;
      ConverterForest::useLabel = true;
    } else {
      cerr << "convert-forest-hdr: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  const long int LINE_MAX_LENGTH = 500000 ;

 const char* const &fileNameIN = argv[1];
 const char* const &fileNameOUT = argv[2];

 Moses::OutputFileStream outFile(fileNameOUT);
 Moses::InputFileStream inFile(fileNameIN);

 istream *inFileP = &inFile;

 int i = 0;
 while(true)
 {
   i++;
   if (i%100 == 0) cerr << "." << flush;
   char inString[LINE_MAX_LENGTH];
   SAFE_GETLINE((*inFileP), inString, LINE_MAX_LENGTH, '\n', __FILE__);
   if (inFileP->eof()) break;

   ConverterForest converter(i,string(inString));
   converter.Process();
   string out = converter.SPrintForest();

   outFile << out << endl;
 }
cerr << "end" << endl;
 inFile.Close();
 outFile.Close();

}
