/*
 * DependencyTree.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "DependencyTree.h"
#include "tables-core.h"
#include "moses/Util.h"


using namespace std;
using namespace Moses;
//using namespace MosesTraining;

//vector<string> tokenize( const char* input );
//vector<string> tokenize( const char* input, const char separator );

vector<string> tokenize( const char* input, const char separator )
{
  vector< string > token;
  bool betweenWords = true;
  int start=0;
  int i=0;
  for(; input[i] != '\0'; i++) {
    bool isSpace = (input[i] == separator);

    if (!isSpace && betweenWords) {
      start = i;
      betweenWords = false;
    } else if (isSpace && !betweenWords) {
      token.push_back( string( input+start, i-start ) );
      betweenWords = true;
    }
  }
  if (!betweenWords)
    token.push_back( string( input+start, i-start ) );
  return token;
}

namespace Moses {
namespace HDR {

void DependencyTree::Init(const string & line) {
  vector<string> tokens = tokenize(line.c_str());
    for(int i = 0; i < tokens.size(); i++) {
      vector<string> nodeAttrib = tokenize(tokens[i].c_str(),'|');
      if (nodeAttrib.size() < 3) {
        cerr << "error source format !" << endl;
        exit(1);
      }
      Node node(i, nodeAttrib[0], nodeAttrib[1], atoi(nodeAttrib[2].c_str()));
      if (nodeAttrib.size() > 3) {
        node.SetRelation(nodeAttrib[3]);
      }
      m_nodes.push_back(node);

      if (node.GetFatherId() == -1) {
        m_rootId.push_back(node.GetId());
      }

    }

    // set leaf flag
    for (int i = 0; i < m_nodes.size(); i++) {
      //if (m_nodes[i].GetFatherId() == -1) {
      //  m_nodes[i].SetLeaf(false);
      //} else {
        int j = m_nodes[i].GetFatherId();
        //while (j >= 0) {
        if (j >= 0)
          m_nodes[j].SetLeaf(false);
        //  j = m_nodes[j].GetFatherId();
        //}
      //}
    }

    CheckSequence();
}

DependencyTree::DependencyTree()
  : m_rootId ()
  , m_nodes() {
}

DependencyTree::DependencyTree(string line) {

  Init(line);
}
void DependencyTree::CheckSequence() const {
  for (int i = 0; i < m_nodes.size(); i++){
    assert(i == m_nodes[i].GetId());
  }
}

void DependencyTree::PrintAnnotation() const
{
  cerr << "headspan  ";
  for (int i = 0; i < m_nodes.size(); i++) {
    Span span = m_nodes[i].GetHeadSpan();
    cerr << "[" << span.first << "," << span.second << "] ";
  }
  cerr << endl;
  cerr << "depspan  ";
  for (int i = 0; i < m_nodes.size(); i++) {
    Span span = m_nodes[i].GetDepSpan();
    cerr << "[" << span.first << "," << span.second << "] ";
  }
  cerr << endl;
  cerr << "leaf  ";
  for (int i = 0; i < m_nodes.size(); i++) {
    cerr << m_nodes[i].IsLeaf() << " ";
  }
  cerr << endl;
  cerr << "consistent  ";
  for (int i = 0; i < m_nodes.size(); i++) {
    cerr << m_nodes[i].IsConsistent() << " ";
  }
  cerr << endl;
}

void DependencyTree::Annotate (int headId, const vector<int> & alignedCountS, const vector<vector<int> > & alignedToT) {
  //WordsRange depSpan(-1,-1);

  for (int i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFatherId() == headId) {
      Annotate(m_nodes[i].GetId(), alignedCountS, alignedToT);

      //if ((!m_nodes[i].IsLeaf()) || m_nodes[i].IsConsistent()) {
      m_nodes[headId].UpdateDepSpan(m_nodes[i].GetDepSpan());
      //}

      m_nodes[headId].UpdateWholeSpan(m_nodes[i].getWholeSpan());
    }
  }

 // WordsRange hspan(alignedCountS.size() +1,-1);
  int min = alignedCountS.size() +1, max = -1;
  vector<int> usedF = alignedCountS;
  for (int i = 0; i < alignedToT[headId].size(); i++) {
    int position = alignedToT[headId][i];
    max = std::max(max, position);
    min = std::min(min, position);
    usedF[position]--;
  }

  //WordsRange hspan(min,max);

  for (int i = min; i <= max; i++) {
    if (usedF[i] > 0) {
      m_nodes[headId].SetConsistent(false);
      break;
    }
  }

  if (max >= min) {
    Span hspan(min,max);
    m_nodes[headId].SetHeadSpan(hspan);
    m_nodes[headId].UpdateWholeSpan(hspan);
    if (m_nodes[headId].IsConsistent())
      m_nodes[headId].UpdateDepSpan(hspan);
  }

  // if alignment consistent in this subtree
  /*Span subtreeSpan = m_nodes[headId].getWholeSpan();

  if (subtreeSpan.second >= subtreeSpan.first) {
    Span sourceSpan = GetSourceSpan(headId);
    usedF = alignedCountS;
    for (int f = sourceSpan.first; f <= sourceSpan.second; f++) {
      for (int i = 0; i < alignedToT[f].size(); i++) {
        int position = alignedToT[f][i];
        usedF[position]--;
      }
    }
    bool out_of_bound = false;
    for (int i = subtreeSpan.first; i < subtreeSpan.second; i++) {
      if (usedF[i] <= 0 && alignedCountS[i] > 0) {
        Span span(i,i);
        m_nodes[headId].UpdateDepSpan(span);
        //out_of_bound = true;
        //break;
      }
    }
    ////if (!out_of_bound) {
    //  m_nodes[headId].UpdateDepSpan(subtreeSpan);
    //}
  }*/

}

vector<int> DependencyTree::GetPostOrder(int headId) const {
  vector<int> ret;
  for (int i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFatherId() == headId) {
      vector<int> kidsOrder = GetPostOrder(m_nodes[i].GetId());
      for (int j = 0; j < kidsOrder.size(); i++) {
        ret.push_back(kidsOrder[j]);
      }
    }
  }
  ret.push_back(headId);
  return ret;
}

Span DependencyTree::GetSourceSpan(int index) const {
  Span retSpan(index,index);
  vector<int> indexes = GetSubtreeIndexes(index);
  if (indexes.size() > 1) {
    for (int i = 0; i < indexes.size(); i++) {
      if (indexes[i] != index) {
        Span span = GetSourceSpan(indexes[i]);
        retSpan.first = std::min(retSpan.first, span.first);
        retSpan.second = std::max(retSpan.second, span.second);
      }
    }
  }
  return retSpan;
}

HDRFragment DependencyTree::CreateHDRFragment(int index) const {
  vector<Node> subtree = GetSubtreeNodes(index);
  HDRFragment fragment(subtree, m_nodes[index].GetId());
  return fragment;
}

vector<Node> DependencyTree::GetSubtreeNodes(int index) const{
  vector<Node> ret;
  vector<int> indexes = GetSubtreeIndexes(index);
  for (int i = 0; i < indexes.size(); i++) {
    ret.push_back(m_nodes[indexes[i]]);
  }
  return ret;
}

vector<int> DependencyTree::GetSubtreeIndexes(int index) const {
  vector<int> ret;
  for (int i = 0; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFatherId() == m_nodes[index].GetId())
      ret.push_back(i);
    else if (i == index)
      ret.push_back(index);
  }
  return ret;
}

vector<int> DependencyTree::GetLeftChildrenIndexes(int index) const {
  vector<int> ret;
  for (int i = 0; i < index; i++) {
    if (m_nodes[i].GetFatherId() == m_nodes[index].GetId())
      ret.push_back(i);
  }
  return ret;
}

vector<int> DependencyTree::GetRightChildrenIndexes(int index) const {
  vector<int> ret;
  for (int i = index+1; i < m_nodes.size(); i++) {
    if (m_nodes[i].GetFatherId() == m_nodes[index].GetId())
      ret.push_back(i);
  }
  return ret;
}

DependencyTree::~DependencyTree() {
  // TODO Auto-generated destructor stub
}

string DependencyTree::GetFactorString() const
{
  string ret = "";
  for (size_t i = 0; i < m_nodes.size(); i++) {
    ret += m_nodes[i].GetWord() + "|" + m_nodes[i].GetPos() + "|" + SPrint<int>(m_nodes[i].GetFatherId()) + "|" + m_nodes[i].GetRelation();
    ret += " ";
  }
  return ret.erase(ret.size()-1);
}

void DependencyTree::SetFatherId(int nid, int fid)
{
  m_nodes[nid].SetFatherId(fid);
}

} /* namespace HDR */
} /* namespace Moses */
