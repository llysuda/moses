#include <string>
#include "HDRSentenceAlignment.h"
#include "ExtractHDROptions.h"
#include "SafeGetline.h"
#include "../InputFileStream.h"
#include "../OutputFileStream.h"
#include "ExtractedHDRRule.h"
#include "HDRFragment.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <set>

using namespace std;
using namespace Moses::HDR;

//class ExtractHDROptions;
//class HDRSentenceAlignment;

string itostring(int i) {
  ostringstream ss;
  ss << i;
  return ss.str();
}

namespace Moses
{

namespace HDR
{



const long int LINE_MAX_LENGTH = 500000 ;

class ExtractHDRTask
{
public:
  ExtractHDRTask(size_t id, HDRSentenceAlignment & sentence, ExtractHDROptions & initoptions,Moses::OutputFileStream &extractFile, Moses::OutputFileStream &extractFileInv) :
    m_sentence(sentence),
    m_options(initoptions),
    m_extractFile(extractFile),
    m_extractFileInv(extractFileInv),
    maxGlueDepth(0) {}

  void Run();
  int maxGlueDepth;

private:
  //vector< string > m_extractedHDR;
  //vector< string > m_extractedHDRInv;
  vector< ExtractedHDRRule > m_extractedRules;

  void extract(HDRSentenceAlignment &);
  void writeHDRToFile();
  void addRuleToCollection(const ExtractedHDRRule &);
  void addRuleToCollection(vector<ExtractedHDRRule> &);
  void ExtractHDRRule (HDRSentenceAlignment & sentence, HDRFragment & fragment);
  void ExtractPhraseRule (HDRSentenceAlignment & sentence, int startF, int endF, int headID);
  ExtractedHDRRule GetPhraseRule(HDRSentenceAlignment & sentence, int startE, int endE, int startF, int endF, int headID);
  bool IsFragmentConsistent (const HDRSentenceAlignment & sentence, const HDRFragment& fragement, int headID) const ;
  bool IsValid(const HDRFragment &)  const;

  HDRSentenceAlignment &m_sentence;
  const ExtractHDROptions &m_options;
  Moses::OutputFileStream &m_extractFile;
  Moses::OutputFileStream &m_extractFileInv;
};

} // namespace HDR
} // namespace Moses

void writeGlueGrammar(const string &, ExtractHDROptions &options, const int& depth);

int main(int argc, char* argv[])
{
  cerr  << "Head-Dependent Rule Extract v1.0, written by Liangyou Li\n"
          << "rule extraction from an aligned parallel corpus (dependentcy tree to string)\n";

  if (argc < 5) {
    cerr << "syntax: extract-hdr en de align extract [-max-length int]";
    cerr<<"| --GZOutput | --IncludeSentenceId | --SentenceOffset n |\n";
    exit(1);
  }

  Moses::OutputFileStream extractFile;
  Moses::OutputFileStream extractFileInv;

  const char* const &fileNameE = argv[1];
  const char* const &fileNameF = argv[2];
  const char* const &fileNameA = argv[3];
  const string fileNameExtract = argv[4];

  string fileNameGlue;

  Moses::HDR::ExtractHDROptions options;

  for(int i=5; i<argc; i++)
  {
    if (strcmp(argv[i], "--IncludeSentenceId") == 0) {
      options.initIncludeSentenceIdFlag(true);
    } else if (strcmp(argv[i], "--SentenceOffset") == 0) {
      if (i+1 >= argc || argv[i+1][0] < '0' || argv[i+1][0] > '9') {
        cerr << "extract-hdr: syntax error, used switch --SentenceOffset without a number" << endl;
        exit(1);
      }
      int sentenceOffset = atoi(argv[++i]);
      options.initSentenceOffset(sentenceOffset);
    } else if (strcmp(argv[i], "--GZOutput") == 0) {
      options.initGzOutput(true);
    } else if (strcmp(argv[i], "-max-length") == 0) {
      if (i+1 >= argc || argv[i+1][0] < '0' || argv[i+1][0] > '9') {
        cerr << "extract-hdr: syntax error, used switch -max-length without a number" << endl;
        exit(1);
      }
      int maxLength = atoi(argv[++i]);
      options.initMaxPhraseLength(maxLength);
    } else if (strcmp(argv[i],"--PhraseRule") == 0) {
      options.initPhraseRule(true);
    } else if (strcmp(argv[i],"--SubPhrase") == 0) {
      options.initSubPhrase(true);
    } else if (strcmp(argv[i],"--SubFragment") == 0) {
      options.initSubFragment(true);
    } else if (strcmp(argv[i],"--AlignConsistent") == 0) {
      options.initAlignConsistent(true);
    } else if (strcmp(argv[i],"--FactionalCount") == 0) {
      options.initFactionalCount(true);
    } else if (strcmp(argv[i],"--GlueGrammar") == 0) {
      fileNameGlue = argv[++i];
      options.initGlueGrammar(true);
    } else if (strcmp(argv[i],"--FloatRule") == 0) {
	  options.initFloatRule(true);
	} else if (strcmp(argv[i],"--Label") == 0) {
      options.initLabel(true);
    }
	else {
      cerr << "extract-hdr: syntax error, unknown option '" << string(argv[i]) << "'\n";
      exit(1);
    }
  }

  // open input files
  Moses::InputFileStream eFile(fileNameE);
  Moses::InputFileStream fFile(fileNameF);
  Moses::InputFileStream aFile(fileNameA);

  istream *eFileP = &eFile;
  istream *fFileP = &fFile;
  istream *aFileP = &aFile;

  // open output files

  string fileNameExtractInv = fileNameExtract + ".inv" + (options.isGzOutput()?".gz":"");
  extractFile.Open( (fileNameExtract + (options.isGzOutput()?".gz":"")).c_str());
  extractFileInv.Open(fileNameExtractInv.c_str());


  int i = options.GetSentenceOffset();
  int maxGlueDepth = 0;
  while(true)
  {
    i++;
    if (i%10000 == 0)
      cerr << "." << flush;
    char englishString[LINE_MAX_LENGTH];
    char foreignString[LINE_MAX_LENGTH];
    char alignmentString[LINE_MAX_LENGTH];
    SAFE_GETLINE((*eFileP), englishString, LINE_MAX_LENGTH, '\n', __FILE__);
    if (eFileP->eof()) break;
    SAFE_GETLINE((*fFileP), foreignString, LINE_MAX_LENGTH, '\n', __FILE__);
    SAFE_GETLINE((*aFileP), alignmentString, LINE_MAX_LENGTH, '\n', __FILE__);

    HDRSentenceAlignment sentence;

    if (sentence.create( englishString, foreignString, alignmentString, i, false))
    {
      ExtractHDRTask *task = new ExtractHDRTask(i-1, sentence, options, extractFile , extractFileInv);
      task->Run();
      maxGlueDepth = std::max(maxGlueDepth, task->maxGlueDepth);
      delete task;

    }
  }

  eFile.Close();
  fFile.Close();
  aFile.Close();

  extractFile.Close();
  extractFileInv.Close();

  if (options.GlueGrammar())
      writeGlueGrammar(fileNameGlue, options, maxGlueDepth);

}

namespace Moses
{
namespace HDR
{

void ExtractHDRTask::Run() {
  extract(m_sentence);
  writeHDRToFile();
  m_extractedRules.clear();
}

void ExtractHDRTask::extract(HDRSentenceAlignment & sentence) {

  if (m_options.ExtractPhraseRule()) {
    int maxPhraseLength = m_options.MaxPhraseLength();
    for (int startF = 0; startF < sentence.source.Size();startF++) {
      for (int endF = startF; endF-startF < maxPhraseLength && endF < sentence.source.Size(); endF++) {
        ExtractPhraseRule(sentence, startF, endF, startF);
      }
    }
  }

  for (int i = 0; i < sentence.source.Size(); i++) {
    if (!m_options.ExtractPhraseRule())
      ExtractPhraseRule(sentence, i, i, i);

    if (!sentence.source.IsLeaf(i)) {
      vector<HDRFragment> fragments;

      HDRFragment fragment = sentence.source.CreateHDRFragment(i);
      fragments.push_back(fragment);

      // in full fragment, substitute subfragment with non-terminal
      if (m_options.SubFragment()) {
        vector<HDRFragment>::iterator frag;
        vector<HDRFragment> subFragment = fragment.GetSubFragments();
        for (frag = subFragment.begin(); frag != subFragment.end(); frag++) {
          fragments.push_back(*frag);
        }
      }
	  
	  // float rule
	  if (m_options.FloatRule()) {
	    // TODO
		vector<HDRFragment>::iterator frag;
        vector<HDRFragment> subFragment = fragment.GetFloatFragments();
        for (frag = subFragment.begin(); frag != subFragment.end(); frag++) {
          fragments.push_back(*frag);
        }
	  }

	  //
	  //
	  // extract
	  //
	  //
	  vector<HDRFragment>::iterator frag;
	  for (frag = fragments.begin(); frag != fragments.end(); frag++) {
	    if (IsValid(*frag))
		  if (!m_options.AlignConsistent() || (m_options.AlignConsistent() && IsFragmentConsistent(m_sentence, *frag, i)))
	        ExtractHDRRule(sentence, *frag);
	  }
    }
  }
}

bool ExtractHDRTask::IsValid(const HDRFragment & frag) const
{
  return frag.IsValid(m_options);
}

void ExtractHDRTask::ExtractHDRRule (HDRSentenceAlignment & sentence, HDRFragment & fragment) {

  //: flag for each node, at most 8 combinations
  vector<vector<bool> > generalFlag = fragment.EnumerateGeneralFlag();
  for (int i = 0; i < generalFlag.size(); i++) {
    vector<bool> & flags = generalFlag[i];
    vector<ExtractedHDRRule> rules = fragment.GetRules(flags, sentence.target, sentence.alignedCountT, m_options);
    addRuleToCollection(rules);
  }
}

bool ExtractHDRTask::IsFragmentConsistent (const HDRSentenceAlignment & sentence, const HDRFragment& fragment, int headID) const {
  set<int> findex;

  if (fragment.IsCoreFragment() || fragment.IsShellFragment() || fragment.IsFloatFragment()) {
    int size = fragment.GetSize();
    for (int i = 0; i < size; i++) {
      int id = fragment.GetID(i);

      if (id == headID) {
        findex.insert(id);
      } else {
        pair<int,int> span = sentence.source.GetSourceSpan(id);
        for (int i = span.first; i <= span.second; i++) {
          findex.insert(i);
        }
      }
    }
  } else {
    pair<int,int> span = sentence.source.GetSourceSpan(headID);
    for (int i = span.first; i <= span.second; i++) {
      findex.insert(i);
    }
  }

  vector<Span> sortedSpan = fragment.SortedSpan();
  if (sortedSpan.size() == 0) return true;
  int startE = sortedSpan[0].first, endE = sortedSpan.back().second;
  for (int i = startE; i <= endE; i++) {
    vector<int> s = sentence.alignedToS[i];
    if (s.size() == 0) continue;
    int j = 0;
    for (; j < s.size(); j++) {
      int f = s[j];
      if (findex.find(f) != findex.end()) {
        break;
      }
    }

    if (j == s.size()) {
      return false;
    }
  }
  return true;
}

void ExtractHDRTask::ExtractPhraseRule (HDRSentenceAlignment & sentence, int startF, int endF, int headID) {
  int countE = sentence.target.size();
  int maxPhraseLength = m_options.MaxPhraseLength();
  bool relaxLimit = m_options.RelaxLimit() || (startF < endF);

  if (startF < endF && endF-startF >= maxPhraseLength) return;

  int minE = 9999;
  int maxE = -1;
  vector< int > usedE = sentence.alignedCountT;
  for(int fi=startF; fi<=endF; fi++) {
    for(size_t i=0; i<sentence.alignedSoT[fi].size(); i++) {
      int ei = sentence.alignedSoT[fi][i];
      if (ei<minE) {
        minE = ei;
      }
      if (ei>maxE) {
        maxE = ei;
      }
      usedE[ ei ]--;
    }
  }

  if (maxE >= 0 && // aligned to any source words at all
    (relaxLimit || maxE-minE < maxPhraseLength)) { // source phrase within limits

    // check if source words are aligned to out of bound target words
    bool out_of_bounds = false;
    for(int ei=minE; ei<=maxE && !out_of_bounds; ei++)
      if (usedE[ei]>0) {
        // cout << "ouf of bounds: " << fi << "\n";
        out_of_bounds = true;
      }

    // cout << "doing if for ( " << minF << "-" << maxF << ", " << startE << "," << endE << ")\n";
    if (!out_of_bounds) {
      vector<ExtractedHDRRule> rules;
      // start point of source phrase may retreat over unaligned
      for(int startE=minE;
          (startE>=0 &&
           (relaxLimit || startE>maxE-maxPhraseLength) && // within length limit
           (startE==minE || sentence.alignedCountT[startE]==0)); // unaligned
          startE--)
        // end point of source phrase may advance over unaligned
        for(int endE=maxE;
            (endE<countE &&
             (relaxLimit || endE<startE+maxPhraseLength) && // within length limit
             (endE==maxE || sentence.alignedCountT[endE]==0)); // unaligned
            endE++) { // at this point we have extracted a phrase

          ExtractedHDRRule r = GetPhraseRule(sentence, startE, endE, startF, endF, headID);
          rules.push_back(r);
        }

      addRuleToCollection(rules);
    }
  }
}

ExtractedHDRRule ExtractHDRTask::GetPhraseRule(HDRSentenceAlignment & sentence, int startE, int endE, int startF, int endF, int headID) {
  ExtractedHDRRule rule(startE,endE,startF,endF, headID);

  stringstream ss;
  //source
  string source = "";
  for (int i = startF; i <= endF; i++)
    source += sentence.source.GetWord(i)+" ";
  //source += "[XH"+itostring(headID-startF)+"]";
  source += "[X]";

  //target
  string target = "";
  for (int i = startE; i <= endE; i++)
    target += sentence.target[i]+" ";
  target += "[X]";

  // align
  string alignment, alignmentInv;
  for (int fi = startF; fi <= endF; fi++) {
    for (int i = 0; i < sentence.alignedSoT[fi].size(); i++) {
      string s = itostring(fi-startF);
      string t = itostring(sentence.alignedSoT[fi][i]-startE);
      alignment += s+"-"+t+" ";
      alignmentInv += t+"-"+s+" ";
    }
  }
  alignment.erase(alignment.size()-1);
  alignmentInv.erase(alignmentInv.size()-1);

  rule.source = source;
  rule.target = target;
  rule.alignment = alignment;
  rule.alignmentInv = alignmentInv;
  rule.count = 1;

  //addRuleToCollection(rule);
  return rule;
}

void ExtractHDRTask::addRuleToCollection(vector<ExtractedHDRRule> & rules) {
  vector<ExtractedHDRRule>::iterator rule;
  bool nonfirst = false;
  float faction = 0.0;
  if (rules.size() > 1) faction = 1.0/(rules.size()-1);
  for(rule = rules.begin(); rule != rules.end(); rule++ ) {
   if (m_options.FactionalCount() && nonfirst) { // overlapping
     rule->count = faction;
   }
   nonfirst = true;
   addRuleToCollection(*rule);
  }
}

void ExtractHDRTask::addRuleToCollection(const ExtractedHDRRule & newRule) {
  if (!m_options.DuplicateRules()) {
    vector<ExtractedHDRRule>::const_iterator rule;
    for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
      if (rule->source.compare( newRule.source ) == 0 &&
          rule->target.compare( newRule.target ) == 0 &&
          rule->headID == newRule.headID
         ) { // overlapping
        return;
      }
    }
  }
  m_extractedRules.push_back( newRule );
}

void ExtractHDRTask::writeHDRToFile() {
  vector<ExtractedHDRRule>::const_iterator rule;
  ostringstream out;
  ostringstream outInv;

  for(rule = m_extractedRules.begin(); rule != m_extractedRules.end(); rule++ ) {
    if (rule->count == 0)
      continue;

    out << rule->source << " ||| "
        << rule->target << " ||| "
        << rule->alignment << " ||| "
        << rule->count << " ||| ";

    out << "\n";

    //if (!m_options.onlyDirectFlag) {
      outInv << rule->target << " ||| "
             << rule->source << " ||| "
             << rule->alignmentInv << " ||| "
             << rule->count << "\n";
    //}
  }
  m_extractFile << out.str();
  m_extractFileInv << outInv.str();
}

}
}

void writeGlueGrammar( const string & fileName, ExtractHDROptions &options, const int& depth)
{
  ofstream grammarFile;
  grammarFile.open(fileName.c_str());
    grammarFile << "<s> [X] ||| <s> [S] ||| 1 ||| 0-0 ||| 0" << endl
                << "[X][S] </s> [X] ||| [X][S] </s> [S] ||| 1 ||| 0-0 ||| 0" << endl
                << "[X][S] [X][X] [X] ||| [X][S] [X][X] [S] ||| 2.718 ||| 0-0 1-1 ||| 0" << endl;

  grammarFile.close();
}

