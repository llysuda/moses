/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2009 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <sstream>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <assert.h>
#include <cstring>
#include <set>
#include <algorithm>

#include "SafeGetline.h"
#include "tables-core.h"
#include "InputFileStream.h"
#include "OutputFileStream.h"

using namespace std;

typedef vector<string> PHRASE;
#define LINE_MAX_LENGTH 100000

bool hierarchicalFlag = false;
bool wordAlignmentFlag = true;
bool unpairedExtractFormatFlag = false;
bool conditionOnTargetLhsFlag = false;
float minCountHierarchical = 0;
bool inverseFlag = false;
map<string,string> srcClassMap;
map<string,string> tgtClassMap;
bool targetSparseFlag = false;
bool targetPOSFlag = false;

inline bool isNonTerminal( const std::string &word )
{
  return (word.length()>=3 && word[0] == '[' && word[word.length()-1] == ']');
}

class FeaturePhraseAlignment
{
protected:
  PHRASE phraseS;
  PHRASE phraseT;

  std::map<size_t, std::pair<size_t, size_t> > m_ntLengths;

  void createAlignVec(size_t sourceSize, size_t targetSize);

public:

  float count;
  int sentenceId;

  set<string> features;

  std::vector< std::set<size_t> > alignedToT;
  std::vector< std::set<size_t> > alignedToS;

  void create( char*, int);
  void clear();
  bool equals( const FeaturePhraseAlignment& );
  bool match( const FeaturePhraseAlignment& );

  bool addFeatures( const FeaturePhraseAlignment&);

  int Compare(const FeaturePhraseAlignment &compare) const;
  inline bool operator<(const FeaturePhraseAlignment &compare) const {
    return Compare(compare) < 0;
  }

  const PHRASE &GetSource() const {
    return phraseS;
  }
  const PHRASE &GetTarget() const {
    return phraseT;
  }

};

typedef std::vector<FeaturePhraseAlignment*>          PhraseAlignmentCollection;
//typedef std::vector<PhraseAlignmentCollection> PhrasePairGroup;

class PhraseAlignmentCollectionOrderer
{
public:
  bool operator()(const PhraseAlignmentCollection &collA, const PhraseAlignmentCollection &collB) const {
    assert(collA.size() > 0);
    assert(collB.size() > 0);

    const FeaturePhraseAlignment &objA = *collA[0];
    const FeaturePhraseAlignment &objB = *collB[0];
    bool ret = objA < objB;

    return ret;
  }
};


//typedef std::set<PhraseAlignmentCollection, PhraseAlignmentCollectionOrderer> PhrasePairGroup;

class PhrasePairGroup
{
private:
  typedef std::set<PhraseAlignmentCollection, PhraseAlignmentCollectionOrderer> Coll;
  Coll m_coll;


public:
  typedef Coll::iterator iterator;
  typedef Coll::const_iterator const_iterator;
  typedef std::vector<const PhraseAlignmentCollection *> SortedColl;

  std::pair<Coll::iterator,bool> insert ( const PhraseAlignmentCollection& obj );

  const SortedColl &GetSortedColl() const {
    return m_sortedColl;
  }
  size_t GetSize() const {
    return m_coll.size();
  }

private:
  SortedColl m_sortedColl;

};


vector<string> tokenize( const char [] );
void processPhrasePairs( vector< FeaturePhraseAlignment > & , ostream &phraseTableFile, bool isSingleton);
const FeaturePhraseAlignment &findBestAlignment(const PhraseAlignmentCollection &phrasePair );
void outputPhrasePair(const PhraseAlignmentCollection &phrasePair, float, int, ostream &phraseTableFile, bool isSingleton);
void printSourcePhrase(const PHRASE &, const PHRASE &, const FeaturePhraseAlignment &, ostream &);
void printTargetPhrase(const PHRASE &, const PHRASE &, const FeaturePhraseAlignment &, ostream &);
void loadclass(map<string,string>& classmap, const string &fileName );

int main(int argc, char* argv[])
{
  cerr << "Score v2.0 written by Philipp Koehn\n"
       << "scoring methods for extracted rules\n";

  if (argc < 4) {
      cerr << "syntax: score extract lex phrase-table [--Inverse] [--Hierarchical] [--LogProb] [--NegLogProb] [--NoLex] [--GoodTuring] [--KneserNey] [--NoWordAlignment] [--UnalignedPenalty] [--UnalignedFunctionWordPenalty function-word-file] [--MinCountHierarchical count] [--OutputNTLengths] [--PCFG] [--UnpairedExtractFormat] [--ConditionOnTargetLHS] [--Singleton] [--CrossedNonTerm] \n";
     // cerr << featureManager.usage() << endl;
      exit(1);
    }

  string fileNameTLex = "";
  for(int i=4; i<argc; i++) {
     if (strcmp(argv[i],"--Hierarchical") == 0) {
       hierarchicalFlag = true;
       cerr << "processing hierarchical rules\n";
     } else if (strcmp(argv[i],"--NoWordAlignment") == 0) {
       wordAlignmentFlag = false;
       cerr << "omitting word alignment" << endl;
     } else if (strcmp(argv[i],"--MinCountHierarchical") == 0) {
       minCountHierarchical = atof(argv[++i]);
       cerr << "dropping all phrase pairs occurring less than " << minCountHierarchical << " times\n";
       minCountHierarchical -= 0.00001; // account for rounding
     } else if (strcmp(argv[i],"--TargetSparse") == 0) {
         targetSparseFlag = true;
         fileNameTLex = argv[++i];
     } else if (strcmp(argv[i],"--TargetPOS") == 0) {
         targetPOSFlag = true;
      }
   }

  string fileNameExtract = argv[1];
  string fileNameLex = argv[2];
  string fileNamePhraseTable = argv[3];
  string fileNameCountOfCounts;

 // vector<string> featureArgs; //all unknown args passed to feature manager


  //cerr << "load class file ..." << endl;
loadclass(srcClassMap,fileNameLex);
if (targetSparseFlag) {
loadclass(tgtClassMap,fileNameTLex);
}
//cerr << classmap.size() << endl;
//cerr << "end" << endl;
  // sorted phrase extraction file
  Moses::InputFileStream extractFile(fileNameExtract);

  //cerr << "open " << fileNameExtract << endl;
  if (extractFile.fail()) {
    cerr << "ERROR: could not open extract file " << fileNameExtract << endl;
    exit(1);
  }
  istream &extractFileP = extractFile;

  // output file: phrase translation table
  ostream *phraseTableFile;

  //cerr << "open " << fileNamePhraseTable << endl;
  if (fileNamePhraseTable == "-") {
    phraseTableFile = &cout;
  } else {
    Moses::OutputFileStream *outputFile = new Moses::OutputFileStream();
    bool success = outputFile->Open(fileNamePhraseTable);
    if (!success) {
      cerr << "ERROR: could not open file phrase table file "
           << fileNamePhraseTable << endl;
      exit(1);
    }
    phraseTableFile = outputFile;
  }

  // loop through all extracted phrase translations
  float lastCount = 0.0f;
  //float lastPcfgSum = 0.0f;
  vector< FeaturePhraseAlignment > phrasePairsWithSameF;
  bool isSingleton = true;
  int i=0;
  char line[LINE_MAX_LENGTH],lastLine[LINE_MAX_LENGTH];
  lastLine[0] = '\0';
  FeaturePhraseAlignment *lastPhrasePair = NULL;
  //cerr << "begin..." << endl;
  while(true) {
    if (extractFileP.eof()) break;
    if (++i % 100000 == 0) cerr << "." << flush;
    //cerr << i << endl;
    SAFE_GETLINE((extractFileP), line, LINE_MAX_LENGTH, '\n', __FILE__);
    if (extractFileP.eof())	break;

    // identical to last line? just add count
    if (strcmp(line,lastLine) == 0) {
      lastPhrasePair->count += lastCount;

      continue;
    }
    strcpy( lastLine, line );

    // create new phrase pair
    FeaturePhraseAlignment phrasePair;
    phrasePair.create( line, i);
    lastCount = phrasePair.count;
    //lastPcfgSum = phrasePair.pcfgSum;

    // only differs in count? just add count
    if (lastPhrasePair != NULL
        && lastPhrasePair->equals( phrasePair )) {
      lastPhrasePair->count += phrasePair.count;
      //cerr << "add sparse" << endl;
      lastPhrasePair->addFeatures(phrasePair);
      continue;
    }

    // if new source phrase, process last batch
    if (lastPhrasePair != NULL &&
        lastPhrasePair->GetSource() != phrasePair.GetSource()) {
    	//cerr << "process" <<endl;
      processPhrasePairs( phrasePairsWithSameF, *phraseTableFile, isSingleton );

      phrasePairsWithSameF.clear();
      isSingleton = false;
      lastPhrasePair = NULL;
    } else {
      isSingleton = true;
    }

    // add phrase pairs to list, it's now the last one
    phrasePairsWithSameF.push_back( phrasePair );
    lastPhrasePair = &phrasePairsWithSameF.back();
  }
  //cerr << "process" <<endl;
  processPhrasePairs( phrasePairsWithSameF, *phraseTableFile, isSingleton );

  phraseTableFile->flush();
  if (phraseTableFile != &cout) {
    delete phraseTableFile;
  }
  extractFile.Close();
  // output count of count statistics
}


void printSourcePhrase(const PHRASE &phraseS, const PHRASE &phraseT,
                       const FeaturePhraseAlignment &bestAlignment, ostream &out)
{
  // output source symbols, except root, in rule table format
  for (std::size_t i = 0; i < phraseS.size()-1; ++i) {
    const std::string &word = phraseS[i];
    if (!unpairedExtractFormatFlag || !isNonTerminal(word)) {
      out << word << " ";
      continue;
    }
    // get corresponding target non-terminal and output pair
    std::set<std::size_t> alignmentPoints = bestAlignment.alignedToS[i];
    assert(alignmentPoints.size() == 1);
    int j = *(alignmentPoints.begin());
    if (inverseFlag) {
      out << phraseT[j] << word << " ";
    } else {
      out << word << phraseT[j] << " ";
    }
  }
  // output source root symbol
  if (conditionOnTargetLhsFlag && !inverseFlag) {
    out << "[X]";
  } else {
    out << phraseS.back();
  }
}

void printTargetPhrase(const PHRASE &phraseS, const PHRASE &phraseT,
                       const FeaturePhraseAlignment &bestAlignment, ostream &out)
{
  // output target symbols, except root, in rule table format
  for (std::size_t i = 0; i < phraseT.size()-1; ++i) {
    const std::string &word = phraseT[i];
    if (!unpairedExtractFormatFlag || !isNonTerminal(word)) {
      out << word << " ";
      continue;
    }
    // get corresponding source non-terminal and output pair
    std::set<std::size_t> alignmentPoints = bestAlignment.alignedToT[i];
    assert(alignmentPoints.size() == 1);
    int j = *(alignmentPoints.begin());
    if (inverseFlag) {
      out << word << phraseS[j] << " ";
    } else {
      out << phraseS[j] << word << " ";
    }
  }
  // output target root symbol
  if (conditionOnTargetLhsFlag) {
    if (inverseFlag) {
      out << "[X]";
    } else {
      out << phraseS.back();
    }
  } else {
    out << phraseT.back();
  }
}

void processPhrasePairs( vector< FeaturePhraseAlignment > &phrasePair, ostream &phraseTableFile, bool isSingleton)
{
  if (phrasePair.size() == 0) return;

  // group phrase pairs based on alignments that matter
  // (i.e. that re-arrange non-terminals)
  PhrasePairGroup phrasePairGroup;

  float totalSource = 0;

  //cerr << "phrasePair.size() = " << phrasePair.size() << endl;

  // loop through phrase pairs
  for(size_t i=0; i<phrasePair.size(); i++) {
    // add to total count
    FeaturePhraseAlignment &currPhrasePair = phrasePair[i];

    totalSource += phrasePair[i].count;

    // check for matches
    //cerr << "phrasePairGroup.size() = " << phrasePairGroup.size() << endl;

    PhraseAlignmentCollection phraseAlignColl;
    phraseAlignColl.push_back(&currPhrasePair);
    pair<PhrasePairGroup::iterator, bool> retInsert;
    retInsert = phrasePairGroup.insert(phraseAlignColl);
    if (!retInsert.second) {
      // already exist. Add to that collection instead
      PhraseAlignmentCollection &existingColl = const_cast<PhraseAlignmentCollection&>(*retInsert.first);
      existingColl.push_back(&currPhrasePair);
    }

  }

  // output the distinct phrase pairs, one at a time
  const PhrasePairGroup::SortedColl &sortedColl = phrasePairGroup.GetSortedColl();
  PhrasePairGroup::SortedColl::const_iterator iter;

  for(iter = sortedColl.begin(); iter != sortedColl.end(); ++iter) {
    const PhraseAlignmentCollection &group = **iter;
    outputPhrasePair( group, totalSource, phrasePairGroup.GetSize(), phraseTableFile, isSingleton);
  }

}

const FeaturePhraseAlignment &findBestAlignment(const PhraseAlignmentCollection &phrasePair )
{
  float bestAlignmentCount = -1;
  FeaturePhraseAlignment* bestAlignment = NULL;

  for(size_t i=0; i<phrasePair.size(); i++) {
    size_t alignInd;

      alignInd = i;

    if (phrasePair[alignInd]->count > bestAlignmentCount) {
      bestAlignmentCount = phrasePair[alignInd]->count;
      bestAlignment = phrasePair[alignInd];
    }
  }

  return *bestAlignment;
}

bool calcCrossedNonTerm(size_t sourcePos, size_t targetPos, const std::vector< std::set<size_t> > &alignedToS)
{
  for (size_t currSource = 0; currSource < alignedToS.size(); ++currSource) {
    if (currSource == sourcePos) {
      // skip
    } else {
      const std::set<size_t> &targetSet = alignedToS[currSource];
      std::set<size_t>::const_iterator iter;
      for (iter = targetSet.begin(); iter != targetSet.end(); ++iter) {
        size_t currTarget = *iter;

        if ((currSource < sourcePos && currTarget > targetPos)
            || (currSource > sourcePos && currTarget < targetPos)
           ) {
          return true;
        }
      }

    }
  }

  return false;
}

int calcCrossedNonTerm(const PHRASE &phraseS, const FeaturePhraseAlignment &bestAlignment)
{
  const std::vector< std::set<size_t> > &alignedToS = bestAlignment.alignedToS;

  for (size_t sourcePos = 0; sourcePos < alignedToS.size(); ++sourcePos) {
    const std::set<size_t> &targetSet = alignedToS[sourcePos];
    bool isNonTerm = isNonTerminal(phraseS[sourcePos]);

    if (isNonTerm) {
      assert(targetSet.size() == 1);
      size_t targetPos = *targetSet.begin();
      bool ret = calcCrossedNonTerm(sourcePos, targetPos, alignedToS);
      if (ret)
        return 1;
    }
  }

  return 0;
}

void outputPhrasePair(const PhraseAlignmentCollection &phrasePair, float totalCount, int distinctCount, ostream &phraseTableFile, bool isSingleton)
{
  if (phrasePair.size() == 0) return;

  const FeaturePhraseAlignment &bestAlignment = findBestAlignment( phrasePair );

  // compute count
  set<string> features;
  float count = 0;
  for(size_t i=0; i<phrasePair.size(); i++) {
    count += phrasePair[i]->count;
    set<string>::const_iterator iter = phrasePair[i]->features.begin();
    while (iter != phrasePair[i]->features.end()) {
    	features.insert(*iter);
    	iter++;
    }
  }


  map< string, float > domainCount;

  // collect count of count statistics


  // output phrases
  const PHRASE &phraseS = phrasePair[0]->GetSource();
  const PHRASE &phraseT = phrasePair[0]->GetTarget();

  // do not output if hierarchical and count below threshold
  if (hierarchicalFlag && count < 0) {
    for(size_t j=0; j<phraseS.size()-1; j++) {
      if (isNonTerminal(phraseS[j]))
        return;
    }
  }


    printSourcePhrase(phraseS, phraseT, bestAlignment, phraseTableFile);
    phraseTableFile << " ||| ";


  // target phrase
  printTargetPhrase(phraseS, phraseT, bestAlignment, phraseTableFile);
  phraseTableFile << " ||| ";

  // source phrase (if inverse)

  // alignment info for non-terminals
    if (hierarchicalFlag) {
      // always output alignment if hiero style, but only for non-terms
      // (eh: output all alignments, needed for some feature functions)
      assert(phraseT.size() == bestAlignment.alignedToT.size() + 1);
      std::vector<std::string> alignment;
      for(size_t j = 0; j < phraseT.size() - 1; j++) {
        if (isNonTerminal( phraseT[j] )) {
          if (bestAlignment.alignedToT[ j ].size() != 1) {
            cerr << "Error: unequal numbers of non-terminals. Make sure the text does not contain words in square brackets (like [xxx])." << endl;
            phraseTableFile.flush();
            assert(bestAlignment.alignedToT[ j ].size() == 1);
          }
          int sourcePos = *(bestAlignment.alignedToT[ j ].begin());
          //phraseTableFile << sourcePos << "-" << j << " ";
          std::stringstream point;
          point << sourcePos << "-" << j;
          alignment.push_back(point.str());
        } else {
          set<size_t>::iterator setIter;
          for(setIter = (bestAlignment.alignedToT[j]).begin(); setIter != (bestAlignment.alignedToT[j]).end(); setIter++) {
            int sourcePos = *setIter;
            //phraseTableFile << sourcePos << "-" << j << " ";
            std::stringstream point;
            point << sourcePos << "-" << j;
            alignment.push_back(point.str());
          }
        }
      }
      // now print all alignments, sorted by source index
      sort(alignment.begin(), alignment.end());
      for (size_t i = 0; i < alignment.size(); ++i) {
        phraseTableFile << alignment[i] << " ";
      }
    } else if (wordAlignmentFlag) {
      // alignment info in pb model
      for(size_t j=0; j<bestAlignment.alignedToT.size(); j++) {
        const set< size_t > &aligned = bestAlignment.alignedToT[j];
        for (set< size_t >::const_iterator p(aligned.begin()); p != aligned.end(); ++p) {
          phraseTableFile << *p << "-" << j << " ";
        }
      }
    }

  // counts

  phraseTableFile << "|||";
  //sparse feature
  set<string>::iterator iter = features.begin();
  while (iter != features.end()) {
	  phraseTableFile << " " << *iter << " 1";
	  iter++;
  }


  phraseTableFile << endl;
}

void loadclass( map<string,string>& classmap, const string &fileName )
{
  cerr << "Loading lexical translation table from " << fileName;
  ifstream inFile;
  inFile.open(fileName.c_str());
  if (inFile.fail()) {
    cerr << " - ERROR: could not open file\n";
    exit(1);
  }
  istream *inFileP = &inFile;

  char line[LINE_MAX_LENGTH];

  int i=0;
  while(true) {
    i++;
    if (i%100000 == 0) cerr << "." << flush;
    SAFE_GETLINE((*inFileP), line, LINE_MAX_LENGTH, '\n', __FILE__);
    if (inFileP->eof()) break;

    vector<string> token = tokenize( line );
    if (token.size() != 2) {
      cerr << "line " << i << " in " << fileName
           << " has wrong number of tokens, skipping:\n"
           << token.size() << " " << token[0] << " " << line << endl;
      continue;
    }

   classmap[token[0]] = token[1];
  }
  inFile.close();
  cerr << endl;
}


std::pair<PhrasePairGroup::Coll::iterator,bool> PhrasePairGroup::insert ( const PhraseAlignmentCollection& obj )
{
  std::pair<iterator,bool> ret = m_coll.insert(obj);

  if (ret.second) {
    // obj inserted. Also add to sorted vector
    const PhraseAlignmentCollection &insertedObj = *ret.first;
    m_sortedColl.push_back(&insertedObj);
  }

  return ret;
}

//! convert string to variable of type T. Used to reading floats, int etc from files
template<typename T>
inline T Scan(const std::string &input)
{
  std::stringstream stream(input);
  T ret;
  stream >> ret;
  return ret;
}


//! speeded up version of above
template<typename T>
inline void Scan(std::vector<T> &output, const std::vector< std::string > &input)
{
  output.resize(input.size());
  for (size_t i = 0 ; i < input.size() ; i++) {
    output[i] = Scan<T>( input[i] );
  }
}


inline void Tokenize(std::vector<std::string> &output
                     , const std::string& str
                     , const std::string& delimiters = " \t")
{
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    output.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}

inline void TokenizeSparse(std::vector<std::string> &output
                     , const std::string& str
                     , const std::string& delimiters = " \t")
{
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters,0);
    // Found a token, add it to the vector.
    output.push_back(str.substr(0, pos));
    //
    std::string::size_type startpos = pos + delimiters.length();
    pos = str.find_first_of(delimiters,startpos);
    output.push_back(str.substr(startpos, pos-startpos));

    pos = pos + delimiters.length();
    output.push_back(str.substr(pos, str.length()-pos));
    // Skip delimiters.  Note the "not_of"

}

// speeded up version of above
template<typename T>
inline void Tokenize( std::vector<T> &output
                      , const std::string &input
                      , const std::string& delimiters = " \t")
{
  std::vector<std::string> stringVector;
  Tokenize(stringVector, input, delimiters);
  return Scan<T>(output, stringVector );
}

// read in a phrase pair and store it
void FeaturePhraseAlignment::create( char line[], int lineID)
{
  assert(phraseS.empty());
  assert(phraseT.empty());

  vector< string > token = tokenize( line );
  int item = 1;
  for (size_t j=0; j<token.size(); j++) {
    if (token[j] == "|||") item++;
    else if (item == 1) { // source phrase
      phraseS.push_back( token[j] );
    }

    else if (item == 2) { // target phrase
      phraseT.push_back( token[j] );
    } else if (item == 3) { // alignment
      int s,t;
      sscanf(token[j].c_str(), "%d-%d", &s, &t);
      if ((size_t)t >= phraseT.size() || (size_t)s >= phraseS.size()) {
        cerr << "WARNING: phrase pair " << lineID
             << " has alignment point (" << s << ", " << t
             << ") out of bounds (" << phraseS.size() << ", " << phraseT.size() << ")\n";
      } else {
        // first alignment point? -> initialize
        createAlignVec(phraseS.size(), phraseT.size());

        // add alignment point
        alignedToT[t].insert( s );
        alignedToS[s].insert( t );
      }
    } else if (item == 4) { // optional sentence id
    	vector<string> onefeature;
    	TokenizeSparse(onefeature,token[j],":");
    	if (onefeature.size() != 3) {
    		cerr << "error feature format: " << token[j] << endl;
    		exit(1);
    	}
    	string f = onefeature[0]+onefeature[1];
		
		
		
    	if (onefeature[0] == "SRC") {
			map<string,string>::const_iterator iter = srcClassMap.find(onefeature[2]);
			//cerr << onefeature[0]<<"<:>"<<onefeature[1]<<endl;
			if (iter == srcClassMap.end()) {
				f += "+"+onefeature[2];
			} else {
				f += "+" + iter->second;
			}
    	} else if (onefeature[0] == "TGT") {
		    if (targetPOSFlag) {
				vector<string> factors;
				Tokenize(factors,onefeature[2],"|");
				if (factors.size() == 2) {
				  f += "+"+factors[1];
				} else {
				  f += "+"+onefeature[2];
				}
			} else {
			
				map<string,string>::const_iterator iter = tgtClassMap.find(onefeature[2]);
				//cerr << onefeature[0]<<"<:>"<<onefeature[1]<<endl;
				if (iter == tgtClassMap.end()) {
					f += "+"+onefeature[2];
				} else {
					f += "+" + iter->second;
				}
			}
    	} else {
    		cerr << "error feature format: " << token[j] << endl;
    		exit(1);
    	}
    	//cerr << lineID << f << endl;
    	features.insert(f);
    	//exit(1);
    }
  }

  createAlignVec(phraseS.size(), phraseT.size());
  count = 1;
}

void FeaturePhraseAlignment::createAlignVec(size_t sourceSize, size_t targetSize)
{
  // in case of no align info. always need align info, even if blank
  if (alignedToT.size() == 0) {
    size_t numTgtSymbols = (hierarchicalFlag ? targetSize-1 : targetSize);
    alignedToT.resize(numTgtSymbols);
  }

  if (alignedToS.size() == 0) {
    size_t numSrcSymbols = (hierarchicalFlag ? sourceSize-1 : sourceSize);
    alignedToS.resize(numSrcSymbols);
  }
}

void FeaturePhraseAlignment::clear()
{
  phraseS.clear();
  phraseT.clear();
  alignedToT.clear();
  alignedToS.clear();
}

// check if two word alignments between a phrase pair are the same
bool FeaturePhraseAlignment::equals( const FeaturePhraseAlignment& other )
{
  if (this == &other) return true;
  if (other.GetTarget() != GetTarget()) return false;
  if (other.GetSource() != GetSource()) return false;
  if (other.alignedToT != alignedToT) return false;
  if (other.alignedToS != alignedToS) return false;
  return true;
}

// check if two word alignments between a phrase pairs "match"
// i.e. they do not differ in the alignment of non-termimals
bool FeaturePhraseAlignment::match( const FeaturePhraseAlignment& other )
{
  if (this == &other) return true;
  if (other.GetTarget() != GetTarget()) return false;
  if (other.GetSource() != GetSource()) return false;
  if (!hierarchicalFlag) return true;

  assert(phraseT.size() == alignedToT.size() + 1);
  assert(alignedToT.size() == other.alignedToT.size());

  // loop over all words (note: 0 = left hand side of rule)
  for(size_t i=0; i<phraseT.size()-1; i++) {
    if (isNonTerminal( phraseT[i] )) {
      if (alignedToT[i].size() != 1 ||
          other.alignedToT[i].size() != 1 ||
          *(alignedToT[i].begin()) != *(other.alignedToT[i].begin()))
        return false;
    }
  }
  return true;
}

int FeaturePhraseAlignment::Compare(const FeaturePhraseAlignment &other) const
{
  if (this == &other) // comparing with itself
    return 0;

  if (GetTarget() != other.GetTarget())
    return ( GetTarget() < other.GetTarget() ) ? -1 : +1;

  if (GetSource() != other.GetSource())
    return ( GetSource() < other.GetSource() ) ? -1 : +1;

  if (!hierarchicalFlag)
    return 0;

  // loop over all words (note: 0 = left hand side of rule)
  for(size_t i=0; i<phraseT.size()-1; i++) {
    if (isNonTerminal( phraseT[i] )) {
      size_t thisAlign = *(alignedToT[i].begin());
      size_t otherAlign = *(other.alignedToT[i].begin());

      if (alignedToT[i].size() != 1 ||
          other.alignedToT[i].size() != 1 ||
          thisAlign != otherAlign) {
        int ret = (thisAlign < otherAlign) ? -1 : +1;
        return ret;
      }
    }
  }
  return 0;

}

bool FeaturePhraseAlignment::addFeatures( const FeaturePhraseAlignment& other) {
	set<string>::const_iterator iter = other.features.begin();

	while (iter != other.features.end()) {
		features.insert(*iter);
		iter++;
	}
}

