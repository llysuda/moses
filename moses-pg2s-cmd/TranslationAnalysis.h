// $Id$

/*
 * also see moses/SentenceStats
 */

#ifndef moses_pg2s_cmd_TranslationAnalysis_h
#define moses_pg2s_cmd_TranslationAnalysis_h

#include <iostream>
#include "moses/graph/Pg2sHypothesis.h"

namespace TranslationAnalysis
{

/***
 * print details about the translation represented in hypothesis to
 * os.  Included information: phrase alignment, words dropped, scores
 */
void PrintTranslationAnalysis(std::ostream &os, const Moses::Graph::Pg2sHypothesis* hypo);

}

#endif
