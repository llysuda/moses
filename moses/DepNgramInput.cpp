// $Id$

#include "DepNgramInput.h"
#include "StaticData.h"
#include "Util.h"
#include "XmlOption.h"
#include <map>
#include <set>
#include <algorithm>

using namespace std;

namespace Moses
{

void DepNgramInput::InitFid() {

  const StaticData& staticData = StaticData::Instance();
  bool exactBigram = staticData.GetExactBigram();
  bool directGraph = staticData.GetDirectGraph();
  bool onlyTree = staticData.GetOnlyTree();
  bool sibling = staticData.GetSibling();
  bool useType = staticData.GetUseLinkType();

	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("<s>");
	m_rel.push_back("<s>");

	for(size_t i = 0; i < GetSize(); i++) {
	  vector<int> dummy1;
	  vector<Edge> dummy2;
	  m_outgoings.push_back(dummy1);
	  m_incomings.push_back(dummy2);
	}

	vector< vector<int> > children;
	children.resize(GetSize());

	for(size_t i = 1; i < GetSize()-1; i++) {
		const Word& word = GetWord(i);
		const Factor* factor = word[2];
		CHECK(factor);
		int fid = Scan<int>(factor->GetString().as_string());
		if (fid >= 0) {
			fid++;
			children[fid].push_back(i);
		}

		m_fid.push_back(fid);
		m_pos.push_back(word.GetString(1).as_string());
		m_word.push_back(word.GetString(0).as_string());
		string rel = word.GetString(3).as_string();
		if (useType)
		  rel = "DP";
		m_rel.push_back(rel);

        if (fid >= 1) {
          Edge edge(fid, rel);
          m_outgoings[fid].push_back(i);
          m_incomings[i].push_back(edge);
        }
	}
	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("</s>");
	m_rel.push_back("</s>");

//	if (!sibling) {
//      if (!onlyTree) {
//        for(int i = 2; i < GetSize()-1; i++) {
//
//            int uid = i;
//            int nid = i-1;
//
//            Edge edge(uid, "BG");
//            m_incomings[nid].push_back(edge);
//            m_outgoings[uid].push_back(nid);
//        }
//      }
//	} else {
	if (!onlyTree) {
	  if (sibling) {
        for(size_t i = 1; i < GetSize()-1; i++) {
          for (size_t j = 1; j < children[i].size(); j++) {
            int uid = children[i][j];
            int nid = children[i][j-1];
            Edge edge(uid, "SG");
            m_incomings[nid].push_back(edge);
            m_outgoings[uid].push_back(nid);
          }
        }
	  } else {
	    for(int i = 2; i < GetSize()-1; i++) {
            int uid = i;
            int nid = i-1;
            Edge edge(uid, "BG");
            m_incomings[nid].push_back(edge);
            m_outgoings[uid].push_back(nid);
        }
	  }
	}
}

bool DepNgramInput::BigramConnected(int im1,int i) const {
  return std::find(m_outgoings[im1].begin(), m_outgoings[im1].end(), i) != m_outgoings[im1].end()
      || std::find(m_outgoings[i].begin(), m_outgoings[i].end(), im1) != m_outgoings[i].end();
}

bool DepNgramInput::NextIsOlder(int im1, int i) const {
  int level_im1 = GetLevel(im1);
  int level_i = GetLevel(i);
  if (level_im1<=level_i)
    return false;
  return true;
}

int DepNgramInput::GetLevel(int index) const {
  int ret = 0;
  while (m_fid[index] >=0 ) {
    index = m_fid[index];
    ret++;
  }
  return ret;
}

void DepNgramInput::ResetWords() {
	for(size_t i = 0; i < GetSize(); i++) {
		for (size_t j = 1; j <=3; j++)
			Phrase::SetFactor(i,j,NULL);
	}
}


std::string DepNgramInput::GetDepString(const WordsRange &range, const std::vector<std::pair<size_t, size_t> > &holes) const {
  std::vector<std::pair<const WordsRange*, const Word*> > hholes;
  for (size_t i = 0; i < holes.size(); i++) {
    hholes.push_back(make_pair(new WordsRange(holes[i].first, holes[i].second), new Word()));
  }
  return GetDepString(range, hholes);
}

std::string DepNgramInput::GetDepString(const WordsRange &range, const std::vector<std::pair<const WordsRange*, const Word*> > &holes) const
{
	const StaticData& staticData = StaticData::Instance();

	if (staticData.GetNoDep())
		return "";

	if (range.GetStartPos() < 1 || range.GetEndPos() >= GetSize()-1)
		return "";

	if (range.GetNumWordsCovered() == 1) {
		return "-1";
	}

	if ((staticData.GetOnlyTree() || staticData.GetSibling()) && (!IsConnected(range.GetStartPos(), range.GetEndPos())))
	  return "INVALID";

	// words sequence
	if (holes.size() == 0) {
		string ret = "";
		int startS = range.GetStartPos();
		int endS = range.GetEndPos();
		for (int i = startS; i <= endS; i++) {

		  const vector<Edge>& incomings = m_incomings[i];
          set<string> indices;
          for (int idx = 0; idx < incomings.size(); idx++) {
            int inID = incomings[idx].incoming;
            string rel = incomings[idx].relation;
            if (inID >= startS && inID <= endS) {
              string str = SPrint<int>(inID-startS);
              if (staticData.GetUseRelation()) {
                str += "."+rel;
              }
              indices.insert(str);
            }
          }
          string suffix = "";
          if (indices.size() == 0) {
             suffix = "-1";
          } else {
            vector<string> vindices(indices.begin(), indices.end());
            std::sort(vindices.begin(), vindices.end());
            for (int idx = 0; idx < vindices.size(); idx++) {
              suffix += vindices[idx]+"_";
            }
            suffix = suffix.erase(suffix.size()-1);
          }
          ret += suffix + ":";
		}
		return ret.erase(ret.size()-1);
	}

	// sequence with holes
	std::vector<std::pair<const WordsRange*, const Word*> >::const_iterator iterHoleList = holes.begin();
	assert(iterHoleList != holes.end());
	size_t startS = range.GetStartPos();
	size_t endS = range.GetEndPos();
	//
	bool hasMST = staticData.GetHasMST();

	map<size_t,size_t> newPos;
	int wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);

			for(size_t i = currPos; i <= hole.GetEndPos(); i++)
				newPos[i] = wordCount;
			currPos = hole.GetEndPos();
			++iterHoleList;

			if (hasMST && !HasMST(startS, endS, hole.GetStartPos(), hole.GetEndPos()))
			  return "INVALID";

		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
	}
	CHECK(iterHoleList == holes.end());

	iterHoleList = holes.begin();
	string out = "";
	wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		string edgeStr = "";
		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);

            set<string> indices;
            const vector<Edge>& incomings = GetSpanIncomings(currPos, hole.GetEndPos());
            for (int ii = 0; ii < incomings.size(); ii++) {
              int inID = incomings[ii].incoming;
              string rel = incomings[ii].relation;
              if (inID >= startS && inID <= endS) {
                string str = SPrint<int>(newPos[inID]);
                if (staticData.GetUseRelation()) {
                  str += "."+rel;
                }
                indices.insert(str);
              }
            }
            if (indices.size() == 0) {
              edgeStr = "-1";
            } else {
              vector<string> vindices(indices.begin(), indices.end());
              std::sort(vindices.begin(), vindices.end());
              for(int vi = 0; vi < vindices.size(); vi++) {
                edgeStr += vindices[vi] + "_";
              }
              edgeStr = edgeStr.erase(edgeStr.size()-1);
            }

			currPos = hole.GetEndPos();
			++iterHoleList;
		} else {
		  const vector<Edge>& incomings = m_incomings[currPos];
          //string edgeStr = "";
		  set<string> indices;
          for (int ii = 0; ii < incomings.size(); ii++) {
            int inID = incomings[ii].incoming;
            string rel = incomings[ii].relation;
            if (inID >= startS && inID <= endS) {
              string str = SPrint<int>(newPos[inID]);
              if (staticData.GetUseRelation()) {
                str += "."+rel;
              }
              indices.insert(str);
            }
          }
          if (indices.size() == 0) {
            edgeStr = "-1";
          } else {
            vector<string> vindices(indices.begin(), indices.end());
            std::sort(vindices.begin(), vindices.end());
            for(int vi = 0; vi < vindices.size(); vi++) {
              edgeStr += vindices[vi] + "_";
            }
            edgeStr = edgeStr.erase(edgeStr.size()-1);
          }
		}
		out += edgeStr + ":";
		wordCount++;
	}
	CHECK(out != "");
	return out.erase(out.size()-1);
}


void DepNgramInput::ExpandNonTerm() {
	size_t length = GetSize();
	m_sourceChart.resize(length);
	for (size_t pos = 0; pos < length; ++pos) {
		m_sourceChart[pos].resize(length - pos);
	}

	const StaticData& staticDataC = StaticData::Instance();
	bool mstLeaf = staticDataC.GetMSTLeaf();

	for (size_t start=1; start < length-1; start++){
		for (size_t end = start; end < length-1; end++) {

		  if (!IsConnected(start, end)) continue;

		  if (end > start) {
		    if (mstLeaf && !IsMSTLeaf(start, end))
		      continue;
		   }

		    vector<string> labels = GetSpanLabel(start,end, false);
            for (size_t i = 0; i < labels.size(); i++) {
                string label = labels[i];
                TreeInput::AddChartLabel(start, end, label,StaticData::Instance().GetInputFactorOrder());
            }
		}
	}
	for (size_t start=0; start <= length-1; start++){
		for (size_t end = start; end <= length-1; end++) {
			TreeInput::AddChartLabel(start, end, StaticData::Instance().GetInputDefaultNonTerminal()
																,StaticData::Instance().GetInputFactorOrder());
		}
	}

}


int DepNgramInput::GetSpanFid(size_t start, size_t end) const {
	set<size_t> fids;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size()>1)
	  return -1;
	CHECK(fids.size() == 1);
	return *(fids.begin());
}

vector<Edge> DepNgramInput::GetSpanIncomings(size_t start, size_t end) const {
  set<Edge, EdgeSetSortor> incomings;
  for(int i = start; i <= end; i++) {
      const vector<Edge>& ic = m_incomings[i];
      for(int j = 0; j < ic.size(); j++) {
        if (ic[j].incoming < start || ic[j].incoming > end)
          incomings.insert(ic[j]);
      }
  }
  vector<Edge> ret(incomings.begin(), incomings.end());
  //std::sort(ret.begin(),ret.end(), SORT::EdgeOrder);
  return ret;
}


vector<string> DepNgramInput::GetSpanLabel(size_t start, size_t end, bool boundary) const {

    if (StaticData::Instance().GetSingleNT()) {
        return vector<string>(1,"SNT");
    }

    bool boundaryNT = StaticData::Instance().GetBoundaryNT();
    bool graphNT = StaticData::Instance().GetGraphNT();

	vector<string> labels;
	  if (boundaryNT) {
	    labels.push_back(m_pos[start]);
	    if (end > start)
	      labels.push_back(m_pos[end]);
	  } else if (graphNT) {
	    for(size_t i = start; i <= end; i++) {
            const vector<Edge>& incomings = m_incomings[i];
            const vector<int>& outgoings = m_outgoings[i];

            set<int> edgeIDs(outgoings.begin(), outgoings.end());
            for(size_t ii = 0; ii < incomings.size(); ii++)
              edgeIDs.insert(incomings[ii].incoming);
            for (set<int>::iterator iter = edgeIDs.begin(); iter != edgeIDs.end(); iter++) {
              if (*iter < start || *iter > end) {
                labels.push_back(m_pos[i]);
                break;
              }
            }
        }
	    if (labels.size() == 0) {
	      labels.push_back("XX");
	    }
	  } else {
	    for(size_t i = start; i <= end; i++) {
          int fid = m_fid[i];
          if(fid < start || fid > end) {
              labels.push_back(m_pos[i]);
          }
	    }
	  }

	string ret = labels[0];
    for(size_t i = 1; i < labels.size(); i++)
        ret += "_" + labels[i];

	vector<string> retVec;
	retVec.push_back(ret);
	return retVec;
}


void DepNgramInput::DepthFirst(size_t start, size_t end, size_t current, set<int>& nodes) const {
  const vector<int>& outgoings = m_outgoings[current];
  const vector<Edge>& incomings = m_incomings[current];

  set<int> adjacent;
  for(size_t i = 0; i < outgoings.size(); i++) {
    int id = outgoings[i];
    if (id >= (int)start && id <= (int)end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }
  for (size_t i = 0; i < incomings.size(); i++) {
    int id = incomings[i].incoming;
    if (id >= (int)start && id <= (int)end && nodes.find(id) == nodes.end()) {
      adjacent.insert(id);
    }
  }

  for(set<int>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
    nodes.insert(*iter);
    DepthFirst(start, end, *iter, nodes);
  }
}

bool DepNgramInput::IsConnected(size_t start, size_t end) const {
  set<int> nodes;
  nodes.insert((int)start);
  DepthFirst(start, end, start, nodes);
  return nodes.size() == end-start+1;
}


bool DepNgramInput::IsMSTLeaf(size_t start, size_t end) const
{
  // if not incoming edges, return false
  size_t i = start;
  for (; i <= end; i++) {
    if (m_incomings[i].size() > 0)
      break;
  }
  if (i > end) return false;

  // in other nodes, at most one node has no incoming edges
  int count = 0;
  set<size_t> restNodes;
  size_t rootNode = 0;

  for (i = 1; i < start; i++) {
    restNodes.insert(i);
    if (m_incomings[i].size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }
  for (i = end+1; i < GetSize()-1; i++) {
    restNodes.insert(i);
    if (m_incomings[i].size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }
  // at last other nodes are connected
  if (count == 1) return IsDirectedConnected(rootNode, restNodes);
  for (set<size_t>::const_iterator iter = restNodes.begin();
       iter != restNodes.end(); ++iter) {
    size_t id = *iter;
    if (IsDirectedConnected(id, restNodes))
        return true;
  }
  return false;
}

bool DepNgramInput::HasMST(size_t start, size_t end) const
{

  // at most one node has no incoming edges
  int count = 0;
  set<size_t> restNodes;
  size_t rootNode = 0;

  for (size_t i = start; i <= end; i++) {
    restNodes.insert(i);
    if (m_incomings[i].size() == 0) {
      count++;
      rootNode = i;
    }
    if (count > 1) return false;
  }

  // at last other nodes are connected
  if (count == 1) return IsDirectedConnected(rootNode, restNodes);
  for (set<size_t>::const_iterator iter = restNodes.begin();
       iter != restNodes.end(); ++iter) {
    size_t id = *iter;
    if (IsDirectedConnected(id, restNodes))
        return true;
  }
  return false;
}

bool DepNgramInput::HasMST(int start, int end, int startHoleS, int endHoleS) const
{
  for(int i = start; i < startHoleS; i++) {
    int fid = m_fid[i];
    if (fid >= startHoleS && fid <= endHoleS)
      return true;
  }
  for(int i = startHoleS; i <= endHoleS; i++) {
    int fid = m_fid[i];
    if (fid >= start && fid <= end && fid < startHoleS && fid > endHoleS)
      return true;
  }
  for(int i = endHoleS+1; i <= end; i++) {
    int fid = m_fid[i];
    if (fid >= startHoleS && fid <= endHoleS)
      return true;
  }
  return false;
}


bool DepNgramInput::IsDirectedConnected(size_t rootNode, const std::set<size_t>& nodes) const
{
  set<size_t> retNodes;
  retNodes.insert(rootNode);
  DirectedDepthFirst(nodes, rootNode, retNodes);
  return retNodes.size() == nodes.size();
}

void DepNgramInput::DirectedDepthFirst(const std::set<size_t>& nodes, size_t current, std::set<size_t>& retNodes) const
{
  const vector<int>& outgoings = m_outgoings[current];

  set<size_t> adjacent;
  for(size_t i = 0; i < outgoings.size(); i++) {
    size_t id = (size_t)outgoings[i];
    if (nodes.find(id) != nodes.end() && retNodes.find(id) == retNodes.end()) {
      adjacent.insert(id);
    }
  }

  for(set<size_t>::const_iterator iter = adjacent.begin(); iter != adjacent.end(); iter++) {
    retNodes.insert(*iter);
    DirectedDepthFirst(nodes, *iter, retNodes);
  }
}

} // namespace

