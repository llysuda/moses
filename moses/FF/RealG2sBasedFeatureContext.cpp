#include "RealG2sBasedFeatureContext.h"
#include "moses/graph/RealGraphParser/RealG2sHypothesis.h"
#include "moses/graph/RealGraphParser/RealG2sManager.h"

namespace Moses
{
RealG2sBasedFeatureContext::RealG2sBasedFeatureContext
(const Moses::RealGraph::RealG2sHypothesis* hypothesis):
  m_hypothesis(hypothesis),
  m_targetPhrase(hypothesis->GetCurrTargetPhrase()),
  m_source(hypothesis->GetManager().GetSource())
{}

RealG2sBasedFeatureContext::RealG2sBasedFeatureContext(
  const TargetPhrase& targetPhrase,
  const InputType& source):
  m_hypothesis(NULL),
  m_targetPhrase(targetPhrase),
  m_source(source)
{}


}

