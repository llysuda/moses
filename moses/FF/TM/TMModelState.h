#pragma once

//#ifndef TMMODELSTATE_H_
//#define TMMODELSTATE_H_

#include "moses/FF/FFState.h"

namespace Moses
{

typedef std::pair<int,int> Range;

class TMModelState : public FFState {
private:
  std::vector<std::string> m_target;
  std::vector< Range > m_tmRangeK_i;
  bool m_nullTM;
public:
  TMModelState(std::vector<std::string> target, std::vector<Range> ranges, bool nullFlag)
    : m_target (target)
    , m_tmRangeK_i (ranges)
    , m_nullTM (nullFlag) {}
  std::vector<Range> GetRange() const {return m_tmRangeK_i;}
  std::vector<std::string> GetTargetPhrase() const {return m_target;}
  bool IsLastTMNULL() const {return m_nullTM;}
  int Compare(const FFState& other) const {
    const TMModelState &otherBase = static_cast<const TMModelState&>(other);
    //
    return 0;
  }

};

}

//#endif /* TMMODELSTATE_H_ */
