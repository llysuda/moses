#include "TMModel.h"
#include <assert.h>
#include "moses/InputType.h"
#include "moses/StaticData.h"
#include <climits>
#include "moses/InputFileStream.h"
#include "TMModelState.h"
#include <cfloat>
#include "TLSWrapper.h"

//#include "FNgramSpecs.h"

using namespace std;

namespace Moses
{
TMModel::TMModel(const string & line) : StatefulFeatureFunction("TMModel", line)
{
  //m_sentID = -1;
  //ReadParameters();
}

/*void TMModel::InitializeForInput(InputType const& source) {

  int m_sentID = source.GetTranslationId();
  vector<string> m_source;
  for (size_t i = 0; i < source.GetSize(); i++) {
   m_source.push_back(source.GetWord(i).GetString(0).as_string());
  }
  m_sourceCache[m_sentID] = m_source;
}*/

void TMModel::Load() {
  for (size_t i = 0; i < m_args.size(); ++i) {
    const vector<string> &args = m_args[i];

    if (args[0] == "tcm" || args[0] == "ltc" || args[0] == "cpm") {
      string factorFile = args[1];
      cerr << "load " << args[0] << " from " << args[1] << endl;
      FactoredVocab *vocab = new FactoredVocab;
      assert(vocab != 0);

      FNgramSpecs<FNgramCount>* fnSpecs = 0;
      File f(factorFile.c_str(),"r");
      fnSpecs = new FNgramSpecs<FNgramCount>(f,*vocab);
      if (!fnSpecs) {
        fprintf(stderr,"Error creating fnspecs object");
        exit(-1);
      }

      FNgramStats *factoredStats = new FNgramStats(*vocab, *fnSpecs);
      assert(factoredStats != 0);

      FNgram* fngramLM = new FNgram(*vocab,*fnSpecs);
      assert(fngramLM != 0);

      factoredStats->estimateDiscounts();
      factoredStats->computeCardinalityFunctions();
      factoredStats->sumCounts();

      fngramLM->read();

      m_fmodels[args[0]] = fngramLM;
      m_fvocabs[args[0]] = vocab;
      m_fstats[args[0]] = factoredStats;
      m_fspecs[args[0]] = fnSpecs;

      cerr << "end" << endl;

    } else if (args[0] == "tm") {
      m_tmPath = args[1];
      ReadTM();
    } else {
      throw "Unknown argument " + args[0];
    }
  }
}

FFState* TMModel::Evaluate(
        const Hypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const {


  const InputType & source = cur_hypo.GetManager().GetSource();
  const int m_sentID = source.GetTranslationId();
  vector<string> m_source;// = m_sourceCache.find(m_sentID)->second;
   for (size_t i = 0; i < source.GetSize(); i++) {
      m_source.push_back(source.GetWord(i).GetString(0).as_string());
   }

  TargetPhrase tp = cur_hypo.GetCurrTargetPhrase();
  vector<string> target;
  for (size_t i = 0; i < tp.GetSize(); i++) {
    target.push_back(tp.GetWord(i).GetString(0).as_string());
  }
  Range range(cur_hypo.GetCurrSourceWordsRange().GetStartPos(),cur_hypo.GetCurrSourceWordsRange().GetEndPos());

  //cerr << "enter TMModel evaluate for sent " << m_sentID << endl;
  //cerr << "\t for source range ( " << range.first << " " << range.second << " )" << endl;

  TMInstance fm = m_tm[m_sentID];
  Range tmSourceRange = fm.GetTMSourceRange(range);

  //cerr << "TM source Range: " << tmSourceRange.first << " " << tmSourceRange.second << endl;

  vector<Range> tmTargetRanges = fm.GetTMTargetRanges(tmSourceRange);

  //cerr << "TM target ranges size: " << tmTargetRanges.size() << endl;

  vector<Range> preRanges = static_cast <const TMModelState *> (prev_state) ->GetRange();
  vector<string> preTarget = static_cast <const TMModelState *> (prev_state) ->GetTargetPhrase();
  bool preNULL = static_cast <const TMModelState *> (prev_state) ->IsLastTMNULL();

  //cerr << "start extracting feature" << endl;

  std::map<Range, std::map<TargetPhrase, std::vector<string> > >::const_iterator fiter = fm.m_featureCache.find(range);
  if (fiter == fm.m_featureCache.end() || fiter->second.find(tp) == fiter->second.end()) {

    //cerr << "\t create feature cache" << endl;

    vector<string> tlFeatures = fm.CollectTLFeatures(target, range, m_source, tmSourceRange, tmTargetRanges);
    fm.m_featureCache[range][tp] = tlFeatures;
    // score tcm and ltcl
    //vector<float> tcmScores,ltcScores;

    //cerr << "\t create score cache" << endl;

    for (int i = 0; i < tlFeatures.size(); i++) {

      //cerr << "\t\t " << tlFeatures[i] << endl;
      float tcmScore,ltcScore;
      if (TMInstance::m_tcmScoreCache.find(tlFeatures[i])== TMInstance::m_tcmScoreCache.end()) {
        tcmScore = FLMScore("tcm",tlFeatures[i]);
        TMInstance::m_tcmScoreCache[tlFeatures[i]] = tcmScore;
      } else {
        tcmScore = TMInstance::m_tcmScoreCache[tlFeatures[i]];
      }
      if (TMInstance::m_ltcScoreCache.find(tlFeatures[i])== TMInstance::m_ltcScoreCache.end()) {
        ltcScore = FLMScore("ltc",tlFeatures[i]);
        TMInstance::m_ltcScoreCache[tlFeatures[i]] = ltcScore;
      } else {
        ltcScore = TMInstance::m_ltcScoreCache[tlFeatures[i]];
      }
      //tcmScores.push_back(tcmScore);
      //ltcScores.push_back(ltcScore);
      //cerr << "\t\t " << tcmScore << "\t\t " << ltcScore << endl;
    }

    //fm.m_tcmScoreCache[range][tp] = tcmScores;
    //fm.m_ltcScoreCache[range][tp] = ltcScores;
  }

  //cerr << "collect cpm" << endl;

  vector<string> cachedFeatures = fm.m_featureCache[range][tp];
  vector<vector<string> > cpm = fm.GetCPM(tmTargetRanges,target,preRanges,preTarget,preNULL);

  assert(cachedFeatures.size() == cpm.size());

  //cerr << "find max value cpm size : " << cpm.size() << endl;

  float maxScore = LONG_MIN;
  //cerr << "init maxScore: " << maxScore << endl;
  vector<float> maxProbs;
  vector<float> weights = StaticData::Instance().GetWeights(this);

  for (int i = 0; i < cachedFeatures.size(); i++) {
    float tcmScore = TMInstance::m_tcmScoreCache[cachedFeatures[i]];
    float ltcScore = TMInstance::m_ltcScoreCache[cachedFeatures[i]];

    //cerr << "tcm score: " << tcmScore << endl;
    //cerr << "ltc score: " << ltcScore << endl;

    for (int j = 0; j < cpm[i].size(); j++) {
      string fstring = cachedFeatures[i]+":CPM-"+cpm[i][j];
      vector<float> modelProb;

      if (TMInstance::m_scoreCache.find(fstring) != TMInstance::m_scoreCache.end()) {
        modelProb = TMInstance::m_scoreCache[fstring];
      } else {
        float cpmScore = FLMScore("cpm",fstring);
        modelProb.push_back(tcmScore);
        modelProb.push_back(ltcScore);
        modelProb.push_back(cpmScore);

        TMInstance::m_scoreCache[fstring] = modelProb;
      }

      float score = 0;
      for (int i = 0; i < weights.size(); i++) {
       score += modelProb[i]*weights[i];
      }
      if (score > maxScore) {
       maxScore = score;
       maxProbs = modelProb;
      }

    }
  }

assert (maxProbs.size() == 3);

  //cerr << " selecting the best one ..." << endl;

  accumulator->PlusEquals(this, maxProbs);

  //
  //cerr << " new state" << endl;
  FFState* retState = new TMModelState(target,tmTargetRanges.size()>0? tmTargetRanges:preRanges,tmTargetRanges.size()>0);
  return retState;

}


float TMModel::FLMScore(const std::string& type, const std::string & feature) const {
//cerr << feature << endl;
  char* feats = new char[feature.size()+1];
  strcpy(feats,feature.c_str());

  VocabString sentence[maxWordsPerLine + 1];
  unsigned int howmany;

  static TLSWC(WordMatrix, pplFileWordMatrixTLS);
  WordMatrix &wordMatrix = TLSW_GET(pplFileWordMatrixTLS);

  FactoredVocab* fvocab = m_fvocabs.at(type);
  FNgram* fmodel = m_fmodels.at(type);
  FNgramSpecs<FNgramCount>* fspec = m_fspecs.at(type);

  //for (unsigned specNum=0; specNum<fspec->fnSpecArray.size(); specNum++) {
  //  fspec->fnSpecArray[specNum].stats.reset();
  //}

  fvocab->parseWords(feats, sentence, maxWordsPerLine + 1);
  howmany = fspec->loadWordFactors(sentence,wordMatrix,maxWordsPerLine + 1);
  LogP prob = fmodel->sentenceProb(wordMatrix,howmany);

  return FloorScore(TransformLMScore(prob));
}

void TMModel::ReadTM() {
  std::string fileName = m_tmPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line(""), key("");
  std::cerr << "Loading TM into memory...";
  while(!getline(file, line).eof()) {
    if (line == "<START>") {
      vector<string> oneTM;
      for (int i = 0; i < 5; i++) {
        getline(file, line);
        oneTM.push_back(line);
      }

      TMInstance fm;
      fm.create(oneTM);
      m_tm.push_back(fm);

    } else {
      cerr << "error TM format" << endl;
      exit(-1);
    }
  }
  std::cerr << "done.\n";
}


FFState* TMModel::EvaluateChart(
        const ChartHypothesis& /* cur_hypo */,
        int /* featureID - used to index the state in the previous hypotheses */,
        ScoreComponentCollection* accumulator) const
{
  abort();
}

      //! return the state associated with the empty hypothesis for a given sentence
const FFState* TMModel::EmptyHypothesisState(const InputType &input) const {
  cerr << "TMModel::EmptyHypothesisState()" << endl;

  vector<string> target;
  vector<Range> tmTargetRanges;
  Range startRange(-1,-1);
  tmTargetRanges.push_back(startRange);
  bool nullFlag = false;
  return new TMModelState(target, tmTargetRanges, nullFlag);

}

bool TMModel::IsUseable(const FactorMask &mask) const {
        return true;
}

}




