#pragma once

#include <string>
#include <map>
#include <vector>
#include "FuzzyMatch.h"
#include "moses/FF/FFState.h"
#include "moses/Hypothesis.h"
#include "moses/ChartHypothesis.h"
#include "moses/InputType.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/FF/StatefulFeatureFunction.h"
#include "moses/TargetPhrase.h"
#include "moses/Manager.h"

#include "FNgramSpecs.h"
#include "FNgramStats.h"
#include "FactoredVocab.h"
#include "FNgram.h"
#include "wmatrix.h"
#include "Vocab.h"
#include "File.h"

namespace Moses {

class TMModel : public StatefulFeatureFunction
{

private:
    //int m_sentID;
    //std::vector<std::string> m_source;


	std::map<std::string, FNgram*> m_fmodels;
	std::map<std::string, FactoredVocab*> m_fvocabs;
	std::map<std::string, FNgramStats*> m_fstats;
	//std::vector<FNgramSpecsType> m_fspectypes;
	std::map<std::string, FNgramSpecs<FNgramCount>* > m_fspecs;
	std::string m_tmPath;
	std::vector< TMInstance > m_tm;
//	std::map<int, std::vector<std::string> > m_sourceCache;

public:
	TMModel(const std::string &line);

	void Load();
	void ReadTM();
	virtual FFState* Evaluate(
	    const Hypothesis& cur_hypo,
	    const FFState* prev_state,
	    ScoreComponentCollection* accumulator) const;
	virtual FFState* EvaluateChart(
	    const ChartHypothesis& /* cur_hypo */,
	    int /* featureID - used to index the state in the previous hypotheses */,
	    ScoreComponentCollection* accumulator) const;

	  //! return the state associated with the empty hypothesis for a given sentence
	  virtual const FFState* EmptyHypothesisState(const InputType &input) const;
	  virtual bool IsUseable(const FactorMask &mask) const;
//	  void InitializeForInput(InputType const& source);

	  float FLMScore(const std::string& type, const std::string & feature) const;


};

}
