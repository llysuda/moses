#include <boost/algorithm/string.hpp>

#include "SourceGapFeature.h"
#include "moses/AlignmentInfo.h"
#include "moses/TargetPhrase.h"
#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"
#include "util/string_piece_hash.hh"
#include "util/exception.hh"

#include "moses/graph/InputSubgraph.h"
#include "moses/graph/WordsSet.h"
#include "moses/graph/MultiDiGraphInput.h"
#include "moses/InputFileStream.h"
#include "moses/Util.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/G2sTranslationOption.h"
#include "moses/graph/G2sHypothesis.h"

#include "moses/graph/RealGraphParser/RealG2sHypothesis.h"

using namespace std;
using namespace Moses::Graph;
using namespace Moses::RealGraph;

namespace Moses
{

SourceGapFeature::SourceGapFeature(const std::string &line)
  :StatelessFeatureFunction("SourceGapFeature", 1, line)
{
  std::cerr << "Initializing SourceGapFeature.." << std::endl;
  ReadParameters();
}


void SourceGapFeature::Evaluate(const PhraseBasedFeatureContextPg2s& context,
                ScoreComponentCollection* accumulator) const
{
  const MultiDiGraph& source = static_cast<const MultiDiGraph&>(context.GetSource());
  const set<size_t>& range = context.GetTranslationOption().GetSourceWordsRange().GetSet();
  size_t prev = NOT_FOUND;
  bool flag = false;
  for(set<size_t>::const_iterator iter = range.begin(); iter != range.end(); iter++) {
    if (prev != NOT_FOUND && *iter != prev + 1) {
      flag = true;
      break;
    }
    prev = *iter;
  }
  if (flag)
    accumulator->PlusEquals(this, 1.0);
}

void SourceGapFeature::Evaluate(const PhraseBasedFeatureContextG2s& context,
                ScoreComponentCollection* accumulator) const
{
    const MultiDiGraph& source = static_cast<const MultiDiGraph&>(context.GetSource());
    const set<size_t>& range = context.GetTranslationOption().GetSourceWordsRange().GetSet();
    size_t prev = NOT_FOUND;
    bool flag = false;
    for(set<size_t>::const_iterator iter = range.begin(); iter != range.end(); iter++) {
      if (prev != NOT_FOUND && *iter != prev + 1) {
        flag = true;
        break;
      }
      prev = *iter;
    }
    if (flag)
      accumulator->PlusEquals(this, 1.0);
}

void SourceGapFeature::EvaluateRealG2s(const RealG2sBasedFeatureContext& context,
                  ScoreComponentCollection* accumulator) const
{
  const RealG2sHypothesis& hypo = context.GetHypothesis();
  const RealG2sWordsRange& range = hypo.GetCurrSourceWordsSet();
  if (range.GetStartPos() == 0)
      return;

  WordsBitmap bitmap = hypo.GetSourceCompleted();
  const vector<const RealG2sHypothesis*>& prevHypos = hypo.GetPrevHypos();
  CHECK(prevHypos.size() <= 2);
  for (size_t i = 0; i < prevHypos.size(); i++) {
    const RealG2sHypothesis* prev = prevHypos[i];
    const RealG2sWordsRange& prevRange = prev->GetCurrSourceWordsSet();
    size_t start = prevRange.GetStartPos();
    size_t end = prevRange.GetEndPos();
    CHECK (start > 0 && end >= start);
    bitmap.SetValue(start, end, true);
  }

  size_t total = bitmap.GetNumWordsCovered();
  size_t start=range.GetStartPos(), end=range.GetEndPos();
  CHECK(end >= start && end-start+1 >= total);
  size_t score = end-start+1-total;
  if (score > 0)
    accumulator->PlusEquals(this, -(int)score);
}

bool SourceGapFeature::IsUseable(const FactorMask &mask) const
{
  return true;
}

}
