#include "ReorderingCountFeature.h"
#include "moses/TargetPhrase.h"
#include "moses/ScoreComponentCollection.h"

using namespace std;

namespace Moses
{
ReorderingCountFeature::ReorderingCountFeature(const std::string &line)
  : StatelessFeatureFunction("ReorderingCountFeature",1, line)
{
  ReadParameters();
}

void ReorderingCountFeature::Evaluate(const Phrase &source
                                   , const TargetPhrase &targetPhrase
                                   , ScoreComponentCollection &scoreBreakdown
                                   , ScoreComponentCollection &estimatedFutureScore) const
{
  if (targetPhrase.GetSize()-targetPhrase.GetNumTerminals() == 2) {
    vector<size_t> ntIndex;
    const AlignmentInfo::NonTermIndexMap &targetPos2SourceInd = targetPhrase.GetAlignNonTerm().GetNonTermIndexMap();
    for (size_t i = 0; i < targetPhrase.GetSize(); i++) {
      if (targetPhrase.GetWord(i).IsNonTerminal()) {
        ntIndex.push_back(targetPos2SourceInd[i]);
        if (ntIndex.size() == 2)
          break;
      }
    }
    CHECK(ntIndex.size() == 2);
    if (ntIndex[0] > ntIndex[1])
      scoreBreakdown.Assign(this, 1);
  }
}

}

