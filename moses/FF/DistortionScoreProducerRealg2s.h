#pragma once

#include <stdexcept>
#include <string>
#include <set>
#include <vector>
#include "StatefulFeatureFunction.h"
#include "util/check.hh"

namespace Moses
{
class FFState;
class ScoreComponentCollection;
class Hypothesis;
class ChartHypothesis;
class WordsRange;

namespace RealGraph
{
class RealG2sHypothesis;
}

/** Calculates Distortion scores
 */
class DistortionScoreProducerRealg2s : public StatefulFeatureFunction
{
public:
  DistortionScoreProducerRealg2s(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }

//  static std::vector<float> CalculateDistortionScore(const Moses::Graph::Pg2sHypothesis& hypo,
//                                        int prevEnd, const std::set<size_t> &curr, bool naive);

  virtual const FFState* EmptyHypothesisState(const InputType &input) const;

  virtual FFState* Evaluate(
      const Hypothesis& cur_hypo,
      const FFState* prev_state,
      ScoreComponentCollection* accumulator) const {
  	throw std::logic_error("DistortionScoreProducerRealg2s not supported in pb decoder, yet");
  }

  virtual FFState* EvaluateChart(
    const ChartHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection*) const {
    throw std::logic_error("DistortionScoreProducerRealg2s not supported in chart decoder, yet");
  }

  virtual FFState* EvaluateRealG2s(
    const Moses::RealGraph::RealG2sHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection* accumulator) const;

//  static float CalculateDistortionScore(const Moses::Graph::Pg2sHypothesis& hypo,
//                                          const std::set<int> &prev, const std::set<int> &curr, const int FirstGapPosition);
//  virtual FFState* Evaluate(
//        const Moses::Graph::Pg2sHypothesis& cur_hypo,
//        const FFState* prev_state,
//        ScoreComponentCollection* accumulator) const;

};
}

