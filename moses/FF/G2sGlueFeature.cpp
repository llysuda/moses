
#include "G2sGlueFeature.h"
#include "FFState.h"
//#include "moses/WordsRange.h"
#include "moses/StaticData.h"
#include "moses/graph/G2sHypothesis.h"
#include "moses/graph/MultiDiGraphInput.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
//struct G2sGlueState : public FFState {
//  string label;
////  WordsSet range;
//  G2sGlueState() : label("") {}
//  G2sGlueState(const string& l) : label(l) {}
//  int Compare(const FFState& other) const {
//    const G2sGlueState& o =
//      static_cast<const G2sGlueState&>(other);
//    return 0;
////      return label.compare(o.label);
//  }
//};

G2sGlueFeature::G2sGlueFeature(const std::string &line)
  : StatelessFeatureFunction("G2sGlueFeature", 2, line)
{
  ReadParameters();
}

void G2sGlueFeature::Evaluate(const PhraseBasedFeatureContextG2s& context,
                ScoreComponentCollection* accumulator) const
{
  vector<float> scores(2, 0.0f);
  const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(context.GetSource());
  const WordsBitmap& bitmap = context.GetHypothesis()->GetWordsBitmap();
  set<size_t> restRange, range;
  for(size_t i = 0; i < bitmap.GetSize(); i++) {
    if (!bitmap.GetValue(i))
      restRange.insert(i);
    else range.insert(i);
  }
  if (range.size() == 0 || graph.is_connected(range))
    scores[0] = 1.0;
  if (restRange.size() == 0 || graph.is_connected(restRange))
    scores[1] = 1.0;

  accumulator->PlusEquals(this, scores);
}

//const FFState* G2sGlueFeature::EmptyHypothesisState(const InputType &input) const
//{
//  return new G2sGlueState();
//}
//
//FFState* G2sGlueFeature::Evaluate(
//  const G2sHypothesis& hypo,
//  const FFState* prev_state,
//  ScoreComponentCollection* out) const
//{
////  const G2sGlueState* s = static_cast<const G2sGlueState*>(prev_state);
////  const MultiDiGraph& source = static_cast<const MultiDiGraph&>(hypo.GetInput());
////  string label = source.GetLabel(hypo.GetCurrSourceWordsRange().GetSet());
//
//  if (hypo.GetTranslationOption().GetSourceLHS() == "X#X")
//    out->PlusEquals(this, 1.0f);
//
//  return new G2sGlueState();
//}


}

