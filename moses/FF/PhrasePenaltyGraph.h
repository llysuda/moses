#pragma once

#include "StatelessFeatureFunction.h"

namespace Moses
{

class PhrasePenaltyGraph : public StatelessFeatureFunction
{
public:
  PhrasePenaltyGraph(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }

  virtual void Evaluate(const InputType &input
      , const Moses::Graph::InputSubgraph &inputPath
      , ScoreComponentCollection &scoreBreakdown) const;
  virtual void Evaluate(const InputType &input
          , const std::set<size_t> &inputPath
          , ScoreComponentCollection &scoreBreakdown) const;
};

} //namespace

