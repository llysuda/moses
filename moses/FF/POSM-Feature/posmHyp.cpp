#include "posmHyp.h"
#include <sstream>

using namespace std;
using namespace lm::ngram;

namespace Moses
{
posmState::posmState(const State & val)
  :j(0)
  ,E(0)
{
  lmState = val;

}

void posmState::saveState(int jVal, int eVal, map <int , string> & gapVal)
{
  gap.clear();
  gap = gapVal;
  j = jVal;
  E = eVal;
}

int posmState::Compare(const FFState& otherBase) const
{
  const posmState &other = static_cast<const posmState&>(otherBase);
  if (j != other.j)
    return (j < other.j) ? -1 : +1;
  if (E != other.E)
    return (E < other.E) ? -1 : +1;
  if (gap != other.gap)
    return (gap < other.gap) ? -1 : +1;

  if (lmState.length < other.lmState.length) return -1;

  if (lmState.length > other.lmState.length) return 1;

  return 0;
}


std::string posmState :: getName() const
{

  return "done";
}

//////////////////////////////////////////////////

posmHypothesis :: posmHypothesis()
{
  opProb = 0;
  gapWidth = 0;
  gapCount = 0;
  openGapCount = 0;
  deletionCount = 0;
  gapCount = 0;
  j = 0;
  E = 0;
  gap.clear();
}

void posmHypothesis :: setState(const FFState* prev_state)
{

  if(prev_state != NULL) {

    j = static_cast <const posmState *> (prev_state)->getJ();
    E =  static_cast <const posmState *> (prev_state)->getE();
    gap = static_cast <const posmState *> (prev_state)->getGap();
    lmState = static_cast <const posmState *> (prev_state)->getLMState();
  }
}

posmState * posmHypothesis :: saveState()
{

  posmState * statePtr = new posmState(lmState);
  statePtr->saveState(j,E,gap);
  return statePtr;
}

int posmHypothesis :: isTranslationOperation(int x)
{
  if (operations[x].find("_JMP_BCK_") != -1)
    return 0;

  if (operations[x].find("_JMP_FWD_") != -1)
    return 0;

  if (operations[x].find("_CONT_CEPT_") != -1)
    return 0;

  if (operations[x].find("_INS_GAP_") != -1)
    return 0;

  return 1;

}

void posmHypothesis :: removeReorderingOperations()
{
  gapCount = 0;
  deletionCount = 0;
  openGapCount = 0;
  gapWidth = 0;
  //cout<<"I came here"<<endl;

  std::vector <std::string> tupleSequence;

  for (int x = 0; x < operations.size(); x++) {
    // cout<<operations[x]<<endl;

    if(isTranslationOperation(x) == 1) {
      tupleSequence.push_back(operations[x]);
    }

  }

  operations.clear();
  operations = tupleSequence;
}

void posmHypothesis :: calculateOSMProb(Model & ptrOp)
{

  opProb = 0;
  State currState = lmState;
  State temp;

  for (int i = 0; i<operations.size(); i++) {
    temp = currState;
    opProb += ptrOp.Score(temp,ptrOp.GetVocabulary().Index(operations[i]),currState);
  }

  lmState = currState;

  //print();
}


int posmHypothesis :: firstOpenGap(vector <int> & coverageVector)
{

  int firstOG =-1;

  for(int nd = 0; nd < coverageVector.size(); nd++) {
    if(coverageVector[nd]==0) {
      firstOG = nd;
      return firstOG;
    }
  }

  return firstOG;

}

string posmHypothesis :: intToString(int num)
{

  std::ostringstream stm;
  stm<<num;

  return stm.str();

}

void posmHypothesis :: generateOperations(int & startIndex , int j1 , int contFlag , WordsBitmap & coverageVector , string english , string german , set <int> & targetNullWords , vector <string> & currF)
{

  int gFlag = 0;
  int gp = 0;
  int ans;


  if ( j < j1) { // j1 is the index of the source word we are about to generate ...
    //if(coverageVector[j]==0) // if source word at j is not generated yet ...
    if(coverageVector.GetValue(j)==0) { // if source word at j is not generated yet ...
      operations.push_back("_INS_GAP_");
      gFlag++;
      gap[j]="Unfilled";
    }
    if (j == E) {
      j = j1;
    } else {
      operations.push_back("_JMP_FWD_");
      j=E;
    }
  }

  if (j1 < j) {
    // if(j < E && coverageVector[j]==0)
    if(j < E && coverageVector.GetValue(j)==0) {
      operations.push_back("_INS_GAP_");
      gFlag++;
      gap[j]="Unfilled";
    }

    j=closestGap(gap,j1,gp);
    operations.push_back("_JMP_BCK_"+ intToString(gp));

    //cout<<"I am j "<<j<<endl;
    //cout<<"I am j1 "<<j1<<endl;

    if(j==j1)
      gap[j]="Filled";
  }

  if (j < j1) {
    operations.push_back("_INS_GAP_");
    gap[j] = "Unfilled";
    gFlag++;
    j=j1;
  }

  if(contFlag == 0) { // First words of the multi-word cept ...

    if(english == "_TRANS_SLF_") { // Unknown word ...
      operations.push_back("_TRANS_SLF_");
    } else {
      operations.push_back("_TRANS_" + english + "_TO_" + german);
    }

    //ans = firstOpenGap(coverageVector);
    ans = coverageVector.GetFirstGapPos();

    if (ans != -1)
      gapWidth += j - ans;

  } else if (contFlag == 2) {

    operations.push_back("_INS_" + german);
    ans = coverageVector.GetFirstGapPos();

    if (ans != -1)
      gapWidth += j - ans;
    deletionCount++;
  } else {
    //operations.push_back("_CONT_CEPT_");
  }

  //coverageVector[j]=1;
  coverageVector.SetValue(j,1);
  j+=1;

  if(E<j)
    E=j;

  if (gFlag > 0)
    gapCount++;

  openGapCount += getOpenGaps();

  //if (coverageVector[j] == 0 && targetNullWords.find(j) != targetNullWords.end())
  if (coverageVector.GetValue(j) == 0 && targetNullWords.find(j) != targetNullWords.end()) {
    j1 = j;
    german = currF[j1-startIndex];
    english = "_INS_";
    generateOperations(startIndex, j1, 2 , coverageVector , english , german , targetNullWords , currF);
  }

}

void posmHypothesis :: print()
{
  for (int i = 0; i< operations.size(); i++) {
    cerr<<operations[i]<<" ";

  }

  cerr<<endl<<endl;

  cerr<<"Operation Probability "<<opProb<<endl;
  cerr<<"Gap Count "<<gapCount<<endl;
  cerr<<"Open Gap Count "<<openGapCount<<endl;
  cerr<<"Gap Width "<<gapWidth<<endl;
  cerr<<"Deletion Count "<<deletionCount<<endl;

  cerr<<"_______________"<<endl;
}

int posmHypothesis :: closestGap(map <int,string> gap, int j1, int & gp)
{

  int dist=1172;
  int value=-1;
  int temp=0;
  gp=0;
  int opGap=0;

  map <int,string> :: iterator iter;

  iter=gap.end();

  do {
    iter--;
    //cout<<"Trapped "<<iter->first<<endl;

    if(iter->first==j1 && iter->second== "Unfilled") {
      opGap++;
      gp = opGap;
      return j1;

    }

    if(iter->second =="Unfilled") {
      opGap++;
      temp = iter->first - j1;

      if(temp<0)
        temp=temp * -1;

      if(dist>temp && iter->first < j1) {
        dist=temp;
        value=iter->first;
        gp=opGap;
      }
    }


  } while(iter!=gap.begin());

  return value;
}



int posmHypothesis :: getOpenGaps()
{
  map <int,string> :: iterator iter;

  int nd = 0;
  for (iter = gap.begin(); iter!=gap.end(); iter++) {
    if(iter->second == "Unfilled")
      nd++;
  }

  return nd;

}

void posmHypothesis :: generateDeleteOperations(std::string english, int currTargetIndex, std::set <int> doneTargetIndexes)
{

  operations.push_back("_DEL_" + english);
  currTargetIndex++;

  while(doneTargetIndexes.find(currTargetIndex) != doneTargetIndexes.end()) {
    currTargetIndex++;
  }

  if (sourceNullWords.find(currTargetIndex) != sourceNullWords.end()) {
    english = currE[currTargetIndex];
    generateDeleteOperations(english,currTargetIndex,doneTargetIndexes);
  }

}

void posmHypothesis :: computeOSMFeature(int startIndex , WordsBitmap & coverageVector)
{

  set <int> doneTargetIndexes;
  set <int> eSide;
  set <int> fSide;
  set <int> :: iterator iter;
  string english;
  string source;
  int j1;
  int start = 0;
  int targetIndex = 0;
  doneTargetIndexes.clear();


  if (targetNullWords.size() != 0) { // Source words to be deleted in the start of this phrase ...
    iter = targetNullWords.begin();

    if (*iter == startIndex) {

      j1 = startIndex;
      source = currF[j1-startIndex];
      english = "_INS_";
      generateOperations(startIndex, j1, 2 , coverageVector , english , source , targetNullWords , currF);
    }
  }

  if (sourceNullWords.find(targetIndex) != sourceNullWords.end()) { // first word has to be deleted ...
    english = currE[targetIndex];
    generateDeleteOperations(english,targetIndex, doneTargetIndexes);
  }


  for (int i = 0; i < ceptsInPhrase.size(); i++) {
    source = "";
    english = "";

    fSide = ceptsInPhrase[i].first;
    eSide = ceptsInPhrase[i].second;

    iter = eSide.begin();
    targetIndex = *iter;
    english += currE[*iter];
    iter++;

    for (; iter != eSide.end(); iter++) {
      if(*iter == targetIndex+1)
        targetIndex++;
      else
        doneTargetIndexes.insert(*iter);

      english += "^_^";
      english += currE[*iter];
    }

    iter = fSide.begin();
    source += currF[*iter];
    iter++;

    for (; iter != fSide.end(); iter++) {
      source += "^_^";
      source += currF[*iter];
    }

    iter = fSide.begin();
    j1 = *iter + startIndex;
    iter++;

    generateOperations(startIndex, j1, 0 , coverageVector , english , source , targetNullWords , currF);


    for (; iter != fSide.end(); iter++) {
      j1 = *iter + startIndex;
      generateOperations(startIndex, j1, 1 , coverageVector , english , source , targetNullWords , currF);
    }

    targetIndex++; // Check whether the next target word is unaligned ...

    while(doneTargetIndexes.find(targetIndex) != doneTargetIndexes.end()) {
      targetIndex++;
    }

    if(sourceNullWords.find(targetIndex) != sourceNullWords.end()) {
      english = currE[targetIndex];
      generateDeleteOperations(english,targetIndex, doneTargetIndexes);
    }
  }

  //removeReorderingOperations();

  //print();

}

void posmHypothesis :: getMeCepts ( pair <int,int> & eSide , pair <int,int> & fSide , vector<vector <int> > & tS , vector<vector <int> > & sT)
{
	set <int> :: iterator iter;

  int startE = eSide.first;
  int endE = eSide.second;
  //int fz = fSide.second-fSide.first+1;
  vector <int> t;

  for (int i = eSide.first; i <= eSide.second; i++) {
    if (tS[i].size() > 0) {
	  t = tS[i];
	  for (int j = 0; j < t.size(); j++) {
		fSide.first = std::min(fSide.first,t[j]);
		fSide.second = std::max(fSide.second,t[j]);
	  }
    }
  }

  for (int i = fSide.first; i <= fSide.second; i++) {
    if (sT[i].size() > 0) {
      t = sT[i];
      for (int j = 0 ; j<t.size(); j++) {
        eSide.first = std::min(eSide.first,t[j]);
        eSide.second = std::max(eSide.second,t[j]);
      }
    }
  }

  if (eSide.second > endE || eSide.first < startE) {
    getMeCepts(eSide,fSide,tS,sT);
  }
}

int posmHypothesis :: setMin(const set<int> & nums) const
{
	int min = * nums.begin();
	set<int>::const_iterator iter;
	for(iter = nums.begin(); iter != nums.end(); iter++) {
		if (min > *iter) {
		  min = *iter;
		}
	}
	return min;
}

int posmHypothesis :: setMax(const set<int> & nums) const
{
	int max = * nums.begin();
	set<int>::const_iterator iter;
	for(iter = nums.begin(); iter != nums.end(); iter++) {
		if (max < *iter) {
		  max = *iter;
		}
	}
	return max;
}

bool posmHypothesis :: overlap( const pair < set <int> , set <int> >& pair1, const pair < set <int> , set <int> >& pair2) const
{
	int startF1 = setMin(pair1.first);
	int endF1 = setMax(pair1.first);
	int startF2 = setMin(pair2.first);
	int endF2 = setMax(pair2.first);
	
	if (!( startF1 > endF2 || startF2 > endF1)) {
		return true;
	}
	
	int startE1 = setMin(pair1.second);
	int endE1 = setMax(pair1.second);
	int startE2 = setMin(pair2.second);
	int endE2 = setMax(pair2.second);
	
	if (!( startE1 > endE2 || startE2 > endE1)) {
		return true;
	}
	
	return false;
}

void posmHypothesis :: constructCepts(vector <int> & align , int startIndex , int endIndex, int targetPhraseLength)
{

  std::vector <vector <int> > sT;
  std::vector <vector <int> > tS;
  std::set <int> eSide;
  std::set <int> fSide;
  std::pair <int, int> eSideSpan;
  std::pair <int, int> fSideSpan;
  std::set <int> :: iterator iter;
  std :: map <int , vector <int> > :: iterator iter2;
  std :: pair < set <int> , set <int> > cept;
  int src;
  int tgt;

  for (int i = 0; i< endIndex-startIndex+1; i++)
  {
	vector<int> dummy;
	sT.push_back(dummy);
  }

  for (int i = 0; i< targetPhraseLength; i++)
  {
	vector<int> dummy;
	tS.push_back(dummy);
  }

  for (int i = 0;  i < align.size(); i+=2) {
    src = align[i];
    tgt = align[i+1];
    tS[tgt].push_back(src);
    sT[src].push_back(tgt);
  }

  for (int i = startIndex; i<= endIndex; i++) { // What are unaligned source words in this phrase ...
    if (sT[i-startIndex].size() == 0) {
      targetNullWords.insert(i);
    }
  }

  for (int i = 0; i < targetPhraseLength; i++) { // What are unaligned target words in this phrase ...
    if (tS[i].size() == 0) {
      sourceNullWords.insert(i);
    }
  }

  int preEndE = -1;
  while (preEndE < targetPhraseLength-1)
  {
	int startE = preEndE + 1;
	if (tS[startE].size() == 0) {
		preEndE++;
		continue;
	}

    eSide.clear();
    fSide.clear();
	eSideSpan = make_pair(startE,startE);
	fSideSpan = make_pair(endIndex+1, -1);
    //eSide.insert (iter2->first);

    getMeCepts(eSideSpan, fSideSpan, tS , sT);
	
	for (int i = eSideSpan.first; i <= eSideSpan.second; i++) {
	  eSide.insert(i);
	  iter = sourceNullWords.find(i);
	  if (tS[i].size() == 0 && iter != sourceNullWords.end()) {
		sourceNullWords.erase(iter);
	  }
	}
	for (int i = fSideSpan.first; i <= fSideSpan.second; i++) {
	  fSide.insert(i);
	  iter = targetNullWords.find(i+startIndex);
	  if (sT[i].size() == 0 && iter != targetNullWords.end()) {
		targetNullWords.erase(iter);
	  }
	}

    cept = make_pair (fSide , eSide);
	vector < int > cache;
	for (int i = ceptsInPhrase.size()-1; i>= 0;  i--) {
	  if (overlap(ceptsInPhrase[i],cept)) {
		cache.push_back(i);
	  }
	}
	for (int i = 0; i < cache.size(); i++) {
	  ceptsInPhrase.erase(ceptsInPhrase.begin()+cache[i]);
	} 
	ceptsInPhrase.push_back(cept);
	
	preEndE = eSideSpan.second;
  }



  /*

  	  cerr<<"Extracted Cepts "<<endl;
  		for (int i = 0; i < ceptsInPhrase.size(); i++)
  			{

  				fSide = ceptsInPhrase[i].first;
  				eSide = ceptsInPhrase[i].second;

  				for (iter = eSide.begin(); iter != eSide.end(); iter++)
  				{
  			   		cerr<<*iter<<" ";
  				}
  			    	cerr<<"<---> ";

  				for (iter = fSide.begin(); iter != fSide.end(); iter++)
  				{
  					cerr<<*iter<<" ";
  				}

  				cerr<<endl;
  			}
  			cerr<<endl;

  		cerr<<"Unaligned Target Words"<<endl;

  		for (iter = sourceNullWords.begin(); iter != sourceNullWords.end(); iter++)
  			cerr<<*iter<<"<--->"<<endl;

  		cerr<<"Unaligned Source Words"<<endl;

  		for (iter = targetNullWords.begin(); iter != targetNullWords.end(); iter++)
  			cerr<<*iter<<"<--->"<<endl;

  */

}

void posmHypothesis :: populateScores(vector <float> & scores)
{
  scores.clear();
  scores.push_back(opProb);
  scores.push_back(gapWidth);
  scores.push_back(gapCount);
  scores.push_back(openGapCount);
  scores.push_back(deletionCount);
}


} // namespace

