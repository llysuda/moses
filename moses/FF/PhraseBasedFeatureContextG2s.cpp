#include "PhraseBasedFeatureContextG2s.h"
#include "moses/graph/G2sTranslationOption.h"
#include "moses/graph/G2sHypothesis.h"
#include "moses/graph/G2sManager.h"


using namespace Moses::Graph;

namespace Moses
{

PhraseBasedFeatureContextG2s::PhraseBasedFeatureContextG2s(const G2sHypothesis* hypothesis) :
  m_hypothesis(hypothesis),
  m_translationOption(m_hypothesis->GetTranslationOption()),
  m_source(m_hypothesis->GetManager().GetSource()) {}

PhraseBasedFeatureContextG2s::PhraseBasedFeatureContextG2s
(const G2sTranslationOption& translationOption, const InputType& source) :
  m_hypothesis(NULL),
  m_translationOption(translationOption),
  m_source(source)
{}

const TargetPhrase& PhraseBasedFeatureContextG2s::GetTargetPhrase() const
{
  return m_translationOption.GetTargetPhrase();
}

const WordsBitmap& PhraseBasedFeatureContextG2s::GetWordsBitmap() const
{
  if (!m_hypothesis) {
    throw std::logic_error("Coverage vector not available during pre-calculation");
  }
  return m_hypothesis->GetWordsBitmap();
}

}
