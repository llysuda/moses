#ifndef moses_HeadLinkFeature_h
#define moses_HeadLinkFeature_h

#include <map>
#include <string>

#include <stdexcept>
#include <boost/unordered_set.hpp>

#include "StatelessFeatureFunction.h"
#include "moses/Factor.h"
#include "moses/Sentence.h"

#include <maxent/maxentmodel.hpp>

namespace Moses
{

namespace Graph
{
class Pg2sHypothesis;
}

/**
  * Phrase pair feature: complete source/target phrase pair
  **/
class HeadLinkFeature: public StatelessFeatureFunction
{
	std::map<std::string, std::string> m_vocabE;
	std::map<std::string, std::string> m_classE;
	maxent::MaxentModel* m_model;

public:
  HeadLinkFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const;

  void Evaluate(const PhraseBasedFeatureContext& context,
                ScoreComponentCollection* accumulator) const;

  void EvaluateChart(const ChartBasedFeatureContext& context,
                     ScoreComponentCollection*) const {
    throw std::logic_error("HeadLinkFeature not valid in chart decoder");
  }

  void SetParameter(const std::string& key, const std::string& value);

  void ReadVocab(std::map<std::string, std::string>& vocab, const std::string& path);
	void ReadClass(std::map<std::string, std::string>& cls, const std::string& path);

	maxent::MaxentModel* ReadModel(const std::string& path);

};

}


#endif
