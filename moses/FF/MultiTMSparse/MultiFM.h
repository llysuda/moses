#pragma once

//#ifndef FUZZYMATCH_H_
//#define FUZZYMATCH_H_

#include <vector>
#include <string>
#include <map>
#include "MultiTMSparseState.h"
#include "moses/TargetPhrase.h"

namespace Moses {

class MultiFM {

public:

  std::map<Range, std::map<TargetPhrase, std::vector< std::string > > > m_featureCache;

public:

private:
  std::map<Range, Range> m_sourceRangeCache;
  std::map<Range, std::vector<Range> > m_targetRangesCache;

  std::vector<std::string> m_source;
  std::vector<std::string> m_target;
  std::vector<std::vector<int> > m_alignSoT;
  std::vector<int> m_alignCountT;
  std::vector<std::string> m_operations;
  float m_score;

public:
  MultiFM();
  ~MultiFM() {}

  std::string GetSourceWord(int index) {return m_source[index];}
  std::string GetTargetWord(int index) {return m_target[index];}
  int GetSourceLength() {return m_source.size();}

  float GetScore() const { return m_score;}
std::string deescape(const std::string& str) const;
  void create (const std::vector<std::string> & );
  void createAlign(std::string);
  Range GetTMSourceRange(const Range &);
  std::vector<Range> GetTMTargetRanges(const Range &);
  std::vector<std::string> CollectTLFeatures (const std::vector<std::string> & tp, const Range & range,const std::vector<std::string> & source,
      const Range & tmSourceRange, const std::vector<Range> & tmTargetRanges) const;
  std::string GetZ() const;
  std::string GetSCM(const Range & range, const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  std::string GetSPL(const Range &) const;
  std::string GetNLN(const Range &, const std::vector<std::string> &, const Range &) const;
  std::string GetSEP(const Range &, const std::vector<std::string> & ) const;
  std::string GetCSS(const std::vector<Range> & ranges) const;
  std::string GetLTC(int i, const std::vector<Range> & ranges) const;
  std::vector<std::string> GetLTC(const std::vector<Range> & ranges) const;
  std::string GetTCM(const std::vector<std::string> & tp, const std::vector<std::string> & tmTP) const;
  std::vector<std::string> GetTCM(const std::vector<std::string> & tp, const std::vector<Range> & ranges) const;
  std::vector<std::string> GetCPM(const Range & range, const std::vector<std::string> & tp, const std::vector<Range> & lastRanges, const std::vector<std::string> lastTP, bool isLastTMNULL) const;
  std::vector<std::string> GetCPM(const std::vector<Range> & ranges, const std::vector<std::string> & tp, const std::vector<Range> & lastRanges, const std::vector<std::string> lastTP, bool isLastTMNULL) const;

  std::string GetInterval(float score) const;
  bool IsPunct(std::string) const;
  int EditDistance(const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  float FuzzyScore (const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  int GetIndex(int index) const;
  int Src2Edit(int index) const;

  //void Cache(const TargetPhrase & tp, const std::vector<std::string> & target,
  //          const Range& range, const Range& tmSourceRange, const std::vector<Range>& tmTargetRanges);
  //std::vector<float> GetFeatureValue(const TargetPhrase & tp, const std::vector<std::string> & target,
  //    const Range& range, const std::vector<Range>& tmTargetRanges,
  //    const std::vector<Range>& preRanges, const std::vector<string> & preTarget, bool preNULL);
};

class MultiTM {

private:
  MultiFM* m_best;
  std::vector<MultiFM*> m_fms;
  std::map<int, std::map<int, int> > m_spans;

public:
  MultiTM() : m_best(NULL) {};
  ~MultiTM() {};
  void init_best_fm (const std::vector<std::string>&);
  void init_span_fm (const std::string&, const std::vector<std::string>&);

  MultiFM* GetSpanFM(Range& range);
  MultiFM* GetFM(int index);
  int GetSpanFMIndex(Range& range);
  std::vector<std::string> GetStatusFeatures() const;
  std::string GetFMSizeFeature() const;
  bool IsBestFM(MultiFM* fm) const;
  MultiFM* GetBestFM() {return m_best;}
};

} /* namespace Moses */
//#endif /* FUZZYMATCH_H_ */
