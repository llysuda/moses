#pragma once

//#ifndef TMSparseState_H_
//#define TMSparseState_H_

#include "moses/FF/FFState.h"

namespace Moses
{

typedef std::pair<int,int> Range;

class MultiTMSparseState : public FFState {
private:
  std::vector<std::string> m_target;
  std::vector< Range > m_tmRangeK_i;
  bool m_nullTM;
  int m_index;
public:
  MultiTMSparseState(int index, std::vector<std::string> target, std::vector<Range> ranges, bool nullFlag)
    : m_target (target)
    , m_tmRangeK_i (ranges)
    , m_nullTM (nullFlag)
    , m_index (index) {}

  int GetFMIndex() const {return m_index;}
  std::vector<Range> GetRange() const {return m_tmRangeK_i;}
  std::vector<std::string> GetTargetPhrase() const {return m_target;}
  bool IsLastTMNULL() const {return m_nullTM;}
  int Compare(const FFState& other) const {
    const MultiTMSparseState &otherBase = static_cast<const MultiTMSparseState&>(other);
    //
    return 0;
  }

};

}

//#endif /* TMSparseState_H_ */
