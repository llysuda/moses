#include "MultiTMSparse.h"
#include <assert.h>
#include "moses/InputType.h"
#include "moses/StaticData.h"
#include <climits>
#include "moses/InputFileStream.h"
#include "MultiTMSparseState.h"
#include <cfloat>
#include "moses/Util.h"

//#include "FNgramSpecs.h"

using namespace std;

namespace Moses
{
MultiTMSparse::MultiTMSparse(const string & line) : StatefulFeatureFunction("MultiTMSparse", 0, line)
{
}


void MultiTMSparse::Load() {
  for (size_t i = 0; i < m_args.size(); ++i) {
    const vector<string> &args = m_args[i];
    if (args[0] == "tm") {
      m_tmPath = args[1];
      ReadTM();
    } else if (args[0] == "vocab") {
      m_vocabPath = args[1];
      ReadVocab();
    } else if (args[0] == "classes") {
      m_classesPath = args[1];
      ReadClasses();
    }
    else {
      throw "Unknown argument " + args[0];
    }
  }
}

FFState* MultiTMSparse::Evaluate(
        const Hypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const {


  const InputType & source = cur_hypo.GetManager().GetSource();
  const int m_sentID = source.GetTranslationId();
  vector<string> m_source;// = m_sourceCache.find(m_sentID)->second;
   for (size_t i = 0; i < source.GetSize(); i++) {
      m_source.push_back(source.GetWord(i).GetString(0).as_string());
      //cerr << i << ":" << m_source.back() << endl;
   }

  TargetPhrase tp = cur_hypo.GetCurrTargetPhrase();
  vector<string> target;
  for (size_t i = 0; i < tp.GetSize(); i++) {
    target.push_back(tp.GetWord(i).GetString(0).as_string());
  }
  Range range(cur_hypo.GetCurrSourceWordsRange().GetStartPos(),cur_hypo.GetCurrSourceWordsRange().GetEndPos());


  vector<Range> preRanges = static_cast <const MultiTMSparseState *> (prev_state) ->GetRange();
  vector<string> preTarget = static_cast <const MultiTMSparseState *> (prev_state) ->GetTargetPhrase();
  bool preNULL = static_cast <const MultiTMSparseState *> (prev_state) ->IsLastTMNULL();
  int preFMIndex = static_cast <const MultiTMSparseState *> (prev_state) ->GetFMIndex();

  FFState* retState;

  MultiTM* tm = m_tm[m_sentID];

  MultiFM* bestFM = tm->GetBestFM();
  Range tmSourceRange_bfm;
  vector<Range> tmTargetRanges_bfm;
  string preword_best, nextword_best, firstword_best, lastword_best;

  string preword, nextword, firstword, lastword;
  if (range.first == 0) {
	preword = "<s>";
  } else {
	preword = m_source[range.first-1];
  }
  if (range.second == m_source.size()-1) {
	nextword = "</s>";
  } else {
	nextword = m_source[range.second+1];
  }
  firstword = m_source[range.first];
  lastword = m_source[range.second];
  
  if (bestFM == NULL) {
    accumulator->PlusEquals(this, "NO-BEST-FM",1.0);
    retState = new MultiTMSparseState(preFMIndex, preTarget, preRanges, preNULL);
    return retState;
  } else {
   tmSourceRange_bfm = bestFM->GetTMSourceRange(range);
   tmTargetRanges_bfm = bestFM->GetTMTargetRanges(tmSourceRange_bfm);
   std::map<Range, std::map<TargetPhrase, std::vector<std::string> > >::const_iterator fiter = bestFM->m_featureCache.find(range);
   if (fiter == bestFM->m_featureCache.end() || fiter->second.find(tp) == fiter->second.end()) {
     vector<string> tlFeatures = bestFM->CollectTLFeatures(target, range, m_source, tmSourceRange_bfm, tmTargetRanges_bfm);
     bestFM->m_featureCache[range][tp] = tlFeatures;
   }

   vector<string>& cachedFeatures = bestFM->m_featureCache[range][tp];
   for (size_t i = 0; i < cachedFeatures.size(); i++) {
     //if (cachedFeatures[i].find("Z") == std::string::npos &&
     //    cachedFeatures[i].find("SPL") == std::string::npos &&
     //    cachedFeatures[i].find("SEP") == std::string::npos )
       accumulator->PlusEquals(this, "BFM-"+cachedFeatures[i],1.0);
   }

   vector<string> cpm = bestFM->GetCPM(tmTargetRanges_bfm,target,preRanges,preTarget,preNULL);
   for (int i = 0; i < cpm.size(); i++) {
     accumulator->PlusEquals(this, "BFM-"+cpm[i],1.0);
   }

   retState = new MultiTMSparseState(-1, target, tmTargetRanges_bfm.size()>0? tmTargetRanges_bfm:preRanges, tmTargetRanges_bfm.size()>0);

if (m_classes.size() > 0) {

		for (size_t i = 0; i < target.size(); i++){
          if (bestFM->IsPunct(target[i])) {
            accumulator->PlusEquals(this, "TGT-PUN-CT",1.0);
          }
          if (GetVocab(target[i]) != "UNK") {
            accumulator->PlusEquals(this, "TGT-FW-CT",1.0);
          }
        }

    if (tmSourceRange_bfm.first < 0) {
      preword_best = nextword_best = firstword_best = lastword_best = "non";
    } else {
      if (tmSourceRange_bfm.first == 0) {
        preword_best = "START";
      } else {
        preword_best = bestFM->GetSourceWord(tmSourceRange_bfm.first-1);
      }
      if (tmSourceRange_bfm.second == bestFM->GetSourceLength()-1) {
        nextword_best = "END";
      } else {
        nextword_best = bestFM->GetSourceWord(tmSourceRange_bfm.second+1);
      }
      firstword_best = bestFM->GetSourceWord(tmSourceRange_bfm.first);
      lastword_best = bestFM->GetSourceWord(tmSourceRange_bfm.second);
    }

      //accumulator->PlusEquals(this, "VCB-BFM-"+GetVocab(preword_best)+"-"+GetVocab(firstword), 1.0);
	  //accumulator->PlusEquals(this, "VCB-BFM-"+GetVocab(lastword_best)+"-"+GetVocab(nextword), 1.0);
	  //accumulator->PlusEquals(this, "VCB-BFM-"+GetVocab(lastword)+"-"+GetVocab(nextword_best), 1.0);
	  //accumulator->PlusEquals(this, "VCB-BFM-"+GetVocab(preword)+"-"+GetVocab(firstword_best), 1.0);
	  accumulator->PlusEquals(this, "CLS-BFM-"+GetClasses(preword_best)+"-"+GetClasses(firstword), 1.0);
	  accumulator->PlusEquals(this, "CLS-BFM-"+GetClasses(lastword_best)+"-"+GetClasses(nextword), 1.0);
	  accumulator->PlusEquals(this, "CLS-BFM-"+GetClasses(lastword)+"-"+GetClasses(nextword_best), 1.0);
	  accumulator->PlusEquals(this, "CLS-BFM-"+GetClasses(preword)+"-"+GetClasses(firstword_best), 1.0);

  }
  }

  int index = tm->GetSpanFMIndex(range);
  MultiFM* fm = tm->GetFM(index);

  Range tmSourceRange;
  vector<Range> tmTargetRanges;

  if (fm == NULL) {
    accumulator->PlusEquals(this, "NO-SPAN-FM",1.0);
    //fm = tm->GetBestFM();
  } else {
    tmSourceRange = fm->GetTMSourceRange(range);
    tmTargetRanges = fm->GetTMTargetRanges(tmSourceRange);

    std::map<Range, std::map<TargetPhrase, std::vector<std::string> > >::const_iterator fiter = fm->m_featureCache.find(range);
    if (fiter == fm->m_featureCache.end() || fiter->second.find(tp) == fiter->second.end()) {
      vector<string> tlFeatures = fm->CollectTLFeatures(target, range, m_source, tmSourceRange, tmTargetRanges);
      fm->m_featureCache[range][tp] = tlFeatures;
    }

    vector<string>& cachedFeatures = fm->m_featureCache[range][tp];
    for (size_t i = 0; i < cachedFeatures.size(); i++) {
      if (cachedFeatures[i].find("SEP") == std::string::npos &&
          cachedFeatures[i].find("SPL") == std::string::npos)
        accumulator->PlusEquals(this, "CURR-"+cachedFeatures[i],1.0);
    }

    /*if (index == preFMIndex || index < -1) {
      vector<string> cpm = fm->GetCPM(tmTargetRanges,target,preRanges,preTarget,preNULL);
      for (int i = 0; i < cpm.size(); i++) {
        accumulator->PlusEquals(this, cpm[i],1.0);
      }
    } else {
      accumulator->PlusEquals(this, "NO-SPAN-CPM",1.0);
    }*/

    ///
    ///  between current FM and Best FM
    ///

    if (tm->IsBestFM(fm)) {
      accumulator->PlusEquals(this, "CURR-BEST-FM",1.0);
    }
	if (m_classes.size() > 0) {
		string preword_cur, nextword_cur, firstword_cur, lastword_cur;
		
		  if (tmSourceRange.first < 0) {
			preword_cur = nextword_cur = firstword_cur = lastword_cur = "non";
		  } else {
			if (tmSourceRange.first == 0) {
			  preword_cur = "START";
			} else {
			  preword_cur = fm->GetSourceWord(tmSourceRange.first-1);
			}
			if (tmSourceRange.second == fm->GetSourceLength()-1) {
			  nextword_cur = "END";
			} else {
			  nextword_cur = fm->GetSourceWord(tmSourceRange.second+1);
			}
		  }
		  firstword_cur = fm->GetSourceWord(tmSourceRange.first);
		  lastword_cur = fm->GetSourceWord(tmSourceRange.second);


		//accumulator->PlusEquals(this, "VCB-CUR-"+GetVocab(preword_cur)+"-"+GetVocab(firstword), 1.0);
		//  accumulator->PlusEquals(this, "VCB-CUR-"+GetVocab(lastword_cur)+"-"+GetVocab(nextword), 1.0);
		//  accumulator->PlusEquals(this, "VCB-CUR-"+GetVocab(lastword)+"-"+GetVocab(nextword_cur), 1.0);
		//  accumulator->PlusEquals(this, "VCB-CUR-"+GetVocab(preword)+"-"+GetVocab(firstword_cur), 1.0);
		accumulator->PlusEquals(this, "CLS-CUR-"+GetClasses(preword_cur)+"-"+GetClasses(firstword), 1.0);
		accumulator->PlusEquals(this, "CLS-CUR-"+GetClasses(lastword_cur)+"-"+GetClasses(nextword), 1.0);
		accumulator->PlusEquals(this, "CLS-CUR-"+GetClasses(lastword)+"-"+GetClasses(nextword_cur), 1.0);
		accumulator->PlusEquals(this, "CLS-CUR-"+GetClasses(preword)+"-"+GetClasses(firstword_cur), 1.0);
	}

    //retState = new MultiTMSparseState(index, target, tmTargetRanges.size()>0? tmTargetRanges:preRanges, tmTargetRanges.size()>0);
    
  }

  //vector<string> statusFeatures = tm->GetStatusFeatures();
  //for (size_t i = 0; i < statusFeatures.size(); i++) {
    //accumulator->PlusEquals(this, statusFeatures[i],1.0);
  //}

  
  return retState;

}


void MultiTMSparse::ReadTM() {
  std::string fileName = m_tmPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading TM into memory...";
  while(!getline(file, line).eof()) {
    if (line == "<sent>") {
      //cerr << "sent" << endl;
      MultiTM* tm = new MultiTM();
      bool fm_flag = false, span_flag = false;
      while (!getline(file, line).eof()) {
        if (line == "</sent>") {
          //cerr << "end sent" << endl;
          m_tm.push_back(tm);
          break;
        } else if (line == "<fm>") {
          fm_flag = true;
        } else if (line == "</fm>") {
          fm_flag = false;
        } else if (line == "<span>") {
          span_flag = true;
        } else if (line == "</span>") {
          span_flag = false;
        } else {
          if (fm_flag) {
            //cerr << "fm" << endl;
            vector<string> fmlines;
            fmlines.push_back(line);
            for (int i = 1; i < 5; i++) {
              getline(file,line);
              fmlines.push_back(line);
            }
            tm->init_best_fm(fmlines);
          } else if (span_flag) {
            //cerr << "span" << endl;
		    string spans = line;
            vector<string> fmlines;
            for (int i = 0; i < 5; i++) {
              getline(file,line);
              fmlines.push_back(line);
            }
            tm->init_span_fm(spans, fmlines);
          }
        }
      }
    } else {
      std::cerr << "error TM format" << endl;
      exit(1);
    }
  }
  std::cerr << "done.\n";
}

void MultiTMSparse::ReadVocab() {
  std::string fileName = m_vocabPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading vocab into memory...";
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 3);
    m_vocab[items[1]] = items[2];
  }
  std::cerr << "done.\n";
}

void MultiTMSparse::ReadClasses() {
  std::string fileName = m_classesPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading classes into memory...";
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 2);
    m_classes[items[0]] = items[1];
  }
  std::cerr << "done.\n";
}

FFState* MultiTMSparse::EvaluateChart(
        const ChartHypothesis& /* cur_hypo */,
        int /* featureID - used to index the state in the previous hypotheses */,
        ScoreComponentCollection* accumulator) const
{
  abort();
}

      //! return the state associated with the empty hypothesis for a given sentence
const FFState* MultiTMSparse::EmptyHypothesisState(const InputType &input) const {
  cerr << "MultiTMSparse::EmptyHypothesisState()" << endl;

  vector<string> target;
  vector<Range> tmTargetRanges;
  Range startRange(-1,-1);
  tmTargetRanges.push_back(startRange);
  bool nullFlag = false;
  return new MultiTMSparseState(-2, target, tmTargetRanges, nullFlag);

}

bool MultiTMSparse::IsUseable(const FactorMask &mask) const {
        return true;
}

}




