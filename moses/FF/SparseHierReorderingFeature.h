#ifndef moses_SparseHierReorderingFeature_h
#define moses_SparseHierReorderingFeature_h

#include <map>
#include <string>
#include <set>

#include <stdexcept>
#include <boost/unordered_set.hpp>
#include <boost/functional/hash.hpp>

#include "StatefulFeatureFunction.h"
#include "moses/Factor.h"
#include "moses/Sentence.h"
#include "FFState.h"

namespace Moses
{

namespace Graph
{
class InputSubgraph;
}
class WordsRange;
class WordsBitmap;

class SparseHierReorderingFeatureState : public FFState
{
public:
  bool m_first;
  WordsRange m_prevRange;
  WordsBitmap m_coverage;
public:
  SparseHierReorderingFeatureState(const SparseHierReorderingFeatureState *prev, const WordsRange &range);
  SparseHierReorderingFeatureState(size_t size);
  int Compare(const FFState& o) const;

  std::string GetOrientationTypeMSLR(WordsRange currRange, WordsBitmap coverage) const;
  std::string GetOrientationTypeMSLRO(WordsRange currRange, WordsBitmap coverage) const;
};

/**
  * Phrase pair feature: complete source/target phrase pair
  **/
class SparseHierReorderingFeature: public StatefulFeatureFunction
{
  typedef boost::unordered_map<std::string,
      std::string, boost::hash<std::string> > VocabType;

  VocabType m_vocab;
  VocabType m_class;
	bool m_naive;

public:
  SparseHierReorderingFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const;

  FFState* Evaluate(
      const Hypothesis& cur_hypo,
      const FFState* prev_state,
      ScoreComponentCollection* accumulator) const {return NULL;};

  FFState* Evaluate(
      const Moses::Graph::Pg2sHypothesis& cur_hypo,
      const FFState* prev_state,
      ScoreComponentCollection* accumulator) const;

  FFState* Evaluate(
        const Moses::Graph::G2sHypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const;

  FFState* EvaluateChart(
    const ChartHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection* accumulator) const {return NULL;};

  const FFState* EmptyHypothesisState(const InputType &input) const;

  void SetParameter(const std::string& key, const std::string& value);

  void ReadVocab(VocabType& vocab, const std::string& path);
	void ReadClass(VocabType& cls, const std::string& path);

  std::string GetVocab(const std::string & word) const {
		VocabType::const_iterator iter = m_vocab.find(word);
		if (iter == m_vocab.end() && m_vocab.size() > 0) {
			return "UNK";
		} else {
			return word;
		}
	}

	std::string GetClass(const std::string & word) const {
		VocabType::const_iterator iter = m_class.find(word);
		if (iter == m_class.end()) {
			return "0";
		} else {
			return iter->second;
		}
	}


};

}


#endif
