
#include "GapCountFeature.h"
#include "moses/TargetPhrase.h"
#include "moses/Word.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/graph/G2sHypothesis.h"
#include "moses/graph/MultiDiGraphInput.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
GapCountFeature::GapCountFeature(const std::string &line)
  : StatelessFeatureFunction("GapCountFeature",2, line)
{
  ReadParameters();
}

void GapCountFeature::Evaluate(const PhraseBasedFeatureContextG2s& context,
                ScoreComponentCollection* accumulator) const
{
  vector<float> scores(2, 0.0f);
  const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(context.GetSource());
  size_t nstart = context.GetWordsBitmap().GetFirstGapPos();
  size_t nend = context.GetWordsBitmap().GetLastGapPos();

  if (nstart == NOT_FOUND || nend == NOT_FOUND)
    return;

  int start = context.GetHypothesis()->GetCurrSourceWordsRange().GetStartPos();
  int end = context.GetHypothesis()->GetCurrSourceWordsRange().GetEndPos();

  if (end < nstart) scores[0] = 1.0f;
  if (start > nend) scores[1] = 1.0f;
//  if (start > nstart && end < nend) scores[2] = 1.0f;

  accumulator->PlusEquals(this, scores);
}


//void GapCountFeature::Evaluate(const Phrase &source
//                             , const TargetPhrase &targetPhrase
//                             , ScoreComponentCollection &scoreBreakdown
//                             , ScoreComponentCollection &estimatedFutureScore) const
//{
//  float count = 0.0f;
//  for (size_t i = 0; i < targetPhrase.GetSize(); i++)
//    if (targetPhrase.GetWord(i).IsNonTerminal())
//      count += 1.0;
//  scoreBreakdown.Assign(this, count);
//}

} // namespace

