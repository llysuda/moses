#pragma once

#include <stdexcept>
#include <string>
#include <set>
#include "StatelessFeatureFunction.h"
#include "util/check.hh"

namespace Moses
{
class FFState;
class ScoreComponentCollection;
class Hypothesis;
class ChartHypothesis;
class WordsRange;

namespace Graph
{
class G2sHypothesis;
}

/** Calculates Distortion scores
 */
class G2sGlueFeature : public StatelessFeatureFunction
{
public:
  G2sGlueFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }
  void Evaluate(const PhraseBasedFeatureContextG2s& context,
                    ScoreComponentCollection* accumulator) const;

//  virtual const FFState* EmptyHypothesisState(const InputType &input) const;
//
//  virtual FFState* Evaluate(
//    const Moses::Graph::G2sHypothesis& cur_hypo,
//    const FFState* prev_state,
//    ScoreComponentCollection* accumulator) const;
//
//  virtual FFState* Evaluate(
//      const Hypothesis& cur_hypo,
//      const FFState* prev_state,
//      ScoreComponentCollection* accumulator) const {
//  	throw std::logic_error("G2sGlueFeature not supported in pb decoder, yet");
//  }
//
//  virtual FFState* EvaluateChart(
//    const ChartHypothesis& /* cur_hypo */,
//    int /* featureID - used to index the state in the previous hypotheses */,
//    ScoreComponentCollection*) const {
//    throw std::logic_error("G2sGlueFeature not supported in chart decoder, yet");
//  }

//  static float CalculateDistortionScore(const Moses::Graph::G2sHypothesis& hypo,
//                                          const std::set<int> &prev, const std::set<int> &curr, const int FirstGapPosition);
//  virtual FFState* Evaluate(
//        const Moses::Graph::G2sHypothesis& cur_hypo,
//        const FFState* prev_state,
//        ScoreComponentCollection* accumulator) const;

};
}

