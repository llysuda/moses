#include <sstream>

#include "G2sOrderModel.h"
#include "moses/FF/FFState.h"
//#include "LexicalReorderingState.h"
#include "moses/StaticData.h"
#include "moses/graph/G2sHypothesis.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
G2sOrderModel::G2sOrderModel(const std::string &line)
  : StatelessFeatureFunction("G2sOrderModel", 3, line)
{
  std::cerr << "Initializing G2sOrderModel.." << std::endl;

  for (size_t i = 0; i < m_args.size(); ++i) {
    const vector<string> &args = m_args[i];

    if (args[0] == "input-factor") {
      m_factorsF =Tokenize<FactorType>(args[1]);
    } else if (args[0] == "output-factor") {
      m_factorsE =Tokenize<FactorType>(args[1]);
    } else if (args[0] == "path") {
      m_filePath = args[1];
    } else {
      throw "Unknown argument " + args[0];
    }
  }

//  switch(m_configuration->GetCondition()) {
//  case LexicalReorderingConfiguration::FE:
//  case LexicalReorderingConfiguration::E:
//    if(m_factorsE.empty()) {
//      throw "TL factor mask for lexical reordering is unexpectedly empty";
//    }
//    if(m_configuration->GetCondition() == LexicalReorderingConfiguration::E)
//      break; // else fall through
//  case LexicalReorderingConfiguration::F:
//    if(m_factorsF.empty()) {
//      throw "SL factor mask for lexical reordering is unexpectedly empty";
//    }
//    break;
//  default:
//    throw "Unknown conditioning option!";
//  }
}

G2sOrderModel::~G2sOrderModel()
{
  if(m_table)
    delete m_table;
//  delete m_configuration;
}

void G2sOrderModel::Load()
{
  m_table = LexicalReorderingTable::LoadAvailable(m_filePath, m_factorsF, m_factorsE, std::vector<FactorType>());
}

Scores G2sOrderModel::GetProb(const Phrase& f, const Phrase& e) const
{
  return m_table->GetScore(f, e, Phrase(ARRAY_SIZE_INCR));
}

void G2sOrderModel::Evaluate(const PhraseBasedFeatureContextG2s& context,
                ScoreComponentCollection* accumulator) const
{
  Scores score(GetNumScoreComponents(), 0);
  const MultiDiGraph& graph = static_cast<const MultiDiGraph&>(context.GetSource());
  size_t nstart = context.GetWordsBitmap().GetFirstGapPos();
  size_t nend = context.GetWordsBitmap().GetLastGapPos();

  if (nstart == NOT_FOUND || nend == NOT_FOUND)
    return;

  int start = context.GetHypothesis()->GetCurrSourceWordsRange().GetStartPos();
  int end = context.GetHypothesis()->GetCurrSourceWordsRange().GetEndPos();

  const Scores& fullScore = context.GetTranslationOption().GetG2sOrderModelScores();
  if (fullScore.size() == 0) return;

  if (start < (int)nstart) score[0] = fullScore[0];
  else if (end > (int)nend) score[1] = fullScore[1];
  else score[2] = fullScore[2];

  accumulator->PlusEquals(this, score);
}

//FFState* G2sOrderModel::Evaluate(const Pg2sHypothesis& hypo,
//                                     const FFState* prev_state,
//                                     ScoreComponentCollection* out) const
//{
//  Scores score(GetNumScoreComponents(), 0);
//  const LexicalReorderingState *prev = dynamic_cast<const LexicalReorderingState *>(prev_state);
//  LexicalReorderingState *next_state = prev->Expand(hypo.GetTranslationOption(), score);
//
//  out->PlusEquals(this, score);
//
//  return next_state;
//}
//
//FFState* G2sOrderModel::Evaluate(const G2sHypothesis& hypo,
//                                     const FFState* prev_state,
//                                     ScoreComponentCollection* out) const
//{
//  Scores score(GetNumScoreComponents(), 0);
//  const LexicalReorderingState *prev = dynamic_cast<const LexicalReorderingState *>(prev_state);
//  LexicalReorderingState *next_state = prev->Expand(hypo.GetTranslationOption(), score);
//
//  out->PlusEquals(this, score);
//
//  return next_state;
//}
//
//FFState* G2sOrderModel::Evaluate(const Hypothesis& hypo,
//                                     const FFState* prev_state,
//                                     ScoreComponentCollection* out) const
//{
//  Scores score(GetNumScoreComponents(), 0);
//  const LexicalReorderingState *prev = dynamic_cast<const LexicalReorderingState *>(prev_state);
//  LexicalReorderingState *next_state = prev->Expand(hypo.GetTranslationOption(), score);
//
//  out->PlusEquals(this, score);
//
//  return next_state;
//}

//const FFState* G2sOrderModel::EmptyHypothesisState(const InputType &input) const
//{
//  return m_configuration->CreateLexicalReorderingState(input);
//}

bool G2sOrderModel::IsUseable(const FactorMask &mask) const
{
  for (size_t i = 0; i < m_factorsE.size(); ++i) {
    const FactorType &factor = m_factorsE[i];
    if (!mask[factor]) {
      return false;
    }
  }
  return true;

}
}

