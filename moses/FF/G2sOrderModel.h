#ifndef moses_G2sOrderModel_h
#define moses_G2sOrderModel_h

#include <string>
#include <vector>
#include "moses/Factor.h"
#include "moses/Phrase.h"
#include "moses/TypeDef.h"
#include "moses/Util.h"
#include "moses/WordsRange.h"

#include "moses/LexicalReorderingTable.h"
#include "StatelessFeatureFunction.h"
#include "moses/graph/MultiDiGraphInput.h"


namespace Moses
{

class Factor;
class Phrase;
class Hypothesis;
class InputType;

//namespace Graph
//{
//class Pg2sHypothesis;
//class G2sHypothesis;
//}

/** implementation of lexical reordering (Tilman ...) for phrase-based decoding
 */
class G2sOrderModel : public StatelessFeatureFunction
{
public:
  G2sOrderModel(const std::string &line);
  virtual ~G2sOrderModel();
  void Load();

  virtual bool IsUseable(const FactorMask &mask) const;

//  virtual const FFState* EmptyHypothesisState(const InputType &input) const;

  void InitializeForInput(const InputType& i) {
    m_table->InitializeForInput(i);
  }

  Scores GetProb(const Phrase& f, const Phrase& e) const;

  void Evaluate(const PhraseBasedFeatureContextG2s& context,
                        ScoreComponentCollection* accumulator) const;

private:
  bool DecodeCondition(std::string s);
  bool DecodeDirection(std::string s);
  bool DecodeNumFeatureFunctions(std::string s);

//  LexicalReorderingConfiguration *m_configuration;
//  std::string m_modelTypeString;
//  std::vector<std::string> m_modelType;
  LexicalReorderingTable* m_table;
  //std::vector<Direction> m_direction;
//  std::vector<LexicalReorderingConfiguration::Condition> m_condition;
  //std::vector<size_t> m_scoreOffset;
  //bool m_oneScorePerDirection;
  std::vector<FactorType> m_factorsE, m_factorsF;
  std::string m_filePath;
};

}

#endif
