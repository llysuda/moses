#include <boost/algorithm/string.hpp>

#include "HeadLinkFeature.h"
#include "moses/AlignmentInfo.h"
#include "moses/TargetPhrase.h"
#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"
#include "util/string_piece_hash.hh"
#include "util/exception.hh"

#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/InputFileStream.h"
#include "moses/Util.h"

using namespace std;
using namespace maxent;
using namespace Moses::Graph;

namespace Moses
{

HeadLinkFeature::HeadLinkFeature(const std::string &line)
  :StatelessFeatureFunction("HeadLinkFeature", 1, line)
{
  std::cerr << "Initializing HeadLinkFeature.." << std::endl;
  ReadParameters();
}

void HeadLinkFeature::SetParameter(const std::string& key, const std::string& value)
{
	if (key == "vocabe") {
		ReadVocab(m_vocabE, value);
	} else if (key == "classe") {
		ReadClass(m_classE, value);
	} else if (key == "model") {
		m_model = ReadModel(value);
  } else {
    StatelessFeatureFunction::SetParameter(key, value);
  }
}

void HeadLinkFeature::Evaluate(
  const PhraseBasedFeatureContext& context,
  ScoreComponentCollection* accumulator) const
{
	//TODO
}

bool HeadLinkFeature::IsUseable(const FactorMask &mask) const
{
  return true;
}

maxent::MaxentModel* HeadLinkFeature::ReadModel(const std::string& path) {
  maxent::MaxentModel* model = new MaxentModel();
  cerr << "loading "<< path << " ... ";
  model->load(path);
  cerr << "end" << endl;
  return model;
}

void HeadLinkFeature::ReadVocab(std::map<std::string, std::string>& vocab, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading vocab into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 3);
    vocab[items[1]] = items[2];
  }
  std::cerr << "done.\n";
}


void HeadLinkFeature::ReadClass(std::map<std::string, std::string>& cls, const std::string& path) {
	string fileName = path;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading classes into memory..." << path << endl;
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 2);
    cls[items[0]] = items[1];
  }
  std::cerr << "done.\n";
}


}
