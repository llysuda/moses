#ifndef moses_SourceGapFeature_h
#define moses_SourceGapFeature_h

#include <map>
#include <string>
#include <set>

#include <stdexcept>
#include <boost/unordered_set.hpp>
#include <boost/functional/hash.hpp>

#include "StatelessFeatureFunction.h"
#include "moses/Factor.h"
#include "moses/Sentence.h"

namespace Moses
{

namespace Graph
{
class InputSubgraph;
}

/**
  * Phrase pair feature: complete source/target phrase pair
  **/
class SourceGapFeature: public StatelessFeatureFunction
{

public:
  SourceGapFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const;

  void Evaluate(const PhraseBasedFeatureContextPg2s& context,
                ScoreComponentCollection* accumulator) const;
  void Evaluate(const PhraseBasedFeatureContextG2s& context,
                  ScoreComponentCollection* accumulator) const;
  void EvaluateRealG2s(const RealG2sBasedFeatureContext& context,
                    ScoreComponentCollection* accumulator) const;


};

}


#endif
