#ifndef moses_GraphSegFeature_h
#define moses_GraphSegFeature_h

#include <map>
#include <string>
#include <set>

#include <stdexcept>
#include <boost/unordered_set.hpp>
#include <boost/functional/hash.hpp>

#include "StatelessFeatureFunction.h"
#include "moses/Factor.h"
#include "moses/Sentence.h"

namespace Moses
{

namespace Graph
{
class InputSubgraph;
}

/**
  * Phrase pair feature: complete source/target phrase pair
  **/
class GraphSegFeature: public StatelessFeatureFunction
{
  typedef boost::unordered_map<std::string,
      std::string, boost::hash<std::string> > VocabType;

  VocabType m_vocab;
  VocabType m_class;
	bool m_enableClass;
	bool m_enableVocab;

public:
  GraphSegFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const;

  void Evaluate(const PhraseBasedFeatureContextPg2s& context,
                ScoreComponentCollection* accumulator) const;
  void Evaluate(const PhraseBasedFeatureContextG2s& context,
                  ScoreComponentCollection* accumulator) const;

  void SetParameter(const std::string& key, const std::string& value);

  void ReadVocab(VocabType& vocab, const std::string& path);
	void ReadClass(VocabType& cls, const std::string& path);

  std::string GetVocab(const std::string & word) const {
		VocabType::const_iterator iter = m_vocab.find(word);
		if (iter == m_vocab.end() && m_vocab.size() > 0) {
			return "UNK";
		} else {
			return word;
		}
	}

	std::string GetClass(const std::string & word) const {
		VocabType::const_iterator iter = m_class.find(word);
		if (iter == m_class.end()) {
			return "0";
		} else {
			return iter->second;
		}
	}


};

}


#endif
