#pragma once

#include "StatelessFeatureFunction.h"

namespace Moses
{

class GraphSimFeature : public StatelessFeatureFunction
{
public:
  GraphSimFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }

  virtual void Evaluate(const InputType &input
      , const Moses::Graph::InputSubgraph &inputPath
      , ScoreComponentCollection &scoreBreakdown) const;
};

} //namespace

