#include <fstream>
#include "DecisionModel.h"
//#include "dmHyp.h"
#include "util/check.hh"
#include "moses/Util.h"

using namespace std;
using namespace lm::ngram;

namespace Moses
{

dmState::dmState(const State & val)
{
  lmState = val;
}

int dmState::Compare(const FFState& otherBase) const
{
  const dmState &other = static_cast<const dmState&>(otherBase);
  if (lmState.length < other.lmState.length) return -1;

  if (lmState.length > other.lmState.length) return 1;

  return 0;
}


std::string dmState :: getName() const
{

  return "done";
}

DecisionModel::DecisionModel(const std::string &line)
  :StatefulFeatureFunction("DecisionModel", 1, line )
{
  ReadParameters();
}

void DecisionModel :: readLanguageModel(const char *lmFile)
{

  string unkOp = "_TRANS_SLF_";
  OSM = new Model(m_lmPath.c_str());
  State startState = OSM->NullContextState();
  State endState;
  unkOpProb = OSM->Score(startState,OSM->GetVocabulary().Index(unkOp),endState);
}


void DecisionModel::Load()
{

  readLanguageModel(m_lmPath.c_str());

}



//void DecisionModel:: Evaluate(const Phrase &source
//                                , const TargetPhrase &targetPhrase
//                                , ScoreComponentCollection &scoreBreakdown
//                                , ScoreComponentCollection &estimatedFutureScore) const
//{
//
//  osmHypothesis obj;
//  obj.setState(OSM->NullContextState());
//  WordsBitmap myBitmap(source.GetSize());
//  vector <string> mySourcePhrase;
//  vector <string> myTargetPhrase;
//  vector<float> scores(5);
//  vector <int> alignments;
//  int startIndex = 0;
//  int endIndex = source.GetSize();
//
//  const AlignmentInfo &align = targetPhrase.GetAlignTerm();
//  AlignmentInfo::const_iterator iter;
//
//
//  for (iter = align.begin(); iter != align.end(); ++iter) {
//    alignments.push_back(iter->first);
//    alignments.push_back(iter->second);
//  }
//
//  for (int i = 0; i < targetPhrase.GetSize(); i++) {
//    if (targetPhrase.GetWord(i).IsOOV())
//      myTargetPhrase.push_back("_TRANS_SLF_");
//    else
//      myTargetPhrase.push_back(targetPhrase.GetWord(i).GetFactor(0)->GetString().as_string());
//  }
//
//  for (int i = 0; i < source.GetSize(); i++) {
//    mySourcePhrase.push_back(source.GetWord(i).GetFactor(0)->GetString().as_string());
//  }
//
//  obj.setPhrases(mySourcePhrase , myTargetPhrase);
//  obj.constructCepts(alignments,startIndex,endIndex-1,targetPhrase.GetSize());
//  obj.computeOSMFeature(startIndex,myBitmap);
//  obj.calculateOSMProb(*OSM);
//  obj.populateScores(scores);
//  estimatedFutureScore.PlusEquals(this, scores);
//
//}


FFState* DecisionModel::Evaluate(
  const Hypothesis& cur_hypo,
  const FFState* prev_state,
  ScoreComponentCollection* accumulator) const
{
//  cerr << " decision model.." << endl;

  const TargetPhrase &target = cur_hypo.GetCurrTargetPhrase();

  const Manager &manager = cur_hypo.GetManager();
  const InputType &source = manager.GetSource();
  const Sentence &sourceSentence = static_cast<const Sentence&>(source);
  //osmHypothesis obj;
  vector <string> mySourcePhrase;
  vector <string> myTargetPhrase;
  //vector<float> scores(5);
  float score;
  lm::ngram::State state = static_cast<const dmState *> (prev_state)->getLMState();

  const WordsRange & sourceRange = cur_hypo.GetCurrSourceWordsRange();
  int startIndex  = sourceRange.GetStartPos();
  int endIndex = sourceRange.GetEndPos();
  const AlignmentInfo &align = cur_hypo.GetCurrTargetPhrase().GetAlignTerm();
  //osmState * statePtr;

  //cerr << "source phrase" << endl;
    for (int i = startIndex; i <= endIndex; i++) {
      mySourcePhrase.push_back(source.GetWord(i).GetFactor(0)->GetString().as_string());
    }
    //cerr << mySourcePhrase.size() << endl;
  //cerr << "target phrase" << endl;
    for (int i = 0; i < target.GetSize(); i++) {

      //if (target.GetWord(i).IsOOV())
      //  return new dmState(state);
      //else
        myTargetPhrase.push_back(target.GetWord(i).GetFactor(0)->GetString().as_string());

    }
   // cerr << myTargetPhrase.size() << endl;

  vector<vector<int> > alignedToT;
  vector<vector<int> > alignedSoT;
  vector<int> alignedCountS;
  //cerr << "alignment" << endl;
  // alignment info
  for(size_t i=0; i<mySourcePhrase.size(); i++) {
    alignedCountS.push_back( 0 );
    vector< int > dummy;
    alignedSoT.push_back( dummy );
  }
  for(size_t i=0; i<myTargetPhrase.size(); i++) {
    vector< int > dummy;
    alignedToT.push_back( dummy );
  }

  AlignmentInfo::const_iterator iter;
  for (iter = align.begin(); iter != align.end(); ++iter) {
      //cerr << iter->first << "----" << iter->second << " ";
    int s = iter->first;
    int t = iter->second;
    alignedToT[t].push_back( s );
    alignedSoT[s].push_back( t );
    alignedCountS[s]++;
  }


//cerr << "collect phrase pairs" << endl;
  vector<vector<int> > phrasePairs;

   for(int startE=0; startE<myTargetPhrase.size(); startE++) {
     if (alignedToT[startE].size() > 0) {
       int endE = startE;
       int minF = myTargetPhrase.size()+1, maxF=-1;
       vector<int> usedF = alignedCountS;
       bool consistent = 1;

       for(; endE<myTargetPhrase.size(); endE++) {

         for (int m = 0; m < alignedToT[endE].size(); m++) {
           minF = std::min(minF, alignedToT[endE][m]);
           maxF = std::max(maxF, alignedToT[endE][m]);
           usedF[alignedToT[endE][m]]--;
         }

         for (int m = 0; m < alignedToT[endE].size(); m++) {
           int f = alignedToT[endE][m];
           for (int k = 0; k < alignedSoT[f].size();k++) {
             int e = alignedSoT[f][k];
             if (e < startE || e > endE) {
               consistent = false;
               break;
             }
           }
           if (!consistent) {
             break;
           }
         }

         if (consistent) {
            int idx = 0;
             for (; idx < phrasePairs.size(); idx++) {
               if (phrasePairs[idx][2] > maxF || phrasePairs[idx][3] < minF) {
                 continue;
               }
               startE = phrasePairs[idx][0];
               break;
             }
             for (int i = phrasePairs.size()-1 ;i >=  idx; i--) {
               vector<int> onePair = phrasePairs.back();
               minF = std::min(minF,onePair[2]);
               maxF = std::max(maxF,onePair[3]);
               phrasePairs.pop_back();
             }

             vector<int> onePair;
             onePair.push_back(startE);
             onePair.push_back(endE);
             onePair.push_back(minF);
             onePair.push_back(maxF);

             phrasePairs.push_back(onePair);
            break;
          }

       }

         startE = endE;
     }
   }
//cerr << "collect features" << endl;
   vector<string> decisionSequence;
   int preEndE=-1;
   for (int i = 0; i < phrasePairs.size(); i++) {
     int startE = phrasePairs[i][0];
     int endE = phrasePairs[i][1];
     int startF = phrasePairs[i][2];
     int endF = phrasePairs[i][3];

    // cerr << startF << " " << endF << " " << startE << " " << endE << endl;

     string srcPhrase = "",tgtPhrase = "";
     for (int f = startF; f <= endF; f++) {
       srcPhrase += mySourcePhrase[f]+"<>";
     }
     //cerr << srcPhrase << endl;
     for(int e = startE; e <= endE; e++) {
       tgtPhrase += myTargetPhrase[e]+"<>";
     }
     //cerr << tgtPhrase << endl;

     srcPhrase = srcPhrase.erase(srcPhrase.size()-2);
     tgtPhrase = tgtPhrase.erase(tgtPhrase.size()-2);

     //cerr << srcPhrase << "    " << tgtPhrase << endl;

     decisionSequence.push_back(srcPhrase+"++"+tgtPhrase);
     
     if (i > 0) {
       for (int j = preEndE+1; j < startE; j++) {
         decisionSequence.push_back(myTargetPhrase[j]);
       }
     }

     preEndE = endE;
   }

for (int j = preEndE+1; j < myTargetPhrase.size(); j++) {
  decisionSequence.push_back(myTargetPhrase[j]);
}

  //cerr<< "compute model score" <<endl;
  State currState = state;
  State temp;
score = 0.0;
  for (int i = 0; i<decisionSequence.size(); i++) {
    temp = currState;
    score += OSM->Score(temp,OSM->GetVocabulary().Index(decisionSequence[i]),currState);
  }

  //state = &currState;


  accumulator->PlusEquals(this, score);

  return new dmState(currState);

}

FFState* DecisionModel::EvaluateChart(
  const ChartHypothesis& /* cur_hypo */,
  int /* featureID - used to index the state in the previous hypotheses */,
  ScoreComponentCollection* accumulator) const
{
  abort();

}

const FFState* DecisionModel::EmptyHypothesisState(const InputType &input) const
{
  cerr << "DecisionModel::EmptyHypothesisState()" << endl;

  State startState = OSM->BeginSentenceState();

  return new dmState(startState);
}

std::string DecisionModel::GetScoreProducerWeightShortName(unsigned idx) const
{
  return "dm";
}

//std::vector<float> DecisionModel::GetFutureScores(const Phrase &source, const Phrase &target) const
//{
//  ParallelPhrase pp(source, target);
//  std::map<ParallelPhrase, Scores>::const_iterator iter;
//  iter = m_futureCost.find(pp);
////iter = m_coll.find(pp);
//  if (iter == m_futureCost.end()) {
//    vector<float> scores(5, 0);
//    scores[0] = unkOpProb;
//    return scores;
//  } else {
//    const vector<float> &scores = iter->second;
//    return scores;
//  }
//}

void DecisionModel::SetParameter(const std::string& key, const std::string& value)
{

  if (key == "path") {
    m_lmPath = value;
  } else if (key == "order") {
    lmOrder = Scan<int>(value);
  } else {
    StatefulFeatureFunction::SetParameter(key, value);
  }
}

bool DecisionModel::IsUseable(const FactorMask &mask) const
{
  bool ret = mask[0];
  return ret;
}

} // namespace
