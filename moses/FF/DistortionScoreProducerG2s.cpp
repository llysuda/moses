
#include "DistortionScoreProducerG2s.h"
#include "FFState.h"
//#include "moses/WordsRange.h"
#include "moses/StaticData.h"
#include "moses/graph/G2sHypothesis.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
struct DistortionG2sState : public FFState {
  int endPos;
  DistortionG2sState(int end) : endPos(end) {}
  int Compare(const FFState& other) const {
    const DistortionG2sState& o =
      static_cast<const DistortionG2sState&>(other);
    if (endPos < o.endPos) return -1;
    if (endPos > o.endPos) return 1;
    return 0;
  }
};

DistortionScoreProducerG2s::DistortionScoreProducerG2s(const std::string &line)
  : StatefulFeatureFunction("DistortionG2s", 2, line)
{
  ReadParameters();
}

const FFState* DistortionScoreProducerG2s::EmptyHypothesisState(const InputType &input) const
{
  // fake previous translated phrase start and end
//  size_t start = NOT_FOUND;
//  size_t end = NOT_FOUND;
//  if (input.m_frontSpanCoveredLength > 0) {
//    // can happen with --continue-partial-translation
//    start = 0;
//    end = input.m_frontSpanCoveredLength -1;
//  }
  return new DistortionG2sState(-1);
}

vector<float> DistortionScoreProducerG2s::CalculateDistortionScore(const G2sHypothesis& hypo,
    int prevEnd, const set<size_t> &curr, bool naive)
{
    return hypo.GetInput().ComputeDistortionDistance(prevEnd, curr, naive);
}


FFState* DistortionScoreProducerG2s::Evaluate(
  const G2sHypothesis& hypo,
  const FFState* prev_state,
  ScoreComponentCollection* out) const
{
  const DistortionG2sState* prev = static_cast<const DistortionG2sState*>(prev_state);
  // glue grammar
  if (hypo.GetCurrSourceWordsRange().GetSize() == 0) {
    DistortionG2sState* res = new DistortionG2sState(prev->endPos);
    return res;
  }
  int max = (int)hypo.GetCurrSourceWordsRange().GetMax();
  DistortionG2sState* res = new DistortionG2sState(max);
  if (max != prev->endPos) {
    const vector<float> distortionScore = CalculateDistortionScore(
                                    hypo,
                                                                      prev->endPos,
                                    hypo.GetCurrSourceWordsRange().GetSet(),
                                    StaticData::Instance().GetNaiveDistortion());
    out->PlusEquals(this, distortionScore);
  }

  return res;
}


}

