/*
 * FuzzyMatch.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#include "FM.h"
#include <string>
#include <vector>
#include <map>
#include "moses/Util.h"

using namespace std;

namespace Moses {

//string puncts[] = {"，", "。", "？", "！", "、", "“", "”", "’", "‘", "（", "）", "【", "】", "……", "——", "《", "》", "』","『"};

FM::FM()
  : m_score (0.0f) {
  // TODO Auto-generated constructor stub
}

std::string FM::makekey(const Range& range) const {
  string key = SPrint<int>(range.first)+"+"+SPrint<int>(range.second);
  return key;
}

Range FM::GetTMSourceRange(const Range & sourceRange) {

  string key = makekey(sourceRange);

  if (m_sourceRangeCache.find(key) != m_sourceRangeCache.end()) {
    return m_sourceRangeCache.find(key)->second;
  }

  int start = sourceRange.first, end = sourceRange.second;
  int minIndex = start, maxIndex = end;
  while (minIndex <= end) {
    int opIndex = Src2Edit(minIndex);
    if (m_operations[opIndex] == "deletion" || m_operations[opIndex] == "d") {
      minIndex++;
    } else {
      break;
    }
  }
  while(maxIndex>=start) {
    int opIndex = Src2Edit(maxIndex);
    if (m_operations[opIndex] == "deletion" || m_operations[opIndex] == "d") {
      maxIndex--;
    } else {
      break;
    }
  }

  if (maxIndex < minIndex) {
    Range range (-1,-1);
    m_sourceRangeCache[key] = range;
    return range;
  }

  int tmStart = GetIndex(minIndex);
  int tmEnd = GetIndex(maxIndex);

  Range range(tmStart, tmEnd);

  m_sourceRangeCache[key] = range;

  return range;

}
vector<Range> FM::GetTMTargetRanges(const Range & tmSourceRange) {

  string key = makekey(tmSourceRange);
  if (m_targetRangesCache.find(key) != m_targetRangesCache.end()) {
      return m_targetRangesCache.find(key)->second;
  }

  vector<Range> ret;
  int start = tmSourceRange.first, end = tmSourceRange.second;
  if (start >= 0 && end >= 0) {
    int minIndex = m_target.size()+1, maxIndex = -1;
    for (int i = start; i<= end; i++) {
      if (m_alignSoT[i].size() > 0) {
        for (int j = 0; j < m_alignSoT[i].size(); j++) {
          minIndex = std::min(minIndex,m_alignSoT[i][j]);
          maxIndex = std::max(maxIndex,m_alignSoT[i][j]);
        }
      }
    }
    if (minIndex >= 0) {
      for (int startE = minIndex; startE>=0 && (startE==minIndex || m_alignCountT[startE] == 0); startE--) {
        for (int endE = maxIndex; endE<m_target.size() && (endE==maxIndex || m_alignCountT[endE] == 0); endE++) {
          Range range(startE,endE);
          ret.push_back(range);
        }
      }
    }
  }

  m_targetRangesCache[key] = ret;

  return ret;
}

int FM::GetIndex(int index) const {
  int opIndex = Src2Edit(index);
  int dcount = 0;
  int i = 0;
  while (i < opIndex) {
    if (m_operations[i] == "deletion" || m_operations[i] == "d") {
      dcount++;
    }
    i++;
  }
  return opIndex-dcount;
}

int FM::Src2Edit(int index) const {
  int nicount = 0;
  int i = 0;
  for (; i < m_operations.size(); i++) {
    if (m_operations[i] != "insertion" || m_operations[i] != "i") {
      nicount++;
    }
    if (nicount == index+1) {
      return i;
    }
  }
  return i;
}

vector<string> FM::CollectTLFeatures (const vector<string> & tp, const Range & sourceRange, const vector<string> & source,
    const Range& tmSourceRange, const vector<Range> & tmTargetRanges) const {

 // Range tmSourceRange = GetTMSourceRange(sourceRange);
  //vector<Range> tmTargetRanges = GetTMTargetRanges(tmSourceRange);

  vector<string> tmSourcePhrase;
  for (int i = tmSourceRange.first; i >= 0 && i <= tmSourceRange.second; i++) {
    tmSourcePhrase.push_back(m_source[i]);
  }

  //cerr << "create source phrase from : " << source.size() << endl;
  vector<string> sourcePhrase(source.begin()+sourceRange.first, source.begin()+sourceRange.second+1);

  //cerr << "start collect each feature " << endl;
  vector<string> features;
  // feature Z
  string feat_z = GetZ();
  features.push_back("Z"+feat_z);
  // feature SCM
  string feat_scm = GetSCM(tmSourceRange,sourcePhrase, tmSourcePhrase);
  features.push_back("SCM"+feat_scm);
  // feature NLN
  string feat_nln = GetNLN(sourceRange,source,tmSourceRange);
  features.push_back("NLN"+feat_nln);
  // feature SPL
  string feat_spl = GetSPL(sourceRange);
  features.push_back("SPL"+feat_spl);
  // feature SEP
  string feat_sep = GetSEP(sourceRange, source);
  features.push_back("SEP-"+feat_sep);
  // feature CSS
  string feat_css = GetCSS(tmTargetRanges);
  features.push_back("CSS"+feat_css);
  // tcm ltc cpm
  //cerr << " TCM " << endl;
  vector<string> feats_tcm = GetTCM(tp, tmTargetRanges);
  //cerr << "LTC" << endl;
  vector<string> feats_ltc = GetLTC(tmTargetRanges);

  for (int i = 0; i < feats_tcm.size(); i++) {
    features.push_back("TCM"+feats_tcm[i]);
    features.push_back("LTC"+feats_ltc[i]);
  }
  //cerr << "end collect" << endl;
  return features;
}

vector<string> FM::GetCPM(const vector<Range> & ranges, const vector<string> & tp, const vector<Range> & lastRanges, const vector<string>& lastTP, bool isLastTMNULL) const {

  vector<string> ret;

  if (ranges.size() == 0) {
    vector<string> one;
    ret.push_back("CPMnon");
  } else {
    for (int j = 0; j < ranges.size(); j++) {
      Range range = ranges[j];
      vector<string> oneCPM = GetCPM(range,tp,lastRanges,lastTP,isLastTMNULL);
      for (size_t i = 0; i < oneCPM.size(); i++) {
        ret.push_back("CPM"+oneCPM[i]);
      }
    }
  }
  return ret;
}

vector<string> FM::GetCPM(const Range& range, const vector<string> & tp, const vector<Range> & lastRanges, const vector<string>& lastTP, bool isLastTMNULL) const {

    vector<string> ret;
    for (int i = 0; i < lastRanges.size(); i++) {
      Range lastRange = lastRanges[i];
      if (!isLastTMNULL) {
        if (lastRange.second < range.first) {
          if(lastRange.second == range.first-1) {
            if ((lastRange.second == -1) || (m_target[lastRange.second] == lastTP[lastTP.size()-1] && m_target[range.first] == tp[0])) {
              ret.push_back("AdjacentSame");
            } else {
              ret.push_back("AdjacentSubstitute");
            }
          } else {
            ret.push_back("LinkedInterleaved");
          }
        } else {
          if (range.second < lastRange.first) {
            ret.push_back("LinkedReversed");
          } else {
            ret.push_back("LinkedCross");
          }
        }
      } else {
        if (lastRange.second < range.first) {
            ret.push_back("SkipForward");
        } else {
          if (range.second < lastRange.first) {
            ret.push_back("SkipReversed");
          } else {
            ret.push_back("SkipCross");
          }
        }
      }
    }
  return ret;
}

vector<string> FM::GetLTC(const vector<Range> & ranges) const {
  vector<string> ret;
  if (ranges.size() == 0) {
    ret.push_back("non");
  } else {
    for (int i = 0; i < ranges.size(); i++) {
      string oneLTC = GetLTC(i, ranges);
      ret.push_back(oneLTC);
    }
  }
  return ret;
}

string FM::GetLTC(int i, const vector<Range> & ranges) const {
  if (i == 0) return "original";
  int orgS = ranges[0].first, orgE = ranges[0].second;
  int curS = ranges[i].first, curE = ranges[i].second;
  int minS = ranges[ranges.size()-1].first, maxE = ranges[ranges.size()-1].second;

  bool left = false, right = false, minLeft = false, maxRight = false;
  if (curS < orgS) left = true;
  if(curE > orgE) right = true;
  if (curS == minS) minLeft = true;
  if (curE == maxE) maxRight = true;

  if (left && right && minLeft && maxRight) return "both";
  if (left && minLeft && !right) return "left";
  if (right && maxRight && !left) return "right";
  return "medium";

}

vector<string> FM::GetTCM(const vector<string> & tp, const vector<Range> & ranges) const {
  vector<string> ret;
  if (ranges.size() == 0) {
     ret.push_back("non");
   } else {
     for (int i = 0; i < ranges.size(); i++) {
       Range range = ranges[i];

       //cerr << range.first << " " << range.second << endl;

       vector<string>::const_iterator start = m_target.begin()+range.first;
       vector<string>::const_iterator end = m_target.begin()+range.second+1;
       vector<string> tmTP(start,end);
       // feature TCM
       string tcm = GetTCM(tp, tmTP);
       ret.push_back(tcm);
     }
   }
  return ret;
}

string FM::GetTCM(const vector<string> & tp, const vector<string> & tmTP) const {
  float score = FuzzyScore(tp,tmTP);
  return GetInterval(score);
}

float FM::FuzzyScore (const vector<string> & test, const vector<string> & ref) const {
  int tlen = test.size();
  int rlen = ref.size();

  int distance = EditDistance(test,ref);

  return 1-distance*1.0/std::max(tlen,rlen);
}

int FM::EditDistance(const vector<string> & test, const vector<string> & ref) const {
  if (test.size() == 0) return ref.size();
  if (ref.size() == 0) return test.size();

  int cur = 0, next = 0;
  vector<int> W;
  for (int i = 0; i <= ref.size(); i++) {
    W.push_back(i);
  }

  for (int i = 0; i < test.size(); i++) {
    cur = i+1;
    for(int j = 0; j < ref.size(); j++) {
      int cost = test[i]==ref[j]? 0 : 1;
      next = std::min(W[j+1]+1, cur+1);
      next = std::min(next, cost+W[j]);
      W[j] = cur;
      cur = next;
    }
    W[ref.size()] = next;
  }
  return next;
}

string FM::GetZ() const {
  float score = GetScore();
  int i = (int)(score*10);
  //cerr << score << " " << i << endl;
  return SPrint<int>(i);
}

string FM::GetInterval(float score) const {
  if (score == 1.0) return "same";
  if (score >= 0.5) return "high";
  return "low";
}

string FM::GetCSS(const vector<Range> & ranges) const {
  if (ranges.size() == 0) return "non";
  if (ranges.size() == 1) return "single";
  int sameL = true, sameR = true;
  for (int i = 1; i < ranges.size(); i++) {
    if (ranges[i].first != ranges[i-1].first) {
      sameL = false;
    }
    if (ranges[i].second != ranges[i-1].second) {
      sameR = false;
    }
  }
  if (!sameL && !sameR) return "both";
  if (!sameL) return "left";
  return "right";
}

string FM::GetSEP(const Range & range, const vector<string> & source) const {
  size_t start = range.first, end = range.second;
  string word = source[start];

  if (start == end && IsPunct(deescape(word))) {
    return "yes";
  }
  return "no";
}

string FM::deescape(const string& str) const {
  //string ret = str;
  if(str == "&bar;" or str == "&#124;") {
    return "|";
  } else if (str == "&lt;") {
    return "<";
  } else if (str == "&gt;") {
    return ">";
  } else if (str == "&bra;" or str == "&#91;") {
    return "[";
  } else if (str == "&ket;" or str == "&#93;") {
    return "]";
  } else if (str == "&quot;") {
    return "\"";
  } else if (str == "&apos;") {
    return "'";
  } else if (str == "&amp;") {
    return "&";
  } else {
    return str;
  }
}

bool FM::IsPunct(string word) const {
  //TODO
  if (word.size() == 1) {
    return std::ispunct(word[0]);
  }
  return false;
}

string FM::GetSCM(const Range & range, const vector<string> & test, const vector<string> & ref) const {
  if (range.first < 0) return "non";
  float score = FuzzyScore(test,ref);
  return GetInterval(score);
}

string FM::GetSPL(const Range & range) const {
  int length = range.second-range.first+1;
  if (length > 7)
  	return "more";
  else
  	return SPrint<int>(length);
}

string FM::GetNLN(const Range & sourceRange, const vector<string> & source, const Range& tmSourceRange) const {
  if (tmSourceRange.first < 0) return "non";
  int link = 0, aligned = 0;

  size_t sourceStart = sourceRange.first, sourceEnd = sourceRange.second;
  size_t tmSourceStart = tmSourceRange.first, tmSourceEnd = tmSourceRange.second;

  if (sourceStart == 0 && tmSourceStart == 0) {
    link++;
    aligned++;
  } else if (sourceStart > 0 && tmSourceStart > 0 && source[sourceStart-1] == m_source[tmSourceStart-1]) {
    link++;
    if (m_alignSoT[tmSourceStart-1].size() > 0) {
      aligned++;
    }
  }
  if (sourceEnd == source.size()-1 && tmSourceEnd == m_source.size()-1) {
    link++;
    aligned++;
  } else if (sourceEnd < source.size()-1 && tmSourceEnd < m_source.size()-1 && source[sourceEnd+1] == m_source[tmSourceEnd+1]) {
    link++;
    if (m_alignSoT[tmSourceEnd+1].size() > 0) {
      aligned++;
    }
  }

  return SPrint<int>(link)+SPrint<int>(aligned);
}

void FM::create(const vector<string> & tm) {
  m_source = Tokenize(tm[0]);
  m_target = Tokenize(tm[1]);
  createAlign(tm[2]);
  m_operations = Tokenize(tm[3]);
  m_score = Scan<float>(tm[4]);
}

void FM::createAlign(string align) {

  for (int i = 0; i < m_source.size(); i++) {
    vector<int> dummy;
    m_alignSoT.push_back(dummy);
  }
  for(int i = 0; i < m_target.size(); i++) {
    m_alignCountT.push_back(0);
  }

  vector<string> tokens = Tokenize(align);
  for(int i = 0; i < tokens.size(); i++) {
    vector<string> onealign = Tokenize(tokens[i],"-");
    int s = Scan<int>(onealign[0]);
    int t = Scan<int>(onealign[1]);

    if (s >= m_source.size() || t >= m_target.size()) {
      cerr << "error alignment" << endl;
      exit(-1);
    }

    m_alignSoT[s].push_back(t);
    m_alignCountT[t]++;
  }
}

} /* namespace Moses */
