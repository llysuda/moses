#pragma once

//#ifndef TMSparseState_H_
//#define TMSparseState_H_

#include "moses/FF/FFState.h"

namespace Moses
{

typedef std::pair<int,int> Range;

class TMSparseState : public FFState {
  public:
  std::string m_lastTword;
private:
  Range m_sourceRange;
  std::vector<std::string> m_target;
  std::vector< Range > m_tmRangeK_i;
  bool m_nullTM;

public:
  TMSparseState(): m_sourceRange(-1,-1)
									,m_lastTword("")
									,m_nullTM(true){}
  TMSparseState(const Range& sourceRange, const std::vector<std::string>& target,
  		const std::vector<Range>& ranges, bool nullFlag, const std::string& lastTword)
    : m_sourceRange(sourceRange)
  	, m_target (target)
    , m_tmRangeK_i (ranges)
    , m_nullTM (nullFlag)
    , m_lastTword (lastTword){}
  const Range& GetSourceRange() const {return m_sourceRange;}
  const std::vector<Range>& GetTargetRanges() const {return m_tmRangeK_i;}
  const std::vector<std::string>& GetTargetPhrase() const {return m_target;}
  bool IsLastTMNULL() const {return m_nullTM;}
  int Compare(const FFState& other) const {
    const TMSparseState &otherBase = static_cast<const TMSparseState&>(other);
    //
    return 0;
  }

};

}

//#endif /* TMSparseState_H_ */
