#include "TMSparse.h"
#include <assert.h>
#include "moses/InputType.h"
#include "moses/StaticData.h"
#include <climits>
#include "moses/InputFileStream.h"
#include "TMSparseState.h"
#include "moses/TypeDef.h"
#include <cfloat>

#include "moses/AlignmentInfo.h"

//#include "FNgramSpecs.h"

using namespace std;

namespace Moses
{

#ifdef WITH_THREADS
boost::shared_mutex TMSparse::m_tmLock;
#endif

TMSparse::TMSparse(const string & line) : StatefulFeatureFunction("TMSparse", 0, line)
{
}


void TMSparse::Load() {
  for (size_t i = 0; i < m_args.size(); ++i) {
    const vector<string> &args = m_args[i];
    if (args[0] == "tm") {
      m_tmPath = args[1];
      ReadTM();
    }  else if (args[0] == "vocab") {
      m_vocabPath = args[1];
      ReadVocab();
    } else if (args[0] == "classes") {
      m_classesPath = args[1];
      ReadClasses();
    } else {
      throw "Unknown argument " + args[0];
    }
  }
}

FFState* TMSparse::Evaluate(
        const Hypothesis& cur_hypo,
        const FFState* prev_state,
        ScoreComponentCollection* accumulator) const {


  const InputType & source = cur_hypo.GetManager().GetSource();
  const int m_sentID = source.GetTranslationId();
  vector<string> m_source;// = m_sourceCache.find(m_sentID)->second;
    for (size_t i = 0; i < source.GetSize(); i++) {
      m_source.push_back(source.GetWord(i).GetString(0).as_string());
  }

//  const vector<string>& m_source = m_sources.find(m_sentID)->second;

  TargetPhrase tp = cur_hypo.GetCurrTargetPhrase();
  vector<string> target;
  for (size_t i = 0; i < tp.GetSize(); i++) {
    target.push_back(tp.GetWord(i).GetString(0).as_string());
  }

  Range range(cur_hypo.GetCurrSourceWordsRange().GetStartPos(),cur_hypo.GetCurrSourceWordsRange().GetEndPos());

  //cerr << "enter TMSparse evaluate for sent " << m_sentID << endl;
  //cerr << "\t for source range ( " << range.first << " " << range.second << " )" << endl;

  FM& fm = *(m_tm[m_sentID]);
  Range tmSourceRange = fm.GetTMSourceRange(range);

  //cerr << "TM source Range: " << tmSourceRange.first << " " << tmSourceRange.second << endl;

  vector<Range> tmTargetRanges = fm.GetTMTargetRanges(tmSourceRange);

  //cerr << "TM target ranges size: " << tmTargetRanges.size() << endl;

  const vector<Range>& preRanges = static_cast <const TMSparseState *> (prev_state) ->GetTargetRanges();
  const vector<string>& preTarget = static_cast <const TMSparseState *> (prev_state) ->GetTargetPhrase();
  bool preNULL = static_cast <const TMSparseState *> (prev_state) ->IsLastTMNULL();

  string lastTword = static_cast <const TMSparseState *> (prev_state) ->m_lastTword;

  string key = fm.makekey(range);
  std::map<string, std::map<const Phrase*, std::vector<std::string> > >::const_iterator fiter = fm.m_featureCache.find(key);
  if (fiter == fm.m_featureCache.end() || fiter->second.find(&tp) == fiter->second.end()) {

    vector<string> tlFeatures = fm.CollectTLFeatures(target, range, m_source, tmSourceRange, tmTargetRanges);
    fm.m_featureCache[key][&tp] = tlFeatures;

  }

  //cerr << "collect cpm" << endl;

  vector<string> cachedFeatures = fm.m_featureCache[key][&tp];
  vector<string> cpm = fm.GetCPM(tmTargetRanges,target,preRanges,preTarget,preNULL);

  for (size_t i = 0; i < cachedFeatures.size(); i++) {
    accumulator->PlusEquals(this, cachedFeatures[i],1.0);
  }
  for (size_t i = 0; i < cpm.size(); i++) {
    accumulator->PlusEquals(this, cpm[i],1.0);
  }

  if (m_classes.size() > 0) {
      string preword_best, nextword_best, firstword_best, lastword_best;
      if (tmSourceRange.first < 0) {
        preword_best = nextword_best = firstword_best = lastword_best = "non";
      } else {
        if (tmSourceRange.first == 0) {
          preword_best = "START";
        } else {
          preword_best = fm.GetSourceWord(tmSourceRange.first-1);
        }
        if (tmSourceRange.second == fm.GetSourceLength()-1) {
          nextword_best = "END";
        } else {
          nextword_best = fm.GetSourceWord(tmSourceRange.second+1);
        }
        firstword_best = fm.GetSourceWord(tmSourceRange.first);
        lastword_best = fm.GetSourceWord(tmSourceRange.second);
      }

      string preword, nextword, firstword, lastword;
      if (range.first == 0) {
        preword = "<s>";
      } else {
        preword = m_source[range.first-1];
      }
      if (range.second == m_source.size()-1) {
        nextword = "</s>";
      } else {
        nextword = m_source[range.second+1];
      }
      firstword = m_source[range.first];
      lastword = m_source[range.second];

      accumulator->PlusEquals(this, "VCB-"+GetVocab(preword_best)+"-"+GetVocab(firstword), 1.0);
      accumulator->PlusEquals(this, "VCB-"+GetVocab(lastword_best)+"-"+GetVocab(nextword), 1.0);
      accumulator->PlusEquals(this, "VCB-"+GetVocab(lastword)+"-"+GetVocab(nextword_best), 1.0);
      accumulator->PlusEquals(this, "VCB-"+GetVocab(preword)+"-"+GetVocab(firstword_best), 1.0);
      accumulator->PlusEquals(this, "CLS-"+GetClasses(preword_best)+"-"+GetClasses(firstword), 1.0);
      accumulator->PlusEquals(this, "CLS-"+GetClasses(lastword_best)+"-"+GetClasses(nextword), 1.0);
      accumulator->PlusEquals(this, "CLS-"+GetClasses(lastword)+"-"+GetClasses(nextword_best), 1.0);
      accumulator->PlusEquals(this, "CLS-"+GetClasses(preword)+"-"+GetClasses(firstword_best), 1.0);


      for (size_t i = 0; i < target.size(); i++){
          if (fm.IsPunct(target[i])) {
            accumulator->PlusEquals(this, "TGT-PUN-CT",1.0);
          }
          if (GetVocab(target[i]) != "UNK") {
            accumulator->PlusEquals(this, "TGT-FW-CT",1.0);
          }
        }
  }
  //
  //cerr << " new state" << endl;
  FFState* retState = new TMSparseState(tmSourceRange,target,tmTargetRanges.size()>0? tmTargetRanges:preRanges,tmTargetRanges.size()>0, target.back());
  return retState;

}


void TMSparse::ReadTM() {
  std::string fileName = m_tmPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line(""), key("");
  std::cerr << "Loading TM into memory...";
  while(!getline(file, line).eof()) {
    if (line == "<START>") {
      vector<string> oneTM;
      for (int i = 0; i < 5; i++) {
        getline(file, line);
        oneTM.push_back(line);
      }

      FM* fm = new FM();
      fm->create(oneTM);
      m_tm.push_back(fm);

    } else {
      cerr << "error TM format" << endl;
      exit(-1);
    }
  }
  std::cerr << "done.\n";
}

void TMSparse::ReadVocab() {
  std::string fileName = m_vocabPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading vocab into memory...";
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 3);
    m_vocab[items[1]] = items[2];
  }
  std::cerr << "done.\n";
}

void TMSparse::ReadClasses() {
  std::string fileName = m_classesPath;
  if(!FileExists(fileName) && FileExists(fileName+".gz")) {
    fileName += ".gz";
  }
  InputFileStream file(fileName);
  std::string line("");
  std::cerr << "Loading classes into memory...";
  while(!getline(file, line).eof()) {
    vector<string> items = Tokenize(line);
    assert(items.size() == 2);
    m_classes[items[0]] = items[1];
  }
  std::cerr << "done.\n";
}

struct RangeComparator{
	inline bool operator() (const Range& i, const Range& j) {
		return i.first < j.first;
	}
}rangeComparator;

FFState* TMSparse::EvaluateChart(
        const ChartHypothesis&  cur_hypo ,
        int featureID /*- used to index the state in the previous hypotheses */,
        ScoreComponentCollection* accumulator) const
{
		const InputType & source = cur_hypo.GetManager().GetSource();
	  const int m_sentID = source.GetTranslationId();

	  vector<string> m_source;// = m_sourceCache.find(m_sentID)->second;
		for (size_t i = 1; i < source.GetSize()-1; i++) {
			m_source.push_back(source.GetWord(i).GetString(0).as_string());
		}

//	  const vector<string>& m_source = m_sources.find(m_sentID)->second;

	  const WordsRange& wr = cur_hypo.GetCurrSourceRange();
	  int start = wr.GetStartPos();
	  int end = wr.GetEndPos();

	  if (start == 0 || end == source.GetSize()-1)
	  	return new TMSparseState();

	  // moses_chart add <s> and </s>
	  start--;
	  end--;

	  Range range(start, end);
	  const TargetPhrase& tp = cur_hypo.GetCurrTargetPhrase();
	  vector<string> target;
	  for (size_t i = 0; i < tp.GetSize(); i++) {
	  	const Word& word = tp.GetWord(i);
	  	if (word.IsNonTerminal()) {
	  		target.push_back("[X]");
	  	} else {
	  		target.push_back(word.GetString(0).as_string());
	  	}
	  }

	  FM& fm = *(m_tm[m_sentID]);
	  Range tmSourceRange = fm.GetTMSourceRange(range);
	  vector<Range> tmTargetRanges = fm.GetTMTargetRanges(tmSourceRange);

	  string key = fm.makekey(range);
		std::map<string, std::vector<std::string > >::const_iterator fiter = fm.m_featureCacheChart.find(key);
		if (fiter == fm.m_featureCacheChart.end()) {
			vector<string> features;
			features.push_back("Z" + fm.GetZ());
			features.push_back("NLN" + fm.GetNLN(range,m_source,tmSourceRange));
			features.push_back("SPL" + fm.GetSPL(range));
			features.push_back("SEP-" + fm.GetSEP(range, m_source));
			features.push_back("CSS" + fm.GetCSS(tmTargetRanges));
			vector<string> feats_ltc = fm.GetLTC(tmTargetRanges);
			for(size_t i = 0; i < feats_ltc.size(); i++)
				features.push_back("LTC" + feats_ltc[i]);
			fm.m_featureCacheChart[key] = features;

		}
		vector<string> features = fm.m_featureCacheChart.find(key)->second;
	  //
	  vector<Range> prevSourceRanges;
	  vector<Range> prevTMSourceRanges;
	  vector<Range> prevTMTargetRanges;
	  const std::vector<const ChartHypothesis*> prevHypos = cur_hypo.GetPrevHypos();
	  for(size_t i = 0; i < prevHypos.size(); i++) {
	  	const ChartHypothesis& h = *(prevHypos[i]);
	  	const WordsRange& r = h.GetCurrSourceRange();
	  	int s = r.GetStartPos();
	  	int e = r.GetEndPos();
			s--;
			e--;

			assert(e>=s && s>=0);

	  	prevSourceRanges.push_back(Range(s,e));

	  	const FFState* fs = h.GetFFState(featureID);
	  	const TMSparseState * tmfs = static_cast <const TMSparseState *> (fs);
	  	const Range& sr = tmfs->GetSourceRange();
	  	if (sr.first >= 0)
	  		prevTMSourceRanges.push_back(Range(sr));
	  	const vector<Range>& tr = tmfs->GetTargetRanges();
	  	if (tr.size() > 0 && tr[0].first >= 0) {
	  		prevTMTargetRanges.push_back(Range(tr[0]));
	  	}
	  }
	  //
	  vector<string> sourcePhrase;
		vector<Range>::iterator iter = prevSourceRanges.begin();
		int holeCount = 0;
		for (int cur_pos = start; cur_pos <= end; cur_pos++) {
			bool is_hole = false;
			if (iter != prevSourceRanges.end())
			 is_hole = cur_pos == iter->first;
			if (is_hole) {
				holeCount++;
				//sourcePhrase.push_back("[X]");
				cur_pos = iter->second;
				iter++;
			} else {
				sourcePhrase.push_back(m_source[cur_pos]);
			}
		}
		assert(iter == prevSourceRanges.end());
		//

	  if (prevTMSourceRanges.size() == 0) {
	  	vector<string> tmSourcePhrase = fm.GetSourcePhrase(tmSourceRange);
	  	features.push_back("SCM" + fm.GetSCM(tmSourceRange, sourcePhrase, tmSourcePhrase));
	  } else {
	  	set<size_t> ntIndex;
	  	for(size_t i = 0; i < prevTMSourceRanges.size(); i++) {
				for(int j = prevTMSourceRanges[i].first;
						j <= prevTMSourceRanges[i].second; j++)
					ntIndex.insert(j);
			}
	  	vector<string> tmSourcePhrase;
	  	if (tmSourceRange.first >= 0) {
				for (int cur_pos = tmSourceRange.first; cur_pos <= tmSourceRange.second; cur_pos++) {
					if(ntIndex.find(cur_pos) == ntIndex.end()) {
						tmSourcePhrase.push_back(fm.GetSourceWord(cur_pos));
					}
				}
	  	}
	  	features.push_back("SCM" + fm.GetSCM(tmSourceRange, sourcePhrase, tmSourcePhrase));
	  }
	  if (prevTMTargetRanges.size() == 0) {
	  	vector<string> feats_tcm = fm.GetTCM(target, tmTargetRanges);
	  	for(size_t i = 0; i < feats_tcm.size(); i++)
	  		  features.push_back("TCM" + feats_tcm[i]);
	  } else if (tmTargetRanges.size() == 0) {
	  	features.push_back("TCMnon");
	  } else {
	  	set<size_t> ntIndex;
	  	for(size_t i = 0; i < prevTMTargetRanges.size(); i++) {
	  		for(int j = prevTMTargetRanges[i].first;
	  						j <= prevTMTargetRanges[i].second; j++)
	  			ntIndex.insert(j);
	  	}
	  	//
	  	for (size_t i = 0; i < tmTargetRanges.size(); i++) {
	  		vector<string> tmTargetPhrase;
				for (size_t cur_pos = tmTargetRanges[i].first; cur_pos <= tmTargetRanges[i].second; cur_pos++) {
					if(ntIndex.find(cur_pos) == ntIndex.end()) {
						tmTargetPhrase.push_back(fm.GetTargetWord(cur_pos));
					}
				}

				features.push_back("TCM" + fm.GetTCM(target, tmTargetPhrase));
	  	}
	  }
	  // reordering features
	  const vector<size_t>& ntmap = tp.GetAlignNonTerm().GetNonTermIndexMap();
	  vector<int> indexes;
	  for (size_t i = 0; i < tp.GetSize(); i++) {
			const Word& word = tp.GetWord(i);
			if (word.IsNonTerminal()) {
				indexes.push_back((int)ntmap[i]);
			}
		}
	  if (indexes.size() == 0) {
	  	features.push_back("CPMnont");
	  } else {
	  	vector<int> tmIndexes;
	  	map<int,int> indexMap;
	  	vector<size_t> nonIndex;
	  	for(size_t i = 0; i < prevHypos.size(); i++) {
	  		 const ChartHypothesis& h = *(prevHypos[i]);
	  		 const FFState* fs = h.GetFFState(featureID);
	  		 const TMSparseState * tmfs = static_cast <const TMSparseState *> (fs);
	  		 const vector<Range>& tr = tmfs->GetTargetRanges();
	  		 if (tr.size() > 0 && tr[0].first >= 0) {
	  			 int key = (int)(tr[0].first);
	  			 while (indexMap.find(key) != indexMap.end()) {
	  				 key++;
	  			 }
	  		 	 indexMap[key] = (int)i;
	  		 } else {
	  			 //indexMap[-1] = (int)i;
	  			 nonIndex.push_back(i);
	  		 }
	  	}
	  	if (indexMap.size() == 0) {
	  		features.push_back("CPMnon");
	  	} else {
	  		for(map<int,int>::const_iterator iterIndexMap = indexMap.begin();
	  				iterIndexMap != indexMap.end(); iterIndexMap++) {
	  			tmIndexes.push_back(iterIndexMap->second);
	  		}
	  		for(vector<size_t>::reverse_iterator iterNonIndex = nonIndex.rbegin();
	  				iterNonIndex != nonIndex.rend(); ++iterNonIndex) {
	  			indexes.erase(indexes.begin() + *(iterNonIndex));
	  		}
	  		assert(indexes.size() == tmIndexes.size());
	  		int sum_d2 = 0;
	  		int n = (int) indexes.size();
	  		for(int j = 0; j < n; j++) {
	  			int diff = indexes[j] - tmIndexes[j];
	  			sum_d2 += diff * diff;
	  		}
	  		float spearman = 1 - ((float)(6 * sum_d2)) / (n * (n*n-1));

	  		string prefix = nonIndex.size()==0 ? "full" : "part";
	  		if (spearman < -0.5) {
	  			features.push_back("CPM" + prefix + "nh");
	  		} else if (spearman < 0.0f) {
	  			features.push_back("CPM" + prefix + "nl");
	  		} else if (spearman == 0.0f) {
	  			features.push_back("CPM" + prefix + "nn");
	  		}	else if (spearman < 0.5) {
	  			features.push_back("CPM" + prefix + "pl");
	  		} else {
	  			features.push_back("CPM" + prefix + "lh");
	  		}
	  	}
	  }

	  //
	  for (int i = 0; i < features.size(); i++) {
	    accumulator->PlusEquals(this, features[i],1.0);
	  }
	  return new TMSparseState(tmSourceRange, target, tmTargetRanges,tmTargetRanges.size()>0, "");
}



      //! return the state associated with the empty hypothesis for a given sentence
const FFState* TMSparse::EmptyHypothesisState(const InputType &input) const {
  cerr << "TMSparse::EmptyHypothesisState()" << endl;

  vector<string> target;
  vector<Range> tmTargetRanges;
  Range startRange(-1,-1);
  tmTargetRanges.push_back(startRange);
  bool nullFlag = false;
  return new TMSparseState(startRange,target, tmTargetRanges, nullFlag,"<s>");

}

bool TMSparse::IsUseable(const FactorMask &mask) const {
        return true;
}

void TMSparse::InitializeForInput(InputType const& source) {
//	vector<string> words;
//	for (size_t i = 0; i < source.GetSize(); i++) {
//		string w = source.GetWord(i).GetString(0).as_string();
//		if (w != BOS_ && w != EOS_)
//			words.push_back(w);
//  }
//	int id =  source.GetTranslationId();
//#ifdef WITH_THREADS
//    boost::unique_lock<boost::shared_mutex> write_lock(m_tmLock);
//#endif
//	m_sources[id] = words;
}

}




