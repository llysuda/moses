#pragma once

#include <string>
#include <map>
#include <vector>
#include "FM.h"
#include "moses/FF/FFState.h"
#include "moses/Hypothesis.h"
#include "moses/ChartHypothesis.h"
#include "moses/InputType.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/FF/StatefulFeatureFunction.h"
#include "moses/TargetPhrase.h"
#include "moses/Phrase.h"
#include "moses/Manager.h"
#include "moses/ChartManager.h"

#ifdef WITH_THREADS
#include <boost/thread/shared_mutex.hpp>
#endif

namespace Moses {

class TMSparse : public StatefulFeatureFunction
{

private:
	std::string m_tmPath;
	std::vector< FM* > m_tm;

	std::string m_vocabPath;
	std::string m_classesPath;

	std::map<std::string, std::string> m_vocab;
	std::map<std::string, std::string> m_classes;

	std::map<int, std::vector<std::string> > m_sources;

#ifdef WITH_THREADS
  //reader-writer lock
  static boost::shared_mutex m_tmLock;
#endif

public:
	TMSparse(const std::string &line);

	std::string GetVocab(const std::string & word) const {
	      std::map<std::string, std::string>::const_iterator iter = m_vocab.find(word);
	      if (iter == m_vocab.end()) {
	        return "UNK";
	      } else {
	        return word;
	      }
	    }

	    std::string GetClasses(const std::string & word) const {
	        std::map<std::string, std::string>::const_iterator iter = m_classes.find(word);
	        if (iter == m_classes.end()) {
	          return "0";
	        } else {
	          return iter->second;
	        }
	      }

	void Load();
	void ReadTM();
	void ReadVocab();
	    void ReadClasses();
	virtual FFState* Evaluate(
	    const Hypothesis& cur_hypo,
	    const FFState* prev_state,
	    ScoreComponentCollection* accumulator) const;
	virtual FFState* EvaluateChart(
	    const ChartHypothesis& /* cur_hypo */,
	    int /* featureID - used to index the state in the previous hypotheses */,
	    ScoreComponentCollection* accumulator) const;

	  //! return the state associated with the empty hypothesis for a given sentence
	  virtual const FFState* EmptyHypothesisState(const InputType &input) const;
	  virtual bool IsUseable(const FactorMask &mask) const;
	  void InitializeForInput(InputType const& source);

	  float FLMScore(const std::string& type, const std::string & feature) const;


};

}
