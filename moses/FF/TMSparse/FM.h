#pragma once

//#ifndef FUZZYMATCH_H_
//#define FUZZYMATCH_H_

#include <vector>
#include <string>
#include <map>
#include "TMSparseState.h"
#include "moses/Phrase.h"

namespace Moses {

class FM {

public:

  std::map<std::string, std::map<const Phrase*, std::vector< std::string > > > m_featureCache;
  std::map<std::string, std::vector< std::string > > m_featureCacheChart;

public:

private:
  std::map<std::string, Range> m_sourceRangeCache;
  std::map<std::string, std::vector<Range> > m_targetRangesCache;

  std::vector<std::string> m_source;
  std::vector<std::string> m_target;
  std::vector<std::vector<int> > m_alignSoT;
  std::vector<int> m_alignCountT;
  std::vector<std::string> m_operations;
  float m_score;

public:
  FM();
  ~FM() {}

  std::string makekey(const Range& range) const;

  std::string GetSourceWord(int index) {return m_source[index];}
  std::vector<std::string> GetSourcePhrase(const Range& range) {
  	std::vector<std::string> ret;
  	if (range.first < 0)
  		return ret;
  	for (int i = range.first; i <= range.second; i++)
  		ret.push_back(m_source[i]);
  	return ret;
  }
  std::string GetTargetWord(int index) {return m_target[index];}
  int GetSourceLength() {return m_source.size();}

  float GetScore() const { return m_score;}
std::string deescape(const std::string& str) const;
  void create (const std::vector<std::string> & );
  void createAlign(std::string);
  Range GetTMSourceRange(const Range &);
  std::vector<Range> GetTMTargetRanges(const Range &);
  std::vector<std::string> CollectTLFeatures (const std::vector<std::string> & tp, const Range & range,const std::vector<std::string> & source,
      const Range & tmSourceRange, const std::vector<Range> & tmTargetRanges) const;
  std::string GetZ() const;
  std::string GetSCM(const Range & range, const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  std::string GetSPL(const Range &) const;
  std::string GetNLN(const Range &, const std::vector<std::string> &, const Range &) const;
  std::string GetSEP(const Range &, const std::vector<std::string> & ) const;
  std::string GetCSS(const std::vector<Range> & ranges) const;
  std::string GetLTC(int i, const std::vector<Range> & ranges) const;
  std::vector<std::string> GetLTC(const std::vector<Range> & ranges) const;
  std::string GetTCM(const std::vector<std::string> & tp, const std::vector<std::string> & tmTP) const;
  std::vector<std::string> GetTCM(const std::vector<std::string> & tp, const std::vector<Range> & ranges) const;
  std::vector<std::string> GetCPM(const Range & range, const std::vector<std::string> & tp, const std::vector<Range> & lastRanges, const std::vector<std::string>& lastTP, bool isLastTMNULL) const;
  std::vector<std::string> GetCPM(const std::vector<Range> & ranges, const std::vector<std::string> & tp, const std::vector<Range> & lastRanges, const std::vector<std::string>& lastTP, bool isLastTMNULL) const;

  std::string GetInterval(float score) const;
  bool IsPunct(std::string) const;
  int EditDistance(const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  float FuzzyScore (const std::vector<std::string> & test, const std::vector<std::string> & ref) const;
  int GetIndex(int index) const;
  int Src2Edit(int index) const;

  //void Cache(const TargetPhrase & tp, const std::vector<std::string> & target,
  //          const Range& range, const Range& tmSourceRange, const std::vector<Range>& tmTargetRanges);
  //std::vector<float> GetFeatureValue(const TargetPhrase & tp, const std::vector<std::string> & target,
  //    const Range& range, const std::vector<Range>& tmTargetRanges,
  //    const std::vector<Range>& preRanges, const std::vector<string> & preTarget, bool preNULL);
};

} /* namespace Moses */
//#endif /* FUZZYMATCH_H_ */
