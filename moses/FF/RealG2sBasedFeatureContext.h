#pragma once

namespace Moses
{

class InputType;
class TargetPhrase;

namespace RealGraph
{
class RealG2sHypothesis;
}

/**
 * Same as PhraseBasedFeatureContext, but for chart-based Moses.
 **/
class RealG2sBasedFeatureContext
{
  //The context either has a hypothesis (during search) or a
  //TargetPhrase and source sentence (during pre-calculation)
  //TODO: should the context also include some info on where the TargetPhrase
  //is anchored (assuming it's lexicalised), which is available at pre-calc?
  const Moses::RealGraph::RealG2sHypothesis* m_hypothesis;
  const TargetPhrase& m_targetPhrase;
  const InputType& m_source;

public:
  RealG2sBasedFeatureContext(const Moses::RealGraph::RealG2sHypothesis* hypothesis);
  RealG2sBasedFeatureContext(const TargetPhrase& targetPhrase,
                           const InputType& source);

  const InputType& GetSource() const {
    return m_source;
  }

  const TargetPhrase& GetTargetPhrase() const {
    return m_targetPhrase;
  }

  const Moses::RealGraph::RealG2sHypothesis& GetHypothesis() const {
    return *m_hypothesis;
  }
};

} // namespace


