
#include "PhrasePenaltyGraph.h"
#include "moses/ScoreComponentCollection.h"

#include "moses/graph/WordsSet.h"
#include "moses/graph/InputSubgraph.h"
#include <set>

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
PhrasePenaltyGraph::PhrasePenaltyGraph(const std::string &line)
  : StatelessFeatureFunction("PhrasePenaltyGraph",2, line)
{
  ReadParameters();
}

void PhrasePenaltyGraph::Evaluate(const InputType &input
        , const std::set<size_t> &inputPath
        , ScoreComponentCollection &scoreBreakdown) const {
  vector<float> scores(2,0.0f);
  size_t prev = NOT_FOUND;
  set<size_t>::const_iterator iter = inputPath.begin();
  for(; iter != inputPath.end(); ++iter) {
    if (prev != NOT_FOUND && *iter != prev+1) {
      scores[1] = 1.0f;
      scoreBreakdown.PlusEquals(this, scores);
      break;
    }
    prev = *iter;
  }
  if (iter == inputPath.end()) {
    scores[0] = 1.0f;
    scoreBreakdown.PlusEquals(this, scores);
  }
}

void PhrasePenaltyGraph::Evaluate(const InputType &input
      , const Moses::Graph::InputSubgraph &inputPath
      , ScoreComponentCollection &scoreBreakdown) const
{

  const set<size_t>& range = inputPath.GetWordsRange().GetSet();
  Evaluate(input, range, scoreBreakdown);
}

} // namespace

