
#include "DistortionScoreProducerRealg2s.h"
#include "FFState.h"
//#include "moses/WordsRange.h"
#include "moses/StaticData.h"
#include "moses/graph/RealGraphParser/RealG2sHypothesis.h"

using namespace std;
using namespace Moses::RealGraph;

namespace Moses
{
struct DistortionRealg2sState : public FFState {
  int endPos;
  DistortionRealg2sState(int end) : endPos(end) {}
  int Compare(const FFState& other) const {
    const DistortionRealg2sState& o =
      static_cast<const DistortionRealg2sState&>(other);
    if (endPos < o.endPos) return -1;
    if (endPos > o.endPos) return 1;
    return 0;
  }
};

DistortionScoreProducerRealg2s::DistortionScoreProducerRealg2s(const std::string &line)
  : StatefulFeatureFunction("DistortionRealg2s", 2, line)
{
  ReadParameters();
}

const FFState* DistortionScoreProducerRealg2s::EmptyHypothesisState(const InputType &input) const
{
  // fake previous translated phrase start and end
//  size_t start = NOT_FOUND;
//  size_t end = NOT_FOUND;
//  if (input.m_frontSpanCoveredLength > 0) {
//    // can happen with --continue-partial-translation
//    start = 0;
//    end = input.m_frontSpanCoveredLength -1;
//  }
  return new DistortionRealg2sState(-1);
}

//vector<float> DistortionScoreProducerRealg2s::CalculateDistortionScore(const Pg2sHypothesis& hypo,
//    int prevEnd, const set<size_t> &curr, bool naive)
//{
//    return hypo.GetInput().ComputeDistortionDistance(prevEnd, curr, naive);
//}


FFState* DistortionScoreProducerRealg2s::EvaluateRealG2s(
    const Moses::RealGraph::RealG2sHypothesis& hypo,
    int featureID,
    ScoreComponentCollection* accumulator) const
{
  if (hypo.GetCurrSourceWordsSet().GetStartPos() > 0 || hypo.GetCurrSourceWordsSet().GetEndPos()+1 == hypo.GetSourceCompleted().GetSize())
    return new DistortionRealg2sState(-1);

  int start=999999, end=0;
  int prevEnd = -1;
  WordsBitmap bitmap = hypo.GetSourceCompleted();
  const vector<const RealG2sHypothesis*>& prevHypos = hypo.GetPrevHypos();
  CHECK(prevHypos.size() <= 2);
  for (size_t i = 0; i < prevHypos.size(); i++) {
    const RealG2sHypothesis* prev = prevHypos[i];
    if (prev->GetCurrSourceWordsSet().GetStartPos() > 0)
      continue;
    CHECK(prevEnd == -1);
    prevEnd = static_cast<const DistortionRealg2sState*>(prev->GetFFState(featureID))->endPos;
    const WordsBitmap& prev_bitmap = prev->GetSourceCompleted();
    for (size_t j = 0; j < prev_bitmap.GetSize(); j++)
      if (prev_bitmap.GetValue(j))
        bitmap.SetValue(j, false);
  }

  vector<float> distScores(2,0.0f);
  int total = 0;
  for (int j = 0; j < (int)bitmap.GetSize(); j++) {
    if (bitmap.GetValue(j)) {
      if (j < start) start = j;
      if (j > end) end = j;
      total++;
    }
  }
  CHECK(start <= end);
  distScores[0] = -std::abs(prevEnd-start+1);
  distScores[1] = -std::abs(end-start+1-total);

//  cerr << hypo.GetCurrSourceWordsSet() << " " << hypo.GetOutputPhrase() << endl;
//  cerr << hypo.GetSourceCompleted() << endl;
//  cerr << bitmap << " " << distScores[0] << endl;
//  cerr <<"start:" << start << " end:" << end << " total:" << total << " prevend:"<< prevEnd << endl;

  accumulator->PlusEquals(this, distScores);

  DistortionRealg2sState* res = new DistortionRealg2sState((int)end);
  return res;
}


}

