#pragma once

#include "StatelessFeatureFunction.h"
#include <string>
#include <set>

namespace Moses
{

class GapCountFeature : public StatelessFeatureFunction
{
public:
  GapCountFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const {
    return true;
  }

  void Evaluate(const PhraseBasedFeatureContextG2s& context,
                      ScoreComponentCollection* accumulator) const;

//  virtual void Evaluate(const Phrase &source
//                        , const TargetPhrase &targetPhrase
//                        , ScoreComponentCollection &scoreBreakdown
//                        , ScoreComponentCollection &estimatedFutureScore) const;
};

} //namespace

