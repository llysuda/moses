
#include "GraphSimFeature.h"
#include "moses/ScoreComponentCollection.h"

#include "moses/graph/WordsSet.h"
#include "moses/graph/InputSubgraph.h"
#include <set>
#include <vector>

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
GraphSimFeature::GraphSimFeature(const std::string &line)
  : StatelessFeatureFunction("GraphSimFeature",1, line)
{
  ReadParameters();
}

void GraphSimFeature::Evaluate(const InputType &input
      , const Moses::Graph::InputSubgraph &inputPath
      , ScoreComponentCollection &scoreBreakdown) const
{
  typedef vector< set<size_t> > StructType;
  const StructType& rsnt = inputPath.GetRuleSourceNT();
  const StructType& isnt = inputPath.GetInputSourceNT();

  if (rsnt.size() <= 1) {
    return;
  }

//  CHECK(rsnt.size() == isnt.size());

  vector<size_t> rvec, ivec;
  size_t length = rsnt.size();
  for (size_t i = 0; i < length; i++) {
    set<size_t> su (rsnt[i]);
    su.insert(isnt[i].begin(), isnt[i].end());
    for(set<size_t>::const_iterator iter = su.begin();
        iter != su.end(); ++iter) {
      size_t value = *iter;
      if (rsnt[i].find(value) == rsnt[i].end()) rvec.push_back(0);
      else rvec.push_back(1);
      if (isnt[i].find(value) == isnt[i].end()) ivec.push_back(0);
      else ivec.push_back(1);
    }
  }

  // cosine similarity
  size_t xy = 0.0f, x2 = 0.0f, y2 = 0.0f;
  for(size_t i = 0; i < rvec.size(); i++) {
    xy += rvec[i] * ivec[i];
    x2 += rvec[i] * rvec[i];
    y2 += ivec[i] * ivec[i];
  }
  float sim = 0.0f;
  if (x2 > 0 && y2 > 0) {
    sim = xy / (sqrt(x2) * sqrt(y2));
  }

  sim = Moses::FloorScore(Moses::TransformScore(sim));
  scoreBreakdown.Assign(this, sim);
}

} // namespace

