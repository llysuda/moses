
#include "DistortionScoreProducerPg2s.h"
#include "FFState.h"
//#include "moses/WordsRange.h"
#include "moses/StaticData.h"
#include "moses/graph/Pg2sHypothesis.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
struct DistortionPg2sState : public FFState {
  int endPos;
  DistortionPg2sState(int end) : endPos(end) {}
  int Compare(const FFState& other) const {
    const DistortionPg2sState& o =
      static_cast<const DistortionPg2sState&>(other);
    if (endPos < o.endPos) return -1;
    if (endPos > o.endPos) return 1;
    return 0;
  }
};

DistortionScoreProducerPg2s::DistortionScoreProducerPg2s(const std::string &line)
  : StatefulFeatureFunction("DistortionPg2s", 2, line)
{
  ReadParameters();
}

const FFState* DistortionScoreProducerPg2s::EmptyHypothesisState(const InputType &input) const
{
  // fake previous translated phrase start and end
//  size_t start = NOT_FOUND;
//  size_t end = NOT_FOUND;
//  if (input.m_frontSpanCoveredLength > 0) {
//    // can happen with --continue-partial-translation
//    start = 0;
//    end = input.m_frontSpanCoveredLength -1;
//  }
  return new DistortionPg2sState(-1);
}

vector<float> DistortionScoreProducerPg2s::CalculateDistortionScore(const Pg2sHypothesis& hypo,
    int prevEnd, const set<size_t> &curr, bool naive)
{
    return hypo.GetInput().ComputeDistortionDistance(prevEnd, curr, naive);
}


FFState* DistortionScoreProducerPg2s::Evaluate(
  const Pg2sHypothesis& hypo,
  const FFState* prev_state,
  ScoreComponentCollection* out) const
{
  const DistortionPg2sState* prev = static_cast<const DistortionPg2sState*>(prev_state);
  const vector<float> distortionScore = CalculateDistortionScore(
                                  hypo,
								  prev->endPos,
                                  hypo.GetCurrSourceWordsRange().GetSet(),
                                  StaticData::Instance().GetNaiveDistortion());
  out->PlusEquals(this, distortionScore);
  int max = hypo.GetCurrSourceWordsRange().GetMax();
  DistortionPg2sState* res = new DistortionPg2sState(max);
  return res;
}


}

