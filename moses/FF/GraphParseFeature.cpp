#include <boost/algorithm/string.hpp>

#include "GraphParseFeature.h"
#include "moses/AlignmentInfo.h"
#include "moses/TargetPhrase.h"
#include "moses/Hypothesis.h"
#include "moses/TranslationOption.h"
#include "util/string_piece_hash.hh"
#include "util/exception.hh"

#include "moses/graph/InputSubgraph.h"
#include "moses/graph/WordsSet.h"
#include "moses/graph/MultiDiGraphInput.h"
#include "moses/InputFileStream.h"
#include "moses/Util.h"
#include "moses/graph/TransOptPg2s.h"
#include "moses/graph/Pg2sHypothesis.h"
#include "moses/graph/G2sTranslationOption.h"
#include "moses/graph/G2sHypothesis.h"

#include "FFState.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{

struct GraphParseState : public FFState {
  size_t width, height;
  GraphParseState(size_t w, size_t h) : width(w), height(h) {}
  int Compare(const FFState& other) const {
    const GraphParseState& o =
          static_cast<const GraphParseState&>(other);
    if (width>o.width) return 1;
    if (width<o.width) return -1;
    if (height>o.height) return 1;
    if (height<o.height) return -1;
    return 0;
  }
};

GraphParseFeature::GraphParseFeature(const std::string &line)
  :StatefulFeatureFunction("GraphParseFeature", 2, line)
{
//  std::cerr << "Initializing GraphParseFeature.." << std::endl;
  ReadParameters();
}


FFState* GraphParseFeature::Evaluate(
    const Moses::Graph::G2sHypothesis& cur_hypo,
    const FFState* prev_state,
    ScoreComponentCollection* accumulator) const
{
//  const GraphParseState* s = static_cast<const GraphParseState*>(prev_state);
//  size_t w = s->width, h = s->height;
//  const G2sHypothesis* prevHypo = cur_hypo.GetPrevHypo();
//  const G2sHypothesis* prevPointHypo = cur_hypo.GetPrevPointHypo();
//  if (prevHypo == prevPointHypo) {
//    h += 1;
//  }
//  if (prevHypo != prevPointHypo) {
//    w += 1;
//  }
//  vector<float> scores(2);
//  scores[0] = h;
//  scores[1] = w;
//  accumulator->Assign(this, scores);
//  return new GraphParseState(w,h);
  return NULL;
}

const FFState* GraphParseFeature::EmptyHypothesisState(const InputType &input) const
{
  return new GraphParseState(0,0);
}

bool GraphParseFeature::IsUseable(const FactorMask &mask) const
{
  return true;
}




}
