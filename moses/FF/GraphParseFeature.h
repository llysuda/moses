#ifndef moses_GraphParseFeature_h
#define moses_GraphParseFeature_h

#include <map>
#include <string>
#include <set>

#include <stdexcept>
#include <boost/unordered_set.hpp>
#include <boost/functional/hash.hpp>

#include "StatefulFeatureFunction.h"
#include "moses/Factor.h"
#include "moses/Sentence.h"

namespace Moses
{
class FFState;

namespace Graph
{
class InputSubgraph;
}

/**
  * Phrase pair feature: complete source/target phrase pair
  **/
class GraphParseFeature: public StatefulFeatureFunction
{

public:
  GraphParseFeature(const std::string &line);

  bool IsUseable(const FactorMask &mask) const;

  virtual const FFState* EmptyHypothesisState(const InputType &input) const;

  virtual FFState* Evaluate(
    const Moses::Graph::G2sHypothesis& cur_hypo,
    const FFState* prev_state,
    ScoreComponentCollection* accumulator) const;

  virtual FFState* Evaluate(
      const Hypothesis& cur_hypo,
      const FFState* prev_state,
      ScoreComponentCollection* accumulator) const {
      throw std::logic_error("G2sGlueFeature not supported in pb decoder, yet");
  }

  virtual FFState* EvaluateChart(
    const ChartHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection*) const {
    throw std::logic_error("G2sGlueFeature not supported in chart decoder, yet");
  }

};

}


#endif
