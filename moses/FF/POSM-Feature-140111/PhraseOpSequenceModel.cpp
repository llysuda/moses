#include <fstream>
#include <set>
#include <map>
#include "PhraseOpSequenceModel.h"
#include "posmHyp.h"
#include "util/check.hh"
#include "moses/Util.h"

using namespace std;
using namespace lm::ngram;

namespace Moses
{

PhraseOpSequenceModel::PhraseOpSequenceModel(const std::string &line)
  :StatefulFeatureFunction("PhraseOpSequenceModel", 5, line )
{
  //cerr << "new PhraseOpSequenceModel" << endl;
  ReadParameters();
}

void PhraseOpSequenceModel :: readLanguageModel(const char *lmFile)
{
  //cerr << "reading " << lmFile << endl;
  string unkOp = "_TRANS_SLF_";
  OSM = new Model(m_lmPath.c_str());
  State startState = OSM->NullContextState();
  State endState;
  unkOpProb = OSM->Score(startState,OSM->GetVocabulary().Index(unkOp),endState);
  //cerr << "end" << endl;
}


void PhraseOpSequenceModel::Load()
{

  readLanguageModel(m_lmPath.c_str());

}



void PhraseOpSequenceModel:: Evaluate(const Phrase &source
                                , const TargetPhrase &targetPhrase
                                , ScoreComponentCollection &scoreBreakdown
                                , ScoreComponentCollection &estimatedFutureScore) const
{
  //cerr << "posm " << endl;
  posmHypothesis obj;
  obj.setState(OSM->NullContextState());
  //WordsBitmap myBitmap(source.GetSize());
  vector <string> mySourcePhrase;
  vector <string> myTargetPhrase;
  vector<float> scores(5);
  vector <int> alignments;
  int startIndex = 0;
  int endIndex = source.GetSize();

  const AlignmentInfo &align = targetPhrase.GetAlignTerm();
  AlignmentInfo::const_iterator iter;


  for (iter = align.begin(); iter != align.end(); ++iter) {
    alignments.push_back(iter->first);
    alignments.push_back(iter->second);
  }

  for (int i = 0; i < targetPhrase.GetSize(); i++) {
    if (targetPhrase.GetWord(i).IsOOV())
      myTargetPhrase.push_back("_TRANS_SLF_");
    else
      myTargetPhrase.push_back(targetPhrase.GetWord(i).GetFactor(0)->GetString().as_string());
  }

  for (int i = 0; i < source.GetSize(); i++) {
    mySourcePhrase.push_back(source.GetWord(i).GetFactor(0)->GetString().as_string());
  }

  //
  vector<concept> concepts = GetPhrasePairs(mySourcePhrase,myTargetPhrase,alignments);
  ReconstructPhrase(mySourcePhrase, myTargetPhrase, alignments, concepts);
  vector<concept> preCepts;
  WordsBitmap myBitmap(mySourcePhrase.size());
  //ResetIndex(startIndex, endIndex, preCepts, concepts);
  startIndex = 0;
  endIndex = mySourcePhrase.size();
  //
  obj.setPhrases(mySourcePhrase , myTargetPhrase);
  obj.constructCepts(alignments,startIndex,endIndex-1,myTargetPhrase.size());
  obj.computeOSMFeature(startIndex,myBitmap);
  obj.calculateOSMProb(*OSM);
  obj.populateScores(scores);
  estimatedFutureScore.PlusEquals(this, scores);

}


FFState* PhraseOpSequenceModel::Evaluate(
  const Hypothesis& cur_hypo,
  const FFState* prev_state,
  ScoreComponentCollection* accumulator) const
{
  const TargetPhrase &target = cur_hypo.GetCurrTargetPhrase();
  const WordsBitmap &bitmap = cur_hypo.GetWordsBitmap();
  WordsBitmap myBitmap = bitmap;
  const Manager &manager = cur_hypo.GetManager();
  const InputType &source = manager.GetSource();
  const Sentence &sourceSentence = static_cast<const Sentence&>(source);
  posmHypothesis obj;
  vector <string> mySourcePhrase;
  vector <string> myTargetPhrase;
  vector<float> scores(5);


  //target.GetWord(0)

  //cerr << target <<" --- "<<target.GetSourcePhrase()<< endl;  // English ...

  //cerr << align << endl;   // Alignments ...
  //cerr << cur_hypo.GetCurrSourceWordsRange() << endl;

  //cerr << source <<endl;

// int a = sourceRange.GetStartPos();
// cerr << source.GetWord(a);
  //cerr <<a<<endl;

  //const Sentence &sentence = static_cast<const Sentence&>(curr_hypo.GetManager().GetSource());


  const WordsRange & sourceRange = cur_hypo.GetCurrSourceWordsRange();
  int startIndex  = sourceRange.GetStartPos();
  int endIndex = sourceRange.GetEndPos();
  const AlignmentInfo &align = cur_hypo.GetCurrTargetPhrase().GetAlignTerm();
  posmState * statePtr;

  vector <int> alignments;

  AlignmentInfo::const_iterator iter;

  for (iter = align.begin(); iter != align.end(); ++iter) {
    //cerr << iter->first << "----" << iter->second << " ";
    alignments.push_back(iter->first);
    alignments.push_back(iter->second);
  }

  for (int i = startIndex; i <= endIndex; i++) {
    mySourcePhrase.push_back(source.GetWord(i).GetFactor(0)->GetString().as_string());
  }

  for (int i = 0; i < target.GetSize(); i++) {

    if (target.GetWord(i).IsOOV())
      myTargetPhrase.push_back("_TRANS_SLF_");
    else
      myTargetPhrase.push_back(target.GetWord(i).GetFactor(0)->GetString().as_string());

  }

// for posm

  vector<concept> concepts = GetPhrasePairs(mySourcePhrase,myTargetPhrase,alignments);
  ReconstructPhrase(mySourcePhrase, myTargetPhrase, alignments, concepts);


  vector<concept>::iterator iter2;
  vector<concept> allCepts = static_cast <const posmState *> (prev_state)->phrasePairs;
  int tstart = cur_hypo.GetCurrTargetWordsRange().GetStartPos();
  for(iter2 = concepts.begin(); iter2 != concepts.end(); iter2++) {
    iter2->first.first += tstart;
    iter2->first.second += tstart;
    iter2->second.first += startIndex;
    iter2->second.second += startIndex;

    allCepts.push_back(*iter2);
  }

  vector<concept> preCepts = static_cast <const posmState *> (prev_state)->phrasePairs;
  WordsBitmap myBitmap2 = ReconstructWordsBitmap(myBitmap, allCepts);

  ResetIndex(startIndex, endIndex, preCepts, concepts);
  for (int i = startIndex; i <= endIndex; i++) {
      myBitmap2.SetValue(i,0); // resetting coverage of this phrase ...
  }
  //cerr<<myBitmap<<endl;

  obj.setState(prev_state);
  obj.constructCepts(alignments,startIndex,endIndex,myTargetPhrase.size());
  obj.setPhrases(mySourcePhrase , myTargetPhrase);
  obj.computeOSMFeature(startIndex,myBitmap2);
  obj.calculateOSMProb(*OSM);
  obj.populateScores(scores);

  /*
    if (bitmap.GetFirstGapPos() == NOT_FOUND)
    {

      int xx;
  	 cerr<<bitmap<<endl;
  	 int a = bitmap.GetFirstGapPos();
  	 obj.print();
      cin>>xx;
    }
    */

  /*
    vector<float> scores(5);
    scores[0] = 0.343423f;
    scores[1] = 1.343423f;
    scores[2] = 2.343423f;
    scores[3] = 3.343423f;
    scores[4] = 4.343423f;
    */

  accumulator->PlusEquals(this, scores);

  posmState* newState = obj.saveState();
  newState->phrasePairs = allCepts;

  return newState;

}

void PhraseOpSequenceModel :: getMeCepts ( pair <int,int> & eSide , pair <int,int> & fSide , map <int , vector <int> > & tS , map <int , vector <int> > & sT) const
{
  set <int> :: iterator iter;

  int sz = eSide.second-eSide.first+1;
  vector <int> t;

  for (int i = eSide.first; i <= eSide.second; i++) {
    t = tS[i];

    for (int j = 0; j < t.size(); j++) {
      fSide.first = std::min(fSide.first,t[j]);
      fSide.second = std::max(fSide.second,t[j]);
    }

  }

  for (int i = fSide.first; i <= fSide.second; i++) {

    t = sT[i];

    for (int j = 0 ; j<t.size(); j++) {
      eSide.first = std::min(eSide.first,t[j]);
      eSide.second = std::max(eSide.second,t[j]);
    }

  }

  if (eSide.second-eSide.first+1 > sz) {
    getMeCepts(eSide,fSide,tS,sT);
  }

}

void PhraseOpSequenceModel::ResetIndex(int& start, int& end, const std::vector<concept> & preCepts, const std::vector<concept> & curCepts) const {
  vector<concept>::const_iterator iter;
  int startDelCount = 0;
  for(iter = preCepts.begin(); iter != preCepts.end(); iter++) {
    if (start > iter->second.first) {
      startDelCount += iter->second.second-iter->second.first;
    }
  }
  start -= startDelCount;
  end -= startDelCount;

  for (iter = curCepts.begin(); iter != curCepts.end(); iter++) {
    end -= (iter->second.second-iter->second.first);
  }
}

WordsBitmap PhraseOpSequenceModel::ReconstructWordsBitmap(WordsBitmap & bitmap, const std::vector<concept> & concepts) const {

  int srcFlag[bitmap.GetSize()];
  for (int i = 0; i < bitmap.GetSize(); i++) {
    srcFlag[i] = -1;
  }
  vector<concept>::const_iterator iter;
  int count = 0, preEnd = -1;
  for(iter = concepts.begin(); iter != concepts.end(); iter++) {
    for (int i = preEnd+1; i < iter->first.first; i++) {
      count++;
    }
    for (int i = iter->second.first; i <= iter->second.second; i++) {
      srcFlag[i] = count;
    }
    count++;
    preEnd = iter->first.second;
  }

  count = 0;
  vector<bool> compressedFlags;
  for (int i = 0; i < bitmap.GetSize(); i++) {
    if (srcFlag[i] == -1) {
      compressedFlags.push_back(bitmap.GetValue(i));
      count++;
    } else {
      bool value = bitmap.GetValue(i);
      int j = i+1;
      for (int j = i+1; j < bitmap.GetSize() && srcFlag[j] == srcFlag[j-1]; j++) {

      }
      compressedFlags.push_back(value);
      count++;
      i = j-1;
    }
  }

  WordsBitmap compressedBitmap(compressedFlags.size(),compressedFlags);
  //bitmap = compressedBitmap;
  return compressedBitmap;
}

void PhraseOpSequenceModel::ReconstructPhrase(vector<string>& source, vector<string>& target, vector<int> & align, const vector<concept>& concepts) const {

  //vector<concept> concepts = GetPhrasePairs(source,target,align);

  int srcFlag[source.size()];
  for (int i = 0; i < source.size(); i++) {
      srcFlag[i] = -1;
    }
  vector<concept>::const_iterator iter;
  vector<string> tgt;
  int count = 0, preEnd = -1;
  for(iter = concepts.begin(); iter != concepts.end(); iter++) {
    for (int i = preEnd+1; i < iter->first.first; i++) {
      tgt.push_back(target[i]);
      count++;
    }
    string word = target[iter->first.first];
    for (int i = iter->first.first+1; i <= iter->first.second; i++) {
      word += "*" + target[i];
    }
    tgt.push_back(word);
    for (int i = iter->second.first; i <= iter->second.second; i++) {
      srcFlag[i] = count;
    }
    count++;
    preEnd = iter->first.second;
  }
  for(int i = preEnd+1; i < target.size(); i++) {
    tgt.push_back(target[i]);
  }

  vector<string> src;
  vector<int> algn;
  count = 0;
  for (int i = 0; i < source.size(); i++) {
    if (srcFlag[i] == -1) {
      src.push_back(source[i]);
      count++;
    } else {
      string word = source[i];
      int j = i+1;
      for (int j = i+1; j < source.size() && srcFlag[j] == srcFlag[j-1]; j++) {
        word += "*" + source[j];
      }
      src.push_back(word);
      algn.push_back(count);
      algn.push_back(srcFlag[i]);
      count++;
      i = j-1;
    }
  }
  //
  source = src;
  target = tgt;
  align = algn;
  //
  //for(iter = concepts.begin(); iter != concepts.end(); iter++) {
  //  iter->first.first += tIndex;
  //  iter->first.second += tIndex;
  //  iter->second.first += sIndex;
  //  iter->second.second += sIndex;
  //}
  //return concepts;
}

vector<pair<pair<int,int>,pair<int,int> > > PhraseOpSequenceModel::GetPhrasePairs(const vector<string>& source, const vector<string>& target, const vector<int> & align) const {

  std::map <int , vector <int> > sT;
  std::map <int , vector <int> > tS;
  std::vector <int> :: iterator iter;
  std :: map <int , vector <int> > :: iterator iter2;
  std::pair <int,int> eSide;
  std::pair <int,int> fSide = make_pair(target.size(),-1);
  int src;
  int tgt;
  for (int i = 0;  i < align.size(); i+=2) {
      src = align[i];
      tgt = align[i+1];
      tS[tgt].push_back(src);
      sT[src].push_back(tgt);
  }

  vector<concept > concepts;
  while (tS.size() != 0 && sT.size() != 0) {

    iter2 = tS.begin();

    //eSide.clear();
    //fSide.clear();
    eSide = make_pair(iter2->first, iter2->first);

    getMeCepts(eSide, fSide, tS , sT);

    for (int i = eSide.first; i<= eSide.second; i++) {
      iter2 = tS.find(i);
      if (iter2 != tS.end())
        tS.erase(iter2);
    }

    for (int i = fSide.first; i <= fSide.second; i++) {
      iter2 = sT.find(i);
      if (iter2 != sT.end())
        sT.erase(iter2);
    }

    concept cept = make_pair(eSide,fSide);
    AddConcept(cept,concepts);
  }
  return concepts;
}

void PhraseOpSequenceModel::AddConcept(concept & cept, std::vector<concept >& concepts) const {

  vector<concept> cache;
  vector<concept>::iterator iter;
  for (iter = concepts.begin(); iter != concepts.end(); iter++) {
    concept& other = *iter;

    if ((cept.first.first > other.first.second || cept.first.second < other.first.first) &&
        (cept.second.first > other.second.second || cept.second.second < other.second.first) ) {
      // not over lap
      cache.push_back(other);
    }
  }

  concepts.clear();

  bool added = false;
  for (iter = cache.begin(); iter != cache.end(); iter++) {
    if (cept.first.first < iter->first.first && !added) {
      concepts.push_back(cept);
      added = true;
    }
    concepts.push_back(*iter);
  }

  if (!added) {
    concepts.push_back(cept);
  }
}

FFState* PhraseOpSequenceModel::EvaluateChart(
  const ChartHypothesis& /* cur_hypo */,
  int /* featureID - used to index the state in the previous hypotheses */,
  ScoreComponentCollection* accumulator) const
{
  abort();

}

const FFState* PhraseOpSequenceModel::EmptyHypothesisState(const InputType &input) const
{
  cerr << "PhraseOpSequenceModel::EmptyHypothesisState()" << endl;

  State startState = OSM->BeginSentenceState();

  return new posmState(startState);
}

std::string PhraseOpSequenceModel::GetScoreProducerWeightShortName(unsigned idx) const
{
  return "posm";
}

std::vector<float> PhraseOpSequenceModel::GetFutureScores(const Phrase &source, const Phrase &target) const
{
  ParallelPhrase pp(source, target);
  std::map<ParallelPhrase, Scores>::const_iterator iter;
  iter = m_futureCost.find(pp);
//iter = m_coll.find(pp);
  if (iter == m_futureCost.end()) {
    vector<float> scores(5, 0);
    scores[0] = unkOpProb;
    return scores;
  } else {
    const vector<float> &scores = iter->second;
    return scores;
  }
}

void PhraseOpSequenceModel::SetParameter(const std::string& key, const std::string& value)
{

  if (key == "path") {
    m_lmPath = value;
  } else if (key == "order") {
    lmOrder = Scan<int>(value);
  } else {
    StatefulFeatureFunction::SetParameter(key, value);
  }
}

bool PhraseOpSequenceModel::IsUseable(const FactorMask &mask) const
{
  bool ret = mask[0];
  return ret;
}

} // namespace
