#pragma once

#include <string>
#include <map>
#include <vector>
#include "moses/FF/StatefulFeatureFunction.h"
#include "moses/Manager.h"
#include "moses/FF/POSM-Feature/posmHyp.h"
#include "lm/model.hh"


namespace Moses
{


class PhraseOpSequenceModel : public StatefulFeatureFunction
{
public:
  typedef std::pair<std::pair<int,int>,std::pair<int,int> > concept;
  lm::ngram::Model * OSM;

  int lmOrder;
  float unkOpProb;

  PhraseOpSequenceModel(const std::string &line);

  void readLanguageModel(const char *);
  void Load();

  FFState* Evaluate(
    const Hypothesis& cur_hypo,
    const FFState* prev_state,
    ScoreComponentCollection* accumulator) const;

  void  Evaluate(const Phrase &source
                 , const TargetPhrase &targetPhrase
                 , ScoreComponentCollection &scoreBreakdown
                 , ScoreComponentCollection &estimatedFutureScore) const;

  virtual FFState* EvaluateChart(
    const ChartHypothesis& /* cur_hypo */,
    int /* featureID - used to index the state in the previous hypotheses */,
    ScoreComponentCollection* accumulator) const;

  virtual const FFState* EmptyHypothesisState(const InputType &input) const;

  virtual std::string GetScoreProducerWeightShortName(unsigned idx=0) const;

  std::vector<float> GetFutureScores(const Phrase &source, const Phrase &target) const;
  void SetParameter(const std::string& key, const std::string& value);

  bool IsUseable(const FactorMask &mask) const;
  void getMeCepts(std::pair <int,int> & eSide , std::pair <int,int> & fSide , std::map <int , std::vector <int> > & tS , std::map <int , std::vector <int> > & sT) const;
  std::vector<std::pair<std::pair<int,int>,std::pair<int,int> > >  GetPhrasePairs(const std::vector<std::string>& source, const std::vector<std::string>& target, const std::vector<int> & align) const;
  void AddConcept(concept & cept, std::vector<concept >& concepts) const;
  void ReconstructPhrase(std::vector<std::string>&, std::vector<std::string>&, std::vector<int> &, const std::vector<concept>&) const;
  WordsBitmap ReconstructWordsBitmap(WordsBitmap & bitmap, const std::vector<concept> & concepts) const;
  void ResetIndex(int& start, int& end, const std::vector<concept> & preCepts, const std::vector<concept> & curCepts) const;

protected:
  typedef std::pair<Phrase, Phrase> ParallelPhrase;
  typedef std::vector<float> Scores;
  std::map<ParallelPhrase, Scores> m_futureCost;

  std::vector < std::pair < std::set <int> , std::set <int> > > ceptsInPhrase;
  std::set <int> targetNullWords;
  std::string m_lmPath;


};


} // namespace
