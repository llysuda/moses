#ifndef moses_DependencyInput_h
#define moses_DependencyInput_h


#include <vector>
#include <map>
#include <set>
#include <string>
#include "TreeInput.h"
#include "Word.h"
#include "Factor.h"
#include "Phrase.h"
#include "Sentence.h"

namespace Moses
{


/** An input to the decoder that represent a parse tree.
 *  Implemented as a sentence with non-terminal labels over certain ranges.
 *  This representation doesn't necessarily have to form a tree, it's up to the user to make sure it does if they really want a tree.
 *  @todo Need to rewrite if you want packed forest, or packed forest over lattice - not sure if can inherit from this
 */
class DependencyInput : public TreeInput
{

private:

	class Hole {
	public:
		size_t start;
		size_t end;
		std::string label;
		Hole(size_t istart, size_t iend, const std::string& ilabel)
			: start(istart)
			, end(iend)
		  , label(ilabel){}

	};

	//std::vector<std::vector<Word> > m_wordColl;
	//std::vector<std::vector<bool> > m_valid;
	std::vector<size_t> m_fid;
	std::vector<std::string> m_pos;
	std::vector<std::string> m_word;
	//std::vector<std::vector<std::set<std::string> > > m_serg;
	//std::vector<std::vector<std::set<std::string> > > m_index;

	//bool valid(size_t start, size_t end) const;

public:
  DependencyInput() {
  }

  InputTypeEnum GetType() const {
    return DependencyInputType;
  }

  //! populate this InputType with data from in stream
  virtual int Read(std::istream& in,const std::vector<FactorType>& factorOrder) {
  	if (!Sentence::Read(in, factorOrder))
  		return 0;
  	InitFid();
  	//
  	//InitSergCollection();
  	//InitDepWordColl();
  	ExpandNonTerm();
  	ResetWords();
  	return 1;
  }

  void ResetWords();

  size_t GetSize() const {
		return Phrase::GetSize();
	}

  //! Output debugging info to stream out
  virtual void Print(std::ostream& out) const {
  	TreeInput::Print(out);
  }

  //! create trans options specific to this InputType
  virtual TranslationOptionCollection* CreateTranslationOptionCollection() const {
  	return NULL;
  }

  virtual const NonTerminalSet &GetLabelSet(size_t startPos, size_t endPos) const {
    return TreeInput::GetLabelSet(startPos, endPos);
  }

  int GetSpanFid(size_t startPos, size_t endPos) const;
  std::vector<size_t> GetHeads(size_t startPos, size_t endPos) const;
  WordsRange GetSourceSpan(size_t head) const;
  WordsRange GetMinCoverCompleteSpan(size_t startPos, size_t endPos) const;
  //void InitDepWordColl();
  //void InitValidSpan();
  void InitFid();
  //void InitSergCollection();
  void ExpandNonTerm();
  //void addHierRule(size_t start, size_t end, std::vector<Hole*>& holes, size_t initS, size_t wcount);
  std::vector<std::string> GetSpanLabel(size_t start, size_t end, bool boundary) const;
  //void saveHierRule(size_t startS, size_t endS, const std::vector<Hole*>& holes);
  //int GetSpanFid(size_t start, size_t end) const;
  //void addRule(size_t start, size_t end);

  std::string GetDepString(const WordsRange &range, const std::vector<std::pair<const WordsRange*, const Word*> > &holes) const;
  std::string GetDepString(const WordsRange &range, const std::vector<std::pair<size_t, size_t> > &holes) const;
  std::string GetSpanLabelWithExtEdge(size_t start, size_t end) const;
  std::map<size_t, size_t> GetExternalNodes(size_t start, size_t end) const;

  /*const std::vector<Word>& GetPossibleDepWords(size_t pos) const {
  	return m_wordColl[pos];
  }*/

  bool HasMoreNT(size_t start, size_t end) const {
  	return m_sourceChart[start][end-start].size() > 1;
  	//return m_valid[start][end-start];
  }

  bool isComplete(size_t start, size_t end) const;
  bool IsConnected(size_t start, size_t end) const;
  bool valid(size_t start, size_t end, size_t startHole, size_t endHole) const;
  bool valid(size_t start, size_t end) const;


  std::map<size_t,size_t> GetExternalNodes(size_t start, size_t end, size_t startHole, size_t endHole) const;
  void InitContextAwareLabel(size_t startS, size_t endS);
  std::vector<std::string> GetLabels(size_t start, size_t end, size_t startHole, size_t endHole) const;

  bool hasPartialChild(size_t start, size_t end) const;

  std::string GetPos(size_t pos) const {
  	return m_pos[pos];
  }

  std::string GetWordStr(size_t pos) const {
    return m_word[pos];
  }

  std::vector<size_t> GetKids(size_t start, size_t end) const;
  std::vector<size_t> GetSibs(size_t start, size_t end) const;

  /*bool IsValidRule(const WordsRange& range, const std::string& seq) const {
  	const std::set<std::string>& serg = m_serg[range.GetStartPos()][range.GetEndPos()-range.GetStartPos()];
  	//std::cerr << "keys for : " << seq << std::endl;
  	//for(std::set<std::string>::const_iterator iter = serg.begin(); iter != serg.end(); iter++) {
  	//	std::cerr << " " << *iter << " " << (*iter == seq) << std::endl;
  	//}
  	//std::cerr << (serg.find(seq) != serg.end()) << std::endl;
  	return serg.find(seq) != serg.end();
  }*/

  /*size_t GetSize() const {
  	return TreeInput::GetSize();
  }*/
  /*int GetDepth(int start, int end, int pos) const;
    int GetDepth(int start, int end, int startHole, int endHole) const;
    bool isInDependent(int start, int end, int startHole, int endHole) const;
    bool isComplete(int start, int end) const;*/
};

}

#endif
