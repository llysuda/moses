#ifndef moses_DepNgramInput_h
#define moses_DepNgramInput_h


#include <vector>
#include <map>
#include <set>
#include <string>
#include "TreeInput.h"
#include "Word.h"
#include "Factor.h"
#include "Phrase.h"
#include "Sentence.h"

namespace Moses
{


class Edge{
    public:
      int incoming;
      std::string relation;

      Edge(int id, const std::string& rel) {
        incoming = id;
        relation = rel;
      }

      Edge(const Edge& copy) {
        incoming = copy.incoming;
        relation = copy.relation;
      }

    };

    struct EdgeSetSortor{
      bool operator()(const Edge& ea, const Edge& eb) const {
          if( ea.incoming == eb.incoming)
            return ea.relation < eb.relation;
          return ea.incoming < eb.incoming;
      }
    };
/** An input to the decoder that represent a parse tree.
 *  Implemented as a sentence with non-terminal labels over certain ranges.
 *  This representation doesn't necessarily have to form a tree, it's up to the user to make sure it does if they really want a tree.
 *  @todo Need to rewrite if you want packed forest, or packed forest over lattice - not sure if can inherit from this
 */
class DepNgramInput : public TreeInput
{

private:

//	class Hole {
//	public:
//		size_t start;
//		size_t end;
//		std::string label;
//		Hole(size_t istart, size_t iend, const std::string& ilabel)
//			: start(istart)
//			, end(iend)
//		  , label(ilabel){}
//
//	};

	//std::vector<std::vector<Word> > m_wordColl;
	//std::vector<std::vector<bool> > m_valid;
	std::vector<int> m_fid;
	std::vector<std::string> m_pos;
	std::vector<std::string> m_word;
	std::vector<std::string> m_rel;
	std::vector<std::vector<Edge> > m_incomings;
	std::vector<std::vector<int> > m_outgoings;
	//std::vector<std::vector<std::set<std::string> > > m_serg;
	//std::vector<std::vector<std::set<std::string> > > m_index;

	//bool valid(size_t start, size_t end) const;

public:
  DepNgramInput() {
  }

  InputTypeEnum GetType() const {
    return DepNgramInputType;
  }

  //! populate this InputType with data from in stream
  virtual int Read(std::istream& in,const std::vector<FactorType>& factorOrder) {
  	if (!Sentence::Read(in, factorOrder))
  		return 0;
  	InitFid();
  	//
  	//InitSergCollection();
  	//InitDepWordColl();
  	ExpandNonTerm();
  	ResetWords();
  	return 1;
  }

  void ResetWords();

  size_t GetSize() const {
		return Phrase::GetSize();
	}

  //! Output debugging info to stream out
  virtual void Print(std::ostream& out) const {
  	TreeInput::Print(out);
  }

  //! create trans options specific to this InputType
  virtual TranslationOptionCollection* CreateTranslationOptionCollection() const {
  	return NULL;
  }

  virtual const NonTerminalSet &GetLabelSet(size_t startPos, size_t endPos) const {
    return TreeInput::GetLabelSet(startPos, endPos);
  }

  int GetSpanFid(size_t startPos, size_t endPos) const;
  std::vector<Edge> GetSpanIncomings(size_t startPos, size_t endPos) const;
  std::vector<size_t> GetHeads(size_t startPos, size_t endPos) const;
  WordsRange GetSourceSpan(size_t head) const;
  void InitFid();
  void ExpandNonTerm();
  std::vector<std::string> GetSpanLabel(size_t start, size_t end, bool boundary) const;

  std::string GetDepString(const WordsRange &range, const std::vector<std::pair<const WordsRange*, const Word*> > &holes) const;
  std::string GetDepString(const WordsRange &range, const std::vector<std::pair<size_t, size_t> > &holes) const;

  bool HasMoreNT(size_t start, size_t end) const {
  	return m_sourceChart[start][end-start].size() > 1;
  }

  std::vector<std::string> GetLabels(size_t start, size_t end, size_t startHole, size_t endHole) const;

  std::string GetPos(size_t pos) const {
  	return m_pos[pos];
  }

  std::string GetWordStr(size_t pos) const {
    return m_word[pos];
  }

  std::vector<size_t> GetKids(size_t start, size_t end) const;
  std::vector<size_t> GetSibs(size_t start, size_t end) const;

  bool BigramConnected(int im1,int i) const;
  bool NextIsOlder(int im1, int i) const;
  int GetLevel(int index) const;
  bool IsConnected(size_t start, size_t end) const;
  void DepthFirst(size_t start, size_t end, size_t current, std::set<int>& nodes) const;

  bool IsMSTLeaf(size_t start, size_t end) const;
  bool HasMST(size_t start, size_t end) const;
  bool HasMST(int start, int end, int startHoleS, int endHoleS) const;
  bool IsDirectedConnected(size_t rootNode, const std::set<size_t>& nodes) const;
  void DirectedDepthFirst(const std::set<size_t>& nodes, size_t current, std::set<size_t>& retNodes) const;

};

}

#endif
