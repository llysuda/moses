// $Id$

#include <list>
#include "TranslationOptionCollectionConfusionNet.h"
#include "ConfusionNet.h"
#include "DecodeStep.h"
#include "DecodeStepTranslation.h"
#include "DecodeStepGeneration.h"
#include "FactorCollection.h"
#include "FF/InputFeature.h"
#include "TranslationModel/PhraseDictionaryTreeAdaptor.h"

using namespace std;

namespace Moses
{

/** constructor; just initialize the base class */
TranslationOptionCollectionConfusionNet::TranslationOptionCollectionConfusionNet(
  const ConfusionNet &input
  , size_t maxNoTransOptPerCoverage, float translationOptionThreshold)
  : TranslationOptionCollection(input, maxNoTransOptPerCoverage, translationOptionThreshold)
{
  const InputFeature *inputFeature = StaticData::Instance().GetInputFeature();
  CHECK(inputFeature);

  size_t size = input.GetSize();
  size_t maxSize = StaticData::Instance().GetMaxPhraseLength();
  if (size < maxSize) {
      maxSize = size;
  }
  m_inputPathMatrix.resize(size);
  for (size_t startPos = 0; startPos < size; ++startPos) {
      vector<InputPathList> &vec = m_inputPathMatrix[startPos];
      //vector<RangeList> &vec2 = m_indexMatrix[startPos];
      vec.push_back(InputPathList());
  }
  for (size_t phaseSize = 2; phaseSize <= maxSize; ++phaseSize) {
	 for (size_t startPos = 0; startPos < size - phaseSize + 1; ++startPos) {
	   vector<InputPathList> &vec = m_inputPathMatrix[startPos];
	   vec.push_back(InputPathList());
	 }
  }

  //m_indexMatrix.resize(size)
//std::cerr << "1-word phrase" << std::endl;
  // 1-word phrases
  for (size_t startPos = 0; startPos < size; ++startPos) {
    vector<InputPathList> &vec = m_inputPathMatrix[startPos];
    //vector<RangeList> &vec2 = m_indexMatrix[startPos];
    //vec.push_back(InputPathList());
    //vec2.push_back(RangeList());
    //InputPathList &list = vec[0];
    //RangeList &list2 = vec2.back();

    const ConfusionNet::Column &col = input.GetColumn(startPos);
    for (size_t i = 0; i < col.size(); ++i) {
    	//if ( > 1)
    	//	continue
    	size_t endPos = startPos + input.GetColumnIncrement(startPos, i) - 1;
    	InputPathList &list = vec[endPos-startPos];
    	WordsRange range(startPos, endPos);
    const NonTerminalSet &labels = input.GetLabelSet(startPos, endPos);
      const Word &word = col[i].first;
      Phrase subphrase;
      subphrase.AddWord(word);

      const std::vector<float> &scores = col[i].second;
      ScoreComponentCollection *inputScore = new ScoreComponentCollection();
      inputScore->Assign(inputFeature, scores);

      InputPath *node = new InputPath(subphrase, labels, range, NULL, inputScore);
      node->index = i;
      list.push_back(node);
      //WordsRange index(startPos, i);
      //list2.push_back(index);

      m_phraseDictionaryQueue.push_back(node);
    }
  }

  // subphrases of 2+ words

 // std::cerr << "2+word" << std::endl;
  for (size_t phaseSize = 2; phaseSize <= maxSize; ++phaseSize) {
    for (size_t startPos = 0; startPos < size - phaseSize + 1; ++startPos) {
      size_t endPos = startPos + phaseSize -1;




      vector<InputPathList> &vec = m_inputPathMatrix[startPos];
      //vec.push_back(InputPathList());


      // loop thru every previous path
      const InputPathList &prevNodes = GetInputPathList(startPos, endPos - 1);

      int prevNodesInd = 0;
      InputPathList::const_iterator iterPath;
      for (iterPath = prevNodes.begin(); iterPath != prevNodes.end(); ++iterPath) {
        //for (size_t pathInd = 0; pathInd < prevNodes.size(); ++pathInd) {
        const InputPath &prevNode = **iterPath;
        //const InputPath &prevNode = *prevNodes[pathInd];

        const Phrase &prevPhrase = prevNode.GetPhrase();
        const ScoreComponentCollection *prevInputScore = prevNode.GetInputScore();
        CHECK(prevInputScore);

        // loop thru every word at this position
        const ConfusionNet::Column &col = input.GetColumn(endPos);

        for (size_t i = 0; i < col.size(); ++i) {
          const Word &word = col[i].first;
          Phrase subphrase(prevPhrase);
          subphrase.AddWord(word);

          if (subphrase.GetSize() > 10) {
            continue;
          }

          const std::vector<float> &scores = col[i].second;
          ScoreComponentCollection *inputScore = new ScoreComponentCollection(*prevInputScore);
          inputScore->PlusEquals(inputFeature, scores);

          WordsRange range(startPos, endPos + input.GetColumnIncrement(endPos,i) -1);
          const NonTerminalSet &labels = input.GetLabelSet(startPos, range.GetEndPos());

          InputPath *node = new InputPath(subphrase, labels, range, &prevNode, inputScore);
          node->index = i;
          InputPathList &list = vec[range.GetEndPos()-startPos];
          list.push_back(node);

          m_phraseDictionaryQueue.push_back(node);
        } // for (size_t i = 0; i < col.size(); ++i) {

        ++prevNodesInd;
      } // for (iterPath = prevNodes.begin(); iterPath != prevNodes.end(); ++iterPath) {
    }
  }
//    std::cerr << "end 2+" << std::endl;
  // check whether we should be using the old code to supportbinary phrase-table.
  // eventually, we'll stop support the binary phrase-table and delete this legacy code
  CheckLegacy();
}

InputPathList &TranslationOptionCollectionConfusionNet::GetInputPathList(size_t startPos, size_t endPos)
{
  size_t offset = endPos - startPos;
  CHECK(offset < m_inputPathMatrix[startPos].size());
  return m_inputPathMatrix[startPos][offset];
}


void TranslationOptionCollectionConfusionNet::ProcessUnknownWord()
{
  const vector<DecodeGraph*>& decodeGraphList = StaticData::Instance().GetDecodeGraphs();
  size_t size = m_source.GetSize();
  size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
  ConfusionNet const& source=dynamic_cast<ConfusionNet const&>(m_source);
  // try to translation for coverage with no trans by expanding table limit
  for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
    const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];
    for (size_t startPos = 0 ; startPos < size ; ++startPos) {
    	//ConfusionNet::Column const& coll=source.GetColumn(startPos);
    	for (size_t endPos = startPos; endPos < size && endPos < startPos + maxSizePhrase; ++endPos) {
    		InputPathList &inputPathList = GetInputPathList(startPos, endPos);
    		TranslationOptionList &fullList = GetTranslationOptionList(startPos, endPos);
    		size_t numTransOpt = fullList.size();
    		if (numTransOpt == 0) {
    			InputPathList::const_iterator iterInputPath;
    			for(iterInputPath = inputPathList.begin();
    					iterInputPath != inputPathList.end();++iterInputPath) {

    				InputPath &inputPath = **iterInputPath;
    				if (inputPath.GetPrevNode() == NULL) {
    					TranslationOptionCollection::CreateTranslationOptionsForRange(decodeGraph, startPos, endPos, false, graphInd, inputPath);
    				}

    			}
    		}
    	}
    }
  }

  bool alwaysCreateDirectTranslationOption = StaticData::Instance().IsAlwaysCreateDirectTranslationOption();
  // create unknown words for 1 word coverage where we don't have any trans options
  for (size_t startPos = 0 ; startPos < size ; ++startPos) {
	  for (size_t endPos = startPos; endPos < size && endPos < startPos + maxSizePhrase; ++endPos) {
		  InputPathList &inputPathList = GetInputPathList(startPos, endPos);
		  TranslationOptionList &fullList = GetTranslationOptionList(startPos, endPos);
		  if (fullList.size() == 0 || alwaysCreateDirectTranslationOption)
			  ProcessUnknownWord(startPos, endPos);
	  }
   }
}

void TranslationOptionCollectionConfusionNet::ProcessUnknownWord(size_t sourcePos)
{
	return;
}
/* forcibly create translation option for a particular source word.
	* call the base class' ProcessOneUnknownWord() for each possible word in the confusion network
	* at a particular source position
*/
void TranslationOptionCollectionConfusionNet::ProcessUnknownWord(size_t startPos, size_t endPos)
{
  ConfusionNet const& source=dynamic_cast<ConfusionNet const&>(m_source);

  ConfusionNet::Column const& coll=source.GetColumn(startPos);
  const InputPathList &inputPathList = GetInputPathList(startPos, endPos);


  InputPathList::const_iterator iterInputPath;
  size_t j=0;
  for(iterInputPath = inputPathList.begin();
		  iterInputPath != inputPathList.end();++iterInputPath) {
    const InputPath &inputPath = **iterInputPath;
    if (inputPath.GetPrevNode() != NULL)
    	continue;
    //size_t length = source.GetColumnIncrement(startPos, inputPath.index);
    ConfusionNet::Column::const_iterator iterCol = coll.begin() + inputPath.index;
    const Scores &inputScores = iterCol->second;
    ProcessOneUnknownWord(inputPath ,startPos, endPos-startPos+1, &inputScores);
  }

}

void TranslationOptionCollectionConfusionNet::CreateTranslationOptions()
{
  if (!m_useLegacy) {
    GetTargetPhraseCollectionBatch();
  }
  CreateTranslationOptions2();
}


/** create translation options that exactly cover a specific input span.
 * Called by CreateTranslationOptions() and ProcessUnknownWord()
 * \param decodeGraph list of decoding steps
 * \param factorCollection input sentence with all factors
 * \param startPos first position in input sentence
 * \param lastPos last position in input sentence
 * \param adhereTableLimit whether phrase & generation table limits are adhered to
 */
void TranslationOptionCollectionConfusionNet::CreateTranslationOptionsForRange(
  const DecodeGraph &decodeGraph
  , size_t startPos
  , size_t endPos
  , bool adhereTableLimit
  , size_t graphInd)
{
  if (m_useLegacy) {
    CreateTranslationOptionsForRangeLegacy(decodeGraph, startPos, endPos, adhereTableLimit, graphInd);
  } else {
    CreateTranslationOptionsForRangeNew(decodeGraph, startPos, endPos, adhereTableLimit, graphInd);
  }
}

void TranslationOptionCollectionConfusionNet::CreateTranslationOptionsForRangeNew(
  const DecodeGraph &decodeGraph
  , size_t startPos
  , size_t endPos
  , bool adhereTableLimit
  , size_t graphInd)
{
  InputPathList &inputPathList = GetInputPathList(startPos, endPos);
  InputPathList::iterator iter;
  for (iter = inputPathList.begin(); iter != inputPathList.end(); ++iter) {
    InputPath &inputPath = **iter;
    TranslationOptionCollection::CreateTranslationOptionsForRange(decodeGraph
        , startPos
        , endPos
        , adhereTableLimit
        , graphInd
        , inputPath);

  }
}

void TranslationOptionCollectionConfusionNet::CreateTranslationOptionsForRangeLegacy(
  const DecodeGraph &decodeGraph
  , size_t startPos
  , size_t endPos
  , bool adhereTableLimit
  , size_t graphInd)
{
  if ((StaticData::Instance().GetXmlInputType() != XmlExclusive) || !TranslationOptionCollection::HasXmlOptionsOverlappingRange(startPos,endPos)) {
    InputPathList &inputPathList = GetInputPathList(startPos, endPos);

    // partial trans opt stored in here
    PartialTranslOptColl* oldPtoc = new PartialTranslOptColl;
    size_t totalEarlyPruned = 0;

    // initial translation step
    list <const DecodeStep* >::const_iterator iterStep = decodeGraph.begin();
    const DecodeStep &decodeStep = **iterStep;

    static_cast<const DecodeStepTranslation&>(decodeStep).ProcessInitialTranslationLegacy
    (m_source, *oldPtoc
     , startPos, endPos, adhereTableLimit, inputPathList );

    // do rest of decode steps
    int indexStep = 0;

    for (++iterStep ; iterStep != decodeGraph.end() ; ++iterStep) {

      const DecodeStep *decodeStep = *iterStep;
      const DecodeStepTranslation *transStep =dynamic_cast<const DecodeStepTranslation*>(decodeStep);
      const DecodeStepGeneration *genStep =dynamic_cast<const DecodeStepGeneration*>(decodeStep);

      PartialTranslOptColl* newPtoc = new PartialTranslOptColl;

      // go thru each intermediate trans opt just created
      const vector<TranslationOption*>& partTransOptList = oldPtoc->GetList();
      vector<TranslationOption*>::const_iterator iterPartialTranslOpt;
      for (iterPartialTranslOpt = partTransOptList.begin() ; iterPartialTranslOpt != partTransOptList.end() ; ++iterPartialTranslOpt) {
        TranslationOption &inputPartialTranslOpt = **iterPartialTranslOpt;

        if (transStep) {
          transStep->ProcessLegacy(inputPartialTranslOpt
                                   , *decodeStep
                                   , *newPtoc
                                   , this
                                   , adhereTableLimit);
        } else {
          CHECK(genStep);
          genStep->Process(inputPartialTranslOpt
                           , *decodeStep
                           , *newPtoc
                           , this
                           , adhereTableLimit);
        }
      }

      // last but 1 partial trans not required anymore
      totalEarlyPruned += newPtoc->GetPrunedCount();
      delete oldPtoc;
      oldPtoc = newPtoc;

      indexStep++;
    } // for (++iterStep

    // add to fully formed translation option list
    PartialTranslOptColl &lastPartialTranslOptColl	= *oldPtoc;
    const vector<TranslationOption*>& partTransOptList = lastPartialTranslOptColl.GetList();
    vector<TranslationOption*>::const_iterator iterColl;
    for (iterColl = partTransOptList.begin() ; iterColl != partTransOptList.end() ; ++iterColl) {
      TranslationOption *transOpt = *iterColl;
      Add(transOpt);
    }

    lastPartialTranslOptColl.DetachAll();
    totalEarlyPruned += oldPtoc->GetPrunedCount();
    delete oldPtoc;
    // TRACE_ERR( "Early translation options pruned: " << totalEarlyPruned << endl);

  } // if ((StaticData::Instance().GetXmlInputType() != XmlExclusive) || !HasXmlOptionsOverlappingRange(startPos,endPos))

  if (graphInd == 0 && StaticData::Instance().GetXmlInputType() != XmlPassThrough && TranslationOptionCollection::HasXmlOptionsOverlappingRange(startPos,endPos)) {
      TranslationOptionCollection::CreateXmlOptionsForRange(startPos, endPos);
  }
}

void TranslationOptionCollectionConfusionNet::CheckLegacy()
{
  const std::vector<PhraseDictionary*> &pts = StaticData::Instance().GetPhraseDictionaries();
  for (size_t i = 0; i < pts.size(); ++i) {
    const PhraseDictionary *phraseDictionary = pts[i];
    if (dynamic_cast<const PhraseDictionaryTreeAdaptor*>(phraseDictionary) != NULL) {
      m_useLegacy = true;
      return;
    }
  }

  m_useLegacy = false;
}

bool TranslationOptionCollectionConfusionNet::HasXmlOptionsOverlappingRange(size_t startPosition, size_t endPosition, InputPath& inputPath) const
{
	ConfusionNet const& source=dynamic_cast<ConfusionNet const&>(m_source);
	return source.XmlOverlap(startPosition,endPosition, inputPath);
}

void TranslationOptionCollectionConfusionNet::CreateXmlOptionsForRange(size_t startPos, size_t endPos, InputPath& inputPath)
{
	ConfusionNet const& source=dynamic_cast<ConfusionNet const&>(m_source);
	  //InputPath &inputPath = GetInputPath(startPos,endPos);

	  vector <TranslationOption*> xmlOptions;
	  source.GetXmlTranslationOptions(xmlOptions,startPos,endPos, inputPath);

	  //get vector of TranslationOptions from Sentence
	  for(size_t i=0; i<xmlOptions.size(); i++) {
	    TranslationOption *transOpt = xmlOptions[i];
	    transOpt->SetInputPath(inputPath);
	    Add(transOpt);
	  }
}

void TranslationOptionCollectionConfusionNet::CreateTranslationOptions2()
{
  // loop over all substrings of the source sentence, look them up
  // in the phraseDictionary (which is the- possibly filtered-- phrase
  // table loaded on initialization), generate TranslationOption objects
  // for all phrases

  // there may be multiple decoding graphs (factorizations of decoding)
  const vector <DecodeGraph*> &decodeGraphList = StaticData::Instance().GetDecodeGraphs();
  const vector <size_t> &decodeGraphBackoff = StaticData::Instance().GetDecodeGraphBackoff();

  // length of the sentence
  const size_t size = m_source.GetSize();

  // loop over all decoding graphs, each generates translation options
  for (size_t graphInd = 0 ; graphInd < decodeGraphList.size() ; graphInd++) {
    if (decodeGraphList.size() > 1) {
      VERBOSE(3,"Creating translation options from decoding graph " << graphInd << endl);
    }

    const DecodeGraph &decodeGraph = *decodeGraphList[graphInd];
    // generate phrases that start at startPos ...
    for (size_t startPos = 0 ; startPos < size; startPos++) {
      size_t maxSize = size - startPos; // don't go over end of sentence
      size_t maxSizePhrase = StaticData::Instance().GetMaxPhraseLength();
      maxSize = std::min(maxSize, maxSizePhrase);

      // ... and that end at endPos
      for (size_t endPos = startPos ; endPos < startPos + maxSize ; endPos++) {
        if (graphInd > 0 && // only skip subsequent graphs
            decodeGraphBackoff[graphInd] != 0 && // use of backoff specified
            (endPos-startPos+1 >= decodeGraphBackoff[graphInd] || // size exceeds backoff limit or ...
             m_collection[startPos][endPos-startPos].size() > 0)) { // no phrases found so far
          VERBOSE(3,"No backoff to graph " << graphInd << " for span [" << startPos << ";" << endPos << "]" << endl);
          // do not create more options
          continue;
        }

        // create translation options for that range
        CreateTranslationOptionsForRange( decodeGraph, startPos, endPos, true, graphInd);
      }
    }
  }

  VERBOSE(2,"Translation Option Collection\n " << *this << endl);

  ProcessUnknownWord();

  EvaluateWithSource();

  // Prune
  Prune();

  Sort();

  // future score matrix
  CalcFutureScore();

  // Cached lex reodering costs
  CacheLexReordering();
}

}


