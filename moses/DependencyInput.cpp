// $Id$

#include "DependencyInput.h"
#include "StaticData.h"
#include "Util.h"
#include "XmlOption.h"
#include <map>
#include <set>

using namespace std;

namespace Moses
{

void DependencyInput::InitFid() {
	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("<s>");
	for(size_t i = 1; i < GetSize()-1; i++) {
		const Word& word = GetWord(i);
		const Factor* factor = word[2];
		CHECK(factor);
		int fid = Scan<int>(factor->GetString().as_string());
		if (fid < 0) {
			fid = 9999;
		} else {
			fid++;
		}
		m_fid.push_back(fid);
		m_pos.push_back(word.GetString(1).as_string());
		m_word.push_back(word.GetString(0).as_string());
		//m_words[i].SetFactor(1, FactorCollection::Instance().AddFactor("-1"));
	}
	m_fid.push_back(0);
	m_pos.push_back("");
	m_word.push_back("</s>");
}

void DependencyInput::ResetWords() {
	for(size_t i = 0; i < GetSize(); i++) {
		for (size_t j = 1; j <=3; j++)
			Phrase::SetFactor(i,j,NULL);
	}
}

/*void DependencyInput::InitDepWordColl() {
	m_wordColl.resize(GetSize());
	m_wordColl[0].push_back(Word(GetWord(0)));
	m_wordColl[GetSize()-1].push_back(Word(GetWord(GetSize()-1)));

	for(size_t i = 1; i < GetSize()-1; i++) {
		vector<Word>& vec = m_wordColl[i];
		const Word& w = GetWord(i);
		//vec.push_back(w);
		string label = w.GetString(0).as_string();

		const set<string>& index = m_index[i][0];
		set<string>::const_iterator iter = index.begin();
		while(iter != index.end()) {
				Word w1;
				w1.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece(label+":"+*iter)));
				vec.push_back(w1);
				++iter;
		}
	}
}
*/

map<size_t, size_t> DependencyInput::GetExternalNodes(size_t start, size_t end) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid >= GetSize()-1 || eid < 1) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}

	for(size_t i = 1; i < start; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = end+1; i < GetSize()-1; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}

/*bool DependencyInput::valid(size_t start, size_t end) const {
	if (start < 1 || end >= GetSize()-1)
		return false;
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid >= GetSize()-1 || eid < 1) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	//

	for(size_t i = 1; i < start; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = end+1; i < GetSize()-1; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}
*/
bool DependencyInput::valid(size_t start, size_t end, size_t startHole, size_t endHole) const {
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = startHole; i <= endHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid > endHole || eid < startHole) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + endHole-startHole+1)
		return false;
	//

	for(size_t i = start; i < startHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = endHole+1; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}

std::string DependencyInput::GetDepString(const WordsRange &range, const std::vector<std::pair<size_t, size_t> > &holes) const {
  std::vector<std::pair<const WordsRange*, const Word*> > hholes;
  for (size_t i = 0; i < holes.size(); i++) {
    hholes.push_back(make_pair(new WordsRange(holes[i].first, holes[i].second), new Word()));
  }
  return GetDepString(range, hholes);
}

std::string DependencyInput::GetDepString(const WordsRange &range, const std::vector<std::pair<const WordsRange*, const Word*> > &holes) const
{
	const StaticData& staticData = StaticData::Instance();

	if (staticData.GetNoDep())
		return "";

	bool relaxdep = staticData.GetRelaxDep();
	bool ignorePhraseDep = staticData.GetIgnorePhraseDep();
	bool useConnection = staticData.GetUseConnection();
	bool contextAwareLabel = staticData.GetContextAwareLabel();

	if (range.GetStartPos() < 1 || range.GetEndPos() >= GetSize()-1)
		return "";

	if (!contextAwareLabel && !HasMoreNT(range.GetStartPos(), range.GetEndPos())) {
		return "INVALID";
	}

	if (contextAwareLabel && !IsConnected(range.GetStartPos(), range.GetEndPos()))
		return "INVALID";

	if (range.GetNumWordsCovered() == 1) {
		if (relaxdep)
			return "0";
		else
			return "-1";
	}
	// words sequence
	if (holes.size() == 0) {
		string ret = "";
		for (size_t i = range.GetStartPos(); i <= range.GetEndPos(); i++) {
			size_t fid = m_fid[i];
			if (relaxdep) {
				if (fid < range.GetStartPos() || fid > range.GetEndPos()) {
					ret += SPrint<size_t>(i-range.GetStartPos()) + ":";
				}
			} else {
				if (fid < range.GetStartPos() || fid > range.GetEndPos())
					ret += "-1";
				else
					ret += SPrint<size_t>(fid-range.GetStartPos());
				ret += ":";
			}

		}
		return ret.erase(ret.size()-1);
	}

	// sequence with holes
	std::vector<std::pair<const WordsRange*, const Word*> >::const_iterator iterHoleList = holes.begin();
	assert(iterHoleList != holes.end());
	size_t startS = range.GetStartPos();
	size_t endS = range.GetEndPos();
	//
	map<size_t,size_t> newPos;
	int wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);

			if ((useConnection || contextAwareLabel) && !valid(startS, endS, currPos, hole.GetEndPos()))
				return "INVALID";

			if (contextAwareLabel) {
				vector<string> labels = GetLabels(range.GetStartPos(), range.GetEndPos(), currPos, hole.GetEndPos());
				string label = iterHoleList->second->GetString(0).as_string();
				if (std::find(labels.begin(), labels.end(), label) == labels.end())
					return "INVALID";
			}

			for(size_t i = currPos; i <= hole.GetEndPos(); i++)
				newPos[i] = wordCount;
			currPos = hole.GetEndPos();
			++iterHoleList;
		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
	}
	CHECK(iterHoleList == holes.end());

	iterHoleList = holes.begin();
	string out = "";
	wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const WordsRange &hole = *(iterHoleList->first);
			isHole = hole.GetStartPos() == currPos;
		}

		int fid = -1;
		if (isHole) {
			const WordsRange &hole = *(iterHoleList->first);
			fid = GetSpanFid(currPos, hole.GetEndPos());
			if (fid < 0) {
				cerr << "no fid specified" << endl;
				assert(false);
			}
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];

			currPos = hole.GetEndPos();
			++iterHoleList;
		} else {
			fid = m_fid[currPos];
			if (fid < startS || fid > endS)
				fid = -1;
			else
				fid = newPos[fid];
		}
		if (relaxdep) {
			if (fid == -1) {
				out += SPrint<int>(wordCount) + ":";
			}
		}else {
			out += SPrint<int>(fid) + ":";
		}
		wordCount++;
	}
	CHECK(out != "");
	return out.erase(out.size()-1);
}

bool DependencyInput::valid(size_t start, size_t end) const {
	if (start == 0 || start == GetSize()-1 || end == 0 || end == GetSize()-1)
		return false;
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid < 1 || eid >= GetSize()-1) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	//

	for(size_t i = 1; i < start; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = end+1; i < GetSize()-1; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	if (external_nodes.size() > 2)
		return false;
	return true;
}

bool DependencyInput::isComplete(size_t start, size_t end) const {
	for (size_t i = 1; i< start; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			return false;
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			return false;
	}
	return true;
}

bool DependencyInput::IsConnected(size_t start, size_t end) const {
	map<size_t, size_t> nodes;

	for(size_t i = start; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		nodes[sid] = 1;
		nodes[eid] = 1;
	}
	if (nodes.size() > 1 + end-start+1)
		return false;
	return true;
}

void DependencyInput::ExpandNonTerm() {
	size_t length = GetSize();
	m_sourceChart.resize(length);
	for (size_t pos = 0; pos < length; ++pos) {
		m_sourceChart[pos].resize(length - pos);
	}
	bool boundary = StaticData::Instance().GetBoundaryNT();
	bool complete = StaticData::Instance().GetSergComplete();
	bool useConnection = StaticData::Instance().GetUseConnection();
	bool contextAwareLabel = StaticData::Instance().GetContextAwareLabel();
	bool allowMoreExternalNodes = StaticData::Instance().GetAllowMoreExternalNodes();
	bool partialChildLabel = StaticData::Instance().GetPartialChildLabel();
	bool includeWordLabel = StaticData::Instance().GetIncludeWordLabel();

	for (size_t start=1; start < length-1; start++){
		for (size_t end = start; end < length-1; end++) {
			if (((!useConnection && valid(start, end))
					|| ((useConnection || contextAwareLabel || allowMoreExternalNodes) && IsConnected(start, end)))
					&& ((start==end) || (!complete) || isComplete(start,end))) {
				if (contextAwareLabel) {
					InitContextAwareLabel(start,end);
				} else {
					vector<string> labels = GetSpanLabel(start,end, boundary);

					for (size_t i = 0; i < labels.size(); i++) {
						string label = labels[i];
						if (partialChildLabel && hasPartialChild(start,end))
							label += "_HPC";

					//const Factor* factor = FactorCollection::Instance().AddFactor(label);
					//const set<string>& index = m_index[start][end-start];
					//set<string>::const_iterator iter1 = index.begin();
					//while(iter1 != index.end()) {
							//Word w1(true);
							//w1.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece(label)));
							//w1.SetFactor(1,FactorCollection::Instance().AddFactor());
						TreeInput::AddChartLabel(start, end, label,StaticData::Instance().GetInputFactorOrder());
					}

					//++iter1;
				}
			//}
			}
		}
	}
	for (size_t start=0; start <= length-1; start++){
		for (size_t end = start; end <= length-1; end++) {
			TreeInput::AddChartLabel(start, end, StaticData::Instance().GetInputDefaultNonTerminal()
																,StaticData::Instance().GetInputFactorOrder());
		}
	}
	//Word w1(true);
	//w1.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece("TOP")));
	//TreeInput::AddChartLabel(1, length-2, w1,StaticData::Instance().GetInputFactorOrder());
}

/*void DependencyInput::addRule(size_t start, size_t end) {
	string phrase;
	for(size_t i = start; i<= end; i++) {
		int fid = (int)(m_fid[i]);
		if(fid < (int)start || fid > (int)end)
			fid = -1;
		else
			fid = fid-(int)start;
		m_index[i][0].insert(SPrint<int>(fid));
		//Word word(false);
		//word.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece(GetWord(i).GetString(0).as_string()+":"+SPrint<int>(fid))));
		phrase += GetWord(i).GetString(0).as_string()+":"+SPrint<int>(fid) + " ";
		//phrase.push_back(&word);
	}
	phrase = phrase.erase(phrase.size()-1);
	//Phrase p(phrase);
	m_serg[start][end-start].insert(phrase);
}*/

int DependencyInput::GetSpanFid(size_t start, size_t end) const {
	set<size_t> fids;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			fids.insert(fid);
		}
	}
	if (fids.size()>1)
	  return -1;
	CHECK(fids.size() == 1);
	return *(fids.begin());
}

std::vector<size_t> DependencyInput::GetHeads(size_t startPos, size_t endPos) const
{
	vector<size_t> heads;
	for(size_t i = startPos; i <= endPos; i++) {
		size_t fid = m_fid[i];
		if(fid < startPos || fid > endPos) {
			heads.push_back(i);
		}
	}
	CHECK(heads.size() >= 1);
	return heads;
}

WordsRange DependencyInput::GetSourceSpan(size_t head) const
{
	size_t minPos = head, maxPos = head;
	vector<size_t> kids = GetKids(minPos, maxPos);
	for (int i = 0; i < kids.size(); i++) {
		WordsRange span = GetSourceSpan(kids[i]);
		if (span.GetStartPos() < minPos)
			minPos = span.GetStartPos();
		if (span.GetEndPos() > maxPos)
			maxPos = span.GetEndPos();
	}
	return WordsRange(minPos, maxPos);
}

WordsRange DependencyInput::GetMinCoverCompleteSpan(size_t startPos, size_t endPos) const
{
	size_t minPos = startPos, maxPos = endPos;
	vector<size_t> kids = GetKids(startPos, endPos);
	for(size_t i = 0; i < kids.size(); i++) {
		WordsRange span = GetMinCoverCompleteSpan(kids[i],kids[i]);
		if (span.GetStartPos() < minPos)
			minPos = span.GetStartPos();
		if (span.GetEndPos() > maxPos)
			maxPos = span.GetEndPos();
	}

	WordsRange range(minPos, startPos);
	return range;
}

/*void DependencyInput::saveHierRule(size_t startS, size_t endS, const vector<Hole*>& holes) {
	vector<Hole*>::const_iterator iterHoleList = holes.begin();
	CHECK(iterHoleList != holes.end());

	//
	map<size_t,size_t> newPos;
	size_t wordCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const Hole &hole = **iterHoleList;
			isHole = hole.start == currPos;
		}

		if (isHole) {
			const Hole &hole = **iterHoleList;
			for(size_t i = currPos; i <= hole.end; i++)
				newPos[i] = wordCount;
			currPos = hole.end;
			++iterHoleList;
		} else {
			newPos[currPos] = wordCount;
		}
		wordCount++;
	}

	iterHoleList = holes.begin();
	CHECK(iterHoleList != holes.end());

	string out;
	int outPos = 0;
	int holeCount = 0;
	for(size_t currPos = startS; currPos <= endS; currPos++) {
		bool isHole = false;
		if (iterHoleList != holes.end()) {
			const Hole &hole = **iterHoleList;
			isHole = hole.start == currPos;
		}

		if (isHole) {
			const Hole &hole = **iterHoleList;

			int fid = GetSpanFid(currPos, hole.end);
			if (fid < 0) {
				cerr << "no fid specified" << endl;
				assert(false);
			}
			if (fid < (int)startS || fid > (int)endS)
				fid = -1;
			else
				fid = (int)(newPos[fid]);
			m_index[currPos][hole.end-currPos].insert(SPrint<int>(fid));
			const string &sourceLabel =  hole.label;
			//Word word(true);
			//word.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece(sourceLabel +":"+SPrint<int>(fid))));
			//out.push_back(&word);
			out += sourceLabel +":"+SPrint<int>(fid) + " ";

			currPos = hole.end;
			++iterHoleList;
			++holeCount;
		} else {
			int fid = (int)(m_fid[currPos]);
			if (fid < (int)startS || fid > (int)endS)
				fid = -1;
			else
				fid = newPos[fid];
			m_index[currPos][0].insert(SPrint<int>(fid));
			//Word word(false);
			//word.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece(GetWord(currPos).GetString(0).as_string()+":"+SPrint<int>(fid))));
			//out.push_back(&word);
			out += GetWord(currPos).GetString(0).as_string()+":"+SPrint<int>(fid) + " ";
		}

		outPos++;
	}

	CHECK(iterHoleList == holes.end());
	out = out.erase(out.size()-1);
	//Phrase p(out);
	m_serg[startS][endS-startS].insert(out);
}*/

string DependencyInput::GetSpanLabelWithExtEdge(size_t start, size_t end) const
{
	map<size_t,size_t> externalNodes = GetExternalNodes(start,end);
	if (externalNodes.size() > 2) {
		CHECK(false);
	}

	if (externalNodes.size() == 1) {
		vector<string> labels;
		for(size_t i = start; i <= end; i++) {
			size_t fid = m_fid[i];
			if(fid < start || fid > end) {
				labels.push_back(GetWord(i).GetString(1).as_string());
			}
		}
		string ret = labels[0];
		//map<string, int> used;
		for(size_t i = 1; i < labels.size(); i++) {
			//if (used.find(labels[i]) != used.end())
			//	continue;
			ret += "_" + labels[i];
			//used[labels[i]] = 1;
		}

		return ret;
	} else {

		size_t fid = GetSpanFid(start, end);

		map<size_t,size_t>::iterator iter = externalNodes.begin();
		size_t s = iter->first;
		iter++;
		size_t e = iter->first;

		if (fid != s && fid != e) {
			cerr << "external nodes error " << start << " " << end << endl;
			assert(false);
		}

		if (fid == s) {
			s = e;
			e = fid;
		}

		vector<string> labels;
		size_t step = s;
		while (m_fid[step] != e) {
				labels.push_back(GetWord(step).GetString(1).as_string());
				step = m_fid[step];
		}
		labels.push_back(GetWord(step).GetString(1).as_string());

		string ret = labels[0];
		if (labels.size() > 1)
			ret += "_" + labels[labels.size()-1];

		return ret;
	}
}

vector<string> DependencyInput::GetSpanLabel(size_t start, size_t end, bool boundary) const {

	if (StaticData::Instance().GetLabelWithExternalEdge()) {
		return vector<string>(1,GetSpanLabelWithExtEdge(start, end));
	}

	if (StaticData::Instance().GetSingleNT()) {
		return vector<string>(1,"SNT");
	}

	bool includeWordLabel = StaticData::Instance().GetIncludeWordLabel();

	vector<string> labels;
	//vector<string> wlabels;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			labels.push_back(m_pos[i]);
			//wlabels.push_back(GetWord(i).GetString(0).as_string());
		}
	}
	string ret = labels[0];
	//string wret = wlabels[0];
	if (boundary) {
		if (labels.size() > 1) {
			ret += "_" + labels[labels.size()-1];
		}
	} else if (StaticData::Instance().GetUniqueLabelSet()) {
		set<string> labelSet(labels.begin(), labels.end());
		set<string>::iterator iter = labelSet.begin();
		ret = *iter;
		iter++;
		for(;iter != labelSet.end(); iter++)
			ret += "_" + *iter;
	} else {
		for(size_t i = 1; i < labels.size(); i++)
			ret += "_" + labels[i];
		//for(size_t i = 1; i < wlabels.size(); i++)
		//	wret += "_" + wlabels[i];
	}

	if (StaticData::Instance().GetFatherLabel()) {
	  if (labels.size() == 1)
	    ret = labels[0];
	  else {
	    int fid = GetSpanFid(start, end);
	    if (fid < 1 || fid >= GetSize()-1)
	      ret = "ROOOT";
	    else
	      ret = m_pos[fid]+"_SS";
	  }
	}

	vector<string> retVec;
	retVec.push_back(ret);
	//if (includeWordLabel)
	//	retVec.push_back(wret);
	return retVec;
}

/*void DependencyInput::addHierRule(size_t start, size_t end, vector<Hole*>& holes, size_t initS, size_t wcount) {
	for (size_t s = initS; s <=end; s++) {
		for (size_t e = s; e <= end; e++){
			//if (!valid(s, e))
			//		continue;
			if (s == start && e == end)
					continue;
			if (m_serg[s][e-s].size() == 0)
					continue;
			if (!isInDependent(start, end, s, e) || GetDepth(start,end,s,e) > 2)
			    continue;
			size_t nextwcount = wcount-(e-s+1);
			if (nextwcount < 1)
				continue;
			string label = GetSpanLabel(s,e);
			holes.push_back(new Hole(s,e, label));

			if (nextwcount + holes.size() <= StaticData::Instance().GetMaxSerg())
					saveHierRule(start, end, holes);
			//initS = e + 1;
			addHierRule(start, end, holes, e+1, nextwcount);
			holes.pop_back();
		}
	}
}*/

/*void DependencyInput::InitSergCollection() {
	size_t length = GetSize();
	m_serg.resize(length);
	m_index.resize(length);
	for(size_t pos = 0; pos < length; pos++) {
		m_serg[pos].resize(length-pos);
		m_index[pos].resize(length-pos);
	}
	for (size_t size=1; size < length-1 && size <= StaticData::Instance().GetMaxPhraseLength(); size++){
		for (size_t start = 1; start <= length-size-1; start++) {
			size_t end = start + size -1;
			if (!valid(start, end))
				continue;
			if (end-start+1 <= StaticData::Instance().GetMaxSerg())
				addRule(start, end);
			if (size == 1)
				continue;
			vector<Hole*> holes;
			addHierRule(start, end, holes, start, end-start+1);
		}
	}

}*/

/*int DependencyInput::GetDepth(int start, int end, int pos) const
{
	int depth = 1;
	int fid = (int)(m_fid[pos]);
	while (fid >= start && fid <= end) {
		depth++;
		fid = (int)(m_fid[fid]);
	}
	return depth;
}

int DependencyInput::GetDepth(int start, int end, int startHole, int endHole) const
{
	int minDepth = 9999;
	for (int pos = startHole; pos <= endHole; pos++) {
		int depth = 1;
		int fid = (int)(m_fid[pos]);
		while (fid >= start && fid <= end) {
			depth++;
			fid = (int)(m_fid[fid]);
		}
		if (depth < minDepth)
			minDepth = depth;
	}
	return minDepth;
}

bool DependencyInput::isInDependent(int start, int end, int startHole, int endHole) const {
	for (int i = start; i< startHole; i++) {
		int fid = (int)(m_fid[i]);
		if (fid >= startHole && fid <= endHole)
			return false;
	}
	for (int i = endHole+1; i <= end; i++) {
		int fid = (int)(m_fid[i]);
		if (fid >= startHole && fid <= endHole)
			return false;
	}
	return true;
}

bool DependencyInput:: isComplete(int start, int end) const {
	for (int i = 1; i< start; i++) {
		int fid = (int)(m_fid[i]);
		if (fid >= start && fid <= end)
			return false;
	}
	for (int i = end+1; i < GetSize(); i++) {
		int fid = (int)(m_fid[i]);
		if (fid >= start && fid <= end)
			return false;
	}
	return true;
}*/

std::map<size_t,size_t> DependencyInput::GetExternalNodes(size_t start, size_t end, size_t startHole, size_t endHole) const
{
	// connected
	// max 2 external node = max 1 unfull subtree
	map<size_t, size_t> nodes;
	map<size_t, size_t> external_nodes;

	for(size_t i = startHole; i <= endHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];

		if (eid > end || eid < start) {
			external_nodes[eid] = 1;
		}
		nodes[sid] = 1;
		nodes[eid] = 1;
	}

	for(size_t i = start; i < startHole; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	for(size_t i = endHole+1; i <= end; i++) {
		size_t sid = i;
		size_t eid = m_fid[i];
		if (nodes.find(sid) != nodes.end())
			external_nodes[sid] = 1;
		if (nodes.find(eid) != nodes.end())
			external_nodes[eid] = 1;
	}
	return external_nodes;
}
void DependencyInput::InitContextAwareLabel(size_t startS, size_t endS)
{
	for(size_t start = 0; start <=endS; start++) {
		for(size_t end=start; end <=endS; end++) {
			if (start >= startS && valid(startS,endS,start,end)) {
				vector<string> labels = GetLabels(startS, endS, start, end);
				for(vector<string>::iterator iter = labels.begin(); iter != labels.end(); ++iter) {
					string label = *iter;
					TreeInput::AddChartLabel(start, end, label, StaticData::Instance().GetInputFactorOrder());
				}
			}
		}
	}
}

std::vector<std::string> DependencyInput::GetLabels(size_t start, size_t end, size_t startHole, size_t endHole) const
{
		vector<string> ret;
		map<size_t,size_t> externalNodes = GetExternalNodes(start,end, startHole, endHole);
		if (externalNodes.size() == 1) {
			vector<string> labels;
			for(size_t i = startHole; i <= endHole; i++) {
				size_t fid = m_fid[i];
				if(fid < startHole || fid > endHole) {
					labels.push_back(m_pos[i]);
				}
			}
			assert(labels.size() > 0);
			//string label = labels[0];
			map<string,int> used;
			for(size_t i = 0; i < labels.size(); i++) {
				if (used.find(labels[i]) == used.end())
				//label += "_" + labels[i];
					ret.push_back(labels[i]);
				used[labels[i]] = 1;
			}
		} else if (externalNodes.size() == 2) {

			int fid = GetSpanFid(startHole, endHole);

			if (fid < 0) {
				cerr << "span fid error " << start << " " << end << endl;
				assert(false);
			}

			map<size_t,size_t>::iterator iter = externalNodes.begin();
			size_t s = iter->first;
			iter++;
			size_t e = iter->first;

			if (fid != s && fid != e) {
				cerr << "external nodes error " << start << " " << end << endl;
				assert(false);
			}

			if (fid == s) {
				s = e;
				e = fid;
			}

			vector<string> labels;
			size_t step = s;
			while (m_fid[step] != e) {
					labels.push_back(m_pos[step]);
					step = m_fid[step];
			}
			labels.push_back(m_pos[step]);

			string label = labels[0];
			if (labels.size() > 1)
				label += "_" + labels[labels.size()-1];

			ret.push_back(label);
		}
		return ret;
}

bool DependencyInput::hasPartialChild(size_t start, size_t end) const
{
	bool hasChild = false;
	set<size_t> heads;
	for(size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if(fid < start || fid > end) {
			heads.insert(i);
		}
	}
	CHECK(heads.size() > 0);
	for (size_t i = start; i <= end; i++) {
		size_t fid = m_fid[i];
		if (heads.find(fid) != heads.end())
			hasChild = true;
	}
	if (!hasChild)
		return false;

	for (size_t i = 1; i < start; i++) {
		size_t fid = m_fid[i];
		if (heads.find(fid) != heads.end())
			return true;
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (heads.find(fid) != heads.end())
			return true;
	}
	return false;
}

std::vector<size_t> DependencyInput::GetKids(size_t start, size_t end) const
{
	vector<size_t> ret;
	for (size_t i = 1; i < start; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			ret.push_back(i);
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (fid >= start && fid <= end)
			ret.push_back(i);
	}
	return ret;
}

std::vector<size_t> DependencyInput::GetSibs(size_t start, size_t end) const
{
	size_t father = GetSpanFid(start,end);

	vector<size_t> ret;
	for (size_t i = 1; i < start; i++) {
		size_t fid = m_fid[i];
		if (fid == father)
			ret.push_back(i);
	}
	for (size_t i = end+1; i < GetSize()-1; i++) {
		size_t fid = m_fid[i];
		if (fid == father)
			ret.push_back(i);
	}
	return ret;
}

} // namespace

