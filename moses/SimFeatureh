#ifndef moses_SimWithCorpus_h
#define moses_SimWithCorpus_h

#include <vector>
#include <string>
#include <map>
#include "Util.h"
#include "ScoreComponentCollection.h"
#include "moses/FF/StatelessFeatureFunction.h"
#include "InputType.h"
#include "TypeDef.h"
#include "Factor.h"
//#include "PrefixTreeMap.h"


namespace Moses
{


class SimFeature : public StatelessFeatureFunction
{
private:
	//InputType & m_source;
	size_t m_linenum;
	std::string m_filepath;
	std::string m_corpuspath;
	std::map<std::size_t, float> m_simCache;

	FactorList m_factorsF;
	FactorList m_factorsE;
	FactorList m_factorsC;

	typedef std::map<std::string, std::vector<size_t> > TableType;
	typedef std::map<std::size_t, std::vector<std::string> > CorpusType;

	TableType m_table;
	CorpusType m_corpus;
public:
  SimFeature(const std::string &line);
  virtual ~SimFeature(){}
  virtual bool IsUseable(const FactorMask &mask) const;
  void Load();
  void  LoadFromFile(const std::string& filePath);
  void LoadCorpus(const std::string& filePath);

  std::string  MakeKey(const std::string& f,
      const std::string& e,
      const std::string& c) const;
  std::string  MakeKey(const Phrase& f,
      const Phrase& e,
      const Phrase& c) const;

  virtual void Evaluate(const TargetPhrase& targetPhrase, const InputType& context,
                          ScoreComponentCollection& accumulator) const;

  virtual void InitializeForInput(InputType const& source);

    /**
      * Same for chart-based features.
      **/
    //virtual void EvaluateChart(const ChartBasedFeatureContext& context,
    //                           ScoreComponentCollection* accumulator) const {
    //	CHECK(0);
    //	return;
    //}
};


}

#endif
