/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once


#include <vector>
#include <map>
#include "G2sTranslationOptions.h"
#include "G2sTranslationOptionList.h"
#include "moses/SquareMatrix.h"

namespace Moses
{

class TargetPhraseCollection;
class InputType;

namespace Graph
{
class WordsSet;

//! a vector of translations options for a specific range, in a specific sentence
class G2sTranslationOptionColl
{
	typedef std::map<WordsSet, G2sTranslationOptionList*> CollType;
public:
	G2sTranslationOptionColl(size_t ruleLimit, const InputType &input);
//	G2sTranslationOptionColl(const G2sTranslationOptionColl& coll);
  ~G2sTranslationOptionColl();

  void Add(const TargetPhraseCollection &, const WordsSet &, const Phrase& source);

  void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const WordsSet &range);

  void ApplyThreshold();
  void Evaluate(const InputType &input, const InputSubgraph &inputPath);

  const G2sTranslationOptionList* GetTranslationOptionList(const WordsSet& range) const {
  	if (m_coll.find(range) == m_coll.end())
  		return NULL;
  	return m_coll.find(range)->second;
  }

  typedef CollType::const_iterator const_iterator;
  typedef CollType::iterator iterator;
	//! iterators
	const_iterator begin() const {
		return m_coll.begin();
	}
	const_iterator end() const {
		return m_coll.end();
	}

  //! returns future cost matrix for sentence
  inline virtual const SquareMatrix &GetFutureScore() const {
    return m_futureScore;
  }

  void Evaluate(const InputType &input);
  void CalcFutureScore();
  void CacheLexReordering();
  void CacheG2sOrderModel();
  void RemoveUnlikely();

private:

  CollType m_coll;
  const size_t m_ruleLimit;
  const InputType & m_input;
  SquareMatrix m_futureScore;
};

}
}
