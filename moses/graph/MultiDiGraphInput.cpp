/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

//#include "tables-core.h"
//#include "moses/Util.h"
#include "MultiDiGraphInput.h"

#include <set>
#include <algorithm>
#include <iostream>

#include "moses/StaticData.h"

using namespace std;
using namespace Moses;

namespace Moses {
namespace Graph {

struct less_than_key
{
    inline bool operator() (const std::pair<std::string,size_t>& struct1,
    												const std::pair<std::string,size_t>& struct2) const
    {
        return (struct1.first < struct2.first);
    }
} node_key;

int MultiDiGraph::Read(std::istream& in, const std::vector<FactorType>& factorOrder) {

	if (!Sentence::Read(in, factorOrder))
	  return 0;

	const StaticData& staticData = StaticData::Instance();
//	bool noBigram = staticData.GetNoBigram();
	bool useLinkType = staticData.GetUseLinkType();
	bool revBigram = staticData.GetReverseBigram();
	bool sibling = staticData.GetSibling();
	bool onlyTree = staticData.GetOnlyTree();
	// sort word and reset fid
	map<size_t, size_t> indexMap = SortWords();

	size_t size = GetSize();
	nodes.resize(size);
	succ.resize(size);
	pred.resize(size);

	vector< vector<size_t> > children;
	children.resize(size);

	for(size_t i = 0; i < size; i++) {
		const Word& word = GetWord(i);
		const Factor* factor = word[2];
		int fid = Scan<int>(factor->GetString().as_string());

		// add node
		struct Node* n = new Node();
		n->id = i;
		n->word = word.GetString(0).as_string();
		n->pos = word.GetString(1).as_string();
		if (fid < 0)
		  n->fid = NOT_FOUND;
		else n->fid = fid;
		add_node(i, n);

		if (fid >= 0)
		  children[fid].push_back(i);

		// add dep edge
		string label = word.GetString(3).as_string();
		if (useLinkType)
			label = "DP";
		if (fid >= 0)
			add_edge(fid, i, label);
		// add bigram edge
		if (!onlyTree && !sibling && i > 0) {
		  add_edge(indexMap.find(i)->second, indexMap.find(i-1)->second, "BG");
		}
	}

	if (sibling) {
      for(size_t i = 0; i < children.size(); i++) {
        for (size_t j = 1; j < children[i].size(); j++) {
          add_edge(children[i][j], children[i][j-1], "SG");
        }
      }
    }

	// reset to pure word
	ResetWords();
	return 1;
}

map<size_t, size_t> MultiDiGraph::SortWords()
{
	map<size_t, size_t> ret;
	size_t size = GetSize();

	if (!StaticData::Instance().GetSort()) {
		for(size_t i = 0; i < size; i++) {
			ret[i] = i;
		}
		return ret;
	}

	vector<pair<string,size_t> > words;
	for(size_t i = 0; i < size; i++) {
		string w = GetWord(i).GetString(0).as_string();
		words.push_back(make_pair<string,size_t>(w,i));
	}

	map<string, string> indexMap;
	std::stable_sort(words.begin(),words.end(),node_key);
	for(size_t i = 0; i < size; i++) {
		size_t j = words[i].second;
		indexMap[SPrint<size_t>(j)] = SPrint<size_t>(i);
		ret[j] = i;
	}
	indexMap["-1"] = "-1";

	FactorCollection &factorCollection = FactorCollection::Instance();
	map<string, string>::const_iterator iterMap;
	Phrase sortedPhrase(size);
	for(size_t i = 0; i < size; i++) {
		Word w(GetWord(words[i].second));
		string fid = w.GetString(2).as_string();
		iterMap = indexMap.find(fid);
		CHECK(iterMap != indexMap.end());
		const Factor* fit = factorCollection.AddFactor(StringPiece(iterMap->second));
		w.SetFactor(2, fit);
		sortedPhrase.AddWord(w);
	}

	const vector<FactorType>& fvec = StaticData::Instance().GetInputFactorOrder();
	MergeFactors(sortedPhrase, fvec);
	return ret;
}

void MultiDiGraph::add_node(size_t id, const struct Node* attr) {
	// no duplicate node
	nodes[id] = attr;
}

void MultiDiGraph::add_edge(size_t u, size_t v, const std::string& label) {
	// add to succ
	if (succ[u].find(v) == succ[u].end()) {
		succ[u][v] = set<string>();
	}
	succ[u][v].insert(label);
	// add to pred
	if (pred[v].find(u) == pred[v].end()) {
		pred[v][u] = set<string>();
	}
	pred[v][u].insert(label);
}

void MultiDiGraph::DepthFirst(size_t curr_nid,
		set<size_t>& old_nids,
		const set<size_t>& all_nids) const {

	LabelsType::const_iterator iter;
	const LabelsType& ems = succ[curr_nid];
	if (ems.size() > 0) {
		for(iter=ems.begin(); iter!=ems.end(); iter++) {
			int sid = iter->first;
			if (all_nids.find(sid) != all_nids.end()
					&& old_nids.find(sid) == old_nids.end()) {
			  old_nids.insert(sid);
			  DepthFirst(sid, old_nids, all_nids);
			}
		}
	}
	const LabelsType& emp = pred[curr_nid];
	if (emp.size() > 0) {
		for(iter=emp.begin(); iter!=emp.end(); iter++) {
			size_t pid = iter->first;
			if (all_nids.find(pid) != all_nids.end()
					&& old_nids.find(pid) == old_nids.end()) {
			  old_nids.insert(pid);
			  DepthFirst(pid, old_nids, all_nids);
			}
		}
	}
}

bool MultiDiGraph::is_connected(const std::set<size_t>& nids) const {
	set<size_t> old_nids;
	size_t curr_nid = *(nids.begin());
	old_nids.insert(curr_nid);
	DepthFirst(curr_nid, old_nids, nids);
	return old_nids.size() == nids.size();
}

//const MultiDiGraph* MultiDiGraph::get_subgraph(const std::set<int>& nids) const {
//	MultiDiGraph* mdg = new MultiDiGraph();
//
//	map<int, set<string> >::const_iterator iter;
//	map<int, map<int, set<string> > >::const_iterator iter2;
//	std::set<int>::const_iterator iter3;
//
//	for(iter3=nids.begin(); iter3!=nids.end(); iter3++) {
//		int nid = *iter3;
//		// add node
//		mdg->add_node(nid, nodes.find(nid)->second);
//		// add edges
//		iter2 = succ.find(nid);
//		if (iter2 != succ.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int sid = iter->first;
//				if (nids.find(sid) != nids.end()) {
//					// add a succ edge
//					if (mdg->succ.find(nid) == mdg->succ.end())
//						mdg->succ[nid] = map<int, set<string> >();
//					mdg->succ[nid][sid] = iter->second;
//				}
//			}
//		}
//		iter2 = pred.find(nid);
//		if (iter2 != pred.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int pid = iter->first;
//				if (nids.find(pid) != nids.end()) {
//					// add a succ edge
//					if (mdg->pred.find(pid) == mdg->pred.end())
//						mdg->pred[nid] = map<int, set<string> >();
//					mdg->pred[nid][pid] = iter->second;
//				}
//			}
//		}
//	}
//	return mdg;
//}

bool MultiDiGraph::between_connected(const std::set<size_t>& g1, const std::set<size_t>& g2) const
{
	if (g1.size() == 0 || g2.size() == 0)
		return true;

	std::set<size_t>::const_iterator iter;
	LabelsType::const_iterator iter3;
	for (iter = g1.begin(); iter != g1.end(); ++iter) {
		size_t nid = *iter;
		const LabelsType& ems = succ[nid];
		if (ems.size() > 0) {
			for(iter3=ems.begin(); iter3!=ems.end(); iter3++) {
				size_t sid = iter3->first;
				if (g2.find(sid) != g2.end())
					return true;
			}
		}

		const LabelsType& emp = pred[nid];
		if (emp.size() > 0) {
			for(iter3=emp.begin(); iter3!=emp.end(); iter3++) {
				size_t pid = iter3->first;
				if (g2.find(pid) != g2.end())
					return true;
			}
		}
	}
	return false;
}

//std::set<int> MultiDiGraph::get_neighbors(const std::set<int>& nids) const {
//	set<int> neighbors;
//	map<int, set<string> >::const_iterator iter;
//	map<int, map<int, set<string> > >::const_iterator iter2;
//	for(set<int>::const_iterator it = nids.begin();
//			it != nids.end(); it++) {
//		int nid = *it;
//		iter2 = succ.find(nid);
//		if (iter2 != succ.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int sid = iter->first;
//				if (nids.find(sid) == nids.end())
//					neighbors.insert(sid);
//			}
//		}
//		iter2 = pred.find(nid);
//		if (iter2 != pred.end()) {
//			for(iter=iter2->second.begin(); iter!=iter2->second.end(); iter++) {
//				int pid = iter->first;
//				if (nids.find(pid) == nids.end())
//					neighbors.insert(pid);
//			}
//		}
//	}
//	return neighbors;
//}

//string MultiDiGraph::get_nt_structure(const std::map<int,int>& termMap, const std::set<int>& hole, bool incLabel) const
//{
//	LabelsType predLabels;
//	LabelsType succLabels;
//	for(set<int>::const_iterator iter = hole.begin(); iter != hole.end(); ++iter) {
//		int nid = *iter;
//		if (has_succ(nid)) {
//			const map<int, set<string> >& succ_edges = get_succ_edges(nid);
//			CollectLabels(termMap, hole, succ_edges, succLabels);
//		}
//		if (has_pred(nid)) {
//			const map<int, set<string> >& pred_edges = get_pred_edges(nid);
//			CollectLabels(termMap, hole, pred_edges, predLabels);
//		}
//	}
//	string ret = ConcatLabels(predLabels, incLabel);
//	ret += "_"+ConcatLabels(succLabels, incLabel);
//	return ret;
//}

//string MultiDiGraph::ConcatLabels(const LabelsType& labels,
//		bool incLabel) const
//{
//	string ret = "-1";
//	if (labels.size() > 0){
//		string retStr = "";
//		for(LabelsType::const_iterator iter = labels.begin();
//				iter != labels.end(); iter++) {
//			int j = iter->first;
//			const set<string>& labels = iter->second;
//			for(set<string>::const_iterator it2 = labels.begin();
//					it2 != labels.end(); it2++) {
//				string label = *it2;
//				retStr += SPrint<int>(j) + ":";
//				if (incLabel)
//					retStr += label + ":";
//				else
//					break;
//			}
//		}
//		ret = retStr.erase(retStr.size()-1);
//	}
//	return ret;
//}

//void MultiDiGraph::CollectLabels(const map<int,int>& I2J,
//		const set<int>& except,
//		const LabelsType& edges,
//		LabelsType& ret) const
//{
//	map<int, set<string> >::const_iterator it = edges.begin();
//	for (; it != edges.end(); ++it) {
//		int j = it->first;
//
//		if (I2J.find(j) == I2J.end())
//			continue;
//
//		j = I2J.find(j)->second;
//		if (ret.find(j) == ret.end())
//			ret[j] = set<string>();
//		ret[j].insert(it->second.begin(), it->second.end());
//	}
//}

std::string MultiDiGraph::get_structure_string(const std::vector<size_t>& nids, bool incLabel) const
{

	string retStr = "";

//	if (StaticData::Instance().GetAugPhrase()) {
/*	  retStr = "";
	  set<int> s(nids.begin(), nids.end());
	  for(size_t i = 0; i < nids.size(); i++) {
	    int fid = nodes[nids[i]]->fid;
	    if (s.find(fid) == s.end()) {
	      retStr = retStr + nodes[nids[i]]->pos + ":";
	    }
	  }
	  retStr = retStr.erase(retStr.size()-1);
	  retStr += "@";*/
//	}

	size_t size = nids.size();
	for(size_t i = 0; i < size; i++) {
		size_t nid = nids[i];
		bool success = false;
		if (has_succ(nid)) {
			const LabelsType& succ_edges = get_succ_edges(nid);
			for(size_t j = 0; j < size; j++) {
				if (i == j)
					continue;
				size_t sid = nids[j];
				LabelsType::const_iterator it = succ_edges.find(sid);
				if ( it != succ_edges.end() ) {
					success = true;
					const set<string>& labels = it->second;
					for(set<string>::const_iterator it2 = labels.begin();
							it2 != labels.end(); it2++) {
						string label = *it2;
						retStr += SPrint<size_t>(j) + ":";
						if (incLabel)
							retStr += label + ":";
						else
							break;
					}
				}
			}
		}
		if (!success){
			retStr += "-1_";
		} else {
			retStr = retStr.erase(retStr.size()-1);
			retStr += "_";
		}
	}
	return retStr.erase(retStr.size()-1);
}

bool MultiDiGraph::within_distance(const std::set<size_t>& curRange, int firstGap, int maxDistance) const
{
if (curRange.size() == 0) return true;

int curStart = (int)*(curRange.begin());
if (abs(curStart-firstGap) > maxDistance)
	return false;
return true;

//	if (nids1.size() == 0 || nids2.size() == 0)
//		return true;
//	// precondition: two subgraphs are not overlapped.
//	set<int> old_nids(nids1.begin(), nids1.end());
//	set<int> curr_nids(nids1.begin(), nids1.end());
//	for(int i = 0; i < maxDistance; i++) {
//		set<int> neighbors = get_neighbors(curr_nids);
//		for(set<int>::const_iterator iter = neighbors.begin();
//				iter != neighbors.end(); iter++) {
//			if (old_nids.find(*iter) != old_nids.end())
//				continue;
//			if (nids2.find(*iter) != nids2.end())
//				return true;
//		}
//		old_nids.insert(curr_nids.begin(), curr_nids.end());
//		curr_nids = neighbors;
//	}
//	return false;
}

vector<float> MultiDiGraph::ComputeDistortionDistance(int prevEnd, const std::set<size_t>& current, bool naive) const
{
  vector<float> ret(2,0.0f);
	if (current.size() == 0)
		return ret;

	set<size_t>::const_iterator iter = current.begin();
	int total = std::abs(prevEnd-int(*iter)+1);
	ret[0] = -(float) total;

	if (naive) return ret;

	int p = (int)*iter;
	total = 0;
	for(++iter; iter != current.end(); iter++) {
		int curr = (int)*iter;
		total += std::abs(p-curr+1);
		p = curr;
	}
	ret[1] = -(float)total;
	return ret;
}

//void MultiDiGraph::print_graph(std::ostream& out) const {
//	out << "----------------------" << endl;
//	out << "nodes:" << endl;
//	out << "size: " << nodes.size() << endl;
//
//	for(map<int, const struct Node*>::const_iterator iter = nodes.begin();
//			iter != nodes.end(); iter++) {
//		const struct Node* n = iter->second;
//		out << " [" << n->id << " " << n->word << "]" << endl;
//	}
//
//	out << "edges:" << endl;
//
//	map<int, set<string> >::const_iterator iter;
//	map<int, map<int, set<string> > >::const_iterator iter2;
//	for (iter2 = succ.begin(); iter2 != succ.end(); iter2++) {
//		for(iter = iter2->second.begin(); iter != iter2->second.end(); iter++) {
//			for(set<string>::const_iterator it = iter->second.begin();
//					it != iter->second.end(); it++)
//				out << " " << "[" << iter2->first << " " << iter->first
//					 << " " << *it << "]" << endl;
//		}
//	}
//
//	out << "----------------------" << endl;
//}

void MultiDiGraph::print_sent(std::ostream& out) const {
	for(size_t i = 0; i < GetSize(); i++) {
		out << GetWord(i).GetString(0).as_string() << " ";
	}
	out << endl;
}

std::string MultiDiGraph::GetLabel(const std::set<size_t>& nids) const {
  string label = "";
  for (set<size_t>::const_iterator iter = nids.begin(); iter != nids.end(); ++iter) {
    size_t id = *iter;
    size_t fid = nodes[id]->fid;
    if (nids.find(fid) == nids.end())
      label += nodes[id]->pos + ":";
  }
  return label.erase(label.size()-1);
}

std::string MultiDiGraph::GetBetweenLink(const std::set<size_t>& currRange, const std::set<size_t>& nextRange) const
{
  string ret = "";
  size_t i = 0;
  for (set<size_t>::const_iterator iter = currRange.begin();
      iter != currRange.end(); ++iter,++i) {
    size_t nid = *iter;
    const LabelsType& succ_edges = get_succ_edges(nid);
    LabelsType::const_iterator it = succ_edges.begin();
    for (; it != succ_edges.end(); ++it) {
      size_t j = it->first;
      if (nextRange.find(j) != nextRange.end()) {
        ret += "s"+SPrint<size_t>(i);
        break;
      }
    }
    const LabelsType& pred_edges = get_pred_edges(nid);
    it = pred_edges.begin();
    for (; it != pred_edges.end(); ++it) {
      size_t j = it->first;
      if (nextRange.find(j) != nextRange.end()) {
        ret += "p"+SPrint<size_t>(i);
        break;
      }
    }
  }
  if (ret.empty()) return "-1";
  return ret;
}

}
} /* namespace Moses */
