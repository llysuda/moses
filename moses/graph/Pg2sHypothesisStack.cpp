
#include "Pg2sHypothesisStack.h"

namespace Moses
{
namespace Graph
{

Pg2sHypothesisStack::~Pg2sHypothesisStack()
{
  // delete all hypos
  while (m_hypos.begin() != m_hypos.end()) {
    Remove(m_hypos.begin());
  }
}

/** Remove hypothesis pointed to by iterator but don't delete the object. */
void Pg2sHypothesisStack::Detach(const Pg2sHypothesisStack::iterator &iter)
{
  m_hypos.erase(iter);
}


void Pg2sHypothesisStack::Remove(const Pg2sHypothesisStack::iterator &iter)
{
  Pg2sHypothesis *h = *iter;
  Detach(iter);
  FREEHYPO(h);
}


}
}

