/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_G2sRuleLookupManager_h
#define moses_G2sRuleLookupManager_h

#include "moses/InputType.h"

namespace Moses
{

class Sentence;

namespace Graph
{

class G2sParser;
class G2sTranslationOptionColl;
class WordsSet;

/** Defines an interface for looking up rules in a rule table.  Concrete
 *  implementation classes should correspond to specific PhraseDictionary
 *  subclasses (memory or on-disk).  Since a G2sRuleLookupManager object
 *  maintains sentence-specific state, exactly one should be created for
 *  each sentence that is to be decoded.
 */
class G2sRuleLookupManager
{
public:
  G2sRuleLookupManager(const G2sParser &parser)
    : m_parser(parser) {}

  virtual ~G2sRuleLookupManager() {}

  const G2sParser &GetParser() const {
    return m_parser;
  }

  /** abstract function. Return a vector of translation options for given a range in the input sentence
   *  \param range source range for which you want the translation options
   *  \param outColl return argument
   */
  virtual void GetG2sRuleCollection(
  		G2sTranslationOptionColl &outColl) = 0;

private:
  //! Non-copyable: copy constructor and assignment operator not implemented.
  G2sRuleLookupManager(const G2sRuleLookupManager &);
  //! Non-copyable: copy constructor and assignment operator not implemented.
  G2sRuleLookupManager &operator=(const G2sRuleLookupManager &);

  const G2sParser &m_parser;
};

}  // namespace Moses
}

#endif
