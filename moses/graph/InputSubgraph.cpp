#include "InputSubgraph.h"

#include "moses/ScoreComponentCollection.h"
#include "moses/TargetPhraseCollection.h"
#include "moses/StaticData.h"
#include "moses/TypeDef.h"
#include "moses/AlignmentInfo.h"
#include "util/check.hh"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

InputSubgraph::InputSubgraph(const Phrase &phrase, const NonTerminalSet &sourceNonTerms, const WordsSet &range, const InputSubgraph *prevNode
                     ,const ScoreComponentCollection *inputScore)
  :m_prevNode(prevNode)
  ,m_phrase(phrase)
  ,m_sourceNonTerms(sourceNonTerms)
  ,m_range(range)
  ,m_inputScore(inputScore)
{
  FactorType placeholderFactor = StaticData::Instance().GetPlaceholderFactor().first;
  if (placeholderFactor != NOT_FOUND) {
    for (size_t pos = 0; pos < m_phrase.GetSize(); ++pos) {
      if (m_phrase.GetFactor(pos, placeholderFactor)) {
        m_placeholders.push_back(pos);
      }
    }
  }
}

InputSubgraph::InputSubgraph(const Phrase &phrase, const NonTerminalSet &sourceNonTerms, const WordsSet &range)
  :m_prevNode(NULL)
  ,m_phrase(phrase)
  ,m_sourceNonTerms(sourceNonTerms)
  ,m_range(range)
  ,m_inputScore(NULL)
{
  FactorType placeholderFactor = StaticData::Instance().GetPlaceholderFactor().first;
  if (placeholderFactor != NOT_FOUND) {
    for (size_t pos = 0; pos < m_phrase.GetSize(); ++pos) {
      if (m_phrase.GetFactor(pos, placeholderFactor)) {
        m_placeholders.push_back(pos);
      }
    }
  }
}

InputSubgraph::~InputSubgraph()
{
  delete m_inputScore;
//  std::map<const PhraseDictionary*, std::pair<const TargetPhraseCollection*, const void*> >::const_iterator iter;
//  for (iter = m_targetPhrases.begin(); iter != m_targetPhrases.end(); ++iter) {
//    delete iter->second.first;
//  }
}

const TargetPhraseCollection *InputSubgraph::GetTargetPhrases(const PhraseDictionary &phraseDictionary) const
{
  std::map<const PhraseDictionary*, std::pair<const TargetPhraseCollection*, const void*> >::const_iterator iter;
  iter = m_targetPhrases.find(&phraseDictionary);
  if (iter == m_targetPhrases.end()) {
    return NULL;
  }
  return iter->second.first;
}

const void *InputSubgraph::GetPtNode(const PhraseDictionary &phraseDictionary) const
{
  std::map<const PhraseDictionary*, std::pair<const TargetPhraseCollection*, const void*> >::const_iterator iter;
  iter = m_targetPhrases.find(&phraseDictionary);
  if (iter == m_targetPhrases.end()) {
    return NULL;
  }
  return iter->second.second;
}

void InputSubgraph::SetTargetPhrases(const PhraseDictionary &phraseDictionary
                                 , const TargetPhraseCollection *targetPhrases
                                 , const void *ptNode)
{
  std::pair<const TargetPhraseCollection*, const void*> value(targetPhrases, ptNode);
  m_targetPhrases[&phraseDictionary] = value;
}

bool InputSubgraph::SetPlaceholders(TargetPhrase *targetPhrase) const
{
  FactorType sourcePlaceholderFactor = StaticData::Instance().GetPlaceholderFactor().first;
  FactorType targetPlaceholderFactor = StaticData::Instance().GetPlaceholderFactor().second;

  const AlignmentInfo &alignments = targetPhrase->GetAlignTerm();
  for (size_t i = 0; i < m_placeholders.size(); ++i) {
    size_t sourcePos = m_placeholders[i];
    set<size_t> targetPos = alignments.GetAlignmentsForSource(sourcePos);
    if (targetPos.size() == 1) {
      const Word &sourceWord = m_phrase.GetWord(sourcePos);
      Word &targetWord = targetPhrase->GetWord(*targetPos.begin());
      targetWord[targetPlaceholderFactor] = sourceWord[sourcePlaceholderFactor];
    } else {
      return false;
    }
  }
  return true;
}

const Word &InputSubgraph::GetLastWord() const
{
  size_t len = m_phrase.GetSize();
  CHECK(len);
  const Word &ret = m_phrase.GetWord(len - 1);
  return ret;
}

void InputSubgraph::ParseStructString(const std::string& str, StructType& ret) {
  vector<string> tokens = Moses::Tokenize(str, "_");
  size_t size = tokens.size();
  if (size > 1) {
    ret.resize(size-1);
    for(size_t i = 1; i < size; i++) {
      vector<string> subtokens = Moses::Tokenize(tokens[i],":");
      for (size_t j = 0; j < subtokens.size(); j++) {
        if (subtokens[j] != "-1") {
          size_t k = Moses::Scan<size_t>(subtokens[j]);
          ret[i-1].insert(k);
        }
      }
    }
  }
}

std::ostream& operator<<(std::ostream& out, const InputSubgraph& obj)
{
  out << &obj << " " << " " << obj.GetPrevNode() << " " << obj.GetPhrase();

  out << "pt: ";
  std::map<const PhraseDictionary*, std::pair<const TargetPhraseCollection*, const void*> >::const_iterator iter;
  for (iter = obj.m_targetPhrases.begin(); iter != obj.m_targetPhrases.end(); ++iter) {
    const PhraseDictionary *pt = iter->first;
    out << pt << " ";
  }

  return out;
}

}
}
