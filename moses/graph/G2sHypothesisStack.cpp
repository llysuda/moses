
#include "G2sHypothesisStack.h"

namespace Moses
{
namespace Graph
{

G2sHypothesisStack::~G2sHypothesisStack()
{
  // delete all hypos
  while (m_hypos.begin() != m_hypos.end()) {
    Remove(m_hypos.begin());
  }
}

/** Remove hypothesis pointed to by iterator but don't delete the object. */
void G2sHypothesisStack::Detach(const G2sHypothesisStack::iterator &iter)
{
  m_hypos.erase(iter);
}


void G2sHypothesisStack::Remove(const G2sHypothesisStack::iterator &iter)
{
  G2sHypothesis *h = *iter;
  Detach(iter);
  FREEHYPO(h);
}


}
}

