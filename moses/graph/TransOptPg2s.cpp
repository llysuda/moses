// $Id$
// vim:tabstop=2

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#include "TransOptPg2s.h"

#include "moses/WordsBitmap.h"
//#include "moses/GenerationDictionary.h"
#include "moses/LexicalReordering.h"
#include "moses/StaticData.h"
#include "moses/InputType.h"

using namespace std;

namespace Moses
{
namespace Graph
{

TransOptPg2s::TransOptPg2s()
  :m_targetPhrase()
  ,m_inputPath(NULL)
	, m_sourceWordsRange()
  , m_futureScore(0.0f)
{
}

//TODO this should be a factory function!
TransOptPg2s::TransOptPg2s(const WordsSet &wordsRange
                                     , const TargetPhrase &targetPhrase)
  : m_targetPhrase(targetPhrase)
  , m_inputPath(NULL)
  , m_sourceWordsRange(wordsRange)
  , m_futureScore(targetPhrase.GetFutureScore())
{
}

bool TransOptPg2s::IsCompatible(const Phrase& phrase, const std::vector<FactorType>& featuresToCheck) const
{
  if (featuresToCheck.size() == 1) {
    return m_targetPhrase.IsCompatible(phrase, featuresToCheck[0]);
  } else if (featuresToCheck.empty()) {
    return true;
    /* features already there, just update score */
  } else {
    return m_targetPhrase.IsCompatible(phrase, featuresToCheck);
  }
}

bool TransOptPg2s::Overlap(const Pg2sHypothesis &hypothesis) const
{
  const WordsBitmap &bitmap = hypothesis.GetWordsBitmap();
  return bitmap.Overlap(GetSourceWordsRange().GetSet());
}

void TransOptPg2s::CacheLexReorderingScores(const LexicalReordering &producer, const Scores &score)
{
  m_lexReorderingScores[&producer] = score;
}

void TransOptPg2s::Evaluate(const InputType &input)
{
  const InputSubgraph &inputPath = GetInputPath();
  m_targetPhrase.Evaluate(input, inputPath);
}

const InputSubgraph &TransOptPg2s::GetInputPath() const
{
  CHECK(m_inputPath);
  return *m_inputPath;
}

void TransOptPg2s::SetInputPath(const InputSubgraph &inputPath)
{
  CHECK(m_inputPath == NULL);
  m_inputPath = &inputPath;
}


TO_STRING_BODY(TransOptPg2s);

// friend
ostream& operator<<(ostream& out, const TransOptPg2s& possibleTranslation)
{
  out << possibleTranslation.GetTargetPhrase()
      << " c=" << possibleTranslation.GetFutureScore()
     // << " [" << possibleTranslation.GetSourceWordsRange() << "]"
      << possibleTranslation.GetScoreBreakdown();
  return out;
}


}
}

