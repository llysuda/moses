#ifndef moses_SearchG2s_h
#define moses_SearchG2s_h

#include <vector>
#include <set>
#include "moses/Search.h"
#include "G2sHypothesisStackNormal.h"
#include "moses/Timer.h"
#include "G2sTranslationOptionColl.h"
#include "G2sTranslationOptionList.h"
#include "G2sTranslationOption.h"
#include "moses/TypeDef.h"
#include "moses/Phrase.h"

#include "SentenceStatsG2s.h"
#include "WordsSet.h"
#include "MultiDiGraphInput.h"

using namespace Moses;

namespace Moses
{

class InputType;

namespace Graph
{

class G2sManager;
class G2sTranslationOptionColl;

/** Functions and variables you need to decoder an input using the phrase-based decoder (NO cube-pruning)
 *  Instantiated by the Manager class
 */
class SearchG2s
{
protected:
  std::vector<size_t> m_stackIndex;
	const Phrase *m_constraint;
	G2sManager& m_manager;
	InputSubgraph m_inputPath; // for initial hypo
	G2sTranslationOption m_initialTransOpt;
  const InputType &m_source;
  std::vector < G2sHypothesisStack* > m_hypoStackColl; /**< stacks to store hypotheses (partial translations) */
  // no of elements = no of words in source + 1
  clock_t m_start; /**< starting time, used for logging */
  size_t interrupted_flag; /**< flag indicating that decoder ran out of time (see switch -time-out) */
  G2sHypothesisStackNormal* actual_hypoStack; /**actual (full expanded) stack of hypotheses*/
  const G2sTranslationOptionColl &m_transOptColl; /**< pre-computed list of translation options for the phrases in this sentence */

  // functions for creating hypotheses
  void ProcessOneHypothesis(const G2sHypothesis &hypothesis);
  void ExpandAllHypotheses(const G2sHypothesis &hypothesis, const WordsSet& nids);
  virtual void ExpandHypothesis(const G2sHypothesis &hypothesis,const G2sTranslationOption &transOpt, float expectedScore);

public:
  SearchG2s(G2sManager& manager, const InputType &source, const G2sTranslationOptionColl &transOptColl);
  ~SearchG2s();

  void ProcessSentence();

  void OutputHypoStackSize();
  void OutputHypoStack(int stack);

  virtual const std::vector < G2sHypothesisStack* >& GetHypothesisStacks() const;
  virtual const G2sHypothesis *GetBestHypothesis() const;
};

}
}

#endif
