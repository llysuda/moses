#ifndef moses_G2sHypothesisStack_h
#define moses_G2sHypothesisStack_h

#include <vector>
#include <set>

#include "G2sHypothesis.h"
#include "moses/WordsBitmap.h"

namespace Moses
{
namespace Graph
{

class G2sManager;

/** abstract unique set of hypotheses that cover a certain number of words,
 *  ie. a stack in phrase-based decoding
 */
class G2sHypothesisStack
{

protected:
  typedef std::set< G2sHypothesis*, G2sHypothesisRecombinationOrderer > _HCType;
  _HCType m_hypos; /**< contains hypotheses */
  G2sManager& m_manager;

public:
  G2sHypothesisStack(G2sManager& manager): m_manager(manager) {}
  typedef _HCType::iterator iterator;
  typedef _HCType::const_iterator const_iterator;
  //! iterators
  const_iterator begin() const {
    return m_hypos.begin();
  }
  const_iterator end() const {
    return m_hypos.end();
  }
  size_t size() const {
    return m_hypos.size();
  }
  virtual inline float GetWorstScore() const {
    return -std::numeric_limits<float>::infinity();
  };
  virtual float GetWorstScoreForBitmap( WordsBitmapID ) {
    return -std::numeric_limits<float>::infinity();
  };
  virtual float GetWorstScoreForBitmap( const WordsBitmap& ) {
    return -std::numeric_limits<float>::infinity();
  };

  virtual ~G2sHypothesisStack();
  virtual bool AddPrune(G2sHypothesis *hypothesis) = 0;
  virtual const G2sHypothesis *GetBestHypothesis() const = 0;
  virtual std::vector<const G2sHypothesis*> GetSortedList() const = 0;

  //! remove hypothesis pointed to by iterator but don't delete the object
  virtual void Detach(const G2sHypothesisStack::iterator &iter);
  /** destroy G2sHypothesis pointed to by iterator (object pool version) */
  virtual void Remove(const G2sHypothesisStack::iterator &iter);

};

}
}
#endif
