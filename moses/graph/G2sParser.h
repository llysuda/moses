// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <list>
#include <vector>
#include "WordsSet.h"

namespace Moses
{
class InputType;
class Sentence;
class Word;
class Phrase;
class TargetPhraseCollection;
class DecodeGraph;

namespace Graph
{

class G2sTranslationOptionColl;
class G2sRuleLookupManager;

class G2sParserUnknown
{
public:
  G2sParserUnknown();
  ~G2sParserUnknown();

  void Process(const Word &sourceWord, const WordsSet &range, G2sTranslationOptionColl &to);
//  void Process(int NTCount, const WordsSet &range, G2sTranslationOptionColl &to);

private:
  std::vector<Phrase*> m_unksrcs;
  std::list<TargetPhraseCollection*> m_cacheTargetPhraseCollection;
};

class G2sParser
{
public:
  G2sParser(const InputType &source);
  ~G2sParser();

  void Create(G2sTranslationOptionColl &to);

  //! the sentence being decoded
  //const Sentence &GetSentence() const;
  long GetTranslationId() const;
  size_t GetSize() const;
//  const InputPath &GetInputPath(size_t startPos, size_t endPos) const;
  const InputType &GetSource() const {
  	return m_source;
  }

private:
  G2sParserUnknown m_unknown;
  std::vector <DecodeGraph*> m_decodeGraphList;
  std::vector<G2sRuleLookupManager*> m_ruleLookupManagers;
  InputType const& m_source; /**< source sentence to be translated */
};

}
}

