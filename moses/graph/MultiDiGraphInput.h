/*
 * DepNgramGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef MOSES_GRAPH_MULTIDOGRAPHINPUT_H_
#define MOSES_GRAPH_MULTIDOGRAPHINPUT_H_

#include <string>
#include <vector>
#include <set>
#include <map>

#include "moses/Sentence.h"
#include "moses/TypeDef.h"

namespace Moses
{
namespace Graph
{

struct Node
{
	size_t id;
	std::string word;
	std::string pos;
	size_t fid;
};

class MultiDiGraph : public Moses::Sentence
{
  typedef std::vector<const struct Node*> NodesType;
  typedef std::map<size_t, std::set<std::string> > LabelsType;
  typedef std::vector< LabelsType > EdgesType;
private:
	std::vector<std::string> m_word;
	NodesType nodes;
	EdgesType succ;
	EdgesType pred;
public:
	MultiDiGraph() {};
	virtual ~MultiDiGraph(){
		DELETE();
	}

	void DELETE() {
	  NodesType::iterator iter = nodes.begin();
      while ( nodes.begin()!=nodes.end()) {
          const Node* node = *nodes.begin();
          nodes.erase(nodes.begin());
          delete node;
      }
      nodes.clear();
	}

	InputTypeEnum GetType() const {
		return MultiDiGraphInput;
	}

	void Print(std::ostream& out) const {
		print_sent(out);
	}

	int Read(std::istream& in,const std::vector<FactorType>& factorOrder);
	std::map<size_t,size_t> SortWords();

	void add_node(size_t id, const struct Node* attr);
	void add_edge(size_t u, size_t v, const std::string& label);
	bool is_connected(const std::set<size_t>& nids) const;
//	const MultiDiGraph* get_subgraph(const std::set<int>& nids) const;
//	std::set<int> get_neighbors(const std::set<int>& nids) const;

//	void print_graph(std::ostream& out) const;
	void print_sent(std::ostream& out) const;

	size_t GetSize() const {
		return Phrase::GetSize();
	}

	void clear() {
		DELETE();
		succ.clear();
		pred.clear();
	}

	std::string get_word(size_t nid) const {
		return nodes[nid]->word;
	}

	bool has_succ(size_t nid) const {
		return succ[nid].size() > 0;
	}

	const LabelsType& get_succ_edges(size_t nid) const {
		return succ[nid];
	}

	bool has_pred(size_t nid) const {
		return pred[nid].size() > 0;
	}

	const LabelsType& get_pred_edges(size_t nid) const {
		return pred[nid];
	}

	void ResetWords() {
		for(size_t i = 0; i < GetSize(); i++) {
			for (size_t j = 1; j <=3; j++)
				Phrase::SetFactor(i,j,NULL);
		}
	}
	std::string GetLabel(const std::set<size_t>&) const;
	std::string get_structure_string(const std::vector<size_t>& nids, bool incLabel) const;
	bool within_distance(const std::set<size_t>& nids1, int firstGap, int maxDistance) const;

//	std::string get_nt_structure(const std::map<int,int>& termMap, const std::set<int>& hole, bool incLabel) const;
//	std::string ConcatLabels(const std::map<int, std::set<std::string> >& labels, bool incLabel) const;
//	void CollectLabels(const std::map<int,int>& I2J,
//			const std::set<int>& except,
//			const std::map<int, std::set<std::string> >& edges,
//			std::map<int, std::set<std::string> >& ret) const;

	bool between_connected(const std::set<size_t>& g1, const std::set<size_t>& g2) const;
	std::string GetBetweenLink(const std::set<size_t>& currRange, const std::set<size_t>& nextRange) const;

	virtual std::vector<float> ComputeDistortionDistance(int prevEnd, const std::set<size_t>& current, bool naive) const;

private:
	void DepthFirst(size_t curr_nid,
			std::set<size_t>& old_nids,
			const std::set<size_t>& all_nids) const;
};

}
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
