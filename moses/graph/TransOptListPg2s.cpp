
#include "TransOptListPg2s.h"
#include "TransOptPg2s.h"
#include "moses/Util.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

TransOptListPg2s::TransOptListPg2s()
  : bestFutureScore(-numeric_limits<float>::infinity())
{}

TransOptListPg2s::TransOptListPg2s(const TransOptListPg2s &copy)
  : bestFutureScore(-numeric_limits<float>::infinity())
{
  const_iterator iter;
  for (iter = copy.begin(); iter != copy.end(); ++iter) {
    const TransOptPg2s &origTransOpt = **iter;
    TransOptPg2s *newTransOpt = new TransOptPg2s(origTransOpt);
    Add(newTransOpt);
  }
}

TransOptListPg2s::~TransOptListPg2s()
{
  RemoveAllInColl(m_coll);
}

void TransOptListPg2s::Add(TransOptPg2s *transOpt) {
    m_coll.push_back(transOpt);
    if (transOpt->GetFutureScore() > bestFutureScore)
      bestFutureScore = transOpt->GetFutureScore();
  }

TO_STRING_BODY(TransOptListPg2s);

std::ostream& operator<<(std::ostream& out, const TransOptListPg2s& coll)
{
  TransOptListPg2s::const_iterator iter;
  for (iter = coll.begin(); iter != coll.end(); ++iter) {
    const TransOptPg2s &transOpt = **iter;
    out << transOpt << endl;
  }

  return out;
}

}
} // namespace
