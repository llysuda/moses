// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#include "G2sTrellisPath.h"

#include "G2sTrellisPathCollection.h"
#include "G2sTrellisPathList.h"
#include "moses/StaticData.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

G2sTrellisPath::G2sTrellisPath(const G2sHypothesis *hypo)
  :	m_prevEdgeChanged(NOT_FOUND)
{
  m_scoreBreakdown					= hypo->GetScoreBreakdown();
  m_totalScore = hypo->GetTotalScore();

  // enumerate path using prevHypo
  while (hypo != NULL) {
    m_path.push_back(hypo);
    hypo = hypo->GetPrevHypo();
  }
}

void G2sTrellisPath::InitScore()
{
  m_totalScore		= m_path[0]->GetWinningHypo()->GetTotalScore();
  m_scoreBreakdown= m_path[0]->GetWinningHypo()->GetScoreBreakdown();

  //calc score
  size_t sizePath = m_path.size();
  for (size_t pos = 0 ; pos < sizePath ; pos++) {
    const G2sHypothesis *hypo = m_path[pos];
    const G2sHypothesis *winningHypo = hypo->GetWinningHypo();
    if (hypo != winningHypo) {
      m_totalScore = m_totalScore - winningHypo->GetTotalScore() + hypo->GetTotalScore();
      m_scoreBreakdown.MinusEquals(winningHypo->GetScoreBreakdown());
      m_scoreBreakdown.PlusEquals(hypo->GetScoreBreakdown());
    }
  }


}

G2sTrellisPath::G2sTrellisPath(const G2sTrellisPath &copy, size_t edgeIndex, const G2sHypothesis *arc)
  :m_prevEdgeChanged(edgeIndex)
{
  m_path.reserve(copy.m_path.size());
  for (size_t currEdge = 0 ; currEdge < edgeIndex ; currEdge++) {
    // copy path from parent
    m_path.push_back(copy.m_path[currEdge]);
  }

  // 1 deviation
  m_path.push_back(arc);

  // rest of path comes from following best path backwards
  const G2sHypothesis *prevHypo = arc->GetPrevHypo();
  while (prevHypo != NULL) {
    m_path.push_back(prevHypo);
    prevHypo = prevHypo->GetPrevHypo();
  }

  InitScore();
}

G2sTrellisPath::G2sTrellisPath(const vector<const G2sHypothesis*> edges)
  :m_prevEdgeChanged(NOT_FOUND)
{
  m_path.resize(edges.size());
  copy(edges.rbegin(),edges.rend(),m_path.begin());
  InitScore();


}


void G2sTrellisPath::CreateDeviantPaths(G2sTrellisPathCollection &pathColl) const
{
  const size_t sizePath = m_path.size();

  if (m_prevEdgeChanged == NOT_FOUND) {
    // initial enumration from a pure hypo
    for (size_t currEdge = 0 ; currEdge < sizePath ; currEdge++) {
      const G2sHypothesis	*hypo		= static_cast<const G2sHypothesis*>(m_path[currEdge]);
      const ArcListG2s *pAL = hypo->GetArcList();
      if (!pAL) continue;
      const ArcListG2s &arcList = *pAL;

      // every possible Arc to replace this edge
      ArcListG2s::const_iterator iterArc;
      for (iterArc = arcList.begin() ; iterArc != arcList.end() ; ++iterArc) {
        const G2sHypothesis *arc = *iterArc;
        G2sTrellisPath *deviantPath = new G2sTrellisPath(*this, currEdge, arc);
        pathColl.Add(deviantPath);
      }
    }
  } else {
    // wiggle 1 of the edges only
    for (size_t currEdge = m_prevEdgeChanged + 1 ; currEdge < sizePath ; currEdge++) {
      const ArcListG2s *pAL = m_path[currEdge]->GetArcList();
      if (!pAL) continue;
      const ArcListG2s &arcList = *pAL;
      ArcListG2s::const_iterator iterArc;

      for (iterArc = arcList.begin() ; iterArc != arcList.end() ; ++iterArc) {
        // copy this Path & change 1 edge
        const G2sHypothesis *arcReplace = *iterArc;

        G2sTrellisPath *deviantPath = new G2sTrellisPath(*this, currEdge, arcReplace);
        pathColl.Add(deviantPath);
      } // for (iterArc...
    } // for (currEdge = 0 ...
  }
}

void G2sTrellisPath::CreateDeviantPaths(G2sTrellisPathList &pathColl) const
{
  const size_t sizePath = m_path.size();

  if (m_prevEdgeChanged == NOT_FOUND) {
    // initial enumration from a pure hypo
    for (size_t currEdge = 0 ; currEdge < sizePath ; currEdge++) {
      const G2sHypothesis	*hypo		= static_cast<const G2sHypothesis*>(m_path[currEdge]);
      const ArcListG2s *pAL = hypo->GetArcList();
      if (!pAL) continue;
      const ArcListG2s &arcList = *pAL;

      // every possible Arc to replace this edge
      ArcListG2s::const_iterator iterArc;
      for (iterArc = arcList.begin() ; iterArc != arcList.end() ; ++iterArc) {
        const G2sHypothesis *arc = *iterArc;
        G2sTrellisPath *deviantPath = new G2sTrellisPath(*this, currEdge, arc);
        pathColl.Add(deviantPath);
      }
    }
  } else {
    // wiggle 1 of the edges only
    for (size_t currEdge = m_prevEdgeChanged + 1 ; currEdge < sizePath ; currEdge++) {
      const ArcListG2s *pAL = m_path[currEdge]->GetArcList();
      if (!pAL) continue;
      const ArcListG2s &arcList = *pAL;
      ArcListG2s::const_iterator iterArc;

      for (iterArc = arcList.begin() ; iterArc != arcList.end() ; ++iterArc) {
        // copy this Path & change 1 edge
        const G2sHypothesis *arcReplace = *iterArc;

        G2sTrellisPath *deviantPath = new G2sTrellisPath(*this, currEdge, arcReplace);
        pathColl.Add(deviantPath);
      } // for (iterArc...
    } // for (currEdge = 0 ...
  }
}

Phrase G2sTrellisPath::GetTargetPhrase() const
{
  Phrase targetPhrase(ARRAY_SIZE_INCR);

  int numHypo = (int) m_path.size();
  for (int node = numHypo - 2 ; node >= 0 ; --node) {
    // don't do the empty hypo - waste of time and decode step id is invalid
    const G2sHypothesis &hypo = *m_path[node];
    targetPhrase.Append(hypo.GetCurrTargetPhrase());
  }

  return targetPhrase;
}

Phrase G2sTrellisPath::GetSurfacePhrase() const
{
  const std::vector<FactorType> &outputFactor = StaticData::Instance().GetOutputFactorOrder();
  Phrase targetPhrase = GetTargetPhrase()
                        ,ret(targetPhrase.GetSize());

  for (size_t pos = 0 ; pos < targetPhrase.GetSize() ; ++pos) {
    Word &newWord = ret.AddWord();
    for (size_t i = 0 ; i < outputFactor.size() ; i++) {
      FactorType factorType = outputFactor[i];
      const Factor *factor = targetPhrase.GetFactor(pos, factorType);
      CHECK(factor);
      newWord[factorType] = factor;
    }
  }

  return ret;
}

WordsRange G2sTrellisPath::GetTargetWordsRange(const G2sHypothesis &hypo) const
{
  size_t startPos = 0;

  for (int indEdge = (int) m_path.size() - 1 ; indEdge >= 0 ; --indEdge) {
    const G2sHypothesis *currHypo = m_path[indEdge];
    size_t endPos = startPos + currHypo->GetCurrTargetLength() - 1;

    if (currHypo == &hypo) {
      return WordsRange(startPos, endPos);
    }
    startPos = endPos + 1;
  }

  // have to give a hypo in the trellis path, but u didn't.
  CHECK(false);
  return WordsRange(NOT_FOUND, NOT_FOUND);
}

TO_STRING_BODY(G2sTrellisPath);


}
}

