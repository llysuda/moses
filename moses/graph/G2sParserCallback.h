#pragma once

#include <list>
#include <string>

namespace Moses
{

class TargetPhraseCollection;
class TargetPhrase;
class InputType;
class Phrase;

namespace Graph
{

class WordsSet;
class InputSubgraph;

class G2sParserCallback
{
public:
  virtual ~G2sParserCallback() {}

  virtual void Add(const TargetPhraseCollection &, const WordsSet &, const Phrase &) = 0;

  virtual bool Empty() const = 0;

  virtual void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const WordsSet &range) = 0;

  virtual void Evaluate(const InputType &input) = 0;
};

} // namespace Moses
}
