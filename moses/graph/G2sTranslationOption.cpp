#include "G2sTranslationOptions.h"
#include "moses/InputType.h"
#include "InputSubgraph.h"
#include "moses/Util.h"
#include "moses/LexicalReordering.h"

using namespace std;

namespace Moses
{
namespace Graph
{

G2sTranslationOption::G2sTranslationOption()
	: m_targetPhrase()
	, m_wordsRange()
	, m_futureScore(0.0f)
    , m_sourcePhrase(NULL)
    , m_targetGap()
{
	//
}

G2sTranslationOption::G2sTranslationOption(const TargetPhrase &targetPhrase, const WordsSet& range, const Phrase* sourcePhrase)
  :m_targetPhrase(targetPhrase)
	,m_wordsRange(range)
  ,m_scoreBreakdown(targetPhrase.GetScoreBreakdown())
	,m_futureScore(targetPhrase.GetFutureScore())
	, m_nextLHS("")
    , m_sourcePhrase(sourcePhrase)
{
  size_t size = m_targetPhrase.GetSize();
  if (size > 1 && m_targetPhrase.GetWord(size-1).IsNonTerminal()) {
    m_targetGap = m_targetPhrase.GetWord(m_targetPhrase.GetSize()-1);
    m_targetPhrase.RemoveWord(m_targetPhrase.GetSize()-1);
  }
  if (size > 0 && m_targetPhrase.GetWord(0).IsNonTerminal()) {
//    m_targetGap = m_targetPhrase.GetWord(m_targetPhrase.GetSize()-1);
    m_targetPhrase.RemoveWord(0);
  }
}

void G2sTranslationOption::Evaluate(const InputType &input)
{
  const std::vector<FeatureFunction*> &ffs = FeatureFunction::GetFeatureFunctions();

  for (size_t i = 0; i < ffs.size(); ++i) {
    const FeatureFunction &ff = *ffs[i];
    ff.Evaluate(input, m_wordsRange.GetSet(), m_scoreBreakdown);
  }
}

void G2sTranslationOption::CacheLexReorderingScores(const LexicalReordering &producer, const Scores &score)
{
  m_lexReorderingScores[&producer] = score;
}

void G2sTranslationOption::CacheG2sOrderModelScores(const Scores &score)
{
  m_orderModelScores = score;
}

}
}

