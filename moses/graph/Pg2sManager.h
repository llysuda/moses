// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_Graph_Pg2sManager_h
#define moses_Graph_Pg2sManager_h

#include <vector>
#include <list>
#include "moses/InputType.h"
#include "moses/StaticData.h"
#include "Pg2sTrellisPathList.h"
#include "moses/SquareMatrix.h"
#include "moses/WordsBitmap.h"
#include "SearchPg2s.h"

#include "Pg2sHypothesis.h"
#include "moses/SearchCubePruning.h"
#include "TransOptCollPg2s.h"
#include "TransOptPg2s.h"

#include "moses/graph/SentenceStatsPg2s.h"
//using namespace Moses;

namespace Moses
{
namespace Graph
{

class Pg2sTrellisPath;
class Pg2sTransOptCollPg2s;

/** Used to output the search graph */
struct SearchGraphNodePg2s {
  const Pg2sHypothesis* hypo;
  const Pg2sHypothesis* recombinationHypo;
  int forward;
  double fscore;

  SearchGraphNodePg2s(const Pg2sHypothesis* theHypo,
                  const Pg2sHypothesis* theRecombinationHypo,
                  int theForward,
                  double theFscore) :
    hypo(theHypo), recombinationHypo(theRecombinationHypo),
    forward(theForward), fscore(theFscore) {}

  bool operator<(const SearchGraphNodePg2s& sgn) const {
    return this->hypo->GetId() < sgn.hypo->GetId();
  }

};

/** The Pg2sManager class implements a stack decoding algorithm for phrase-based decoding
 * Hypotheses are organized in stacks. One stack contains all hypothesis that have
 * the same number of foreign words translated.  The data structure for hypothesis
 * stacks is the class HypothesisStack. The data structure for a hypothesis
 * is the class Pg2sHypothesis.
 *
 * The main decoder loop in the function ProcessSentence() consists of the steps:
 * - Create the list of possible translation options. In phrase-based decoding
 *   (and also the first mapping step in the factored model) is a phrase translation
 *   from the source to the target. Given a specific input sentence, only a limited
 *   number of phrase translation can be applied. For efficient lookup of the
 *   translation options later, these options are first collected in the function
 *   CreateTranslationOption (for more information check the class
 *   TransOptCollPg2s)
 * - Create initial hypothesis: Pg2sHypothesis stack 0 contains only one empty hypothesis.
 * - Going through stacks 0 ... (sentence_length-1):
 *   - The stack is pruned to the maximum size
 *   - Going through all hypotheses in the stack
 *     - Each hypothesis is expanded by ProcessOneHypothesis()
 *     - Expansion means applying a translation option to the hypothesis to create
 *       new hypotheses
 *     - What translation options may be applied depends on reordering limits and
 *       overlap with already translated words
 *     - With a applicable translation option and a hypothesis at hand, a new
 *       hypothesis can be created in ExpandHypothesis()
 *     - New hypothesis are either discarded (because they are too bad), added to
 *       the appropriate stack, or re-combined with existing hypotheses
 **/

class Pg2sManager
{
  Pg2sManager();
  Pg2sManager(Pg2sManager const&);
  void operator=(Pg2sManager const&);
private:

  // Helper functions to output search graph in HTK standard lattice format
  void OutputFeatureWeightsForSLF(std::ostream &outputSearchGraphStream) const;
  size_t OutputFeatureWeightsForSLF(size_t index, const FeatureFunction* ff, std::ostream &outputSearchGraphStream) const;
  void OutputFeatureValuesForSLF(const Pg2sHypothesis* hypo, bool zeros, std::ostream &outputSearchGraphStream) const;
  size_t OutputFeatureValuesForSLF(size_t index, bool zeros, const Pg2sHypothesis* hypo, const FeatureFunction* ff, std::ostream &outputSearchGraphStream) const;

  // Helper functions to output search graph in the hypergraph format of Kenneth Heafield's lazy hypergraph decoder
  void OutputFeatureValuesForHypergraph(const Pg2sHypothesis* hypo, std::ostream &outputSearchGraphStream) const;
  size_t OutputFeatureValuesForHypergraph(size_t index, const Pg2sHypothesis* hypo, const FeatureFunction* ff, std::ostream &outputSearchGraphStream) const;


protected:
  // data
//	InputType const& m_source; /**< source sentence to be translated */
  TransOptCollPg2s *m_transOptColl; /**< pre-computed list of translation options for the phrases in this sentence */
  SearchPg2s *m_search;

  Pg2sHypothesisStack* actual_hypoStack; /**actual (full expanded) stack of hypotheses*/
  size_t interrupted_flag;
  std::auto_ptr<SentenceStatsPg2s> m_sentenceStats;
  int m_hypoId; //used to number the hypos as they are created.
  size_t m_lineNumber;

  void GetConnectedGraph(
    std::map< int, bool >* pConnected,
    std::vector< const Pg2sHypothesis* >* pConnectedList) const;
  void GetWinnerConnectedGraph(
    std::map< int, bool >* pConnected,
    std::vector< const Pg2sHypothesis* >* pConnectedList) const;


public:
  InputType const& m_source; /**< source sentence to be translated */
  Pg2sManager(size_t lineNumber, InputType const& source, SearchAlgorithm searchAlgorithm);
  ~Pg2sManager();
  const  TransOptCollPg2s* getSntTranslationOptions();

  void ProcessSentence();
  const Pg2sHypothesis *GetBestHypothesis() const;
  const Pg2sHypothesis *GetActualBestHypothesis() const;
  void CalcNBest(size_t count, Pg2sTrellisPathList &ret,bool onlyDistinct=0) const;
  void CalcLatticeSamples(size_t count, Pg2sTrellisPathList &ret) const;
  void PrintAllDerivations(long translationId, std::ostream& outputStream) const;
  void printDivergentHypothesis(long translationId, const Pg2sHypothesis* hypo, const std::vector <const TargetPhrase*> & remainingPhrases, float remainingScore , std::ostream& outputStream) const;
  void printThisHypothesis(long translationId, const Pg2sHypothesis* hypo, const std::vector <const TargetPhrase* > & remainingPhrases, float remainingScore , std::ostream& outputStream) const;
  void GetWordGraph(long translationId, std::ostream &outputWordGraphStream) const;
  int GetNextHypoId();
#ifdef HAVE_PROTOBUF
  void SerializeSearchGraphPB(long translationId, std::ostream& outputStream) const;
#endif

  void OutputSearchGraph2(long translationId, std::ostream &outputSearchGraphStream) const;
  void OutputSearchGraph(long translationId, std::ostream &outputSearchGraphStream) const;
  void OutputSearchGraphAsSLF(long translationId, std::ostream &outputSearchGraphStream) const;
  void OutputSearchGraphAsHypergraph(long translationId, std::ostream &outputSearchGraphStream) const;
  void GetSearchGraph(std::vector<SearchGraphNodePg2s>& searchGraph) const;
  const InputType& GetSource() const {
    return m_source;
  }

  /***
   * to be called after processing a sentence (which may consist of more than just calling ProcessSentence() )
   */
  void CalcDecoderStatistics() const;
  void ResetSentenceStats(const InputType& source);
  SentenceStatsPg2s& GetSentenceStats() const;

  /***
   *For Lattice MBR
  */
  void GetForwardBackwardSearchGraph(std::map< int, bool >* pConnected,
                                     std::vector< const Pg2sHypothesis* >* pConnectedList, std::map < const Pg2sHypothesis*, std::set < const Pg2sHypothesis* > >* pOutgoingHyps, std::vector< float>* pFwdBwdScores) const;

};

}
}
#endif
