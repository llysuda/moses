// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_graph_WordsSet_h
#define moses_graph_WordsSet_h

#include <iostream>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include "moses/TypeDef.h"
#include "moses/Util.h"

#include "moses/WordsRange.h"

#ifdef WIN32
#undef max
#endif

namespace Moses
{
namespace Graph
{

/***
 * Efficient version of WordsBitmap for contiguous ranges
 */
class WordsSet
{
  friend std::ostream& operator << (std::ostream& out, const WordsSet& range);

  std::set<size_t> m_set;
  std::vector<size_t> m_sorted;
public:
  inline WordsSet()
  	:m_set(),m_sorted(){}

  inline WordsSet(const WordsSet &copy)
    : m_set(copy.m_set)
  	, m_sorted(copy.m_sorted){
  }

  inline WordsSet(const WordsRange &copy): m_set(),m_sorted() {
    if (copy.GetNumWordsCovered() > 0) {
      size_t start = copy.GetStartPos(), end = copy.GetEndPos();
      for (size_t i = start; i <= end; i++) {
        Add(i);
      }
    }
  }

  inline WordsSet(size_t startPos, size_t endPos): m_set(),m_sorted() {
    if (endPos - startPos + 1 > 0) {
      for (size_t i = startPos; i <= endPos; i++) {
        Add(i);
      }
    }
  }

  inline WordsSet(const std::vector<size_t>& vec)
    : m_set(vec.begin(), vec.end())
    , m_sorted(vec.begin(), vec.end()){
  }

  inline size_t GetStartPos() const {
    if (m_set.size() == 0)
      return NOT_FOUND;
    return *m_set.begin();
  }
  inline size_t GetEndPos() const {
    if (m_set.size() == 0)
      return NOT_FOUND;
    return *m_set.rbegin();
  }

  size_t GetMin() const {
    CHECK(m_set.size()>0);
  	return *m_set.begin();
  }

  size_t GetMax() const {
    CHECK(m_set.size()>0);
    return *m_set.rbegin();
  }

  void Add (size_t pos, size_t index) {
  	m_set.insert(pos);
  	m_sorted.push_back(pos);
  }

  void Add (const WordsSet& ws) {
    m_set.insert(ws.m_set.begin(), ws.m_set.end());
    m_sorted.insert(m_sorted.end(), ws.m_sorted.begin(), ws.m_sorted.end());
  }

  void Add (size_t pos) {
    m_set.insert(pos);
    m_sorted.push_back(pos);
  }

  void Remove(size_t pos) {
  	m_set.erase(pos);
  	m_sorted.erase(std::find(m_sorted.begin(), m_sorted.end(), pos));
  }

  bool HasIndex (size_t pos) const {
  	return m_set.find(pos) != m_set.end();
  }

  const std::set<size_t>& GetSet() const {
  	return m_set;
  }

  size_t GetSize() const {
  	return m_set.size();
  }

  std::vector<size_t> GetSortedVec() const {
  	return m_sorted;
  }
  //! count of words translated
  inline size_t GetNumWordsCovered() const {
//    return (m_startPos == NOT_FOUND) ? 0 : m_endPos - m_startPos + 1;
  	return m_set.size();
  }

  //! transitive comparison
  inline bool operator<(const WordsSet& x) const {
//    return (m_startPos<x.m_startPos
//            || (m_startPos==x.m_startPos && m_endPos<x.m_endPos));
  	return m_set < x.m_set;
//  	size_t size = m_set.size();
//  	size_t xsize = x.m_set.size();
//
//  	if (size == 0 && xsize == 0)
//  		return true;
//  	if (size == 0)
//  		return false;
//  	if (xsize == 0)
//  		return true;
//
//  	return (size > xsize || (size == xsize && *(m_set.begin()) < *(x.m_set.begin())));
  }

  // equality operator
  inline bool operator==(const WordsSet& x) const {
//    return (m_startPos==x.m_startPos && m_endPos==x.m_endPos);
  	if (m_set.size() != x.m_set.size())
  		return false;
  	for (std::set<size_t>::const_iterator iter = m_set.begin();
  			iter != m_set.end(); ++iter)
  		if (x.m_set.find(*iter) == x.m_set.end())
  			return false;
  	return true;
  }
  // Whether two word ranges overlap or not
  inline bool Overlap(const WordsSet& x) const {
  	if (m_set.size() == 0 || x.m_set.size() == 0)
  	  return false;
  	if (m_set.size() < x.m_set.size()) {
      for (std::set<size_t>::const_iterator iter = m_set.begin();
                  iter != m_set.end(); ++iter)
        if (x.m_set.find(*iter) != x.m_set.end())
            return true;
    } else {
      for (std::set<size_t>::const_iterator iter = x.m_set.begin();
                        iter != x.m_set.end(); ++iter)
              if (m_set.find(*iter) != m_set.end())
                  return true;
    }
	return false;
  }

  WordsRange ToRange() const {
    return WordsRange(GetStartPos(), GetEndPos());
  }

//  inline size_t GetNumWordsBetween(const WordsSet& x) const {
//    CHECK(!Overlap(x));
//
//    if (x.m_endPos < m_startPos) {
//      return m_startPos - x.m_endPos - 1;
//    }
//
//    return x.m_startPos - m_endPos - 1;
//  }

  std::string GetCovString () const {
    CHECK (m_set.size() > 0);
    std::string ret = "";
    for (std::set<size_t>::const_iterator iter = m_set.begin();
        iter != m_set.end(); ++iter) {
      ret += SPrint<size_t>(*iter) + ":";
    }
    return ret.erase(ret.size()-1);
  }

  bool IsContinuous () const {
    size_t prev = NOT_FOUND;
    for (std::set<size_t>::const_iterator iter = m_set.begin();
        iter != m_set.end(); ++iter) {
      if (prev != NOT_FOUND && prev + 1 != *iter)
        return false;
      prev = *iter;
    }
    return true;
  }

  size_t GetRoot() const {
    CHECK(m_sorted.size()>0);
    //CHECK(m_sorted[0] == *m_set.begin());
    return m_sorted[0];
  }

  size_t GetFirstGap() const {
    if (m_set.size() == 0) return 0;

    size_t prev = NOT_FOUND;
    for (std::set<size_t>::const_iterator iter = m_set.begin();
        iter != m_set.end(); ++iter) {
      if (prev != NOT_FOUND && prev + 1 != *iter)
        break;
      prev = *iter;
    }
    return prev+1;
  }

  int Compare(const WordsSet& a) const {
    int sizeDiff = (int)a.GetSize() - (int)GetSize();
    if (sizeDiff != 0)
      return sizeDiff;
  }

  TO_STRING();
};


}
}
#endif
