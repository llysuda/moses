// $Id$
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/
#pragma once

#include "moses/graph/RealGraphParser/RealG2sCellLabel.h"
#include <string>
#include <vector>
#include "moses/Word.h"
#include "moses/Phrase.h"
#include "moses/Util.h"
#include "RealG2sWordsRange.h"

namespace Moses
{
namespace RealGraph
{
/** @todo what is this?
 */
class RealG2sDottedRule
{
public:
  // used only to init dot stack.
  RealG2sDottedRule()
    : m_cellLabel(NULL)
    , m_prev(NULL)
		, m_sourceLabel(NULL)
, m_count(0)
, m_visited(NOT_FOUND)
, m_stack(), dummy(false) {}

  RealG2sDottedRule(const RealG2sCellLabel &ccl, const RealG2sDottedRule &prev)
    : m_cellLabel(&ccl)
    , m_prev(&prev)
  	, m_sourceLabel(NULL)
    , m_count(0)
    , m_visited(prev.GetVisited())
    , m_stack(prev.GetStack()), dummy(false){

  }

  void SetDummy(bool value) {
    dummy = value;
  }

  bool IsDummy() const {
    return dummy;
  }

  const RealG2sWordsRange &GetWordsRange() const {
    return m_cellLabel->GetCoverage();
  }
  /*const Word &GetSourceWord() const {
    return m_cellLabel->GetLabel();
  }*/
  bool IsNonTerminal() const {
    if (dummy) return false;
    return m_cellLabel->GetLabel().IsNonTerminal();
  }
  const RealG2sDottedRule *GetPrev() const {
    return m_prev;
  }
  bool IsRoot() const {
    return m_prev == NULL;
  }
  const RealG2sCellLabel &GetRealG2sCellLabel() const {
    return *m_cellLabel;
  }


  const Word& GetSourceLabel() const {
  	return *m_sourceLabel;
  }

//  int GetCount() const {
//    return m_count;
//  }
//  void SetCount(int count) {
//    m_count = count;
//  }
  size_t GetVisited() const {
    return m_visited;
  }
  void SetVisited(size_t visited) {
    m_visited = visited;
  }
  const std::vector<size_t>& GetStack() const {
    return m_stack;
  }
  void SetStack(const std::vector<size_t>& stack) {
    m_stack = stack;
  }

  void StackPop() {
    m_stack.pop_back();
  }

  void StackPush(size_t x) {
    m_stack.push_back(x);
  }

  size_t GetFirstVisited() const {
    const RealG2sDottedRule * curr = this;
    while (!curr->m_prev->IsRoot())
      curr = curr->m_prev;
    return curr->m_visited;
  }
//  bool Visited(size_t x) const {
//    const RealG2sDottedRule * curr = this;
//    while (!curr->IsRoot()) {
//      if (x == curr->m_visited)
//        return true;
//      curr = curr->m_prev;
//    }
//    return false;
//  }

  void GetVisitedVec(std::vector<size_t>& ret) const {
    const RealG2sDottedRule * curr = this;
    while (!curr->IsRoot()) {
      if (!curr->dummy)
//      if (ret.size() == 0 || ret[0] != curr->m_visited)
        ret.insert(ret.begin(), curr->m_visited);
      curr = curr->m_prev;
    }
  }

  bool Overlap(const RealG2sWordsRange& cov) const {
    const RealG2sDottedRule * curr = this;
      while (!curr->IsRoot()) {
        if ((!curr->IsDummy()) && curr->m_cellLabel != NULL && curr->GetWordsRange().Overlap(cov))
          return true;
        curr = curr->m_prev;
      }
      return false;
  }

  void GetWholeWordsSet(RealG2sWordsRange& ret) const {
    const RealG2sDottedRule * curr = this;
    while (!curr->IsRoot()) {
      if ((!curr->IsDummy()) && curr->m_cellLabel != NULL)
        ret.Add(curr->GetWordsRange());
      curr = curr->m_prev;
    }
  }

//  std::string GetWordOrder() const {
//    std::string ret = "";
//    std::set<size_t> keys;
//    for (std::map<size_t,size_t>::const_iterator iter = m_visited.begin();
//        iter != m_visited.end(); iter++) {
//      keys.insert(iter->first);
//    }
//    for (std::set<size_t>::const_iterator iter = keys.begin();
//        iter != keys.end(); iter++) {
//      size_t key = *iter;
//      size_t value = m_visited.find(key)->second;
//      ret += Moses::SPrint<size_t>(value) + ":";
//    }
//    return ret.erase(ret.size()-1);
//  }
//
//  size_t GetCurrentSize() const {
//    return m_count;
//  }

  void SetSouceLabel(const Word& word) {
  	CHECK(m_sourceLabel == NULL);
  	m_sourceLabel = &word;
  }

private:
  const RealG2sCellLabel *m_cellLabel; // usually contains something, unless
  // it's the init processed rule
  const RealG2sDottedRule *m_prev;
  const Word* m_sourceLabel;
  int m_count;
  size_t m_visited;
  std::vector<size_t> m_stack;
  bool dummy;
  //std::string m_label4serg;
};

}
}
