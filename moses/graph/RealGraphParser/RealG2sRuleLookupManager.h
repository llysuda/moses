/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_RealG2sRuleLookupManager_h
#define moses_RealG2sRuleLookupManager_h

#include "moses/InputType.h"
#include "RealG2sCellCollection.h"

namespace Moses
{
class WordsRange;
class Sentence;

namespace RealGraph
{
class RealG2sParser;
class RealG2sParserCallback;


/** Defines an interface for looking up rules in a rule table.  Concrete
 *  implementation classes should correspond to specific PhraseDictionary
 *  subclasses (memory or on-disk).  Since a RealG2sRuleLookupManager object
 *  maintains sentence-specific state, exactly one should be created for
 *  each sentence that is to be decoded.
 */
class RealG2sRuleLookupManager
{
public:
  RealG2sRuleLookupManager(const RealG2sParser &parser,
                         const RealG2sCellCollectionBase &cellColl)
    : m_parser(parser)
    , m_cellCollection(cellColl) {}

  virtual ~RealG2sRuleLookupManager() {}

  const RealG2sCellLabelSet &GetTargetLabelSet(size_t begin, size_t end) const {
    return m_cellCollection.GetBase(WordsRange(begin, end)).GetTargetLabelSet();
  }

  const RealG2sParser &GetParser() const {
    return m_parser;
  }
  //const Sentence &GetSentence() const;

  const RealG2sCellLabel &GetSourceAt(size_t at) const {
    return m_cellCollection.GetSourceWordLabel(at);
  }

  /** abstract function. Return a vector of translation options for given a range in the input sentence
   *  \param range source range for which you want the translation options
   *  \param outColl return argument
   */
  virtual void GetRealG2sRuleCollection(
    const WordsRange &range,
    RealG2sParserCallback &outColl) = 0;

  virtual void GetRealG2sRuleCollectionLexGlue(
      const WordsRange &range,
      RealG2sParserCallback &outColl) {}

  virtual void GetRealG2sRuleCollectionTerm(
      RealG2sParserCallback &outColl) {}

private:
  //! Non-copyable: copy constructor and assignment operator not implemented.
  RealG2sRuleLookupManager(const RealG2sRuleLookupManager &);
  //! Non-copyable: copy constructor and assignment operator not implemented.
  RealG2sRuleLookupManager &operator=(const RealG2sRuleLookupManager &);

  const RealG2sParser &m_parser;
  const RealG2sCellCollectionBase &m_cellCollection;
};

}
}  // namespace Moses

#endif
