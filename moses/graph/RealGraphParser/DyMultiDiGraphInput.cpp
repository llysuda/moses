/*
 * DepNgramGraph.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

//#include "tables-core.h"
//#include "moses/Util.h"
#include <set>
#include <algorithm>
#include <iostream>

#include "DyMultiDiGraphInput.h"
#include "RealG2sWordsRange.h"
#include "moses/StaticData.h"
#include "moses/Util.h"
#include "moses/WordsRange.h"

using namespace std;
using namespace Moses;
using namespace Moses::Graph;

namespace Moses {
namespace RealGraph {

struct less_than_key
{
    inline bool operator() (const std::pair<std::string,int>& struct1,
    												const std::pair<std::string,int>& struct2) const
    {
        return (struct1.first < struct2.first);
    }
} node_key;

int DyMultiDiGraph::Read(std::istream& in, const std::vector<FactorType>& factorOrder) {

	if (!Sentence::Read(in, factorOrder))
	  return 0;

	const StaticData& staticData = StaticData::Instance();
//	bool noBigram = staticData.GetNoBigram();
//	bool useLinkType = staticData.GetUseLinkType();
//	bool revBigram = staticData.GetReverseBigram();
	bool sibling = staticData.GetSibling();
	bool onlyTree = staticData.GetOnlyTree();
//	// sort word and reset fid
//	map<size_t, size_t> indexMap = SortWords();

	size_t size = GetSize();
	m_word.resize(size);
	m_pos.resize(size);
	m_fid.resize(size);
//	succ.resize(size);
//	pred.resize(size);
	m_relpos.resize(size);
	m_termWordsset.resize(size);
//	m_idx2cov.resize(size);
	m_termWordsset[0] = RealG2sWordsRange(WordsRange(0,0));
	m_termWordsset[size-1] = RealG2sWordsRange(WordsRange(size-1,size-1));

	vector< vector<size_t> > children;
	children.resize(size);

	for(size_t i = 1; i < size-1; i++) {
	  m_relpos[i].resize(size-i);

	  m_relpos[i][0].push_back(i);
//	  m_abs2rel[i] = i;
	  RealG2sWordsRange cov(WordsRange(i,i));
//	  m_wordssetColl.push_back(cov);
	  m_termWordsset[i] = cov;
//	  m_idx2cov[i]
      //AddNT(m_termWordsset[i], i+GetSize());

      const Word& word = GetWord(i);
      const Factor* factor = word[2];
      int fid = Scan<int>(factor->GetString().as_string());
      fid++;

      // add node
      m_word[i] = word.GetString(0).as_string();
      m_pos[i] = word.GetString(1).as_string();
      if (fid > 0) {
        m_fid[i] = fid;
        children[fid].push_back(i);
      } else m_fid[i] = NOT_FOUND;

      if (fid > 0)
          add_edge(fid, i);
      if (!sibling && !onlyTree && i > 1)
        add_edge(i,i-1);
	}

	if (sibling) {
      for(size_t i = 0; i < children.size(); i++) {
        for (size_t j = 1; j < children[i].size(); j++) {
          add_edge(children[i][j], children[i][j-1]);
        }
      }
	}

	// reset to pure word
	ResetWords();
	return 1;
}

const std::vector<size_t>& DyMultiDiGraph::GetWordsIndex(size_t pos, size_t relSize) const
{
  CHECK(pos < GetSize()-1 && pos > 0);
  CHECK(relSize >=0 && relSize < GetSize()-pos);

  if (relSize > 0)
    return m_relpos[pos][relSize-1];
  return m_relpos[pos][0];
}

const std::set<size_t>& DyMultiDiGraph::GetChildren(size_t pos) const
{
  EdgesType::const_iterator iter = succ.find(pos);
  if (iter == succ.end()) return DUMMYSET;
  return iter->second;
}

const std::set<size_t>& DyMultiDiGraph::GetParent(size_t pos) const
{
  EdgesType::const_iterator iter = pred.find(pos);
  if (iter == pred.end()) return DUMMYSET;
  return iter->second;
}

size_t DyMultiDiGraph::GetRelPos(size_t pos) const {
  if (pos < GetSize()) return pos;
  return m_abs2rel[pos-GetSize()];
}

std::string DyMultiDiGraph::GetWordOrder(const std::vector<size_t>& visitedVec) const
{
  std::set<size_t> keys;
  for (size_t i = 0; i < visitedVec.size(); i++) {
    size_t ind = GetRelPos(visitedVec[i]);
    //if (ind >= GetSize()) ind -= m_ntIndexR.find(ind)->second;
    CHECK(keys.insert(ind).second);
  }
  std::map<size_t, size_t> visitMap2;
  size_t count = 0;
  for (std::set<size_t>::const_iterator iter = keys.begin();
        iter != keys.end(); iter++, count++) {
    size_t key = *iter;
    visitMap2[key] = count;
  }
  string ret = "";
  for (size_t i = 0; i < visitedVec.size(); i++) {
    size_t value = visitMap2.find(GetRelPos(visitedVec[i]))->second;
    ret += Moses::SPrint<size_t>(value) + ":";
  }
  return ret.erase(ret.size()-1);
}

void DyMultiDiGraph::GetWholeWordsSet(RealG2sWordsRange& ret, const std::vector<size_t>& vec) const
{
  for (size_t i = 0; i < vec.size(); i++) {
    ret.Add(GetWordsSet(vec[i]));
  }
//  if (ret.GetSize() > 0) {
//    CHECK (ret.GetMin() == ret.GetRoot());
//  }
}

size_t DyMultiDiGraph::GetWholeSize(const std::vector<size_t>& vec) const
{
  size_t ret = 0;
  for (size_t i = 0; i < vec.size(); i++) {
    ret += GetWordsSet(vec[i]).GetSize();
  }
  return ret;
}

const RealG2sWordsRange& DyMultiDiGraph::GetWordsSet(size_t pos) const
{
  if (pos < GetSize()) return m_termWordsset[pos];
  CHECK(pos-GetSize() < m_idx2cov.size());
  return *(m_idx2cov[pos-GetSize()]);
}

size_t DyMultiDiGraph::AddNT(const RealG2sWordsRange& cov) {
  if (cov.GetMin() == 0 || cov.GetMax() == GetSize()-1)
    return NOT_FOUND;
  size_t ntID = m_idx2cov.size()+GetSize();
//  if (m_cov2Idx.find(cov) == m_cov2Idx.end()) {
//    ntID = m_cov2Idx.size()+GetSize();
//  } else {
//    ntID = m_cov2Idx.find(cov)->second;
//    return ntID;
//  }

  return AddNT(cov, ntID);
}

size_t DyMultiDiGraph::AddNT(const RealG2sWordsRange& cov, size_t ntID)
{
//  cerr << "add nt " << ntID << " " << cov << endl;
  // add links

  CHECK(cov.GetEndPos()-cov.GetStartPos() < StaticData::Instance().GetSubgraphMaxspan());

  size_t SIZE = GetSize();
  vector<size_t> covSet;
  cov.GetVector(covSet);
  vector< pair<size_t, size_t> > edgeColl;
  for(vector<size_t>::const_iterator iter = covSet.begin();
      iter != covSet.end(); iter++) {
    size_t val = *iter;
    EdgesType::const_iterator sit = succ.find(val);
    if (sit != succ.end()) {
      for (set<size_t>::const_iterator liter = sit->second.begin();
          liter != sit->second.end(); liter++) {
        size_t sid = *liter;
        if ((sid < SIZE && !cov.Covered(sid)) ||
            (sid >= SIZE && !cov.Overlap(GetWordsSet(sid)))) {
          edgeColl.push_back(make_pair(ntID, sid));
        }
      }
    }

    EdgesType::const_iterator pit = pred.find(val);
    if (pit != pred.end()) {
      for (set<size_t>::const_iterator liter = pit->second.begin();
          liter != pit->second.end(); liter++) {
        size_t pid = *liter;
        if ((pid < SIZE && !cov.Covered(pid)) ||
            (pid >= SIZE && !cov.Overlap(GetWordsSet(pid)))) {
          edgeColl.push_back(make_pair(pid, ntID));
        }
      }
    }
  }

  // for multiple roots, this check could be false
  //CHECK(edgeColl.size() > 0 || cov.GetSize() == GetSize()-2);

  for (size_t i = 0; i < edgeColl.size(); i++) {
    add_edge(edgeColl[i].first, edgeColl[i].second);
  }

//  CHECK(cov.GetMin() == cov.GetRoot());

  m_relpos[cov.GetMin()][cov.GetSize()-1].push_back(ntID);
  m_abs2rel.push_back(cov.GetMin());
  m_idx2cov.push_back(&cov);
//  m_cov2Idx[cov] = ntID;

  return ntID;
}

bool DyMultiDiGraph::NTOverlap(size_t pos, const std::vector<size_t>& visited) const
{
  const RealG2sWordsRange& cov = GetWordsSet(pos);
  for (std::vector<size_t>::const_iterator iter = visited.begin();
      iter != visited.end(); iter++) {
    size_t ind = *iter;
    const RealG2sWordsRange& cov2 = GetWordsSet(ind);
    if (cov.Overlap(cov2))
      return true;
  }
  return false;
}

void DyMultiDiGraph::add_edge(size_t u, size_t v) {
	// add to succ
	if (succ.find(u) == succ.end()) {
		succ[u] = set<size_t>();
	}
	succ[u].insert(v);
	// add to pred
	if (pred.find(v) == pred.end()) {
		pred[v] = set<size_t>();
	}
	pred[v].insert(u);
}


void DyMultiDiGraph::print_sent(std::ostream& out) const {
	for(size_t i = 0; i < GetSize(); i++) {
		out << GetWord(i).GetString(0).as_string() << " ";
	}
	out << endl;
}


}
} /* namespace Moses */
