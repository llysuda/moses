/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2012 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once
#ifndef moses_RealG2sRuleLookupManagerCYKPlus_h
#define moses_RealG2sRuleLookupManagerCYKPlus_h

#include "RealG2sRuleLookupManager.h"
#include "RealG2sStackVec.h"
#include "RealG2sWordsRange.h"

namespace Moses
{

class TargetPhraseCollection;
class WordsRange;

namespace RealGraph
{

class RealG2sDottedRule;


/** @todo what is this?
 */
class RealG2sRuleLookupManagerCYKPlus : public RealG2sRuleLookupManager
{
public:
  RealG2sRuleLookupManagerCYKPlus(const RealG2sParser &parser,
                                const RealG2sCellCollectionBase &cellColl)
    : RealG2sRuleLookupManager(parser, cellColl) {}

protected:
  void AddCompletedRule(
    const RealG2sDottedRule &dottedRule,
    const TargetPhraseCollection &tpc,
    const RealG2sWordsRange &range,
    RealG2sParserCallback &outColl);

  RealG2sStackVec m_stackVec;
};

}  // namespace Moses
}

#endif
