#pragma once

#include "moses/ScoreComponentCollection.h"

namespace Moses
{

class TargetPhrase;
class InputPath;
class InputType;

namespace RealGraph
{

class RealG2sTranslationOption
{
protected:
  const TargetPhrase &m_targetPhrase;
  ScoreComponentCollection m_scoreBreakdown;

public:
  RealG2sTranslationOption(const TargetPhrase &targetPhrase);

  const TargetPhrase &GetPhrase() const {
    return m_targetPhrase;
  }

  const ScoreComponentCollection &GetScores() const {
    return m_scoreBreakdown;
  }

  void Evaluate(const InputType &input, const InputPath &inputPath);
};

}
}

