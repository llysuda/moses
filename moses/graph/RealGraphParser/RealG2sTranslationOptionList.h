/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once

#include "RealG2sStackVec.h"

#include <vector>
#include <map>
#include "RealG2sParserCallback.h"
#include "RealG2sTranslationOptions.h"

namespace Moses
{

class TargetPhraseCollection;
class WordsRange;
class InputType;
class InputPath;

namespace Graph
{
class RealG2sWordsRange;
}

namespace RealGraph
{

//! a vector of translations options for a specific range, in a specific sentence
class RealG2sTranslationOptionListBasic
{
public:

  RealG2sTranslationOptionListBasic(size_t ruleLimit);
  ~RealG2sTranslationOptionListBasic();

  const RealG2sTranslationOptions &Get(size_t i) const {
    return *m_collection[i];
  }

  //! number of translation options
  size_t GetSize() const {
    return m_size;
  }

  void Add(const TargetPhraseCollection &, const RealG2sStackVec &,
           const RealG2sWordsRange &);

  void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range);

  bool Empty() const {
    return m_size == 0;
  }

  void Clear();
  void ApplyThreshold();
  void Evaluate(const InputType &input, const InputPath &inputPath);

  float GetScoreThresholdPred() const {
    return m_scoreThreshold;
  }

  float GetBestScore() const {
    return m_collection[0]->GetEstimateOfBestScore();
  }

private:
  typedef std::vector<RealG2sTranslationOptions*> CollType;

  struct ScoreThresholdPred {
    ScoreThresholdPred(float threshold) : m_thresholdScore(threshold) {}
    bool operator()(const RealG2sTranslationOptions *option)  {
      return option->GetEstimateOfBestScore() >= m_thresholdScore;
    }
    float m_thresholdScore;
  };

  CollType m_collection;
  size_t m_size;
  float m_scoreThreshold;
  size_t m_ruleLimit;
};

//! a vector of translations options for a specific range, in a specific sentence
class RealG2sTranslationOptionList : public RealG2sParserCallback
{
public:
  RealG2sTranslationOptionList(size_t ruleLimit, const InputType &input);
  ~RealG2sTranslationOptionList();

//  const RealG2sTranslationOptions &Get(size_t i) const {
//    return *m_collection[i];
//  }

  //! number of translation options
  size_t GetSize() const {
    return m_size;
  }

  void Add(const TargetPhraseCollection &, const RealG2sStackVec &,
           const RealG2sWordsRange &);

  void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range);

  bool Empty() const {
    return m_size == 0;
  }

  void Clear();
  void ApplyThreshold();
  void Evaluate(const InputType &input, const InputPath &inputPath);

private:
  typedef std::map<RealG2sWordsRange, RealG2sTranslationOptionListBasic* > MapCollType;

  MapCollType m_range2coll;
  size_t m_size;
  const size_t m_ruleLimit;
  float m_worstScore;

public:
  typedef MapCollType::const_iterator const_iterator;
  typedef MapCollType::iterator iterator;
    //! iterators
    const_iterator begin() const {
        return m_range2coll.begin();
    }
    const_iterator end() const {
        return m_range2coll.end();
    }

};

}
}
