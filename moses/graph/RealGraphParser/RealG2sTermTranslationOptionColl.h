/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once

#include "RealG2sStackVec.h"

#include <exception>
#include <vector>
#include <map>
#include <list>
#include "RealG2sParserCallback.h"
#include "RealG2sTranslationOptions.h"
#include "moses/SquareMatrix.h"
#include "moses/Util.h"

namespace Moses
{

class TargetPhraseCollection;
class WordsRange;
class InputType;
class InputPath;

namespace Graph
{
class RealG2sWordsRange;
}

namespace RealGraph
{

//! a vector of translations options for a specific range, in a specific sentence
class RealG2sTermTranslationOptionColl : public RealG2sParserCallback
{
public:
  RealG2sTermTranslationOptionColl(const InputType &input);
  ~RealG2sTermTranslationOptionColl();

  float Get(const RealG2sWordsRange* cov) const {
    CollType::const_iterator iter = m_collection.find(*cov);
    CHECK(iter != m_collection.end());
    return iter->second;
  }

  //! number of translation options
  size_t GetSize() const {
    return m_collection.size();
  }

  void DeleteColl() {
//    CollType::iterator iter = m_collection.begin();
//    for (; iter != m_collection.end(); iter++)
//      delete iter->second;
    m_collection.clear();
  }

  void Add(const TargetPhraseCollection &, const RealG2sStackVec &,
           const RealG2sWordsRange &);
  void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range);

  bool Empty() const {
    return GetSize() == 0;
  }

  void ApplyThreshold();

  void Evaluate(const InputType &input, const InputPath &inputPath){
    CHECK(false);
  };

  void CalcFutureScore(SquareMatrix& m_futureScore);
  void UpdateFutureScore(SquareMatrix& m_futureScore, const std::map<const RealG2sWordsRange*, float>&);

  const std::vector<size_t>& GetUnkFlag() const {
    return m_unkFlag;
  }

private:
  typedef std::map<RealG2sWordsRange, float> CollType;

  CollType m_collection;
  InputType const& m_source;
  std::vector<size_t> m_unkFlag;

//  struct ScoreThresholdPred {
//    ScoreThresholdPred(float threshold) : m_thresholdScore(threshold) {}
//    bool operator()(const std::pair<const RealG2sWordsRange*, RealG2sTranslationOptions*>& option)  {
//      return option.second->GetEstimateOfBestScore() >= m_thresholdScore;
//    }
//    float m_thresholdScore;
//  };

};

}
}
