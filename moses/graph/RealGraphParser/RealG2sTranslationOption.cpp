#include "RealG2sTranslationOption.h"
#include "moses/InputType.h"
#include "moses/InputPath.h"

namespace Moses
{
namespace RealGraph
{

RealG2sTranslationOption::RealG2sTranslationOption(const TargetPhrase &targetPhrase)
  :m_targetPhrase(targetPhrase)
  ,m_scoreBreakdown(targetPhrase.GetScoreBreakdown())
{
}

void RealG2sTranslationOption::Evaluate(const InputType &input, const InputPath &inputPath)
{
  const std::vector<FeatureFunction*> &ffs = FeatureFunction::GetFeatureFunctions();

  for (size_t i = 0; i < ffs.size(); ++i) {
    const FeatureFunction &ff = *ffs[i];
    ff.Evaluate(input, inputPath, m_scoreBreakdown);
  }
}
}
}
