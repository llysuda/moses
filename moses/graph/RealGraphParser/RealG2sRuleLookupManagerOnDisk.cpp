/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>

#include "RealG2sParser.h"
#include "moses/TranslationModel/RuleTable/PhraseDictionaryOnDisk.h"
#include "moses/StaticData.h"
#include "RealG2sParserCallback.h"
#include "OnDiskPt/TargetPhraseCollection.h"
#include "RealG2sRuleLookupManagerOnDisk.h"
#include "RealG2sDotChartOnDisk.h"
#include "moses/Util.h"
#include "DyMultiDiGraphInput.h"

using namespace std;
using namespace Moses;
using namespace Moses::Graph;

namespace Moses
{
namespace RealGraph
{

RealG2sRuleLookupManagerOnDisk::RealG2sRuleLookupManagerOnDisk(
  const RealG2sParser &parser,
  const RealG2sCellCollectionBase &cellColl,
  const PhraseDictionaryOnDisk &dictionary,
  OnDiskPt::OnDiskWrapper &dbWrapper,
  const std::vector<FactorType> &inputFactorsVec,
  const std::vector<FactorType> &outputFactorsVec,
  const std::string &filePath)
  : RealG2sRuleLookupManagerCYKPlus(parser, cellColl)
  , m_dictionary(dictionary)
  , m_dbWrapper(dbWrapper)
  , m_inputFactorsVec(inputFactorsVec)
  , m_outputFactorsVec(outputFactorsVec)
  , m_filePath(filePath)
  , maxspan(StaticData::Instance().GetSubgraphMaxspan())
  , m_distLimit(StaticData::Instance().GetMaxDistortion())
{
  CHECK(m_expandableDottedRuleListVec.size() == 0);

  size_t sourceSize = parser.GetSize();
  m_expandableDottedRuleListVec.resize(sourceSize);

  for (size_t ind = 0; ind < m_expandableDottedRuleListVec.size(); ++ind) {
    RealG2sDottedRuleOnDisk *initDottedRule = new RealG2sDottedRuleOnDisk(m_dbWrapper.GetRootSourceNode());

    RealG2sDottedRuleStackOnDisk *processedStack = new RealG2sDottedRuleStackOnDisk(sourceSize - ind + 1);
    processedStack->Add(0, initDottedRule); // init rule. stores the top node in tree

    m_expandableDottedRuleListVec[ind] = processedStack;
  }
}

RealG2sRuleLookupManagerOnDisk::~RealG2sRuleLookupManagerOnDisk()
{
  std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache;
  for (iterCache = m_cache.begin(); iterCache != m_cache.end(); ++iterCache) {
    delete iterCache->second;
  }
  m_cache.clear();

  RemoveAllInColl(m_expandableDottedRuleListVec);
  RemoveAllInColl(m_sourcePhraseNode);
}

void RealG2sRuleLookupManagerOnDisk::GetRealG2sRuleCollectionTerm(
  RealG2sParserCallback &outColl)
{
  const StaticData &staticData = StaticData::Instance();
  const DyMultiDiGraph& source = static_cast<const DyMultiDiGraph&>(GetParser().GetSource());
  size_t source_size = source.GetSize();
  size_t MAX_WIDTH = source_size >= 7 ? 5 : source_size-2;

  Word PAR(false);
  PAR.SetFactor(0, FactorCollection::Instance().AddFactor("_)_"));
  OnDiskPt::Word *PARDB = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, PAR);

  for (size_t width = 1; width <= MAX_WIDTH; width++) {
    for(size_t startPos = 1; startPos+width-1 < source_size -1; startPos++) {

//      cerr << "-------" << width << " " << startPos << endl;

      size_t relEndPos = width - 1;
      size_t absEndPos = startPos + width - 1;

      RealG2sDottedRuleStackOnDisk &expandableDottedRuleList = *m_expandableDottedRuleListVec[startPos];
      // sort save nodes so only do nodes with most counts
      expandableDottedRuleList.SortSavedNodes();
      const RealG2sDottedRuleStackOnDisk::SavedNodeColl &savedNodeColl = expandableDottedRuleList.GetSavedNodeColl();

      for (size_t ind = 0; ind < (savedNodeColl.size()) ; ++ind) {
        const RealG2sSavedNodeOnDisk &savedNode = *savedNodeColl[ind];

        const RealG2sDottedRuleOnDisk &prevDottedRule = savedNode.GetDottedRule();
        const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();

        vector<size_t> visitedVec;
        prevDottedRule.GetVisitedVec(visitedVec);
        RealG2sWordsRange wholeWS;
        source.GetWholeWordsSet(wholeWS, visitedVec);
        size_t wholeSize = wholeWS.GetSize();

        if (wholeSize != relEndPos)
          continue;


        // search for _)_ symbol
        if (prevDottedRule.GetStack().size() > 0 && wholeSize == relEndPos) {
          const OnDiskPt::PhraseNode *node = prevNode.GetChild(*PARDB, m_dbWrapper);
          if (node != NULL) {
//            const RealG2sCellLabel &sourceWordLabel = GetSourceAt(absEndPos);
            RealG2sDottedRuleOnDisk *dottedRule = new RealG2sDottedRuleOnDisk(*node, prevDottedRule.GetRealG2sCellLabel(), prevDottedRule);
            expandableDottedRuleList.Add(wholeSize, dottedRule);
            dottedRule->StackPop();
            dottedRule->SetDummy(true);
            // cache for cleanup
            m_sourcePhraseNode.push_back(node);
          }
        }

        // search for terminal and non-terminal symbol
        size_t minVisitedRelPos = NOT_FOUND;
        if (visitedVec.size() > 0) {
          minVisitedRelPos = source.GetRelPos(visitedVec[0]);
//          CHECK(wholeWS.GetMin() == wholeWS.GetRoot());
          CHECK(minVisitedRelPos == wholeWS.GetMin());
        }

        map<size_t,size_t> visitedMap;
//        cerr << "visitedvec: ";
        for(size_t i = 0; i < visitedVec.size(); i++) {
//          cerr << visitedVec[i] << " ";
          visitedMap[visitedVec[i]] = i;
        }
//        cerr << endl;

        const vector<size_t>& stack = prevDottedRule.GetStack();
        vector<size_t> tryPos;
        if (visitedVec.size() == 0) {
          // new start from root
          tryPos.push_back(startPos);
        } else if (stack.size() == 0) {
          // need to find another root
          for (size_t i = minVisitedRelPos+1; i < source.GetSize()-1 && i < minVisitedRelPos+maxspan; i++) {
            if (visitedMap.find(i) == visitedMap.end()) {
              const set<size_t>& parents = source.GetParent(i);
              size_t j = 0;
              for (; j < visitedVec.size(); j++)
                if (parents.find(visitedVec[j]) != parents.end())
                  break;
              if (j >= visitedVec.size())
                tryPos.push_back(i);
            }
          }
        } else {
          // find children
          size_t active_pos = stack.back();
          const set<size_t>& kids = source.GetChildren(active_pos);
          for (set<size_t>::const_iterator kit = kids.begin();
              kit != kids.end(); kit++) {
            size_t p = *kit;
            if (p > minVisitedRelPos && p < minVisitedRelPos+maxspan && visitedMap.find(p) == visitedMap.end()) {
              const set<size_t>& parents = source.GetParent(p);
              size_t j = 0;
              for (; j < visitedVec.size(); j++)
                if (visitedVec[j] != active_pos && parents.find(visitedVec[j]) != parents.end())
                  break;
              if (j >= visitedVec.size())
                tryPos.push_back(p);
            }
          }
        }

        for(size_t i = 0; i < tryPos.size(); i++) {
          size_t pos = tryPos[i];

//          cerr << "try pos: " << pos << endl;

          OnDiskPt::Word *sourceWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, source.GetWord(pos));

          if (sourceWordBerkeleyDb != NULL) {
            const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceWordBerkeleyDb, m_dbWrapper);
            if (node != NULL) {
              //
              // cache for cleanup
              m_sourcePhraseNode.push_back(node);

              // word ref of pos should be expaned as well
              // find word ref
              set<size_t> refRelPos;
              const set<size_t>& kids = source.GetChildren(pos);
              if (kids.size() > 0) {
                for (map<size_t,size_t>::const_iterator vmit = visitedMap.begin();
                    vmit != visitedMap.end(); vmit++) {
                  size_t p = vmit->first;
                  if (kids.find(p) != kids.end()) {
                    refRelPos.insert(vmit->second);
                  }
                }
              }

              OnDiskPt::Word *refBerkeleyDb = NULL;
              bool foundAllRef = true;
              for (set<size_t>::const_iterator riter = refRelPos.begin();
                  riter != refRelPos.end(); riter++) {
                size_t rpos = *riter;

//                cerr << "ref pos: " << rpos << endl;

                Word tmp(false);
                tmp.SetFactor(0, FactorCollection::Instance().AddFactor("w@"+Moses::SPrint<size_t>(rpos)));

                refBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, tmp);
                if (refBerkeleyDb == NULL) {
                  foundAllRef = false;
                  break;
                }

                const OnDiskPt::PhraseNode *rnode = node->GetChild(*refBerkeleyDb, m_dbWrapper);
                if (rnode == NULL) {
                  foundAllRef = false;
                  break;
                }

                // cache for cleanup
                m_sourcePhraseNode.push_back(rnode);
                node = rnode;
                delete refBerkeleyDb;
                refBerkeleyDb = NULL;
              }

//              cerr << "found all ref? " << foundAllRef << endl;

              // TODO sourceWordLabel
              if (foundAllRef) {
                RealG2sDottedRuleOnDisk *dottedRule = NULL;
                const RealG2sCellLabel &sourceWordLabel = GetSourceAt(pos);
                dottedRule = new RealG2sDottedRuleOnDisk(*node, sourceWordLabel, prevDottedRule);
                expandableDottedRuleList.Add(relEndPos+1, dottedRule);
                dottedRule->SetVisited(pos);
                dottedRule->StackPush(pos);
              }
            }

            delete sourceWordBerkeleyDb;
          }
        }

        //TODO

        // return list of target phrases
        RealG2sDottedRuleCollOnDisk &nodes = expandableDottedRuleList.Get(relEndPos + 1);

        // source LHS
        RealG2sDottedRuleCollOnDisk::const_iterator iterDottedRuleColl;
        for (iterDottedRuleColl = nodes.begin(); iterDottedRuleColl != nodes.end(); ++iterDottedRuleColl) {
          // node of last source word
          const RealG2sDottedRuleOnDisk &prevDottedRule = **iterDottedRuleColl;
          if (prevDottedRule.Done())
            continue;

          prevDottedRule.Done(true);

          const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();
          vector<size_t> visitedVec2;
          prevDottedRule.GetVisitedVec(visitedVec2);

            //const Word &sourceLHS = *iterLabelSet;
            Word sourceLHS(true);
            sourceLHS.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece("X@"+source.GetWordOrder(visitedVec2))));

            OnDiskPt::Word *sourceLHSBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, sourceLHS);
            if (sourceLHSBerkeleyDb == NULL) {
              continue;
            }

            const TargetPhraseCollection *targetPhraseCollection = NULL;
            const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceLHSBerkeleyDb, m_dbWrapper);
            if (node) {
              UINT64 tpCollFilePos = node->GetValue();
              std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache = m_cache.find(tpCollFilePos);
              if (iterCache == m_cache.end()) {

                const OnDiskPt::TargetPhraseCollection *tpcollBerkeleyDb = node->GetTargetPhraseCollection(m_dictionary.GetTableLimit(), m_dbWrapper);

                std::vector<float> weightT = staticData.GetWeights(&m_dictionary);
                targetPhraseCollection
                = tpcollBerkeleyDb->ConvertToMoses(m_inputFactorsVec
                                                   ,m_outputFactorsVec
                                                   ,m_dictionary
                                                   ,weightT
                                                   ,m_dbWrapper.GetVocab()
                                                   ,true);

                delete tpcollBerkeleyDb;
                m_cache[tpCollFilePos] = targetPhraseCollection;
              } else {
                // just get out of cache
                targetPhraseCollection = iterCache->second;
              }

              CHECK(targetPhraseCollection);
              if (!targetPhraseCollection->IsEmpty()) {
                RealG2sWordsRange wss;
                source.GetWholeWordsSet(wss, visitedVec2);
                AddCompletedRule(prevDottedRule, *targetPhraseCollection,
                    wss, outColl);
              }

            } // if (node)

            delete node;
            delete sourceLHSBerkeleyDb;
    //      }
        }
      } // for (size_t ind = 0; ind < savedNodeColl.size(); ++ind)

    }
  }

  delete PARDB;

  // re-init
  RemoveAllInColl(m_expandableDottedRuleListVec);
  RemoveAllInColl(m_sourcePhraseNode);
  m_expandableDottedRuleListVec.resize(source_size);

  for (size_t ind = 0; ind < m_expandableDottedRuleListVec.size(); ++ind) {
    RealG2sDottedRuleOnDisk *initDottedRule = new RealG2sDottedRuleOnDisk(m_dbWrapper.GetRootSourceNode());

    RealG2sDottedRuleStackOnDisk *processedStack = new RealG2sDottedRuleStackOnDisk(source_size - ind + 1);
    processedStack->Add(0, initDottedRule); // init rule. stores the top node in tree

    m_expandableDottedRuleListVec[ind] = processedStack;
  }
}

void RealG2sRuleLookupManagerOnDisk::GetRealG2sRuleCollection(
  const WordsRange &range,
  RealG2sParserCallback &outColl)
{
  const StaticData &staticData = StaticData::Instance();
  size_t relEndPos = range.GetEndPos() - range.GetStartPos();
  size_t absEndPos = range.GetEndPos();

  const DyMultiDiGraph& source = static_cast<const DyMultiDiGraph&>(GetParser().GetSource());

  if (range.GetStartPos() == 0 || range.GetEndPos() == source.GetSize()-1)
    return;

  Word PAR(false);
  PAR.SetFactor(0, FactorCollection::Instance().AddFactor("_)_"));
  OnDiskPt::Word *PARDB = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, PAR);
  size_t NT_OFFSET = source.GetSize();

  // MAIN LOOP. create list of nodes of target phrases
  RealG2sDottedRuleStackOnDisk &expandableDottedRuleList = *m_expandableDottedRuleListVec[range.GetStartPos()];

  // sort save nodes so only do nodes with most counts
  expandableDottedRuleList.SortSavedNodes();

  const RealG2sDottedRuleStackOnDisk::SavedNodeColl &savedNodeColl = expandableDottedRuleList.GetSavedNodeColl();
  //cerr << "savedNodeColl=" << savedNodeColl.size() << " ";

  for (size_t ind = 0; ind < (savedNodeColl.size()) ; ++ind) {
    const RealG2sSavedNodeOnDisk &savedNode = *savedNodeColl[ind];

    const RealG2sDottedRuleOnDisk &prevDottedRule = savedNode.GetDottedRule();
    const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();
    //size_t startPos = prevDottedRule.IsRoot() ? range.GetStartPos() : prevDottedRule.GetWordsRange().GetEndPos() + 1;

    vector<size_t> visitedVec;
    prevDottedRule.GetVisitedVec(visitedVec);
    RealG2sWordsRange wholeWS;
    source.GetWholeWordsSet(wholeWS, visitedVec);
    size_t wholeSize = wholeWS.GetSize();
//    size_t wholeSize = source.GetWholeSize(visitedVec);

    if (wholeSize >= relEndPos+1)
      continue;

    // search for _)_ symbol
    // BUG TODO
    if (prevDottedRule.GetStack().size() > 0 && wholeSize == relEndPos) {
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*PARDB, m_dbWrapper);
        if (node != NULL) {
//          const RealG2sCellLabel &sourceWordLabel = GetSourceAt(absEndPos);
          RealG2sDottedRuleOnDisk *dottedRule = new RealG2sDottedRuleOnDisk(*node, prevDottedRule.GetRealG2sCellLabel(), prevDottedRule);
          expandableDottedRuleList.Add(wholeSize, dottedRule);
          dottedRule->StackPop();
          dottedRule->SetDummy(true);
          // cache for cleanup
          m_sourcePhraseNode.push_back(node);
        }
    }

    // search for terminal and non-terminal symbol
    size_t minVisitedRelPos = NOT_FOUND;
    if (visitedVec.size() > 0) {
      minVisitedRelPos = source.GetRelPos(visitedVec[0]);
//      CHECK(wholeWS.GetMin() == wholeWS.GetRoot());
      CHECK(minVisitedRelPos == wholeWS.GetMin());
    }

    map<size_t,size_t> visitedMap;
    for(size_t i = 0; i < visitedVec.size(); i++)
      visitedMap[visitedVec[i]] = i;
    const vector<size_t>& stack = prevDottedRule.GetStack();
    vector<size_t> tryPos;
    if (visitedVec.size() == 0) {
      // new start from root
      tryPos = source.GetWordsIndex(range.GetStartPos(), relEndPos);
    } else if (stack.size() == 0) {
      // need to find another root
      for (size_t i = minVisitedRelPos+1;
          i < source.GetSize()-1 + wholeSize - relEndPos && i < minVisitedRelPos+maxspan; i++) {
        const vector<size_t>& indexes = source.GetWordsIndex(i, relEndPos+1-wholeSize);
        for (size_t ii = 0; ii < indexes.size(); ii++) {
          size_t x = indexes[ii];
          if (visitedMap.find(x) == visitedMap.end()
              && source.GetWordsSet(x).GetEndPos() < minVisitedRelPos+maxspan
              && !wholeWS.Overlap(source.GetWordsSet(x))) {
            const set<size_t>& parents = source.GetParent(x);
            size_t j = 0;
            for (; j < visitedVec.size(); j++)
              if (parents.find(visitedVec[j]) != parents.end())
                break;
            if (j >= visitedVec.size())
              tryPos.push_back(x);
          }
        }
      }
    } else {
      // find children
      size_t active_pos = stack.back();
      const set<size_t>& kids = source.GetChildren(active_pos);
      for (set<size_t>::const_iterator kit = kids.begin();
          kit != kids.end(); kit++) {
        size_t p = *kit;
        if (visitedMap.find(p) == visitedMap.end()
            && source.GetRelPos(p) > minVisitedRelPos
            && source.GetWordsSet(p).GetEndPos() < minVisitedRelPos+maxspan
            && !wholeWS.Overlap(source.GetWordsSet(p))) {
          const set<size_t>& parents = source.GetParent(p);
          size_t j = 0;
          for (; j < visitedVec.size(); j++)
            if (visitedVec[j] != active_pos && parents.find(visitedVec[j]) != parents.end())
              break;
          if (j >= visitedVec.size())
            tryPos.push_back(p);
        }
      }
    }

    for(size_t i = 0; i < tryPos.size(); i++) {
      size_t pos = tryPos[i];

      bool isTerminal = pos < NT_OFFSET;
      // terminal, only use it for the last length-1 symbol
      if (isTerminal && wholeSize < relEndPos)
        continue;

      // non-terminals,
      // no need to use non-terminals which are too small or large
      if (!isTerminal && (
          (visitedVec.size() == 0 && wholeSize+source.GetWordsSet(pos).GetSize() != relEndPos) ||
          (visitedVec.size() > 0 && wholeSize+source.GetWordsSet(pos).GetSize() != relEndPos+1) )) {
        continue;
      }

      OnDiskPt::Word *sourceWordBerkeleyDb = NULL;
      if (isTerminal)
        sourceWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, source.GetWord(pos));
      else sourceWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, staticData.GetInputDefaultNonTerminal());

      if (sourceWordBerkeleyDb != NULL) {
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceWordBerkeleyDb, m_dbWrapper);
        if (node != NULL) {

          //
          // cache for cleanup
          m_sourcePhraseNode.push_back(node);

          // word ref of pos should be expaned as well
          // find word ref
          set<size_t> refRelPos;
          const set<size_t>& kids = source.GetChildren(pos);
          if (kids.size() > 0) {
            for (map<size_t,size_t>::const_iterator vmit = visitedMap.begin();
                vmit != visitedMap.end(); vmit++) {
              size_t p = vmit->first;
              if (kids.find(p) != kids.end()) {
                refRelPos.insert(vmit->second);
              }
            }
          }

          OnDiskPt::Word *refBerkeleyDb = NULL;
          bool foundAllRef = true;
          for (set<size_t>::const_iterator riter = refRelPos.begin();
              riter != refRelPos.end(); riter++) {
            size_t rpos = *riter;

            Word tmp(false);
            tmp.SetFactor(0, FactorCollection::Instance().AddFactor("w@"+Moses::SPrint<size_t>(rpos)));

            refBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, tmp);
            if (refBerkeleyDb == NULL) {
              foundAllRef = false;
              break;
            }

            const OnDiskPt::PhraseNode *rnode = node->GetChild(*refBerkeleyDb, m_dbWrapper);
            if (rnode == NULL) {
              delete refBerkeleyDb;
              foundAllRef = false;
              break;
            }

            // cache for cleanup
            m_sourcePhraseNode.push_back(rnode);
            node = rnode;
            delete refBerkeleyDb;
            refBerkeleyDb = NULL;
          }

          // TODO sourceWordLabel
          if (foundAllRef) {

            RealG2sDottedRuleOnDisk *dottedRule = NULL;
            if (isTerminal) {
              const RealG2sCellLabel &sourceWordLabel = GetSourceAt(pos);
              dottedRule = new RealG2sDottedRuleOnDisk(*node, sourceWordLabel, prevDottedRule);
            } else {
              Word targetNT(true);
              const RealG2sWordsRange& wordsset = source.GetWordsSet(pos);
//              if (!wordsset.IsContinuous())
                targetNT.SetFactor(0, FactorCollection::Instance().AddFactor("X@"+wordsset.GetCovString()));
//              else targetNT = staticData.GetOutputDefaultNonTerminal();

//              const RealG2sCellLabelSet &chartNonTermSet =
//                    GetTargetLabelSet(wordsset.GetMin(), wordsset.GetMin() + wordsset.GetSize()-1);
//                WordsRange temprange(0, wordsset.GetSize()-1);
                WordsRange temprange(1, wordsset.GetSize());
//                if (wordsset.GetSize() == 1)
//                  temprange = WordsRange(wordsset.GetMin(), wordsset.GetMin());
              const RealG2sCellLabelSet &chartNonTermSet =
                      GetTargetLabelSet(temprange.GetStartPos(), temprange.GetEndPos());
              const RealG2sCellLabel* tcell = chartNonTermSet.Find(targetNT);
//              cerr << pos << " " << wordsset << endl;
              CHECK(tcell != NULL);
              dottedRule = new RealG2sDottedRuleOnDisk(*node, *(tcell), prevDottedRule);
            }

            if (isTerminal || visitedVec.size() > 0) {
              expandableDottedRuleList.Add(relEndPos+1, dottedRule);
            } else {
              expandableDottedRuleList.Add(relEndPos, dottedRule);
            }

            dottedRule->SetVisited(pos);
            dottedRule->StackPush(pos);
          }
        }

        delete sourceWordBerkeleyDb;
      }
    }

    //TODO

    // return list of target phrases
    RealG2sDottedRuleCollOnDisk &nodes = expandableDottedRuleList.Get(relEndPos + 1);

    // source LHS
    RealG2sDottedRuleCollOnDisk::const_iterator iterDottedRuleColl;
    for (iterDottedRuleColl = nodes.begin(); iterDottedRuleColl != nodes.end(); ++iterDottedRuleColl) {
      // node of last source word
      const RealG2sDottedRuleOnDisk &prevDottedRule = **iterDottedRuleColl;
      if (prevDottedRule.Done())
        continue;

      prevDottedRule.Done(true);

      const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();
      vector<size_t> visitedVec2;
      prevDottedRule.GetVisitedVec(visitedVec2);

      //get node for each source LHS
//      const NonTerminalSet &lhsSet = GetParser().GetInputPath(range.GetStartPos(), range.GetEndPos()).GetNonTerminalSet();
//      NonTerminalSet::const_iterator iterLabelSet;
//      for (iterLabelSet = lhsSet.begin(); iterLabelSet != lhsSet.end(); ++iterLabelSet) {

        //const Word &sourceLHS = *iterLabelSet;
        Word sourceLHS(true);
        sourceLHS.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece("X@"+source.GetWordOrder(visitedVec2))));

        OnDiskPt::Word *sourceLHSBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, sourceLHS);
        if (sourceLHSBerkeleyDb == NULL) {
          continue;
        }

        const TargetPhraseCollection *targetPhraseCollection = NULL;
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceLHSBerkeleyDb, m_dbWrapper);
        if (node) {

          UINT64 tpCollFilePos = node->GetValue();
          std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache = m_cache.find(tpCollFilePos);
          if (iterCache == m_cache.end()) {

            const OnDiskPt::TargetPhraseCollection *tpcollBerkeleyDb = node->GetTargetPhraseCollection(m_dictionary.GetTableLimit(), m_dbWrapper);

            std::vector<float> weightT = staticData.GetWeights(&m_dictionary);
            targetPhraseCollection
            = tpcollBerkeleyDb->ConvertToMoses(m_inputFactorsVec
                                               ,m_outputFactorsVec
                                               ,m_dictionary
                                               ,weightT
                                               ,m_dbWrapper.GetVocab()
                                               ,true);

            delete tpcollBerkeleyDb;
            m_cache[tpCollFilePos] = targetPhraseCollection;
          } else {
            // just get out of cache
            targetPhraseCollection = iterCache->second;
          }

          CHECK(targetPhraseCollection);
          if (!targetPhraseCollection->IsEmpty()) {
            RealG2sWordsRange wss;
            source.GetWholeWordsSet(wss, visitedVec2);
            AddCompletedRule(prevDottedRule, *targetPhraseCollection,
                wss, outColl);

//            for(size_t ii = 0; ii < visitedVec2.size(); ii++)
//              cerr << visitedVec2[ii] << " ";
//            cerr << " " << wss << endl;
          }

        } // if (node)

        delete node;
        delete sourceLHSBerkeleyDb;
//      }
    }
  } // for (size_t ind = 0; ind < savedNodeColl.size(); ++ind)

  //cerr << numDerivations << " ";

  delete PARDB;

}

void RealG2sRuleLookupManagerOnDisk::GetRealG2sRuleCollectionLexGlue(
  const WordsRange &range,
  RealG2sParserCallback &outColl)
{
  if (range.GetStartPos() != 0 || range.GetEndPos()==0)
    return;

//  cerr << range << endl;

  const StaticData &staticData = StaticData::Instance();
  size_t relEndPos = range.GetEndPos() - range.GetStartPos();
  size_t absEndPos = range.GetEndPos();

  const DyMultiDiGraph& source = static_cast<const DyMultiDiGraph&>(GetParser().GetSource());

  Word PAR(false);
  PAR.SetFactor(0, FactorCollection::Instance().AddFactor("_)_"));
  OnDiskPt::Word *PARDB = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, PAR);
  size_t source_size = source.GetSize();

  // MAIN LOOP. create list of nodes of target phrases
  RealG2sDottedRuleStackOnDisk &expandableDottedRuleList = *m_expandableDottedRuleListVec[range.GetStartPos()];

  // sort save nodes so only do nodes with most counts
  expandableDottedRuleList.SortSavedNodes();

  const RealG2sDottedRuleStackOnDisk::SavedNodeColl &savedNodeColl = expandableDottedRuleList.GetSavedNodeColl();
  //cerr << "savedNodeColl=" << savedNodeColl.size() << " ";

  for (size_t ind = 0; ind < (savedNodeColl.size()) ; ++ind) {
    const RealG2sSavedNodeOnDisk &savedNode = *savedNodeColl[ind];

    const RealG2sDottedRuleOnDisk &prevDottedRule = savedNode.GetDottedRule();
    const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();
    //size_t startPos = prevDottedRule.IsRoot() ? range.GetStartPos() : prevDottedRule.GetWordsRange().GetEndPos() + 1;

    // find nonterminal
    if (prevDottedRule.IsRoot()) {
      const RealG2sCellLabelSet &chartNonTermSet =
                   GetTargetLabelSet(0, relEndPos-1);
      RealG2sCellLabelSet::const_iterator q = chartNonTermSet.begin();
      for (; q != chartNonTermSet.end(); q++) {
        const RealG2sCellLabel &cellLabel = q->second;

        Word tlabel = cellLabel.GetLabel();
        vector<string> tokens = Moses::Tokenize(tlabel.GetString(0).as_string(), "@");
        CHECK(tokens.size() == 2);
        if (tokens[0] != "S")
          continue;
        tlabel.SetFactor(0, FactorCollection::Instance().AddFactor(tokens[0]));
        OnDiskPt::Word *sourceWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, tlabel);

        if (sourceWordBerkeleyDb != NULL) {
          const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceWordBerkeleyDb, m_dbWrapper);
          if (node != NULL) {
            RealG2sDottedRuleOnDisk *dottedRule = new RealG2sDottedRuleOnDisk(*node, cellLabel, prevDottedRule);
            expandableDottedRuleList.Add(relEndPos, dottedRule);
            m_sourcePhraseNode.push_back(node);
          }
          delete sourceWordBerkeleyDb;
        }
      }
      continue;
    }

    vector<size_t> visitedVec;
    prevDottedRule.GetVisitedVec(visitedVec);
    CHECK(visitedVec[0] == NOT_FOUND);
    visitedVec.erase(visitedVec.begin());

    RealG2sWordsRange wholeWS;
    prevDottedRule.GetWholeWordsSet(wholeWS);
    size_t wholeSize = wholeWS.GetSize();
//    size_t wholeSize = source.GetWholeSize(visitedVec);

//    cerr << "node: " << wholeWS << endl;
//    cerr << "visitedvec: ";
//    for(size_t x = 0; x < visitedVec.size(); x++)
//      cerr << visitedVec[x] << " ";
//    cerr << endl;
//    cerr << "wholesize: " << wholeSize << endl;

    if (wholeSize != relEndPos)
      continue;

//    cerr << "stack size: " << prevDottedRule.GetStack().size() << endl;
    // search for _)_ symbol
    // BUG TODO
    if (prevDottedRule.GetStack().size() > 0 && wholeSize == relEndPos) {
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*PARDB, m_dbWrapper);
        if (node != NULL) {
//          const RealG2sCellLabel &sourceWordLabel = GetSourceAt(absEndPos-1);
          RealG2sDottedRuleOnDisk *dottedRule = new RealG2sDottedRuleOnDisk(*node, prevDottedRule.GetRealG2sCellLabel(), prevDottedRule);
          expandableDottedRuleList.Add(wholeSize, dottedRule);
          dottedRule->StackPop();
          dottedRule->SetDummy(true);
          // cache for cleanup
          m_sourcePhraseNode.push_back(node);
        }
    }

//    // search for terminal and non-terminal symbol
    size_t minVisitedRelPos = NOT_FOUND;
    if (visitedVec.size() > 0) {
      minVisitedRelPos = source.GetRelPos(visitedVec[0]);
//      cerr << minVisitedRelPos << " " << wholeWS.GetMin() << endl;
//      CHECK(minVisitedRelPos == wholeWS.GetMin());
    }

    map<size_t,size_t> visitedMap;
    for(size_t i = 0; i < visitedVec.size(); i++)
      visitedMap[visitedVec[i]] = i;
    const vector<size_t>& stack = prevDottedRule.GetStack();
    vector<size_t> tryPos;
    if (visitedVec.size() == 0) {
      // new start from root
      size_t GAP = wholeWS.GetFirstGapPos();
      size_t endx = GAP + m_distLimit < source.GetSize()-1 ? GAP + m_distLimit : source.GetSize()-2;
      for (size_t i = GAP; i <= endx; i++)
        if (!wholeWS.Covered(i))
          tryPos.push_back(i);
//      tryPos.push_back(wholeWS.GetFirstGapPos());
    } else if (stack.size() == 0) {
//      cerr << "stack empty" << endl;
      // need to find another root
      for (size_t i = minVisitedRelPos+1; i < source.GetSize()-1 && i < minVisitedRelPos+maxspan; i++) {
        if (i + relEndPos - wholeSize >= source.GetSize()-1)
          break;
        size_t x = i;
//        cerr << "check " << x << endl;
        if (visitedMap.find(x) == visitedMap.end() && !wholeWS.Covered(x)) {
          const set<size_t>& parents = source.GetParent(x);
          size_t j = 0;
          for (; j < visitedVec.size(); j++)
            if (parents.find(visitedVec[j]) != parents.end())
              break;
          if (j >= visitedVec.size())
            tryPos.push_back(x);
        }
      }
    } else {
      // find children
      size_t active_pos = stack.back();
      const set<size_t>& kids = source.GetChildren(active_pos);
      for (set<size_t>::const_iterator kit = kids.begin();
          kit != kids.end(); kit++) {
        size_t p = *kit;
        if (p < source_size
            && p > minVisitedRelPos
            && p < minVisitedRelPos + maxspan
            && visitedMap.find(p) == visitedMap.end()
            && !wholeWS.Covered(p)) {
          const set<size_t>& parents = source.GetParent(p);
          size_t j = 0;
          for (; j < visitedVec.size(); j++)
            if (visitedVec[j] != active_pos && parents.find(visitedVec[j]) != parents.end())
              break;
          if (j >= visitedVec.size())
            tryPos.push_back(p);
        }
      }
    }

    for(size_t i = 0; i < tryPos.size(); i++) {
      size_t pos = tryPos[i];
      CHECK(pos < source_size);

//      cerr << "try pos: " << pos << endl;

      OnDiskPt::Word *sourceWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, source.GetWord(pos));
      if (sourceWordBerkeleyDb != NULL) {
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceWordBerkeleyDb, m_dbWrapper);
        if (node != NULL) {

          //
          // cache for cleanup
          m_sourcePhraseNode.push_back(node);

          // word ref of pos should be expaned as well
          // find word ref
          set<size_t> refRelPos;
          const set<size_t>& kids = source.GetChildren(pos);
          if (kids.size() > 0) {
            for (map<size_t,size_t>::const_iterator vmit = visitedMap.begin();
                vmit != visitedMap.end(); vmit++) {
              size_t p = vmit->first;
              if (kids.find(p) != kids.end()) {
                refRelPos.insert(vmit->second);
              }
            }
          }

          OnDiskPt::Word *refBerkeleyDb = NULL;
          bool foundAllRef = true;
          for (set<size_t>::const_iterator riter = refRelPos.begin();
              riter != refRelPos.end(); riter++) {
            size_t rpos = *riter;

//            cerr << "ref pos: " << rpos << endl;

            Word tmp(false);
            tmp.SetFactor(0, FactorCollection::Instance().AddFactor("w@"+Moses::SPrint<size_t>(rpos)));

            refBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, tmp);
            if (refBerkeleyDb == NULL) {
              foundAllRef = false;
              break;
            }

            const OnDiskPt::PhraseNode *rnode = node->GetChild(*refBerkeleyDb, m_dbWrapper);
            if (rnode == NULL) {
              foundAllRef = false;
              break;
            }

            // cache for cleanup
            m_sourcePhraseNode.push_back(rnode);
            node = rnode;
            delete refBerkeleyDb;
            refBerkeleyDb = NULL;
          }

          // TODO sourceWordLabel
          if (foundAllRef) {

//            cerr << "found all ref" << endl;

            const RealG2sCellLabel &sourceWordLabel = GetSourceAt(pos);
            RealG2sDottedRuleOnDisk *dottedRule = new RealG2sDottedRuleOnDisk(*node, sourceWordLabel, prevDottedRule);
            expandableDottedRuleList.Add(relEndPos+1, dottedRule);

            dottedRule->SetVisited(pos);
            dottedRule->StackPush(pos);
          }
        }

        delete sourceWordBerkeleyDb;
      }
    }

    //TODO

    // return list of target phrases
    RealG2sDottedRuleCollOnDisk &nodes = expandableDottedRuleList.Get(relEndPos + 1);

    // source LHS
    RealG2sDottedRuleCollOnDisk::const_iterator iterDottedRuleColl;
    for (iterDottedRuleColl = nodes.begin(); iterDottedRuleColl != nodes.end(); ++iterDottedRuleColl) {
      // node of last source word
      const RealG2sDottedRuleOnDisk &prevDottedRule = **iterDottedRuleColl;
      if (prevDottedRule.Done())
        continue;

      prevDottedRule.Done(true);

      const OnDiskPt::PhraseNode &prevNode = prevDottedRule.GetLastNode();
      vector<size_t> visitedVec2;
      prevDottedRule.GetVisitedVec(visitedVec2);
      visitedVec2.erase(visitedVec2.begin());

//      for (size_t ii=0; ii < visitedVec2.size(); ii++)
//        cerr << visitedVec2[ii] << " ";
//      cerr << endl;

      //get node for each source LHS
//      const NonTerminalSet &lhsSet = GetParser().GetInputPath(range.GetStartPos(), range.GetEndPos()).GetNonTerminalSet();
//      NonTerminalSet::const_iterator iterLabelSet;
//      for (iterLabelSet = lhsSet.begin(); iterLabelSet != lhsSet.end(); ++iterLabelSet) {

        //const Word &sourceLHS = *iterLabelSet;
        Word sourceLHS(true);
        sourceLHS.SetFactor(0,FactorCollection::Instance().AddFactor(StringPiece("X@"+source.GetWordOrder(visitedVec2))));

        OnDiskPt::Word *sourceLHSBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, sourceLHS);
        if (sourceLHSBerkeleyDb == NULL) {
          continue;
        }

        const TargetPhraseCollection *targetPhraseCollection = NULL;
        const OnDiskPt::PhraseNode *node = prevNode.GetChild(*sourceLHSBerkeleyDb, m_dbWrapper);
        if (node) {

          UINT64 tpCollFilePos = node->GetValue();
          std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache = m_cache.find(tpCollFilePos);
          if (iterCache == m_cache.end()) {

            const OnDiskPt::TargetPhraseCollection *tpcollBerkeleyDb = node->GetTargetPhraseCollection(m_dictionary.GetTableLimit(), m_dbWrapper);

            std::vector<float> weightT = staticData.GetWeights(&m_dictionary);
            targetPhraseCollection
            = tpcollBerkeleyDb->ConvertToMoses(m_inputFactorsVec
                                               ,m_outputFactorsVec
                                               ,m_dictionary
                                               ,weightT
                                               ,m_dbWrapper.GetVocab()
                                               ,true);

            delete tpcollBerkeleyDb;
            m_cache[tpCollFilePos] = targetPhraseCollection;
          } else {
            // just get out of cache
            targetPhraseCollection = iterCache->second;
          }

          CHECK(targetPhraseCollection);
          if (!targetPhraseCollection->IsEmpty()) {
            RealG2sWordsRange wss;
            prevDottedRule.GetWholeWordsSet(wss);
            AddCompletedRule(prevDottedRule, *targetPhraseCollection,
                wss, outColl);

//            for(size_t ii = 0; ii < visitedVec2.size(); ii++)
//              cerr << visitedVec2[ii] << " ";
//            cerr << " " << wss << endl;
          }

        } // if (node)

        delete node;
        delete sourceLHSBerkeleyDb;
//      }
    }
  } // for (size_t ind = 0; ind < savedNodeColl.size(); ++ind)

  //cerr << numDerivations << " ";

  delete PARDB;

}

}
} // namespace Moses
