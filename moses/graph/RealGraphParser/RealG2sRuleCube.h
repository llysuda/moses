// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <boost/functional/hash.hpp>
#include <boost/unordered_set.hpp>
#include <boost/version.hpp>

#include "util/check.hh"
#include <queue>
#include <set>
#include <vector>
#include "RealG2sRuleCubeItem.h"

namespace Moses
{
namespace RealGraph
{

class RealG2sCellCollection;
class RealG2sManager;
class RealG2sTranslationOptions;

/** Define an ordering between RealG2sRuleCubeItems based on their scores.
 * This is used to order items in the cube's priority queue.
 */
class RealG2sRuleCubeItemScoreOrderer
{
public:
  bool operator()(const RealG2sRuleCubeItem *p, const RealG2sRuleCubeItem *q) const {
    return p->GetScore() < q->GetScore();
  }
};

/** Define an ordering between RealG2sRuleCubeItems based on their positions in the
 * cube.  This is used to record which positions in the cube have been covered
 * during search.
 */
class RealG2sRuleCubeItemPositionOrderer
{
public:
  bool operator()(const RealG2sRuleCubeItem *p, const RealG2sRuleCubeItem *q) const {
    return *p < *q;
  }
};

/** @todo what is this?
 */
class RealG2sRuleCubeItemHasher
{
public:
  size_t operator()(const RealG2sRuleCubeItem *p) const {
    size_t seed = 0;
    boost::hash_combine(seed, p->GetHypothesisDimensions());
    boost::hash_combine(seed, p->GetTranslationDimension().GetTranslationOption());
    return seed;
  }
};

/** @todo what is this?
 */
class RealG2sRuleCubeItemEqualityPred
{
public:
  bool operator()(const RealG2sRuleCubeItem *p, const RealG2sRuleCubeItem *q) const {
    return p->GetHypothesisDimensions() == q->GetHypothesisDimensions() &&
           p->GetTranslationDimension() == q->GetTranslationDimension();
  }
};

/** @todo what is this?
 */
class RealG2sRuleCube
{
public:
  RealG2sRuleCube(const RealG2sTranslationOptions &, const RealG2sCellCollection &,
           RealG2sManager &);

  ~RealG2sRuleCube();

  float GetTopScore() const {
    CHECK(!m_queue.empty());
    RealG2sRuleCubeItem *item = m_queue.top();
    return item->GetScore();
  }

  RealG2sRuleCubeItem *Pop(RealG2sManager &);

  bool IsEmpty() const {
    return m_queue.empty();
  }

  const RealG2sTranslationOptions &GetTranslationOption() const {
    return m_transOpt;
  }

private:
#if defined(BOOST_VERSION) && (BOOST_VERSION >= 104200)
  typedef boost::unordered_set<RealG2sRuleCubeItem*,
          RealG2sRuleCubeItemHasher,
          RealG2sRuleCubeItemEqualityPred
          > ItemSet;
#else
  typedef std::set<RealG2sRuleCubeItem*, RealG2sRuleCubeItemPositionOrderer> ItemSet;
#endif

  typedef std::priority_queue<RealG2sRuleCubeItem*,
          std::vector<RealG2sRuleCubeItem*>,
          RealG2sRuleCubeItemScoreOrderer
          > Queue;

  RealG2sRuleCube(const RealG2sRuleCube &);  // Not implemented
  RealG2sRuleCube &operator=(const RealG2sRuleCube &);  // Not implemented

  void CreateNeighbors(const RealG2sRuleCubeItem &, RealG2sManager &);
  void CreateNeighbor(const RealG2sRuleCubeItem &, int, RealG2sManager &);

  const RealG2sTranslationOptions &m_transOpt;
  ItemSet m_covered;
  Queue m_queue;
};

}
}
