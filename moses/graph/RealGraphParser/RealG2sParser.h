// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <list>
#include <vector>
#include "moses/WordsRange.h"
#include "moses/StackVec.h"
#include "moses/InputPath.h"

#include "RealG2sWordsRange.h"
#include "RealG2sTermTranslationOptionColl.h"
#include "moses/SquareMatrix.h"

namespace Moses
{
class InputType;
class Sentence;
class Word;
class Phrase;
class TargetPhraseCollection;
class DecodeGraph;

namespace RealGraph
{

class RealG2sParserCallback;
class RealG2sRuleLookupManager;
class RealG2sCellCollectionBase;


class RealG2sParserUnknown
{
public:
  RealG2sParserUnknown();
  ~RealG2sParserUnknown();

  void Process(const Word &sourceWord, const RealG2sWordsRange &range, RealG2sParserCallback &to);
  void Process(int NTCount, const RealG2sWordsRange &range, RealG2sParserCallback &to);

private:
  std::vector<Phrase*> m_unksrcs;
  std::list<TargetPhraseCollection*> m_cacheTargetPhraseCollection;
};

class RealG2sParser
{
public:
  RealG2sParser(const InputType &source, RealG2sCellCollectionBase &cells);
  ~RealG2sParser();

  bool Create(const WordsRange &range, RealG2sParserCallback &to);

  //! the sentence being decoded
  //const Sentence &GetSentence() const;
  long GetTranslationId() const;
  size_t GetSize() const;
  const InputPath &GetInputPath(size_t startPos, size_t endPos) const;
  const InputType &GetSource() const {
  	return m_source;
  }
  void CalculateFutureCost();
  void UpdateFutureCost(const std::map<const RealG2sWordsRange*, float>&);

  const SquareMatrix& GetFutureCostMatrix() const {
    return m_futureScore;
  }

private:
  RealG2sTermTranslationOptionColl termColl;
  RealG2sParserUnknown m_unknown;
  std::vector <DecodeGraph*> m_decodeGraphList;
  std::vector<RealG2sRuleLookupManager*> m_ruleLookupManagers;
  InputType const& m_source; /**< source sentence to be translated */


  typedef std::vector< std::vector<InputPath*> > InputPathMatrix;
  InputPathMatrix	m_inputPathMatrix;
  SquareMatrix m_futureScore;

  void CreateInputPaths(const InputType &input);
  InputPath &GetInputPath(size_t startPos, size_t endPos);

};

}
}
