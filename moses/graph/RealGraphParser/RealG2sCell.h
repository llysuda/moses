// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <iostream>
#include <queue>
#include <map>
#include <vector>
#include "moses/Word.h"
#include "moses/WordsRange.h"
#include "moses/NonTerminal.h"
#include "RealG2sHypothesisCollection.h"
#include "RealG2sRuleCube.h"
#include "RealG2sCellLabelSet.h"

#include <boost/scoped_ptr.hpp>
#include <boost/functional/hash.hpp>
#include <boost/unordered_map.hpp>
#include <boost/version.hpp>
#include "RealG2sHypothesis.h"

namespace Moses
{
namespace RealGraph
{
class RealG2sTranslationOptionList;
class RealG2sCellCollection;
class RealG2sManager;

class RealG2sCellBase
{
public:
  RealG2sCellBase(size_t startPos, size_t endPos);

  virtual ~RealG2sCellBase();

  const RealG2sCellLabelSet &GetTargetLabelSet() const {
    return m_targetLabelSet;
  }

  RealG2sCellLabelSet &MutableTargetLabelSet() {
    return m_targetLabelSet;
  }

  const WordsRange &GetCoverage() const {
    return m_coverage;
  }

protected:
  const WordsRange m_coverage;
  RealG2sCellLabelSet m_targetLabelSet;
};

/** 1 cell in chart decoder.
 *  Doesn't directly hold hypotheses. Each cell contain a map of RealG2sHypothesisCollection that have different constituent labels
 */
class RealG2sCell : public RealG2sCellBase
{
  friend std::ostream& operator<<(std::ostream&, const RealG2sCell&);
public:
#if defined(BOOST_VERSION) && (BOOST_VERSION >= 104200)
  typedef boost::unordered_map<Word,
          RealG2sHypothesisCollection,
          NonTerminalHasher,
          NonTerminalEqualityPred
          > MapType;
#else
  typedef std::map<Word, RealG2sHypothesisCollection> MapType;
#endif

protected:
  MapType m_hypoColl;
  RealG2sHypothesisCollection m_singleColl, m_glueColl;

  bool m_nBestIsEnabled; /**< flag to determine whether to keep track of old arcs */
  RealG2sManager &m_manager;

public:
  RealG2sCell(size_t startPos, size_t endPos, RealG2sManager &manager);
  ~RealG2sCell();

  void ProcessSentence(const RealG2sTranslationOptionList &transOptList
                       ,const RealG2sCellCollection &allRealG2sCells);

  //! Get all hypotheses in the cell that have the specified constituent label
  const RealG2sHypoList *GetSortedHypotheses(const Word &constituentLabel) const {
    MapType::const_iterator p = m_hypoColl.find(constituentLabel);
    return (p == m_hypoColl.end()) ? NULL : &(p->second.GetSortedHypotheses());
  }

  //! for n-best list
  const RealG2sHypoList *GetAllSortedHypotheses() const;

  bool AddHypothesis(RealG2sHypothesis *hypo);

  void SortHypotheses();
  void PruneToSize();
  void CollectRangeScore(std::map<const RealG2sWordsRange*, float>& range2score) const;

  void GroupHypo(RealG2sHypothesisCollection&);
  void GroupHypo();

  const RealG2sHypothesis *GetBestHypothesis() const;

  void CleanupArcList();

  void OutputSizes(std::ostream &out) const;
  size_t GetSize() const;

  //! transitive comparison used for adding objects into set
  inline bool operator<(const RealG2sCell &compare) const {
    return m_coverage < compare.m_coverage;
  }

  void GetSearchGraph(long translationId, std::ostream &outputSearchGraphStream, const std::map<unsigned,bool> &reachable) const;

};

}
}

