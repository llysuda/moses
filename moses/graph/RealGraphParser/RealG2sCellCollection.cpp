// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "RealG2sCellCollection.h"
#include "moses/InputType.h"
#include "moses/WordsRange.h"

namespace Moses
{
namespace RealGraph
{

RealG2sCellCollectionBase::~RealG2sCellCollectionBase()
{
  m_source.clear();
  for (std::vector<std::vector<RealG2sCellBase*> >::iterator i = m_cells.begin(); i != m_cells.end(); ++i)
    RemoveAllInColl(*i);
}

class RealG2sCubeCellFactory
{
public:
  explicit RealG2sCubeCellFactory(RealG2sManager &manager) : m_manager(manager) {}

  RealG2sCell *operator()(size_t start, size_t end) const {
    return new RealG2sCell(start, end, m_manager);
  }

private:
  RealG2sManager &m_manager;
};

/** Costructor
 \param input the input sentence
 \param manager reference back to the manager
 */
RealG2sCellCollection::RealG2sCellCollection(const InputType &input, RealG2sManager &manager)
  :RealG2sCellCollectionBase(input, RealG2sCubeCellFactory(manager)) {}

}
} // namespace

