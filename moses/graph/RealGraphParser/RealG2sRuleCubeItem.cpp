/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2011 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "RealG2sCell.h"
#include "RealG2sCellCollection.h"
#include "RealG2sTranslationOptions.h"
#include "RealG2sRuleCubeItem.h"
#include "RealG2sRuleCubeQueue.h"
#include "moses/WordsRange.h"
#include "moses/Util.h"

#include <boost/functional/hash.hpp>

namespace Moses
{
namespace RealGraph
{

std::size_t hash_value(const RealG2sHypothesisDimension &dimension)
{
  boost::hash<const RealG2sHypothesis*> hasher;
  return hasher(dimension.GetHypothesis());
}

RealG2sRuleCubeItem::RealG2sRuleCubeItem(const RealG2sTranslationOptions &transOpt,
                           const RealG2sCellCollection &/*allRealG2sCells*/)
  : m_translationDimension(0, transOpt.GetTargetPhrases())
  , m_hypothesis(0)
{
  CreateHypothesisDimensions(transOpt.GetStackVec());
}

// create the RuleCube from an existing one, differing only in one dimension
RealG2sRuleCubeItem::RealG2sRuleCubeItem(const RealG2sRuleCubeItem &copy, int hypoDimensionIncr)
  : m_translationDimension(copy.m_translationDimension)
  , m_hypothesisDimensions(copy.m_hypothesisDimensions)
  , m_hypothesis(0)
{
  if (hypoDimensionIncr == -1) {
    m_translationDimension.IncrementPos();
  } else {
    RealG2sHypothesisDimension &dimension = m_hypothesisDimensions[hypoDimensionIncr];
    dimension.IncrementPos();
  }
}

RealG2sRuleCubeItem::~RealG2sRuleCubeItem()
{
  delete m_hypothesis;
}

void RealG2sRuleCubeItem::EstimateScore()
{
  m_score = m_translationDimension.GetTranslationOption()->GetPhrase().GetFutureScore();
  std::vector<RealG2sHypothesisDimension>::const_iterator p;
  for (p = m_hypothesisDimensions.begin();
       p != m_hypothesisDimensions.end(); ++p) {
    m_score += p->GetHypothesis()->GetTotalScore();
  }
}

void RealG2sRuleCubeItem::CreateHypothesis(const RealG2sTranslationOptions &transOpt,
                                    RealG2sManager &manager)
{
  m_hypothesis = new RealG2sHypothesis(transOpt, *this, manager);
  m_hypothesis->Evaluate();
  m_score = m_hypothesis->GetTotalScore();
}

RealG2sHypothesis *RealG2sRuleCubeItem::ReleaseHypothesis()
{
  CHECK(m_hypothesis);
  RealG2sHypothesis *hypo = m_hypothesis;
  m_hypothesis = 0;
  return hypo;
}

// for each non-terminal, create a ordered list of matching hypothesis from the
// chart
void RealG2sRuleCubeItem::CreateHypothesisDimensions(const RealG2sStackVec &stackVec)
{
  for (RealG2sStackVec::const_iterator p = stackVec.begin(); p != stackVec.end();
       ++p) {
    const HypoList *stack = (*p)->GetStack().cube;
    assert(stack);

    // there have to be hypothesis with the desired non-terminal
    // (otherwise the rule would not be considered)
    assert(!stack->empty());

    // create a list of hypotheses that match the non-terminal
    RealG2sHypothesisDimension dimension(0, *stack);
    // add them to the vector for such lists
    m_hypothesisDimensions.push_back(dimension);
  }
}

bool RealG2sRuleCubeItem::operator<(const RealG2sRuleCubeItem &compare) const
{
  if (m_translationDimension == compare.m_translationDimension) {
    return m_hypothesisDimensions < compare.m_hypothesisDimensions;
  }
  return m_translationDimension < compare.m_translationDimension;
}

}
}
