// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include "RealG2sCell.h"
#include "RealG2sCellCollection.h"
#include "RealG2sRuleCubeQueue.h"
#include "RealG2sRuleCube.h"
#include "moses/WordsRange.h"
#include "moses/Util.h"
#include "moses/StaticData.h"
#include "RealG2sManager.h"
#include "RealG2sTranslationOptionList.h"
#include "RealG2sTranslationOptions.h"
#include "RealG2sWordsRange.h"

using namespace std;

namespace Moses
{
namespace RealGraph
{
extern bool g_debug;

RealG2sCellBase::RealG2sCellBase(size_t startPos, size_t endPos) :
  m_coverage(startPos, endPos),
  m_targetLabelSet(m_coverage) {}

RealG2sCellBase::~RealG2sCellBase() {}

/** Constructor
 * \param startPos endPos range of this cell
 * \param manager pointer back to the manager
 */
RealG2sCell::RealG2sCell(size_t startPos, size_t endPos, RealG2sManager &manager) :
  RealG2sCellBase(startPos, endPos), m_manager(manager), m_singleColl(), m_glueColl()
{
  const StaticData &staticData = StaticData::Instance();
  m_nBestIsEnabled = staticData.IsNBestEnabled();
}

RealG2sCell::~RealG2sCell() {}

/** Add the given hypothesis to the cell.
 *  Returns true if added, false if not. Maybe it already exists in the collection or score falls below threshold etc.
 *  This function just calls the correspondind AddHypothesis() in RealG2sHypothesisCollection
 *  \param hypo Hypothesis to be added
 */
bool RealG2sCell::AddHypothesis(RealG2sHypothesis *hypo)
{
//  m_manager.AddSourceNT(hypo->GetCurrSourceWordsSet());
  if (hypo->GetCurrSourceWordsSet().GetMin() == 0)
    return m_glueColl.AddHypothesis(hypo, m_manager);
  if (hypo->GetCurrSourceWordsSet().GetSize() == 1) {
    Word targetLHS(true);
    targetLHS.SetFactor(0, FactorCollection::Instance().AddFactor(
        hypo->GetTargetLHS().GetString(0).as_string()+
        "@"+hypo->GetCurrSourceWordsSet().GetCovString()));
    return m_hypoColl[targetLHS].AddHypothesis(hypo, m_manager);
  }
//      || hypo->GetCurrSourceWordsSet().GetMax() == m_manager.GetSource().GetSize()-1
//      || hypo->GetCurrSourceWordsSet().IsContinuous()) {
//    const Word &targetLHS = hypo->GetTargetLHS();
    return m_singleColl.AddHypothesis(hypo, m_manager);
//  } else {
//    Word targetLHS(true);
//    targetLHS.SetFactor(0, FactorCollection::Instance().AddFactor("X@"+hypo->GetCurrSourceWordsSet().GetCovString()));
//    return m_hypoColl[targetLHS].AddHypothesis(hypo, m_manager);
//  }
}

void RealG2sCell::GroupHypo() {

    GroupHypo(m_singleColl);
    GroupHypo(m_glueColl);
}

void RealG2sCell::GroupHypo(RealG2sHypothesisCollection& coll) {

    RealG2sHypothesisCollection::iterator iter2 = coll.begin();

    while (iter2 != coll.end()) {
      RealG2sHypothesis *hypo = *iter2;
      const RealG2sWordsRange& ws = hypo->GetCurrSourceWordsSet();

      Word targetLHS(true);
      targetLHS.SetFactor(0, FactorCollection::Instance().AddFactor(
          hypo->GetTargetLHS().GetString(0).as_string()+"@"+
          hypo->GetCurrSourceWordsSet().GetCovString()));
      m_hypoColl[targetLHS].AddHypothesis(hypo, m_manager);
      iter2++;
    }

    coll.Clear();
}

/** Prune each collection in this cell to a particular size */
void RealG2sCell::PruneToSize()
{
  MapType::iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    RealG2sHypothesisCollection &coll = iter->second;
    coll.PruneToSize(m_manager);
  }
  m_singleColl.PruneToSize(m_manager);
  m_glueColl.PruneToSize(m_manager);
}

/** Decoding at span level: fill chart cell with hypotheses
 *  (implementation of cube pruning)
 * \param transOptList list of applicable rules to create hypotheses for the cell
 * \param allRealG2sCells entire chart - needed to look up underlying hypotheses
 */
void RealG2sCell::ProcessSentence(const RealG2sTranslationOptionList &transOptList
                                , const RealG2sCellCollection &allRealG2sCells)
{
  const StaticData &staticData = StaticData::Instance();

  typedef std::map<RealG2sWordsRange, RealG2sTranslationOptionListBasic* > MapCollType;

  for (MapCollType::const_iterator iter = transOptList.begin();
      iter != transOptList.end(); ++iter) {
    const RealG2sWordsRange& range = iter->first;
    const RealG2sTranslationOptionListBasic& options = *(iter->second);

    // priority queue for applicable rules with selected hypotheses
    RealG2sRuleCubeQueue queue(m_manager);

    // add all trans opt into queue. using only 1st child node.
//    for (size_t i = 0; i < transOptList.GetSize(); ++i) {
//      const RealG2sTranslationOptions &transOpt = transOptList.Get(i);
    for (size_t i = 0; i < options.GetSize(); ++i) {
       const RealG2sTranslationOptions &transOpt = options.Get(i);;
      RealG2sRuleCube *ruleCube = new RealG2sRuleCube(transOpt, allRealG2sCells, m_manager);
      queue.Add(ruleCube);
    }

    // pluck things out of queue and add to hypo collection
    const size_t popLimit = staticData.GetCubePruningPopLimit();
    for (size_t numPops = 0; numPops < popLimit && !queue.IsEmpty(); ++numPops) {
      RealG2sHypothesis *hypo = queue.Pop();

      IFVERBOSE(3) {
        cerr << *hypo << endl;
      }

      AddHypothesis(hypo);
    }
  }
}

//! call SortHypotheses() in each hypo collection in this cell
void RealG2sCell::SortHypotheses()
{
  CHECK(m_targetLabelSet.Empty());
  MapType::iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    RealG2sHypothesisCollection &coll = iter->second;
    coll.SortHypotheses();
    const RealG2sWordsRange& ws = (*coll.begin())->GetCurrSourceWordsSet();
    m_targetLabelSet.AddConstituent(iter->first, &coll.GetSortedHypotheses(), ws);
    // add X to input
    if (ws.GetMin() > 0)
      m_manager.AddSourceNT(ws);
//    else cerr << ws << endl;
//    cerr << iter->first << endl;
  }
}

void RealG2sCell::CollectRangeScore(std::map<const RealG2sWordsRange*, float>& range2score) const
{
  MapType::const_iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    const RealG2sHypothesisCollection &coll = iter->second;
    const RealG2sWordsRange* ws = &((*coll.begin())->GetCurrSourceWordsSet());
    float score = -numeric_limits<float>::infinity();
    for (RealG2sHypothesisCollection::const_iterator iter2=coll.begin(); iter2 != coll.end(); ++iter2)
      if (score < (*iter2)->GetScore())
        score = (*iter2)->GetScore();
    range2score[ws] = score;
  }
}

/** Return the highest scoring hypothesis out of all the  hypo collection in this cell */
const RealG2sHypothesis *RealG2sCell::GetBestHypothesis() const
{
  const RealG2sHypothesis *ret = NULL;
  float bestScore = -std::numeric_limits<float>::infinity();

  MapType::const_iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    const RealG2sHypoList &sortedList = iter->second.GetSortedHypotheses();
    CHECK(sortedList.size() > 0);

    const RealG2sHypothesis *hypo = sortedList[0];
    if (hypo->GetTotalScore() > bestScore) {
      bestScore = hypo->GetTotalScore();
      ret = hypo;
    };
  }

  return ret;
}

//! call CleanupArcList() in each hypo collection in this cell
void RealG2sCell::CleanupArcList()
{
  // only necessary if n-best calculations are enabled
  if (!m_nBestIsEnabled) return;

  MapType::iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    RealG2sHypothesisCollection &coll = iter->second;
    coll.CleanupArcList();
  }
  m_singleColl.CleanupArcList();
  m_glueColl.CleanupArcList();
}

//! debug info - size of each hypo collection in this cell
void RealG2sCell::OutputSizes(std::ostream &out) const
{
  MapType::const_iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    const Word &targetLHS = iter->first;
    const RealG2sHypothesisCollection &coll = iter->second;

    out << targetLHS << "=" << coll.GetSize() << " ";
  }
}

//! debug info - total number of hypos in all hypo collection in this cell
size_t RealG2sCell::GetSize() const
{
  size_t ret = 0;
  MapType::const_iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    const RealG2sHypothesisCollection &coll = iter->second;

    ret += coll.GetSize();
  }

  return ret;
}

const RealG2sHypoList *RealG2sCell::GetAllSortedHypotheses() const
{
  RealG2sHypoList *ret = new RealG2sHypoList();

  MapType::const_iterator iter;
  for (iter = m_hypoColl.begin(); iter != m_hypoColl.end(); ++iter) {
    const RealG2sHypothesisCollection &coll = iter->second;
    const RealG2sHypoList &list = coll.GetSortedHypotheses();
    std::copy(list.begin(), list.end(), std::inserter(*ret, ret->end()));
  }
  return ret;
}

//! call GetSearchGraph() for each hypo collection
void RealG2sCell::GetSearchGraph(long translationId, std::ostream &outputSearchGraphStream, const std::map<unsigned, bool> &reachable) const
{
  MapType::const_iterator iterOutside;
  for (iterOutside = m_hypoColl.begin(); iterOutside != m_hypoColl.end(); ++iterOutside) {
    const RealG2sHypothesisCollection &coll = iterOutside->second;
    coll.GetSearchGraph(translationId, outputSearchGraphStream, reachable);
  }
}

std::ostream& operator<<(std::ostream &out, const RealG2sCell &cell)
{
  RealG2sCell::MapType::const_iterator iterOutside;
  for (iterOutside = cell.m_hypoColl.begin(); iterOutside != cell.m_hypoColl.end(); ++iterOutside) {
    const Word &targetLHS = iterOutside->first;
    cerr << targetLHS << ":" << endl;

    const RealG2sHypothesisCollection &coll = iterOutside->second;
    cerr << coll;
  }

  /*
  RealG2sCell::HCType::const_iterator iter;
  for (iter = cell.m_hypos.begin(); iter != cell.m_hypos.end(); ++iter)
  {
  	const RealG2sHypothesis &hypo = **iter;
  	out << hypo << endl;
  }
   */

  return out;
}

}
} // namespace
