/*
 * DepNgramGraph.h
 *
 *  Created on: Dec 4, 2013
 *      Author: lly
 */

#ifndef MOSES_REALGRAPH_DYMULTIDOGRAPHINPUT_H_
#define MOSES_REALGRAPH_DYMULTIDOGRAPHINPUT_H_

#include <string>
#include <vector>
#include <set>
#include <map>
#include <list>

#include "moses/Sentence.h"
#include "moses/TypeDef.h"
#include "moses/Util.h"

#include "RealG2sWordsRange.h"

namespace Moses
{

//namespace Graph {
//  class RealG2sWordsRange;
//}

namespace RealGraph
{

class DMGNode {
private:
  size_t m_id;
  const RealG2sWordsRange* m_wordsset;
public:
  DMGNode(size_t id, const RealG2sWordsRange& wordsset) :
    m_id(id), m_wordsset(&wordsset) {}

  size_t GetID() const {
    return m_id;
  }

  const RealG2sWordsRange& GetWordsSet() const {
    return *m_wordsset;
  }

//  //! transitive comparison
  inline bool operator<(const DMGNode& x) const {
    return *(m_wordsset) < *(x.m_wordsset);
  }
//
//  // equality operator
  inline bool operator==(const DMGNode& x) const {
    return *m_wordsset == *(x.m_wordsset);
  }
};


class DyMultiDiGraph : public Moses::Sentence
{
  typedef std::set<size_t> LabelsType;
  typedef std::map<size_t, LabelsType > EdgesType;
  typedef std::vector< std::vector< std::vector<size_t> > > RelPosColl;
  typedef std::vector<size_t> Abs2RelColl;
//  typedef std::map<RealG2sWordsRange, size_t> Cov2IdxColl;
  typedef std::vector<const RealG2sWordsRange*> Idx2CovColl;
private:
	std::vector<std::string> m_word;
	std::vector<std::string> m_pos;
	std::vector<size_t> m_fid;
	EdgesType succ;
	EdgesType pred;
	RelPosColl m_relpos;
	Abs2RelColl m_abs2rel;
	Idx2CovColl m_idx2cov;
//	Cov2IdxColl m_cov2Idx;
	std::vector<RealG2sWordsRange> m_termWordsset;
	std::set<size_t> DUMMYSET;
//	std::list<const RealG2sWordsRange*> m_wordssetColl;

public:
	virtual ~DyMultiDiGraph(){
//	  Idx2CovColl::iterator iter = m_idx2cov.begin();
//	  for(; iter != m_idx2cov.end(); iter++)
//	    delete iter->second;
//	  m_idx2cov.clear();
	}

	InputTypeEnum GetType() const {
		return DyMultiDiGraphInput;
	}

	void Print(std::ostream& out) const {
		print_sent(out);
	}

	int Read(std::istream& in,const std::vector<FactorType>& factorOrder);
	std::map<size_t,size_t> SortWords();

	void add_edge(size_t u, size_t v);

	void print_sent(std::ostream& out) const;

	size_t GetSize() const {
		return Phrase::GetSize();
	}

	std::string get_word(size_t nid) const {
		return m_word[nid];
	}

//	bool has_succ(size_t nid) const {
//		return succ[nid].size() > 0;
//	}
//
//	bool has_pred(size_t nid) const {
//		return pred[nid].size() > 0;
//	}

	void ResetWords() {
		for(size_t i = 0; i < GetSize(); i++) {
			for (size_t j = 1; j <=3; j++)
				Phrase::SetFactor(i,j,NULL);
		}
	}

	const std::vector<size_t>& GetWordsIndex(size_t pos, size_t relSize) const;
	const std::set<size_t>& GetChildren(size_t pos) const;
	const std::set<size_t>& GetParent(size_t pos) const;
	const RealG2sWordsRange& GetWordsSet(size_t pos) const;
	size_t AddNT(const RealG2sWordsRange& cov);
	size_t AddNT(size_t pos);
	size_t AddNT(const RealG2sWordsRange& cov, size_t ntID);
	std::string GetWordOrder(const std::vector<size_t>&) const;
	bool NTOverlap(size_t pos, const std::vector<size_t>&) const;
	void GetWholeWordsSet(RealG2sWordsRange&, const std::vector<size_t>&) const;
	size_t GetRelPos(size_t pos) const;

	size_t GetWholeSize(const std::vector<size_t>&) const;

};

}
} /* namespace Moses */

#endif /* DEPENDENCYTREE_H_ */
