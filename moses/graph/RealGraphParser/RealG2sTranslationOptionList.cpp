/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include <iostream>
#include <vector>
#include "moses/StaticData.h"
#include "RealG2sCellCollection.h"
#include "moses/WordsRange.h"
#include "moses/InputType.h"
#include "moses/InputPath.h"
#include "RealG2sTranslationOptionList.h"
#include "RealG2sTranslationOptions.h"

#include "RealG2sWordsRange.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
namespace RealGraph
{

RealG2sTranslationOptionListBasic::RealG2sTranslationOptionListBasic(size_t ruleLimit)
  : m_size(0)
  , m_ruleLimit(ruleLimit)
{
  m_scoreThreshold = std::numeric_limits<float>::infinity();
}

RealG2sTranslationOptionListBasic::~RealG2sTranslationOptionListBasic()
{
  RemoveAllInColl(m_collection);
}

void RealG2sTranslationOptionListBasic::Clear()
{
  m_size = 0;
  m_scoreThreshold = std::numeric_limits<float>::infinity();
}

class RealG2sTranslationOptionOrderer
{
public:
  bool operator()(const RealG2sTranslationOptions* itemA, const RealG2sTranslationOptions* itemB) const {
    return itemA->GetEstimateOfBestScore() > itemB->GetEstimateOfBestScore();
  }
};

void RealG2sTranslationOptionListBasic::Add(const TargetPhraseCollection &tpc,
                                     const RealG2sStackVec &stackVec,
                                     const RealG2sWordsRange &range)
{
  if (tpc.IsEmpty()) {
    return;
  }

//  cerr << range << endl;

  float score = RealG2sTranslationOptions::CalcEstimateOfBestScore(tpc, stackVec);

  // If the rule limit has already been reached then don't add the option
  // unless it is better than at least one existing option.
  if (m_size > m_ruleLimit && score < m_scoreThreshold) {
    return;
  }

  // Add the option to the list.
  if (m_size == m_collection.size()) {
    // m_collection has reached capacity: create a new object.
    m_collection.push_back(new RealG2sTranslationOptions(tpc, stackVec,
                           range, score));
  } else {
    // Overwrite an unused object.
    *(m_collection[m_size]) = RealG2sTranslationOptions(tpc, stackVec,
                              range, score);
  }
  ++m_size;

  // If the rule limit hasn't been exceeded then update the threshold.
  if (m_size <= m_ruleLimit) {
    m_scoreThreshold = (score < m_scoreThreshold) ? score : m_scoreThreshold;
  }

  // Prune if bursting
  if (m_size == m_ruleLimit * 2) {
    std::nth_element(m_collection.begin(),
                     m_collection.begin() + m_ruleLimit - 1,
                     m_collection.begin() + m_size,
                     RealG2sTranslationOptionOrderer());
    m_scoreThreshold = m_collection[m_ruleLimit-1]->GetEstimateOfBestScore();
    m_size = m_ruleLimit;
  }
}

void RealG2sTranslationOptionListBasic::AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range)
{
  TargetPhraseCollection *tpc = new TargetPhraseCollection();
  tpc->Add(&phrase);
  waste_memory.push_back(tpc);
  RealG2sStackVec empty;
  Add(*tpc, empty, range);
}

void RealG2sTranslationOptionListBasic::ApplyThreshold()
{
  if (m_size > m_ruleLimit) {
    // Something's gone wrong if the list has grown to m_ruleLimit * 2
    // without being pruned.
    assert(m_size < m_ruleLimit * 2);
    // Reduce the list to the best m_ruleLimit options.  The remaining
    // options can be overwritten on subsequent calls to Add().
    std::nth_element(m_collection.begin(),
                     m_collection.begin()+m_ruleLimit,
                     m_collection.begin()+m_size,
                     RealG2sTranslationOptionOrderer());
    m_size = m_ruleLimit;
  }

  // keep only those over best + threshold

  float scoreThreshold = -std::numeric_limits<float>::infinity();

  CollType::const_iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.begin()+m_size; ++iter) {
    const RealG2sTranslationOptions *transOpt = *iter;
    float score = transOpt->GetEstimateOfBestScore();
    scoreThreshold = (score > scoreThreshold) ? score : scoreThreshold;
  }

  scoreThreshold += StaticData::Instance().GetTranslationOptionThreshold();

  CollType::iterator bound = std::partition(m_collection.begin(),
                             m_collection.begin()+m_size,
                             ScoreThresholdPred(scoreThreshold));

  m_size = std::distance(m_collection.begin(), bound);
}

void RealG2sTranslationOptionListBasic::Evaluate(const InputType &input, const InputPath &inputPath)
{
  CollType::iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.end(); ++iter) {
    RealG2sTranslationOptions &transOpts = **iter;
    transOpts.Evaluate(input, inputPath);
  }
}

////////////////////////////////////////////////////////

RealG2sTranslationOptionList::RealG2sTranslationOptionList(size_t ruleLimit, const InputType &input)
  : m_size(0)
  , m_ruleLimit(ruleLimit)
{
  m_worstScore = std::numeric_limits<float>::infinity();
}

RealG2sTranslationOptionList::~RealG2sTranslationOptionList()
{
  Clear();
}

void RealG2sTranslationOptionList::Clear()
{
  m_size = 0;
  MapCollType::iterator iter;
  for (iter = m_range2coll.begin(); iter != m_range2coll.end(); ++iter) {
    RealG2sTranslationOptionListBasic* optlist = iter->second;
    delete optlist;
  }
  m_range2coll.clear();
}


void RealG2sTranslationOptionList::Add(const TargetPhraseCollection &tpc,
                                     const RealG2sStackVec &stackVec,
                                     const RealG2sWordsRange &range)
{
  if (tpc.IsEmpty()) {
    return;
  }

  MapCollType::iterator iter = m_range2coll.find(range);
  if (iter == m_range2coll.end()) {
    pair<MapCollType::iterator,bool> ret = m_range2coll.insert(make_pair(range, new RealG2sTranslationOptionListBasic(m_ruleLimit)));
    m_size++;
    iter = ret.first;
  }
  iter->second->Add(tpc, stackVec, range);
}

void RealG2sTranslationOptionList::AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range)
{
  TargetPhraseCollection *tpc = new TargetPhraseCollection();
  tpc->Add(&phrase);
  waste_memory.push_back(tpc);
  RealG2sStackVec empty;
  Add(*tpc, empty, range);
}

void RealG2sTranslationOptionList::ApplyThreshold()
{
  MapCollType::iterator iter;
  for (iter = m_range2coll.begin(); iter != m_range2coll.end(); ++iter) {
    RealG2sTranslationOptionListBasic * optlist = iter->second;
    optlist->ApplyThreshold();
  }
}

void RealG2sTranslationOptionList::Evaluate(const InputType &input, const InputPath &inputPath)
{
  MapCollType::iterator iter;
   for (iter = m_range2coll.begin(); iter != m_range2coll.end(); ++iter) {
     RealG2sTranslationOptionListBasic * optlist = iter->second;
     optlist->Evaluate(input, inputPath);
  }
}

}
}
