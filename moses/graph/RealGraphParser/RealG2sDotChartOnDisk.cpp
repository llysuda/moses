// $Id$
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/
#include "RealG2sDotChartOnDisk.h"

#include <algorithm>
#include "moses/Util.h"
#include "OnDiskPt/PhraseNode.h"

using namespace std;

namespace Moses
{
namespace RealGraph
{

RealG2sDottedRuleStackOnDisk::RealG2sDottedRuleStackOnDisk(size_t size)
  :m_coll(size)
{
  for (size_t ind = 0; ind < size; ++ind) {
    m_coll[ind] = new RealG2sDottedRuleCollOnDisk();
  }
}

RealG2sDottedRuleStackOnDisk::~RealG2sDottedRuleStackOnDisk()
{
  RemoveAllInColl(m_coll);
  RemoveAllInColl(m_savedNode);
}

class RealG2sSavedNodesOderer
{
public:
  bool operator()(const RealG2sSavedNodeOnDisk* a, const RealG2sSavedNodeOnDisk* b) const {
    bool ret = a->GetDottedRule().GetLastNode().GetCount(0) > b->GetDottedRule().GetLastNode().GetCount(0);
    return ret;
  }
};

void RealG2sDottedRuleStackOnDisk::SortSavedNodes()
{
  sort(m_savedNode.begin(), m_savedNode.end(), RealG2sSavedNodesOderer());
}

};
};
