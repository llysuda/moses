/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_RealG2sRuleLookupManagerMemory_h
#define moses_RealG2sRuleLookupManagerMemory_h

#include <vector>
#include <string>
#include <vector>

#ifdef USE_BOOST_POOL
#include <boost/pool/object_pool.hpp>
#endif

#include "RealG2sRuleLookupManagerCYKPlus.h"
#include "RealG2sDotChartInMemory.h"
#include "moses/NonTerminal.h"
#include "moses/TranslationModel/PhraseDictionaryMemory.h"
#include "moses/TranslationModel/PhraseDictionaryNodeMemory.h"
#include "RealG2sStackVec.h"

namespace Moses
{

class WordsRange;

namespace RealGraph
{
class RealG2sParserCallback;
class RealG2sDottedRuleColl;

//! Implementation of RealG2sRuleLookupManager for in-memory rule tables.
class RealG2sRuleLookupManagerMemory : public RealG2sRuleLookupManagerCYKPlus
{
public:
  RealG2sRuleLookupManagerMemory(const RealG2sParser &parser,
                               const RealG2sCellCollectionBase &cellColl,
                               const PhraseDictionaryMemory &ruleTable);

  ~RealG2sRuleLookupManagerMemory();

  virtual void GetRealG2sRuleCollection(
    const WordsRange &range,
    RealG2sParserCallback &outColl);
  virtual void GetRealG2sRuleCollectionTerm(RealG2sParserCallback &outColl);

private:
  void ExtendPartialRuleApplication(
    const RealG2sDottedRuleInMemory &prevDottedRule,
    size_t startPos,
    size_t endPos,
    size_t stackInd,
    RealG2sDottedRuleColl &dottedRuleColl, size_t);

  std::vector<RealG2sDottedRuleColl*> m_dottedRuleColls;
  const PhraseDictionaryMemory &m_ruleTable;
  int m_distLimit;
#ifdef USE_BOOST_POOL
  // Use an object pool to allocate the dotted rules for this sentence.  We
  // allocate a lot of them and this has been seen to significantly improve
  // performance, especially for multithreaded decoding.
  boost::object_pool<RealG2sDottedRuleInMemory> m_dottedRulePool;
#endif
};

}
}  // namespace Moses

#endif
