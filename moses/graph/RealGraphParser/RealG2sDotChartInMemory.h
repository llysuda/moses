/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2011 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include "RealG2sDotChart.h"
#include "moses/TranslationModel/PhraseDictionaryNodeMemory.h"

#include "util/check.hh"
#include <vector>

namespace Moses
{
namespace RealGraph
{

/** @todo what is this?
 */
class RealG2sDottedRuleInMemory : public RealG2sDottedRule
{
public:
  // used only to init dot stack.
  explicit RealG2sDottedRuleInMemory(const PhraseDictionaryNodeMemory &node)
    : RealG2sDottedRule()
    , m_node(node) {}

  RealG2sDottedRuleInMemory(const PhraseDictionaryNodeMemory &node,
                     const RealG2sCellLabel &cellLabel,
                     const RealG2sDottedRuleInMemory &prev)
    : RealG2sDottedRule(cellLabel, prev)
    , m_node(node) {}

  const PhraseDictionaryNodeMemory &GetLastNode() const {
    return m_node;
  }

private:
  const PhraseDictionaryNodeMemory &m_node;
};

typedef std::vector<const RealG2sDottedRuleInMemory*> RealG2sDottedRuleList;

// Collection of all in-memory RealG2sDottedRules that share a common start point,
// grouped by end point.  Additionally, maintains a list of all
// RealG2sDottedRules that could be expanded further, i.e. for which the
// corresponding PhraseDictionaryNodeMemory is not a leaf.
class RealG2sDottedRuleColl
{
protected:
  typedef std::vector<RealG2sDottedRuleList> CollType;
  CollType m_coll;
  RealG2sDottedRuleList m_expandableRealG2sDottedRuleList;

public:
  typedef CollType::iterator iterator;
  typedef CollType::const_iterator const_iterator;

  const_iterator begin() const {
    return m_coll.begin();
  }
  const_iterator end() const {
    return m_coll.end();
  }
  iterator begin() {
    return m_coll.begin();
  }
  iterator end() {
    return m_coll.end();
  }

  RealG2sDottedRuleColl(size_t size)
    : m_coll(size) {
  }

  ~RealG2sDottedRuleColl();

  const RealG2sDottedRuleList &Get(size_t pos) const {
    return m_coll[pos];
  }
  RealG2sDottedRuleList &Get(size_t pos) {
    return m_coll[pos];
  }

  void Add(size_t pos, const RealG2sDottedRuleInMemory *dottedRule) {
    CHECK(dottedRule);
    m_coll[pos].push_back(dottedRule);
    if (!dottedRule->GetLastNode().IsLeaf()) {
      m_expandableRealG2sDottedRuleList.push_back(dottedRule);
    }
  }

  void Clear(size_t pos) {
#ifdef USE_BOOST_POOL
    m_coll[pos].clear();
#endif
  }

  const RealG2sDottedRuleList &GetExpandableRealG2sDottedRuleList() const {
    return m_expandableRealG2sDottedRuleList;
  }

};

}
}
