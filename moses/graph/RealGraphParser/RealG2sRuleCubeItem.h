/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2011 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include "RealG2sStackVec.h"
#include "RealG2sTranslationOptions.h"
#include <vector>

namespace Moses
{
class TargetPhrase;

namespace RealGraph
{
class RealG2sCellCollection;
class RealG2sHypothesis;
class RealG2sManager;


typedef std::vector<const RealG2sHypothesis*> HypoList;

/** wrapper around list of target phrase translation options
 * @todo How is this used. Split out into separate source file
 */
class RealG2sTranslationDimension
{
public:
  RealG2sTranslationDimension(std::size_t pos,
                       const RealG2sTranslationOptions::CollType &orderedTargetPhrases)
    : m_pos(pos)
    , m_orderedTargetPhrases(orderedTargetPhrases) {
  }

  std::size_t IncrementPos() {
    return m_pos++;
  }

  bool HasMoreTranslations() const {
    return m_pos+1 < m_orderedTargetPhrases.size();
  }

  const boost::shared_ptr<RealG2sTranslationOption> &GetTranslationOption() const {
    return m_orderedTargetPhrases[m_pos];
  }

  bool operator<(const RealG2sTranslationDimension &compare) const {
    return GetTranslationOption()->GetPhrase() < compare.GetTranslationOption()->GetPhrase();
  }

  bool operator==(const RealG2sTranslationDimension &compare) const {
    return GetTranslationOption()->GetPhrase() == compare.GetTranslationOption()->GetPhrase();
  }

private:
  std::size_t m_pos;
  const RealG2sTranslationOptions::CollType &m_orderedTargetPhrases;
};


/** wrapper around list of hypotheses for a particular non-term of a trans opt
 * @todo How is this used. Split out into separate source file
 */
class RealG2sHypothesisDimension
{
public:
  RealG2sHypothesisDimension(std::size_t pos, const HypoList &orderedHypos)
    : m_pos(pos)
    , m_orderedHypos(&orderedHypos) {
  }

  std::size_t IncrementPos() {
    return m_pos++;
  }

  bool HasMoreHypo() const {
    return m_pos+1 < m_orderedHypos->size();
  }

  const RealG2sHypothesis *GetHypothesis() const {
    return (*m_orderedHypos)[m_pos];
  }

  bool operator<(const RealG2sHypothesisDimension &compare) const {
    return GetHypothesis() < compare.GetHypothesis();
  }

  bool operator==(const RealG2sHypothesisDimension &compare) const {
    return GetHypothesis() == compare.GetHypothesis();
  }

private:
  std::size_t m_pos;
  const HypoList *m_orderedHypos;
};

std::size_t hash_value(const RealG2sHypothesisDimension &);

/** @todo How is this used. Split out into separate source file */
class RealG2sRuleCubeItem
{
public:
  RealG2sRuleCubeItem(const RealG2sTranslationOptions &, const RealG2sCellCollection &);
  RealG2sRuleCubeItem(const RealG2sRuleCubeItem &, int);
  ~RealG2sRuleCubeItem();

  const RealG2sTranslationDimension &GetTranslationDimension() const {
    return m_translationDimension;
  }

  const std::vector<RealG2sHypothesisDimension> &GetHypothesisDimensions() const {
    return m_hypothesisDimensions;
  }

  float GetScore() const {
    return m_score;
  }

  void EstimateScore();

  void CreateHypothesis(const RealG2sTranslationOptions &, RealG2sManager &);

  RealG2sHypothesis *ReleaseHypothesis();

  bool operator<(const RealG2sRuleCubeItem &) const;

private:
  RealG2sRuleCubeItem(const RealG2sRuleCubeItem &);  // Not implemented
  RealG2sRuleCubeItem &operator=(const RealG2sRuleCubeItem &);  // Not implemented

  void CreateHypothesisDimensions(const RealG2sStackVec &);

  RealG2sTranslationDimension m_translationDimension;
  std::vector<RealG2sHypothesisDimension> m_hypothesisDimensions;
  RealG2sHypothesis *m_hypothesis;
  float m_score;
};

}
}
