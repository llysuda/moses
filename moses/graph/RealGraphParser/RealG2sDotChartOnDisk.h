// $Id$
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/
#pragma once

#include <vector>

#include "RealG2sDotChart.h"
#include "util/check.hh"


namespace OnDiskPt
{
class PhraseNode;
}

namespace Moses
{
namespace RealGraph
{
/** @todo what is this?
 */
class RealG2sDottedRuleOnDisk : public RealG2sDottedRule
{
public:
  // used only to init dot stack.
  explicit RealG2sDottedRuleOnDisk(const OnDiskPt::PhraseNode &lastNode)
    : RealG2sDottedRule()
    , m_lastNode(lastNode)
    , m_done(false) {}

  RealG2sDottedRuleOnDisk(const OnDiskPt::PhraseNode &lastNode,
                   const RealG2sCellLabel &cellLabel,
                   const RealG2sDottedRuleOnDisk &prev)
    : RealG2sDottedRule(cellLabel, prev)
    , m_lastNode(lastNode)
    , m_done(false) {}

  const OnDiskPt::PhraseNode &GetLastNode() const {
    return m_lastNode;
  }

  bool Done() const {
    return m_done;
  }
  void Done(bool value) const {
    m_done = value;
  }

private:
  const OnDiskPt::PhraseNode &m_lastNode;
  mutable bool m_done;
};

class RealG2sDottedRuleCollOnDisk
{
protected:
  typedef std::vector<const RealG2sDottedRuleOnDisk*> CollType;
  CollType m_coll;

public:
  typedef CollType::iterator iterator;
  typedef CollType::const_iterator const_iterator;

  const_iterator begin() const {
    return m_coll.begin();
  }
  const_iterator end() const {
    return m_coll.end();
  }
  iterator begin() {
    return m_coll.begin();
  }
  iterator end() {
    return m_coll.end();
  }

  const RealG2sDottedRuleOnDisk &Get(size_t ind) const {
    return *m_coll[ind];
  }

  void Add(const RealG2sDottedRuleOnDisk *dottedRule) {
    m_coll.push_back(dottedRule);
  }
  void Delete(size_t ind) {
    //delete m_coll[ind];
    m_coll.erase(m_coll.begin() + ind);
  }

  size_t GetSize() const {
    return m_coll.size();
  }

};

class RealG2sSavedNodeOnDisk
{
  const RealG2sDottedRuleOnDisk *m_dottedRule;

public:
  RealG2sSavedNodeOnDisk(const RealG2sDottedRuleOnDisk *dottedRule)
    :m_dottedRule(dottedRule) {
    CHECK(m_dottedRule);
  }

  ~RealG2sSavedNodeOnDisk() {
    delete m_dottedRule;
  }

  const RealG2sDottedRuleOnDisk &GetDottedRule() const {
    return *m_dottedRule;
  }
};

class RealG2sDottedRuleStackOnDisk
{
  // coll of coll of processed rules
public:
  typedef std::vector<RealG2sSavedNodeOnDisk*> SavedNodeColl;

protected:
  typedef std::vector<RealG2sDottedRuleCollOnDisk*> CollType;
  CollType m_coll;

  SavedNodeColl m_savedNode;

public:
  typedef CollType::iterator iterator;
  typedef CollType::const_iterator const_iterator;

  const_iterator begin() const {
    return m_coll.begin();
  }
  const_iterator end() const {
    return m_coll.end();
  }
  iterator begin() {
    return m_coll.begin();
  }
  iterator end() {
    return m_coll.end();
  }

  RealG2sDottedRuleStackOnDisk(size_t size);
  ~RealG2sDottedRuleStackOnDisk();

  const RealG2sDottedRuleCollOnDisk &Get(size_t pos) const {
    return *m_coll[pos];
  }
  RealG2sDottedRuleCollOnDisk &Get(size_t pos) {
    return *m_coll[pos];
  }

  const RealG2sDottedRuleCollOnDisk &back() const {
    return *m_coll.back();
  }

  void Add(size_t pos, const RealG2sDottedRuleOnDisk *dottedRule) {
    CHECK(dottedRule);

    m_coll[pos]->Add(dottedRule);
    m_savedNode.push_back(new RealG2sSavedNodeOnDisk(dottedRule));
  }

  const SavedNodeColl &GetSavedNodeColl() const {
    return m_savedNode;
  }

  void SortSavedNodes();

};

}
}

