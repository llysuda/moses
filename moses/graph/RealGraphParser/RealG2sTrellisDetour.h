/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2011 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>

namespace Moses
{
namespace RealGraph
{
class RealG2sHypothesis;
class RealG2sTrellisNode;
class RealG2sTrellisPath;

/** @todo Something to do with make deviant paths
 */
class RealG2sTrellisDetour
{
public:
  RealG2sTrellisDetour(boost::shared_ptr<const RealG2sTrellisPath>,
                     const RealG2sTrellisNode &, const RealG2sHypothesis &);

  const RealG2sTrellisPath &GetBasePath() const {
    return *m_basePath;
  }
  const RealG2sTrellisNode &GetSubstitutedNode() const {
    return m_substitutedNode;
  }
  const RealG2sHypothesis &GetReplacementHypo() const {
    return m_replacementHypo;
  }
  float GetTotalScore() const {
    return m_totalScore;
  }

private:
  boost::shared_ptr<const RealG2sTrellisPath> m_basePath;
  const RealG2sTrellisNode &m_substitutedNode;
  const RealG2sHypothesis &m_replacementHypo;
  float m_totalScore;
};

}
}  // namespace Moses
