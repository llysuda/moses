/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include "moses/TargetPhrase.h"
#include "moses/TargetPhraseCollection.h"
#include "moses/WordsRange.h"

#include "util/check.hh"
#include <vector>
#include <boost/shared_ptr.hpp>

#include "RealG2sStackVec.h"
#include "RealG2sTranslationOption.h"
#include "RealG2sWordsRange.h"

namespace Moses
{
class InputPath;
class InputType;

namespace RealGraph
{
class RealG2sTranslationOption;

/** Similar to a DottedRule, but contains a direct reference to a list
 * of translations and provdes an estimate of the best score. For a specific range in the input sentence
 */
class RealG2sTranslationOptions
{
public:
  typedef std::vector<boost::shared_ptr<RealG2sTranslationOption> > CollType;

  /** Constructor
      \param targetPhraseColl @todo dunno
      \param stackVec @todo dunno
      \param wordsRange the range in the source sentence this translation option covers
      \param score @todo dunno
   */
  RealG2sTranslationOptions(const TargetPhraseCollection &targetPhraseColl,
                          const RealG2sStackVec &stackVec,
                          const RealG2sWordsRange &wordsRange,
                          float score);
  ~RealG2sTranslationOptions();

  static float CalcEstimateOfBestScore(const TargetPhraseCollection &,
                                       const RealG2sStackVec &);

  //! @todo dunno
  const RealG2sStackVec &GetStackVec() const {
    return m_stackVec;
  }

  //! @todo isn't the translation suppose to just contain 1 target phrase, not a whole collection of them?
  const CollType &GetTargetPhrases() const {
    return m_collection;
  }

  //! the range in the source sentence this translation option covers
  const RealG2sWordsRange &GetSourceWordsSet() const {
    return m_wordsSet;
  }

  /** return an estimate of the best score possible with this translation option.
    * the estimate is the sum of the top target phrase's estimated score plus the
    * scores of the best child hypotheses.
    */
  inline float GetEstimateOfBestScore() const {
    return m_estimateOfBestScore;
  }

  void Evaluate(const InputType &input, const InputPath &inputPath);

private:

  RealG2sStackVec m_stackVec; //! vector of hypothesis list!
  CollType m_collection;

  RealG2sWordsRange m_wordsSet;
  float m_estimateOfBestScore;
};

}
}
