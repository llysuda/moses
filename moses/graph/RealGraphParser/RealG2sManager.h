// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <vector>
#include <boost/unordered_map.hpp>
#include "RealG2sCell.h"
#include "RealG2sCellCollection.h"
#include "moses/InputType.h"
#include "moses/WordsRange.h"
#include "moses/SentenceStats.h"
#include "RealG2sTranslationOptionList.h"
#include "RealG2sParser.h"
#include "RealG2sWordsRange.h"
#include "moses/SquareMatrix.h"

#include <boost/shared_ptr.hpp>

namespace Moses
{
namespace RealGraph
{
class RealG2sHypothesis;
class RealG2sTrellisDetourQueue;
class RealG2sTrellisNode;
class RealG2sTrellisPath;
class RealG2sTrellisPathList;

/** Holds everything you need to decode 1 sentence with the hierachical/syntax decoder
 */
class RealG2sManager
{
private:
  static void CreateDeviantPaths(boost::shared_ptr<const RealG2sTrellisPath>,
                                 RealG2sTrellisDetourQueue &);

  static void CreateDeviantPaths(boost::shared_ptr<const RealG2sTrellisPath>,
                                 const RealG2sTrellisNode &,
                                 RealG2sTrellisDetourQueue &);

  InputType& m_source; /**< source sentence to be translated */
  RealG2sCellCollection m_hypoStackColl;
  std::auto_ptr<SentenceStats> m_sentenceStats;
  clock_t m_start; /**< starting time, used for logging */
  unsigned m_hypothesisId; /* For handing out hypothesis ids to RealG2sHypothesis */

  RealG2sParser m_parser;

  RealG2sTranslationOptionList m_translationOptionList; /**< pre-computed list of translation options for the phrases in this sentence */

public:
  RealG2sManager(InputType& source);
  ~RealG2sManager();
  void ProcessSentence();
  void AddXmlRealG2sOptions();
  const RealG2sHypothesis *GetBestHypothesis() const;
  void CalcNBest(size_t count, RealG2sTrellisPathList &ret, bool onlyDistinct=0) const;

  void GetSearchGraph(long translationId, std::ostream &outputSearchGraphStream) const;
  void FindReachableHypotheses( const RealG2sHypothesis *hypo, std::map<unsigned,bool> &reachable ) const; /* auxilliary function for GetSearchGraph */

  //! the input sentence being decoded
  const InputType& GetSource() const {
    return m_source;
  }

  const SquareMatrix& GetFutureCostMatrix() {
    return m_parser.GetFutureCostMatrix();
  }

  void AddSourceNT(const RealG2sWordsRange&);

  //! debug data collected when decoding sentence
  SentenceStats& GetSentenceStats() const {
    return *m_sentenceStats;
  }

  /***
   * to be called after processing a sentence (which may consist of more than just calling ProcessSentence() )
   * currently an empty function
   */
  void CalcDecoderStatistics() const {
  }

  void ResetSentenceStats(const InputType& source) {
    m_sentenceStats = std::auto_ptr<SentenceStats>(new SentenceStats(source));
  }

  //! contigious hypo id for each input sentence. For debugging purposes
  unsigned GetNextHypoId() {
    return m_hypothesisId++;
  }

};
}
}

