// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <vector>
#include "moses/Util.h"
#include "moses/WordsRange.h"
#include "moses/ScoreComponentCollection.h"
#include "moses/Phrase.h"
#include "moses/ObjectPool.h"

#include "RealG2sTranslationOptions.h"
#include "RealG2sWordsRange.h"
#include "moses/WordsBitmap.h"

namespace Moses
{
namespace RealGraph
{

class RealG2sHypothesis;
class RealG2sManager;
class RealG2sRuleCubeItem;

typedef std::vector<RealG2sHypothesis*> RealG2sArcList;

/** a hypothesis in the hierarchical/syntax decoder.
 * Contain a pointer to the current target phrase, a vector of previous hypos, and some scores
 */
class RealG2sHypothesis
{
  friend std::ostream& operator<<(std::ostream&, const RealG2sHypothesis&);

protected:
#ifdef USE_HYPO_POOL
  static ObjectPool<RealG2sHypothesis> s_objectPool;
#endif

  boost::shared_ptr<RealG2sTranslationOption> m_transOpt;

  RealG2sWordsRange					m_currSourceWordsSet;
  WordsBitmap               m_sourceCompleted;
  std::vector<const FFState*> m_ffStates; /*! stateful feature function states */
  ScoreComponentCollection m_scoreBreakdown /*! detailed score break-down by components (for instance language model, word penalty, etc) */
  ,m_lmNGram
  ,m_lmPrefix;
  float m_totalScore, m_futureScore;

  RealG2sArcList 					*m_arcList; /*! all arcs that end at the same trellis point as this hypothesis */
  const RealG2sHypothesis 	*m_winningHypo;

  std::vector<const RealG2sHypothesis*> m_prevHypos; // always sorted by source position?

  RealG2sManager& m_manager;

  unsigned m_id; /* pkoehn wants to log the order in which hypotheses were generated */


  //! not implemented
  RealG2sHypothesis();

  //! not implemented
  RealG2sHypothesis(const RealG2sHypothesis &copy);

public:
#ifdef USE_HYPO_POOL
  void *operator new(size_t /* num_bytes */) {
    void *ptr = s_objectPool.getPtr();
    return ptr;
  }

  //! delete \param hypo. Works with object pool too
  static void Delete(RealG2sHypothesis *hypo) {
    s_objectPool.freeObject(hypo);
  }
#else
  //! delete \param hypo. Works with object pool too
  static void Delete(RealG2sHypothesis *hypo) {
    delete hypo;
  }
#endif

  RealG2sHypothesis(const RealG2sTranslationOptions &, const RealG2sRuleCubeItem &item,
                  RealG2sManager &manager);

  ~RealG2sHypothesis();

  unsigned GetId() const {
    return m_id;
  }

  const RealG2sTranslationOption &GetTranslationOption()const {
    return *m_transOpt;
  }

  //! Get the rule that created this hypothesis
  const TargetPhrase &GetCurrTargetPhrase()const {
    return m_transOpt->GetPhrase();
  }

  //! the source range that this hypothesis spans
  const RealG2sWordsRange &GetCurrSourceWordsSet()const {
    return m_currSourceWordsSet;
  }

  //! the arc list when creating n-best lists
  inline const RealG2sArcList* GetArcList() const {
    return m_arcList;
  }

  //! the feature function states for a particular feature \param featureID
  inline const FFState* GetFFState( size_t featureID ) const {
    return m_ffStates[ featureID ];
  }

  //! reference back to the manager
  inline const RealG2sManager& GetManager() const {
    return m_manager;
  }

  void CreateOutputPhrase(Phrase &outPhrase) const;
  Phrase GetOutputPhrase() const;

  int RecombineCompare(const RealG2sHypothesis &compare) const;

  void Evaluate();

  void AddArc(RealG2sHypothesis *loserHypo);
  void CleanupArcList();
  void SetWinningHypo(const RealG2sHypothesis *hypo);

  //! get the unweighted score for each feature function
  const ScoreComponentCollection &GetScoreBreakdown() const {
    return m_scoreBreakdown;
  }

  //! Get the weighted total score
  float GetTotalScore() const {
    return m_totalScore;
  }

  float GetScore() const {
    return m_totalScore - m_futureScore;
  }

  //! vector of previous hypotheses this hypo is built on
  const std::vector<const RealG2sHypothesis*> &GetPrevHypos() const {
    return m_prevHypos;
  }

  //! get a particular previous hypos
  const RealG2sHypothesis* GetPrevHypo(size_t pos) const {
    return m_prevHypos[pos];
  }

  //! get the constituency label that covers this hypo
  const Word &GetTargetLHS() const {
    return GetCurrTargetPhrase().GetTargetLHS();
  }

  //! get the best hypo in the arc list when doing n-best list creation. It's either this hypothesis, or the best hypo is this hypo is in the arc list
  const RealG2sHypothesis* GetWinningHypothesis() const {
    return m_winningHypo;
  }

  const WordsBitmap& GetSourceCompleted() const {
    return m_sourceCompleted;
  }

  TO_STRING();

}; // class RealG2sHypothesis

}
}
