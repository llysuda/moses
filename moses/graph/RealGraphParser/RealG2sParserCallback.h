#pragma once

#include "RealG2sStackVec.h"

#include <list>

namespace Moses
{
class TargetPhraseCollection;
class WordsRange;
class TargetPhrase;
class InputPath;
class InputType;

namespace RealGraph
{

class RealG2sWordsRange;

class RealG2sParserCallback
{
public:
  virtual ~RealG2sParserCallback() {}

  virtual void Add(const TargetPhraseCollection &, const RealG2sStackVec &, const RealG2sWordsRange &) = 0;

  virtual bool Empty() const = 0;

  virtual void AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range) = 0;

  virtual void Evaluate(const InputType &input, const InputPath &inputPath) = 0;
};

}
} // namespace Moses
