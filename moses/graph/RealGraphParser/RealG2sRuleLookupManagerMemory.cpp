/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <iostream>

#include "RealG2sDotChartInMemory.h"

#include "RealG2sParser.h"
#include "moses/InputType.h"
#include "RealG2sParserCallback.h"
#include "moses/StaticData.h"
#include "moses/NonTerminal.h"
#include "RealG2sCellCollection.h"
#include "moses/TranslationModel/PhraseDictionaryMemory.h"
#include "DyMultiDiGraphInput.h"
#include "RealG2sRuleLookupManagerMemory.h"
#include "RealG2sWordsRange.h"
#include "moses/Util.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
namespace RealGraph
{

RealG2sRuleLookupManagerMemory::RealG2sRuleLookupManagerMemory(
  const RealG2sParser &parser,
  const RealG2sCellCollectionBase &cellColl,
  const PhraseDictionaryMemory &ruleTable)
  : RealG2sRuleLookupManagerCYKPlus(parser, cellColl)
  , m_ruleTable(ruleTable)
  , m_distLimit(StaticData::Instance().GetMaxDistortion())
{
  CHECK(m_dottedRuleColls.size() == 0);

  size_t sourceSize = parser.GetSize();
  m_dottedRuleColls.resize(sourceSize);

  const PhraseDictionaryNodeMemory &rootNode = m_ruleTable.GetRootNode();

  for (size_t ind = 0; ind < m_dottedRuleColls.size(); ++ind) {
#ifdef USE_BOOST_POOL
    RealG2sDottedRuleInMemory *initDottedRule = m_dottedRulePool.malloc();
    new (initDottedRule) RealG2sDottedRuleInMemory(rootNode);
#else
    RealG2sDottedRuleInMemory *initDottedRule = new RealG2sDottedRuleInMemory(rootNode);
#endif

    RealG2sDottedRuleColl *dottedRuleColl = new RealG2sDottedRuleColl(sourceSize - ind + 1);
    dottedRuleColl->Add(0, initDottedRule); // init rule. stores the top node in tree

    m_dottedRuleColls[ind] = dottedRuleColl;
  }
}

RealG2sRuleLookupManagerMemory::~RealG2sRuleLookupManagerMemory()
{
  RemoveAllInColl(m_dottedRuleColls);
}

void RealG2sRuleLookupManagerMemory::GetRealG2sRuleCollectionTerm(
  RealG2sParserCallback &outColl)
{
  const StaticData &staticData = StaticData::Instance();
  const DyMultiDiGraph& source = static_cast<const DyMultiDiGraph&>(GetParser().GetSource());
  size_t sourceSize = source.GetSize();
  size_t MAX_WIDTH = 1;//source_size >= 7 ? 5 : source_size-2;

  WordsRange range(0,0);
  size_t relEndPos = range.GetEndPos() - range.GetStartPos();
  size_t absEndPos = range.GetEndPos();

  RealG2sDottedRuleColl &dottedRuleCol = *m_dottedRuleColls[range.GetStartPos()];
   const RealG2sDottedRuleList &expandableDottedRuleList = dottedRuleCol.GetExpandableRealG2sDottedRuleList();

   const RealG2sCellLabel &sourceWordLabel = GetSourceAt(absEndPos);

   // loop through the rules
   // (note that expandableDottedRuleList can be expanded as the loop runs
   //  through calls to ExtendPartialRuleApplication())
   for (size_t ind = 0; ind < expandableDottedRuleList.size(); ++ind) {
     // rule we are about to extend
     const RealG2sDottedRuleInMemory &prevDottedRule = *expandableDottedRuleList[ind];

     // search for terminal symbol
     // (if only one more word position needs to be covered)
//     CHECK (startPos == absEndPos);

       // look up in rule dictionary, if the current rule can be extended
       // with the source word in the last position
       const Word &sourceWord = sourceWordLabel.GetLabel();

             const PhraseDictionaryNodeMemory *node = prevDottedRule.GetLastNode().GetChild(sourceWord);

             // if we found a new rule -> create it and add it to the list
             if (node != NULL) {
                 // create the rule
 #ifdef USE_BOOST_POOL
                 RealG2sDottedRuleInMemory *dottedRule = m_dottedRulePool.malloc();
                 new (dottedRule) RealG2sDottedRuleInMemory(*node, sourceWordLabel,
                                                                                         prevDottedRule);
 #else
                 RealG2sDottedRuleInMemory *dottedRule = new RealG2sDottedRuleInMemory(*node,
                         sourceWordLabel,
                         prevDottedRule);
 #endif
                 //dottedRule->SetLabel2Serg(sourceWord.GetString(staticData.GetInputFactorOrder(),false));
                 dottedRuleCol.Add(relEndPos+1, dottedRule);
     }
   }


   // list of rules that that cover the entire span
   RealG2sDottedRuleList &rules = dottedRuleCol.Get(relEndPos + 1);

   // look up target sides for the rules
   RealG2sDottedRuleList::const_iterator iterRule;
   for (iterRule = rules.begin(); iterRule != rules.end(); ++iterRule) {
     const RealG2sDottedRuleInMemory &dottedRule = **iterRule;
     RealG2sWordsRange ws;
     dottedRule.GetWholeWordsSet(ws);
     const PhraseDictionaryNodeMemory &node = dottedRule.GetLastNode();
         // look up target sides
         const TargetPhraseCollection &tpc = node.GetTargetPhraseCollection();
         // add the fully expanded rule (with lexical target side)
         AddCompletedRule(dottedRule, tpc, ws, outColl);
   }

   dottedRuleCol.Clear(relEndPos+1);
   RemoveAllInColl(m_dottedRuleColls);

   m_dottedRuleColls.resize(sourceSize);

   const PhraseDictionaryNodeMemory &rootNode = m_ruleTable.GetRootNode();

   for (size_t ind = 0; ind < m_dottedRuleColls.size(); ++ind) {
 #ifdef USE_BOOST_POOL
     RealG2sDottedRuleInMemory *initDottedRule = m_dottedRulePool.malloc();
     new (initDottedRule) RealG2sDottedRuleInMemory(rootNode);
 #else
     RealG2sDottedRuleInMemory *initDottedRule = new RealG2sDottedRuleInMemory(rootNode);
 #endif

     RealG2sDottedRuleColl *dottedRuleColl = new RealG2sDottedRuleColl(sourceSize - ind + 1);
     dottedRuleColl->Add(0, initDottedRule); // init rule. stores the top node in tree

     m_dottedRuleColls[ind] = dottedRuleColl;
   }

}

void RealG2sRuleLookupManagerMemory::GetRealG2sRuleCollection(
  const WordsRange &range,
  RealG2sParserCallback &outColl)
{

  if (range.GetStartPos() > 0)
    return;

  size_t relEndPos = range.GetEndPos() - range.GetStartPos();
  size_t absEndPos = range.GetEndPos();

  const StaticData & staticData = StaticData::Instance();

  InputTypeEnum intype = StaticData::Instance().GetInputType();
  //

  // MAIN LOOP. create list of nodes of target phrases

  // get list of all rules that apply to spans at same starting position
  RealG2sDottedRuleColl &dottedRuleCol = *m_dottedRuleColls[range.GetStartPos()];
  const RealG2sDottedRuleList &expandableDottedRuleList = dottedRuleCol.GetExpandableRealG2sDottedRuleList();

//  const RealG2sCellLabel &sourceWordLabel = GetSourceAt(absEndPos);

  // loop through the rules
  // (note that expandableDottedRuleList can be expanded as the loop runs
  //  through calls to ExtendPartialRuleApplication())
  for (size_t ind = 0; ind < expandableDottedRuleList.size(); ++ind) {
    // rule we are about to extend
    const RealG2sDottedRuleInMemory &prevDottedRule = *expandableDottedRuleList[ind];

    RealG2sWordsRange wholeSet;
    prevDottedRule.GetWholeWordsSet(wholeSet);
    size_t GAP = wholeSet.GetFirstGapPos();
    size_t covered = wholeSet.GetSize();

    const RealG2sCellLabel &sourceWordLabel = GetSourceAt(GAP);

    // we will now try to extend it, starting after where it ended
    size_t startPos = prevDottedRule.IsRoot()
                      ? range.GetStartPos()
                          : GAP;
//                      : wholeSet.GetSize();
//                      : prevDottedRule.GetWordsRange().GetEndPos() + 1;


    // search for terminal symbol
    // (if only one more word position needs to be covered)
//    if (startPos == absEndPos) {
      if (wholeSet.GetSize() == relEndPos) {

      // look up in rule dictionary, if the current rule can be extended
      // with the source word in the last position
      const Word &sourceWord = sourceWordLabel.GetLabel();

			const PhraseDictionaryNodeMemory *node = prevDottedRule.GetLastNode().GetChild(sourceWord);

			// if we found a new rule -> create it and add it to the list
			if (node != NULL) {
				// create the rule
#ifdef USE_BOOST_POOL
				RealG2sDottedRuleInMemory *dottedRule = m_dottedRulePool.malloc();
				new (dottedRule) RealG2sDottedRuleInMemory(*node, sourceWordLabel,
																						prevDottedRule);
#else
				RealG2sDottedRuleInMemory *dottedRule = new RealG2sDottedRuleInMemory(*node,
						sourceWordLabel,
						prevDottedRule);
#endif
				//dottedRule->SetLabel2Serg(sourceWord.GetString(staticData.GetInputFactorOrder(),false));
				dottedRuleCol.Add(relEndPos+1, dottedRule);
      }
    }

      if (relEndPos == 0)
        continue;

    // search for non-terminals
    size_t endPos, stackInd;

    // span is already complete covered? nothing can be done
//    if (startPos > absEndPos)
    if (wholeSet.GetSize() > relEndPos)
      continue;

    else if (startPos == range.GetStartPos() && range.GetEndPos() > range.GetStartPos()) {
      // We're at the root of the prefix tree so won't try to cover the full
      // span (i.e. we don't allow non-lexical unary rules).  However, we need
      // to match non-unary rules that begin with a non-terminal child, so we
      // do that in two steps: during this iteration we search for non-terminals
      // that cover all but the last source word in the span (there won't
      // already be running nodes for these because that would have required a
      // non-lexical unary rule match for an earlier span).  Any matches will
      // result in running nodes being appended to the list and on subsequent
      // iterations (for this same span), we'll extend them to cover the final
      // word.
      endPos = absEndPos - 1;
      stackInd = relEndPos;
    } else {
//      endPos = absEndPos;
      endPos = relEndPos+1-wholeSet.GetSize()+startPos-1;
      stackInd = relEndPos + 1;
    }


    ExtendPartialRuleApplication(prevDottedRule, startPos, endPos, stackInd,
                                 dottedRuleCol, GAP);
  }


  // list of rules that that cover the entire span
  RealG2sDottedRuleList &rules = dottedRuleCol.Get(relEndPos + 1);

  // look up target sides for the rules
  RealG2sDottedRuleList::const_iterator iterRule;
  for (iterRule = rules.begin(); iterRule != rules.end(); ++iterRule) {
    const RealG2sDottedRuleInMemory &dottedRule = **iterRule;
    RealG2sWordsRange ws;
    dottedRule.GetWholeWordsSet(ws);
    const PhraseDictionaryNodeMemory &node = dottedRule.GetLastNode();
		// look up target sides
		const TargetPhraseCollection &tpc = node.GetTargetPhraseCollection();
		// add the fully expanded rule (with lexical target side)
		AddCompletedRule(dottedRule, tpc, ws, outColl);
  }

  dottedRuleCol.Clear(relEndPos+1);
}

// Given a partial rule application ending at startPos-1 and given the sets of
// source and target non-terminals covering the span [startPos, endPos],
// determines the full or partial rule applications that can be produced through
// extending the current rule application by a single non-terminal.
void RealG2sRuleLookupManagerMemory::ExtendPartialRuleApplication(
  const RealG2sDottedRuleInMemory &prevDottedRule,
  size_t startPos,
  size_t endPos,
  size_t stackInd,
  RealG2sDottedRuleColl & dottedRuleColl, size_t GAP)
{
	//const StaticData & staticData = StaticData::Instance();
  // source non-terminal labels for the remainder
//  const InputPath &inputPath = GetParser().GetInputPath(startPos, endPos);
  const InputPath &inputPath = GetParser().GetInputPath(startPos, endPos);
  const NonTerminalSet &sourceNonTerms = inputPath.GetNonTerminalSet();

  // target non-terminal labels for the remainder
//  const RealG2sCellLabelSet &targetNonTerms = GetTargetLabelSet(startPos, endPos);
  WordsRange temprange(1, endPos-startPos+1);
  if (startPos == 0)
    temprange = WordsRange(startPos, endPos);
//  if (endPos == startPos)
//    temprange = WordsRange(startPos, endPos);
  const RealG2sCellLabelSet &targetNonTerms = GetTargetLabelSet(temprange.GetStartPos(), temprange.GetEndPos());
//  const RealG2sCellLabelSet &targetNonTerms = GetTargetLabelSet(startPos > 0 ? 1 : 0, startPos > 0 ? endPos-startPos+1:endPos-startPos);

  // note where it was found in the prefix tree of the rule dictionary
  const PhraseDictionaryNodeMemory &node = prevDottedRule.GetLastNode();

  const PhraseDictionaryNodeMemory::NonTerminalMap & nonTermMap =
    node.GetNonTerminalMap();

  const size_t numChildren = nonTermMap.size();
  CHECK (numChildren > 0);

  const size_t numSourceNonTerms = sourceNonTerms.size();
  const size_t numTargetNonTerms = targetNonTerms.GetSize();
//  CHECK (numTargetNonTerms > 0);
  const size_t numCombinations = numSourceNonTerms * numTargetNonTerms;

  // We can search by either:
  //   1. Enumerating all possible source-target NT pairs that are valid for
  //      the span and then searching for matching children in the node,
  // or
  //   2. Iterating over all the NT children in the node, searching
  //      for each source and target NT in the span's sets.
  // We'll do whichever minimises the number of lookups:
//  if (numCombinations <= numChildren*2) {

    // loop over possible target non-terminal labels (as found in chart)
//    RealG2sCellLabelSet::const_iterator q = targetNonTerms.begin();
//    RealG2sCellLabelSet::const_iterator tEnd = targetNonTerms.end();
//    const RealG2sCellLabel *bestCellLabel = NULL;
//    string bestStr = "";
//    for (; q != tEnd; ++q) {
//      const RealG2sCellLabel &cellLabel = q->second;
//      Word tlabel = cellLabel.GetLabel();
//      vector<string> tokens = Moses::Tokenize(tlabel.GetString(0).as_string(), "@");
//      CHECK(tokens.size() == 2);
//      if ((bestCellLabel == NULL ||
//          cellLabel.GetCoverage().GetMin() < bestCellLabel->GetCoverage().GetMin())
//          && !prevDottedRule.Overlap(cellLabel.GetCoverage())) {
//        bestCellLabel = &(q->second);
//        bestStr = tokens[0];
//      }
//    }
//
//    if (bestCellLabel == NULL)
//      return;

    // loop over possible source non-terminal labels (as found in input tree)
    NonTerminalSet::const_iterator p = sourceNonTerms.begin();
    NonTerminalSet::const_iterator sEnd = sourceNonTerms.end();
    for (; p != sEnd; ++p) {
      const Word & sourceNonTerm = *p;

      // loop over possible target non-terminal labels (as found in chart)
      RealG2sCellLabelSet::const_iterator q = targetNonTerms.begin();
      RealG2sCellLabelSet::const_iterator tEnd = targetNonTerms.end();
      for (; q != tEnd; ++q) {
        const RealG2sCellLabel &cellLabel = q->second;
//      const RealG2sCellLabel &cellLabel = *bestCellLabel;

        Word tlabel = cellLabel.GetLabel();
        vector<string> tokens = Moses::Tokenize(tlabel.GetString(0).as_string(), "@");
        CHECK(tokens.size() == 2);
        size_t cellStartPos = cellLabel.GetCoverage().GetStartPos();
        int dist = abs((int)cellStartPos -(int)GAP);
//        if (cellLabel.GetCoverage().GetStartPos() != GAP || prevDottedRule.Overlap(cellLabel.GetCoverage()))
        if (((GAP == 0 || cellStartPos == 0) && cellStartPos != GAP) ||
            dist > m_distLimit ||
            prevDottedRule.Overlap(cellLabel.GetCoverage()))
          continue;
        tlabel.SetFactor(0, FactorCollection::Instance().AddFactor(tokens[0]));
//        tlabel.SetFactor(0, FactorCollection::Instance().AddFactor(bestStr));

        // try to match both source and target non-terminal
        const PhraseDictionaryNodeMemory * child =
          node.GetChild(sourceNonTerm, tlabel);

        // nothing found? then we are done
        if (child == NULL) {
          continue;
        }

        // create new rule
#ifdef USE_BOOST_POOL
        RealG2sDottedRuleInMemory *rule = m_dottedRulePool.malloc();
        new (rule) RealG2sDottedRuleInMemory(*child, cellLabel, prevDottedRule);
#else
        RealG2sDottedRuleInMemory *rule = new RealG2sDottedRuleInMemory(*child, cellLabel,
            prevDottedRule);
#endif

        //rule->SetLabel2Serg(sourceNonTerm.GetString(0).as_string());
        dottedRuleColl.Add(stackInd, rule);
      }
    }
//  } else {
//    // loop over possible expansions of the rule
//    PhraseDictionaryNodeMemory::NonTerminalMap::const_iterator p;
//    PhraseDictionaryNodeMemory::NonTerminalMap::const_iterator end =
//      nonTermMap.end();
//    for (p = nonTermMap.begin(); p != end; ++p) {
//      // does it match possible source and target non-terminals?
//      const PhraseDictionaryNodeMemory::NonTerminalMapKey &key = p->first;
//      const Word &sourceNonTerm = key.first;
//      if (sourceNonTerms.find(sourceNonTerm) == sourceNonTerms.end()) {
//        continue;
//      }
////      const Word &targetNonTerm = key.second;
//      Word targetNonTerm = key.second;
//      targetNonTerm.SetFactor(0, FactorCollection::Instance().AddFactor(targetNonTerm.GetString(0).as_string()+"@"+covStr));
//      const RealG2sCellLabel *cellLabel = targetNonTerms.Find(targetNonTerm);
//      if (!cellLabel) {
//        continue;
//      }
//
//      // create new rule
//      const PhraseDictionaryNodeMemory &child = p->second;
//#ifdef USE_BOOST_POOL
//      RealG2sDottedRuleInMemory *rule = m_dottedRulePool.malloc();
//      new (rule) RealG2sDottedRuleInMemory(child, *cellLabel, prevDottedRule);
//#else
//      RealG2sDottedRuleInMemory *rule = new RealG2sDottedRuleInMemory(child, *cellLabel,
//          prevDottedRule);
//#endif
//      //rule->SetLabel2Serg(sourceNonTerm.GetString(0).as_string());
//      dottedRuleColl.Add(stackInd, rule);
//    }
//  }
}

}
}  // namespace Moses
