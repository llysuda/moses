// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <queue>
#include <vector>
#include "RealG2sRuleCube.h"

namespace Moses
{
namespace RealGraph
{

class RealG2sManager;

/** Define an ordering between RealG2sRuleCube based on their best item scores.  This
 * is used to order items in the priority queue.
 */
class RealG2sRuleCubeOrderer
{
public:
  bool operator()(const RealG2sRuleCube *p, const RealG2sRuleCube *q) const {
    return p->GetTopScore() < q->GetTopScore();
  }
};

/** @todo how is this used */
class RealG2sRuleCubeQueue
{
public:
  RealG2sRuleCubeQueue(RealG2sManager &manager) : m_manager(manager) {}
  ~RealG2sRuleCubeQueue();

  void Add(RealG2sRuleCube *);
  RealG2sHypothesis *Pop();
  bool IsEmpty() const {
    return m_queue.empty();
  }

private:
  typedef std::priority_queue<RealG2sRuleCube*, std::vector<RealG2sRuleCube*>,
          RealG2sRuleCubeOrderer > Queue;

  Queue m_queue;
  RealG2sManager &m_manager;
};

}
}
