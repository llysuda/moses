// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once

#include <vector>
#include "moses/Phrase.h"

namespace Moses
{
class ScoreComponentCollection;

namespace RealGraph
{
class RealG2sHypothesis;
class RealG2sTrellisDetour;

/**  1 node in the output hypergraph. Used in RealG2sTrellisPath
 */
class RealG2sTrellisNode
{
public:
  typedef std::vector<RealG2sTrellisNode*> NodeChildren;

  RealG2sTrellisNode(const RealG2sHypothesis &hypo);
  RealG2sTrellisNode(const RealG2sTrellisDetour &, RealG2sTrellisNode *&);

  ~RealG2sTrellisNode();

  const RealG2sHypothesis &GetHypothesis() const {
    return m_hypo;
  }

  const NodeChildren &GetChildren() const {
    return m_children;
  }

  const RealG2sTrellisNode &GetChild(size_t i) const {
    return *m_children[i];
  }

  Phrase GetOutputPhrase() const;

private:
  RealG2sTrellisNode(const RealG2sTrellisNode &);  // Not implemented
  RealG2sTrellisNode& operator=(const RealG2sTrellisNode &);  // Not implemented

  RealG2sTrellisNode(const RealG2sTrellisNode &, const RealG2sTrellisNode &,
                   const RealG2sHypothesis &, RealG2sTrellisNode *&);

  void CreateChildren();
  void CreateChildren(const RealG2sTrellisNode &, const RealG2sTrellisNode &,
                      const RealG2sHypothesis &, RealG2sTrellisNode *&);

  const RealG2sHypothesis &m_hypo;
  NodeChildren m_children;
};

}
}
