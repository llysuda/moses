// $Id$

/***********************************************************************
Moses - factored phrase-based language decoder
Copyright (C) 2006 University of Edinburgh

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#ifndef moses_RealG2sWordsRange_h
#define moses_RealG2sWordsRange_h

#include <limits>
#include <vector>
#include <iostream>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <set>
#include "moses/TypeDef.h"
#include "moses/WordsRange.h"
#include "moses/WordsBitmap.h"

namespace Moses
{
/** vector of boolean used to represent whether a word has been translated or not
*/

namespace RealGraph
{

typedef unsigned long RealG2sWordsRangeID;

class RealG2sWordsRange
{
  friend std::ostream& operator<<(std::ostream& out, const RealG2sWordsRange& wordsBitmap);
protected:
  size_t m_size; /**< number of words in source sentence */
  size_t m_cov; /**< number of words covered*/
  std::vector<WordsRange> m_bitmap;

public:

  RealG2sWordsRange()
    :m_size(0), m_cov(0) {
  }

  RealG2sWordsRange(size_t size)
    :m_size(size), m_cov(0) {
  }

  //! deep copy
  RealG2sWordsRange(const RealG2sWordsRange &copy)
    :m_size	(copy.m_size), m_cov (copy.m_cov) {
//    m_bitmap.resize(copy.m_bitmap.size());
    for (size_t pos = 0 ; pos < copy.m_bitmap.size(); pos++) {
      m_bitmap.push_back(copy.m_bitmap[pos]);
    }
  }

  RealG2sWordsRange(const WordsRange &range)
    :m_size (0), m_cov (range.GetNumWordsCovered()) {
    m_bitmap.push_back(range);
  }

  ~RealG2sWordsRange() {
  }
  //! count of words translated
//  size_t GetNumWordsCovered() const {
//    size_t count = 0;
//    for (size_t pos = 0 ; pos < m_size ; pos++) {
//      if (m_bitmap[pos])
//        count++;
//    }
//    return count;
//  }

  //! position of 1st word not yet translated, or NOT_FOUND if everything already translated
  size_t GetFirstGapPos() const {
    if (m_cov == 0) return 0;
    if (m_bitmap.front().GetStartPos() > 0) return 0;
    size_t pos = m_bitmap.front().GetEndPos()+1;
//    if (pos >= m_size) return NOT_FOUND;
    return pos;
  }


  //! position of last word not yet translated, or NOT_FOUND if everything already translated
//  size_t GetLastGapPos() const {
//    if (m_size == 0) return 0;
//    return m_bitmap.back().GetStartPos()+1;
//  }


  //! position of last translated word
//  size_t GetLastPos() const {
//    for (int pos = (int) m_size - 1 ; pos >= 0 ; pos--) {
//      if (m_bitmap[pos]) {
//        return pos;
//      }
//    }
//    // no starting pos
//    return NOT_FOUND;
//  }

  //! whether a word has been translated at a particular position
//  bool GetValue(size_t pos) const {
//    return m_bitmap[pos];
//  }
//  //! set value at a particular position
//  void SetValue( size_t pos, bool value ) {
//    m_bitmap[pos] = value;
//  }
//
//  //! set value between 2 positions, inclusive
//  void SetValue( size_t startPos, size_t endPos, bool value ) {
//    for(size_t pos = startPos ; pos <= endPos ; pos++) {
//      m_bitmap[pos] = value;
//    }
//  }
  //! whether every word has been translated
//  bool IsComplete() const {
//    return GetSize() == GetNumWordsCovered();
//  }
  //! whether the wordrange overlaps with any translated word in this bitmap
  inline bool Overlap(const RealG2sWordsRange &x) const {
    size_t l = 0, r = 0;
    size_t lsize = m_bitmap.size(), rsize = x.m_bitmap.size();
    while (l < lsize && r < rsize) {
      if (m_bitmap[l].Overlap(x.m_bitmap[r]))
        return true;
      if (m_bitmap[l] < x.m_bitmap[r])
        l++;
      else r++;
    }
    return false;
  }

  inline bool Overlap(const WordsRange &range) const {
    for(size_t l = 0; l < m_bitmap.size(); l++) {
      if (m_bitmap[l].Overlap(range))
        return true;
    }
    return false;
  }

  //! number of elements
//  inline size_t GetSourceSize() const {
//    return m_size;
//  }

  inline size_t GetSize() const {
    return m_cov;
  }

  inline void GetVector(std::vector<size_t>& ret) const {
    for (size_t i = 0; i < m_bitmap.size(); i++) {
      size_t startPos = m_bitmap[i].GetStartPos(),
             endPos = m_bitmap[i].GetEndPos();
      for (size_t x = startPos; x <= endPos; x++)
        ret.push_back(x);
    }
  }

  inline bool Covered(size_t pos) const {
    for (size_t i = 0; i < m_bitmap.size(); i++) {
      size_t startPos = m_bitmap[i].GetStartPos(),
             endPos = m_bitmap[i].GetEndPos();
      if (pos < startPos) return false;
      else if (pos <= endPos) return true;
    }
    return false;
  }

  //! transitive comparison of RealG2sWordsRange
  inline int Compare (const RealG2sWordsRange &x) const {
    // -1 = less than
    // +1 = more than
    // 0	= same

    if (m_cov > x.m_cov)
      return 1;
    if (m_cov < x.m_cov)
      return -1;
    size_t lsize = m_bitmap.size(), rsize = x.m_bitmap.size();
    if (lsize < rsize)
      return 1;
    if (lsize > rsize)
      return -1;
    for (size_t i = 0; i < lsize; i++) {
      if (m_bitmap[i] == x.m_bitmap[i])
        continue;
      if (m_bitmap[i] < x.m_bitmap[i])
        return -1;
      return 1;
    }
    return 0;
  }

  inline size_t GetStartPos() const {
    CHECK (m_cov > 0);
//      return NOT_FOUND;
    return m_bitmap.front().GetStartPos();
  }
  inline size_t GetEndPos() const {
    if (m_cov == 0)
      return NOT_FOUND;
    return m_bitmap.back().GetEndPos();
  }

  inline size_t GetMin() const {
    return GetStartPos();
  }

  inline size_t GetMax() const {
    return GetEndPos();
  }

  inline void Add (const RealG2sWordsRange& x) {
    if (x.GetSize() == 0) return;
    if (m_bitmap.size() == 0) {
      m_bitmap = x.m_bitmap;
      m_cov = x.m_cov;
      return;
    }
    for(size_t i = 0; i < x.m_bitmap.size(); i++)
      Add(x.m_bitmap[i]);
  }

  inline void Add (const WordsRange& range) {
    size_t xl = range.GetStartPos(), xr = range.GetEndPos();
    if (xl > GetMax()+1) {
      m_bitmap.push_back(range);
      m_cov += range.GetNumWordsCovered();
      return;
    }
    if (xr+1 < GetMin()) {
      m_bitmap.insert(m_bitmap.begin(), range);
      m_cov += range.GetNumWordsCovered();
      return;
    }
    for(std::vector<WordsRange>::iterator iter = m_bitmap.begin();
        iter != m_bitmap.end();) {
      size_t l = iter->GetStartPos(), r = iter->GetEndPos();
      if (xr+1 < l) {
        // one the left
        m_bitmap.insert(iter, WordsRange(xl,xr));
        m_cov += xr-xl+1;
        return;
      } else if (xl > r+1) {
        iter++;
      } else {
        // overlap or adjacent
        xr = xr > r ? xr : r;
        xl = xl < l ? xl : l;
        m_bitmap.erase(iter);
        m_cov -= r-l+1;
      }
    }
    m_bitmap.insert(m_bitmap.end(), WordsRange(xl,xr));
    m_cov += xr-xl+1;
  }

//  inline void Add (size_t pos) {
//    //TODO
//  }

  //! transitive comparison
  inline bool operator<(const RealG2sWordsRange& x) const {
    int comp = Compare(x);
    if (comp < 0)
      return true;
    return false;
  }

  // equality operator
  inline bool operator==(const RealG2sWordsRange& x) const {
    int comp = Compare(x);
    if (comp == 0)
      return true;
    return false;
  }

  WordsRange ToRange() const {
    return WordsRange(GetStartPos(), GetEndPos());
  }

//  inline size_t GetNumWordsBetween(const RealG2sWordsRange& x) const {
//    CHECK(!Overlap(x));
//
//    if (x.m_endPos < m_startPos) {
//      return m_startPos - x.m_endPos - 1;
//    }
//
//    return x.m_startPos - m_endPos - 1;
//  }

  std::string GetCovString () const {
    CHECK (m_cov > 0 && m_bitmap.size() > 0);
    std::string ret = "";
    for (size_t i = 0; i < m_bitmap.size(); i++) {
      ret += SPrint<size_t>(m_bitmap[i].GetStartPos()) + "-"
          + SPrint<size_t>(m_bitmap[i].GetEndPos()) + ":";
    }
    return ret.erase(ret.size()-1);
  }

  bool IsContinuous () const {
    if(m_bitmap.size() <= 1) return true;
    return false;
  }

  void SetWordsBitmap(WordsBitmap& bitmap, bool val) const {
    for (size_t i = 0; i < m_bitmap.size(); i++) {
      size_t startPos = m_bitmap[i].GetStartPos(),
             endPos = m_bitmap[i].GetEndPos();
      bitmap.SetValue(startPos, endPos, val);
    }
  }

  void GetGapsWordsRange(std::vector<WordsRange>& gaps) const {
    if (m_bitmap.size() <= 1) return;
    for (size_t i = 1; i < m_bitmap.size(); i++) {
      size_t startPos = m_bitmap[i-1].GetEndPos() + 1;
      size_t endPos = m_bitmap[i].GetStartPos() - 1;
      CHECK(startPos <= endPos);
      gaps.push_back(WordsRange(startPos, endPos));
    }
  }

//  size_t GetRoot() const {
//    CHECK(m_sorted.size()>0);
//    //CHECK(m_sorted[0] == *m_set.begin());
//    return m_sorted[0];
//  }

//  bool operator< (const RealG2sWordsRange &compare) const {
//    return Compare(compare) < 0;
//  }
//
//  inline size_t GetEdgeToTheLeftOf(size_t l) const {
//    if (l == 0) return l;
//    while (l && !m_bitmap[l-1]) {
//      --l;
//    }
//    return l;
//  }
//
//  inline size_t GetEdgeToTheRightOf(size_t r) const {
//    if (r+1 == m_size) return r;
//    while (r+1 < m_size && !m_bitmap[r+1]) {
//      ++r;
//    }
//    return r;
//  }


  //! TODO - ??? no idea
//  int GetFutureCosts(int lastPos) const ;

  //! converts bitmap into an integer ID: it consists of two parts: the first 16 bit are the pattern between the first gap and the last word-1, the second 16 bit are the number of filled positions. enforces a sentence length limit of 65535 and a max distortion of 16
//  WordsBitmapID GetID() const {
//    CHECK(m_size < (1<<16));
//
//    size_t start = GetFirstGapPos();
//    if (start == NOT_FOUND) start = m_size; // nothing left
//
//    size_t end = GetLastPos();
//    if (end == NOT_FOUND) end = 0; // nothing translated yet
//
//    CHECK(end < start || end-start <= 16);
//    WordsBitmapID id = 0;
//    for(size_t pos = end; pos > start; pos--) {
//      id = id*2 + (int) GetValue(pos);
//    }
//    return id + (1<<16) * start;
//  }
//
//  //! converts bitmap into an integer ID, with an additional span covered
//  WordsBitmapID GetIDPlus( size_t startPos, size_t endPos ) const {
//    CHECK(m_size < (1<<16));
//
//    size_t start = GetFirstGapPos();
//    if (start == NOT_FOUND) start = m_size; // nothing left
//
//    size_t end = GetLastPos();
//    if (end == NOT_FOUND) end = 0; // nothing translated yet
//
//    if (start == startPos) start = endPos+1;
//    if (end < endPos) end = endPos;
//
//    CHECK(end < start || end-start <= 16);
//    WordsBitmapID id = 0;
//    for(size_t pos = end; pos > start; pos--) {
//      id = id*2;
//      if (GetValue(pos) || (startPos<=pos && pos<=endPos))
//        id++;
//    }
//    return id + (1<<16) * start;
//  }

  TO_STRING();
};

// friend
inline std::ostream& operator<<(std::ostream& out, const RealG2sWordsRange& range)
{
  out << "{" << range.GetCovString() << "}";
  return out;
}

}
}
#endif
