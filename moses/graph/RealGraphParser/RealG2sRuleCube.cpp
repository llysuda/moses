// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "RealG2sCell.h"
#include "RealG2sCellCollection.h"
#include "RealG2sTranslationOptions.h"
#include "moses/StaticData.h"
#include "moses/Util.h"
#include "moses/WordsRange.h"

#include <boost/functional/hash.hpp>
#include "RealG2sRuleCube.h"
#include "RealG2sRuleCubeQueue.h"

namespace Moses
{
namespace RealGraph
{

// initialise the RealG2sRuleCube by creating the top-left corner item
RealG2sRuleCube::RealG2sRuleCube(const RealG2sTranslationOptions &transOpt,
                   const RealG2sCellCollection &allRealG2sCells,
                   RealG2sManager &manager)
  : m_transOpt(transOpt)
{
  RealG2sRuleCubeItem *item = new RealG2sRuleCubeItem(transOpt, allRealG2sCells);
  m_covered.insert(item);
  if (StaticData::Instance().GetCubePruningLazyScoring()) {
    item->EstimateScore();
  } else {
    item->CreateHypothesis(transOpt, manager);
  }
  m_queue.push(item);
}

RealG2sRuleCube::~RealG2sRuleCube()
{
  RemoveAllInColl(m_covered);
}

RealG2sRuleCubeItem *RealG2sRuleCube::Pop(RealG2sManager &manager)
{
  RealG2sRuleCubeItem *item = m_queue.top();
  m_queue.pop();
  CreateNeighbors(*item, manager);
  return item;
}

// create new RealG2sRuleCube for neighboring principle rules
void RealG2sRuleCube::CreateNeighbors(const RealG2sRuleCubeItem &item, RealG2sManager &manager)
{
  // create neighbor along translation dimension
  const RealG2sTranslationDimension &translationDimension =
    item.GetTranslationDimension();
  if (translationDimension.HasMoreTranslations()) {
    CreateNeighbor(item, -1, manager);
  }

  // create neighbors along all hypothesis dimensions
  for (size_t i = 0; i < item.GetHypothesisDimensions().size(); ++i) {
    const RealG2sHypothesisDimension &dimension = item.GetHypothesisDimensions()[i];
    if (dimension.HasMoreHypo()) {
      CreateNeighbor(item, i, manager);
    }
  }
}

void RealG2sRuleCube::CreateNeighbor(const RealG2sRuleCubeItem &item, int dimensionIndex,
                              RealG2sManager &manager)
{
  RealG2sRuleCubeItem *newItem = new RealG2sRuleCubeItem(item, dimensionIndex);
  std::pair<ItemSet::iterator, bool> result = m_covered.insert(newItem);
  if (!result.second) {
    delete newItem;  // already seen it
  } else {
    if (StaticData::Instance().GetCubePruningLazyScoring()) {
      newItem->EstimateScore();
    } else {
      newItem->CreateHypothesis(m_transOpt, manager);
    }
    m_queue.push(newItem);
  }
}

}
}

