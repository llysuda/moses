/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <algorithm>
#include <iostream>
#include <vector>
#include "moses/StaticData.h"
#include "RealG2sCellCollection.h"
#include "moses/WordsRange.h"
#include "moses/InputType.h"
#include "moses/InputPath.h"
#include "RealG2sTranslationOptions.h"

#include "RealG2sWordsRange.h"
#include "RealG2sTermTranslationOptionColl.h"

#include "moses/StaticData.h"
#include "moses/FF/UnknownWordPenaltyProducer.h"

#include "util/string_piece.hh"
#include "moses/Util.h"
#include <string>

#include "RealG2sParserCallback.h"
#include "RealG2sWordsRange.h"

using namespace std;
using namespace Moses::Graph;

namespace Moses
{
namespace RealGraph
{

RealG2sTermTranslationOptionColl::RealG2sTermTranslationOptionColl(const InputType &input)
  : m_source(input), m_unkFlag(input.GetSize(), 1)
{
}

RealG2sTermTranslationOptionColl::~RealG2sTermTranslationOptionColl()
{
  DeleteColl();
}

void RealG2sTermTranslationOptionColl::Add(const TargetPhraseCollection &tpc,
                                     const RealG2sStackVec &stackVec,
                                     const RealG2sWordsRange &range)
{
  if (tpc.IsEmpty()) {
    return;
  }

//  cerr << range << endl;


//  if (!range.IsContinuous()) {
//    cerr << endl << range << endl;
//    for(TargetPhraseCollection::const_iterator iter = tpc.begin();
//          iter != tpc.end(); ++iter) {
//      std::cerr << **iter << "\'t" << (*iter)->GetFutureScore() << std::endl;
//    }
//  }

  float score = RealG2sTranslationOptions::CalcEstimateOfBestScore(tpc, stackVec);
  // m_collection has reached capacity: create a new object.
//  RealG2sTranslationOptions* options = new RealG2sTranslationOptions(tpc, stackVec,
//                                range, score);
//  if (m_collection.find(range) != m_collection.end()) {
//    cerr << range << endl;
//    CHECK(false);
//  }
  CHECK(m_collection.find(range) == m_collection.end());
  CHECK(stackVec.size() == 0);

  m_collection[range] = score;
  if (range.GetSize() == 1)
    m_unkFlag[range.GetMin()] = 0;
}

void RealG2sTermTranslationOptionColl::AddPhraseOOV(TargetPhrase &phrase, std::list<TargetPhraseCollection*> &waste_memory, const RealG2sWordsRange &range)
{
  TargetPhraseCollection *tpc = new TargetPhraseCollection();
  tpc->Add(&phrase);
  waste_memory.push_back(tpc);
  RealG2sStackVec empty;
  Add(*tpc, empty, range);
}

void RealG2sTermTranslationOptionColl::ApplyThreshold()
{
//  float scoreThreshold = -std::numeric_limits<float>::infinity();
//
//  CollType::iterator iter;
//  for (iter = m_collection.begin(); iter != m_collection.end(); ++iter) {
//    const RealG2sTranslationOptions *transOpt = iter->second;
//    float score = transOpt->GetEstimateOfBestScore();
//    scoreThreshold = (score > scoreThreshold) ? score : scoreThreshold;
//  }
//
//  scoreThreshold += StaticData::Instance().GetTranslationOptionThreshold();
//
//  for (iter = m_collection.begin(); iter != m_collection.end();) {
//    RealG2sTranslationOptions *transOpt = iter->second;
//    float score = transOpt->GetEstimateOfBestScore();
//    if (score < scoreThreshold) {
//      delete transOpt;
//      m_collection.erase(iter);
//    } else iter++;
//
//  }
}

void RealG2sTermTranslationOptionColl::UpdateFutureScore(SquareMatrix& m_futureScore, const std::map<const RealG2sWordsRange*, float>& range2score)
{
  std::map<const RealG2sWordsRange*, float>::const_iterator iter;
  for(iter = range2score.begin(); iter != range2score.end(); ++iter) {
    const RealG2sWordsRange& range = *(iter->first);
    float score = iter->second;

    CollType::iterator iter2 = m_collection.find(range);
    if (iter2 == m_collection.end() || iter2->second < score)
      m_collection[range] = score;
  }
  CalcFutureScore(m_futureScore);
}

void RealG2sTermTranslationOptionColl::CalcFutureScore(SquareMatrix& m_futureScore)
{
    size_t size = m_source.GetSize(); // the width of the matrix
    vector< vector< vector<const RealG2sWordsRange*> > > dtuRange;
    dtuRange.resize(size);

    for(size_t row=0; row<size; row++) {
        for(size_t col=row; col<size; col++) {
            vector<const RealG2sWordsRange*> dummy;
            dtuRange[row].push_back(dummy);
            m_futureScore.SetScore(row, col, -numeric_limits<float>::infinity());
        }
    }

    // walk all the translation options and record the cheapest option for each span
    CollType::const_iterator iter;
    for(iter = m_collection.begin(); iter != m_collection.end(); iter++) {
//      const RealG2sTranslationOptions &transOptList = *(iter->second);
      const RealG2sWordsRange* range = &(iter->first);

//      cerr << *range << " " << iter->second << endl;

      size_t min = range->GetMin();
      size_t max = range->GetMax();

      if (!range->IsContinuous()) {
        dtuRange[min][max-min].push_back(range);
        continue;
      }

      float score = iter->second;
      if (score > m_futureScore.GetScore(min, max))
        m_futureScore.SetScore(min, max, score);
    }

    m_futureScore.SetScore(size-1, size-1, 0.0f);

//    cerr << m_collection.size() << endl;
//    cerr << m_futureScore << endl;

    // now fill all the cells in the strictly upper triangle
    //   there is no way to modify the diagonal now, in the case
    //   where no translation option covers a single-word span,
    //   we leave the +inf in the matrix
    // like in chart parsing we want each cell to contain the highest score
    // of the full-span trOpt or the sum of scores of joining two smaller spans

    for(size_t colstart = 1; colstart < size ; colstart++) {
      for(size_t diagshift = 0; diagshift < size-colstart ; diagshift++) {
        size_t startPos = diagshift;
        size_t endPos = colstart+diagshift;

        // dtu
        const vector< const RealG2sWordsRange* >& ranges = dtuRange[startPos][endPos-startPos];
        size_t sizeDTU = ranges.size();
        if (sizeDTU > 0) {
          for(size_t d = 0; d < sizeDTU; d++) {
            float scoreDTU = 0.0f;
            const RealG2sWordsRange* range = ranges[d];
            vector<WordsRange> gaps;
            range->GetGapsWordsRange(gaps);
            for(size_t gi = 0; gi < gaps.size(); gi++) {
              scoreDTU += m_futureScore.GetScore(gaps[gi].GetStartPos(), gaps[gi].GetEndPos());
            }
//            const RealG2sTranslationOptions &transOptList = Get(range);

            float score = Get(range) + scoreDTU;
            if (score > m_futureScore.GetScore(startPos, endPos))
              m_futureScore.SetScore(startPos, endPos, score);
          }
        }

        for(size_t joinAt = startPos; joinAt < endPos ; joinAt++)  {
          float joinedScore = m_futureScore.GetScore(startPos, joinAt)
                                                  + m_futureScore.GetScore(joinAt+1, endPos);
          if (joinedScore > m_futureScore.GetScore(startPos, endPos))
              m_futureScore.SetScore(startPos, endPos, joinedScore);
        }
      }
    }

//    cerr << m_futureScore << endl;
}

}
}
