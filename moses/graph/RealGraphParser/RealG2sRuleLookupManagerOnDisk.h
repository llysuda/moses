/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_RealG2sRuleLookupManagerOnDisk_h
#define moses_RealG2sRuleLookupManagerOnDisk_h

#include "OnDiskPt/OnDiskWrapper.h"

#include "moses/TranslationModel/RuleTable/PhraseDictionaryOnDisk.h"
#include "RealG2sParserCallback.h"
#include "moses/InputType.h"
#include "RealG2sDotChartOnDisk.h"
#include "RealG2sRuleLookupManagerCYKPlus.h"

namespace Moses
{

namespace RealGraph
{

//! Implementation of RealG2sRuleLookupManager for on-disk rule tables.
class RealG2sRuleLookupManagerOnDisk : public RealG2sRuleLookupManagerCYKPlus
{
public:
  RealG2sRuleLookupManagerOnDisk(const RealG2sParser &parser,
                               const RealG2sCellCollectionBase &cellColl,
                               const PhraseDictionaryOnDisk &dictionary,
                               OnDiskPt::OnDiskWrapper &dbWrapper,
                               const std::vector<FactorType> &inputFactorsVec,
                               const std::vector<FactorType> &outputFactorsVec,
                               const std::string &filePath);

  ~RealG2sRuleLookupManagerOnDisk();

  virtual void GetRealG2sRuleCollection(const WordsRange &range,
                                      RealG2sParserCallback &outCol);
  virtual void GetRealG2sRuleCollectionLexGlue(const WordsRange &range,
                                      RealG2sParserCallback &outCol);
  virtual void GetRealG2sRuleCollectionTerm(RealG2sParserCallback &outColl);

private:
  const PhraseDictionaryOnDisk &m_dictionary;
  OnDiskPt::OnDiskWrapper &m_dbWrapper;
  const std::vector<FactorType> &m_inputFactorsVec;
  const std::vector<FactorType> &m_outputFactorsVec;
  const std::string &m_filePath;
  std::vector<RealG2sDottedRuleStackOnDisk*> m_expandableDottedRuleListVec;
  std::map<UINT64, const TargetPhraseCollection*> m_cache;
  std::list<const OnDiskPt::PhraseNode*> m_sourcePhraseNode;
  size_t maxspan;
  int m_distLimit;
};

}
}  // namespace Moses

#endif
