/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "RealG2sHypothesis.h"
#include "RealG2sCellLabel.h"
#include "RealG2sTranslationOption.h"
#include "RealG2sTranslationOptions.h"

using namespace Moses::Graph;

namespace Moses
{
namespace RealGraph
{

RealG2sTranslationOptions::RealG2sTranslationOptions(const TargetPhraseCollection &targetPhraseColl,
    const RealG2sStackVec &stackVec,
    const RealG2sWordsRange &wordsRange,
    float score)
  : m_stackVec(stackVec)
  , m_wordsSet(wordsRange)
  , m_estimateOfBestScore(score)
{
  TargetPhraseCollection::const_iterator iter;
  for (iter = targetPhraseColl.begin(); iter != targetPhraseColl.end(); ++iter) {
    const TargetPhrase *origTP = *iter;
    boost::shared_ptr<RealG2sTranslationOption> ptr(new RealG2sTranslationOption(*origTP));
    m_collection.push_back(ptr);
  }
}

RealG2sTranslationOptions::~RealG2sTranslationOptions()
{

}

float RealG2sTranslationOptions::CalcEstimateOfBestScore(
  const TargetPhraseCollection &tpc,
  const RealG2sStackVec &stackVec)
{
  const TargetPhrase &targetPhrase = **(tpc.begin());
  float estimateOfBestScore = targetPhrase.GetFutureScore();

  for (RealG2sStackVec::const_iterator p = stackVec.begin(); p != stackVec.end();
       ++p) {
    const RealG2sHypoList *stack = (*p)->GetStack().cube;
    assert(stack);
    assert(!stack->empty());
    const RealG2sHypothesis &bestHypo = **(stack->begin());
    estimateOfBestScore += bestHypo.GetTotalScore();
  }
  return estimateOfBestScore;
}

void RealG2sTranslationOptions::Evaluate(const InputType &input, const InputPath &inputPath)
{
  CollType::iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.end(); ++iter) {
    RealG2sTranslationOption &transOpt = **iter;
    transOpt.Evaluate(input, inputPath);
  }

}

}
}
