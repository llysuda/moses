// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include <stdio.h>
#include "RealG2sCell.h"
#include "RealG2sHypothesis.h"
#include "RealG2sTranslationOptions.h"
#include "RealG2sTrellisDetourQueue.h"
#include "RealG2sTrellisNode.h"
#include "RealG2sTrellisPath.h"
#include "RealG2sTrellisPathList.h"
#include "moses/StaticData.h"
#include "moses/DecodeStep.h"
#include "moses/TreeInput.h"
#include "moses/FF/WordPenaltyProducer.h"
#include "RealG2sManager.h"
#include "DyMultiDiGraphInput.h"

using namespace std;
using namespace Moses;

namespace Moses
{
extern bool g_debug;

namespace RealGraph
{

/* constructor. Initialize everything prior to decoding a particular sentence.
 * \param source the sentence to be decoded
 * \param system which particular set of models to use.
 */
RealG2sManager::RealG2sManager(InputType& source)
  :m_source(source)
  ,m_hypoStackColl(source, *this)
  ,m_start(clock())
  ,m_hypothesisId(0)
  ,m_parser(source, m_hypoStackColl)
  ,m_translationOptionList(StaticData::Instance().GetRuleLimit(), source)
{
}

RealG2sManager::~RealG2sManager()
{
  clock_t end = clock();
  float et = (end - m_start);
  et /= (float)CLOCKS_PER_SEC;
  VERBOSE(1, "Translation took " << et << " seconds" << endl);

}

void RealG2sManager::AddSourceNT(const RealG2sWordsRange& cov)
{
  DyMultiDiGraph& source = static_cast<DyMultiDiGraph&>(m_source);
  source.AddNT(cov);
}

//! decode the sentence. This contains the main laps. Basically, the CKY++ algorithm
void RealG2sManager::ProcessSentence()
{
  VERBOSE(1,"Translating: " << m_source << endl);

  ResetSentenceStats(m_source);

  VERBOSE(2,"Decoding: " << endl);
  //RealG2sHypothesis::ResetHypoCount();

//  AddXmlRealG2sOptions();

  m_parser.CalculateFutureCost();

  typedef std::map<const RealG2sWordsRange*, float> Range2ScoreColl;
  Range2ScoreColl range2score;

  // MAIN LOOP
  size_t size = m_source.GetSize();
  for (size_t width = 1; width <= size-2; ++width) {
    for (size_t startPos = 1; startPos <= size-1-width; ++startPos) {
      size_t endPos = startPos + width - 1;
      WordsRange range(startPos, endPos);

      // create trans opt
      m_translationOptionList.Clear();
      bool notunk = m_parser.Create(range, m_translationOptionList);

//      if (width==1 && startPos > 0 && notunk)
//        m_translationOptionList.Clear();

//      if (m_translationOptionList.GetSize() > 0) {
//        cerr << "[" << startPos << "," << width << "] = " << m_translationOptionList.GetSize() << endl;
//        for (size_t si = 0;si < m_translationOptionList.GetSize(); si++) {
//          cerr << m_translationOptionList.Get(si).GetSourceWordsSet() << " ";
//          cerr << m_translationOptionList.Get(si).GetTargetPhrases()[0]->GetPhrase() << endl;
//        }
//        cerr << endl;
//      }

      m_translationOptionList.ApplyThreshold();
//      m_translationOptionList.GroupTranslationOptions();

      // decode
//      if (width > 1)
        range = WordsRange(1, width);
      RealG2sCell &cell = m_hypoStackColl.Get(range);

      cell.ProcessSentence(m_translationOptionList, m_hypoStackColl);
//
      m_translationOptionList.Clear();

//      if (width == 1) {
//        cell.PruneToSize();
//        cell.CleanupArcList();
//
//        // clean empty hypocoll and add source nonterm
//        cell.GroupHypo();
//
//        cell.SortHypotheses();
//      }

    }

//    if (width > 1) {
      RealG2sCell &cell = m_hypoStackColl.Get(WordsRange(1,width));
      cell.PruneToSize();
      cell.CleanupArcList();

      // clean empty hypocoll and add source nonterm
      cell.GroupHypo();

      cell.SortHypotheses();

      cell.CollectRangeScore(range2score);
//    }
  }


  if (StaticData::Instance().GetUpdateFutureCost()) {
    cerr << "update future cost before using glue rules" << endl;
    m_parser.UpdateFutureCost(range2score);
    range2score.clear();
  }
  /// glue
  for (size_t width = 1; width <= size; ++width) {
    size_t startPos = 0;
    size_t endPos = startPos + width - 1;
    WordsRange range(startPos, endPos);

    // create trans opt
    m_translationOptionList.Clear();
    bool notunk = m_parser.Create(range, m_translationOptionList);

//      if (width==1 && startPos > 0 && notunk)
//        m_translationOptionList.Clear();

//      if (m_translationOptionList.GetSize() > 0) {
//        cerr << "[" << startPos << "," << width << "] = " << m_translationOptionList.GetSize() << endl;
//        for (size_t si = 0;si < m_translationOptionList.GetSize(); si++) {
//          cerr << m_translationOptionList.Get(si).GetSourceWordsSet() << " ";
//          cerr << m_translationOptionList.Get(si).GetTargetPhrases()[0]->GetPhrase() << endl;
//        }
//        cerr << endl;
//      }

      m_translationOptionList.ApplyThreshold();

      RealG2sCell &cell = m_hypoStackColl.Get(range);

      cell.ProcessSentence(m_translationOptionList, m_hypoStackColl);
//
      m_translationOptionList.Clear();

      cell.PruneToSize();
      cell.CleanupArcList();

      // clean empty hypocoll and add source nonterm
      cell.GroupHypo();
      cell.SortHypotheses();
  }


  IFVERBOSE(1) {

    for (size_t startPos = 0; startPos < size; ++startPos) {
      cerr.width(3);
      cerr << startPos << " ";
    }
    cerr << endl;
    for (size_t width = 1; width <= size; width++) {
      for( size_t space = 0; space < width-1; space++ ) {
        cerr << "  ";
      }
      for (size_t startPos = 0; startPos <= size-width; ++startPos) {
        WordsRange range(startPos, startPos+width-1);
        cerr.width(3);
        cerr << m_hypoStackColl.Get(range).GetSize() << " ";
      }
      cerr << endl;
    }
  }
}

/** add specific translation options and hypotheses according to the XML override translation scheme.
 *  Doesn't seem to do anything about walls and zones.
 *  @todo check walls & zones. Check that the implementation doesn't leak, xml options sometimes does if you're not careful
 */
void RealG2sManager::AddXmlRealG2sOptions()
{
//  const StaticData &staticData = StaticData::Instance();
//  const std::vector <RealG2sTranslationOptions*> xmlRealG2sOptionsList = m_source.GetXmlRealG2sTranslationOptions();
//  IFVERBOSE(2) {
//    cerr << "AddXmlRealG2sOptions " << xmlRealG2sOptionsList.size() << endl;
//  }
//  if (xmlRealG2sOptionsList.size() == 0) return;
//
//  for(std::vector<RealG2sTranslationOptions*>::const_iterator i = xmlRealG2sOptionsList.begin();
//      i != xmlRealG2sOptionsList.end(); ++i) {
//    RealG2sTranslationOptions* opt = *i;
//
//    const TargetPhrase &targetPhrase = opt->GetTargetPhrases()[0]->GetPhrase();
//    const WordsRange &range = opt->GetSourceWordsRange();
//
//    RealG2sRuleCubeItem* item = new RealG2sRuleCubeItem( *opt, m_hypoStackColl );
//    RealG2sHypothesis* hypo = new RealG2sHypothesis(*opt, *item, *this);
//    hypo->Evaluate();
//
//    const Word &targetLHS = hypo->GetTargetLHS();
//
//    RealG2sCell &cell = m_hypoStackColl.Get(range);
//    cell.AddHypothesis(hypo);
//  }
}

//! get best complete translation from the top chart cell.
const RealG2sHypothesis *RealG2sManager::GetBestHypothesis() const
{
  size_t size = m_source.GetSize();

  if (size == 0) // empty source
    return NULL;
  else {
    WordsRange range(0, size-1);
    const RealG2sCell &lastCell = m_hypoStackColl.Get(range);
    return lastCell.GetBestHypothesis();
  }
}

/** Calculate the n-best paths through the output hypergraph.
 * Return the list of paths with the variable ret
 * \param count how may paths to return
 * \param ret return argument
 * \param onlyDistinct whether to check for distinct output sentence or not (default - don't check, just return top n-paths)
 */
void RealG2sManager::CalcNBest(size_t count, RealG2sTrellisPathList &ret,bool onlyDistinct) const
{
  size_t size = m_source.GetSize();
  if (count == 0 || size == 0)
    return;

  // Build a RealG2sTrellisPath for the 1-best path, if any.
  WordsRange range(0, size-1);
  const RealG2sCell &lastCell = m_hypoStackColl.Get(range);
  const RealG2sHypothesis *hypo = lastCell.GetBestHypothesis();
  if (hypo == NULL) {
    // no hypothesis
    return;
  }
  boost::shared_ptr<RealG2sTrellisPath> basePath(new RealG2sTrellisPath(*hypo));

  // Add it to the n-best list.
  if (count == 1) {
    ret.Add(basePath);
    return;
  }

  // Set a limit on the number of detours to pop.  If the n-best list is
  // restricted to distinct translations then this limit should be bigger
  // than n.  The n-best factor determines how much bigger the limit should be.
  const StaticData &staticData = StaticData::Instance();
  const size_t nBestFactor = staticData.GetNBestFactor();
  size_t popLimit;
  if (!onlyDistinct) {
    popLimit = count-1;
  } else if (nBestFactor == 0) {
    // 0 = 'unlimited.'  This actually sets a large-ish limit in case too many
    // translations are identical.
    popLimit = count * 1000;
  } else {
    popLimit = count * nBestFactor;
  }

  // Create an empty priority queue of detour objects.  It is bounded to
  // contain no more than popLimit items.
  RealG2sTrellisDetourQueue contenders(popLimit);

  // Get all complete translations
  const HypoList *topHypos = lastCell.GetAllSortedHypotheses();

  // Create a RealG2sTrellisDetour for each complete translation and add it to the queue
  HypoList::const_iterator iter;
  for (iter = topHypos->begin(); iter != topHypos->end(); ++iter) {
    const RealG2sHypothesis &hypo = **iter;
    boost::shared_ptr<RealG2sTrellisPath> basePath(new RealG2sTrellisPath(hypo));
    RealG2sTrellisDetour *detour = new RealG2sTrellisDetour(basePath, basePath->GetFinalNode(), hypo);
    contenders.Push(detour);
  }

  delete topHypos;

  // Record the output phrase if distinct translations are required.
  set<Phrase> distinctHyps;

  // MAIN loop
  for (size_t i = 0; ret.GetSize() < count && !contenders.Empty() && i < popLimit; ++i) {
    // Get the best detour from the queue.
    std::auto_ptr<const RealG2sTrellisDetour> detour(contenders.Pop());
    CHECK(detour.get());

    // Create a full base path from the chosen detour.
    //basePath.reset(new RealG2sTrellisPath(*detour));
    boost::shared_ptr<RealG2sTrellisPath> path(new RealG2sTrellisPath(*detour));

    // Generate new detours from this base path and add them to the queue of
    // contenders.  The new detours deviate from the base path by a single
    // replacement along the previous detour sub-path.
    CHECK(path->GetDeviationPoint());
    CreateDeviantPaths(path, *(path->GetDeviationPoint()), contenders);

    // If the n-best list is allowed to contain duplicate translations (at the
    // surface level) then add the new path unconditionally, otherwise check
    // whether the translation has seen before.
    if (!onlyDistinct) {
      ret.Add(path);
    } else {
      Phrase tgtPhrase = path->GetOutputPhrase();
      if (distinctHyps.insert(tgtPhrase).second) {
        ret.Add(path);
      }
    }
  }
}

void RealG2sManager::GetSearchGraph(long translationId, std::ostream &outputSearchGraphStream) const
{
  size_t size = m_source.GetSize();

  // which hypotheses are reachable?
  std::map<unsigned,bool> reachable;
  WordsRange fullRange(0, size-1);
  const RealG2sCell &lastCell = m_hypoStackColl.Get(fullRange);
  const RealG2sHypothesis *hypo = lastCell.GetBestHypothesis();

  if (hypo == NULL) {
    // no hypothesis
    return;
  }
  FindReachableHypotheses( hypo, reachable);

  for (size_t width = 1; width <= size; ++width) {
    for (size_t startPos = 0; startPos <= size-width; ++startPos) {
      size_t endPos = startPos + width - 1;
      WordsRange range(startPos, endPos);
      TRACE_ERR(" " << range << "=");

      const RealG2sCell &cell = m_hypoStackColl.Get(range);
      cell.GetSearchGraph(translationId, outputSearchGraphStream, reachable);
    }
  }
}

void RealG2sManager::FindReachableHypotheses( const RealG2sHypothesis *hypo, std::map<unsigned,bool> &reachable ) const
{
  // do not recurse, if already visited
  if (reachable.find(hypo->GetId()) != reachable.end()) {
    return;
  }

  // recurse
  reachable[ hypo->GetId() ] = true;
  const std::vector<const RealG2sHypothesis*> &previous = hypo->GetPrevHypos();
  for(std::vector<const RealG2sHypothesis*>::const_iterator i = previous.begin(); i != previous.end(); ++i) {
    FindReachableHypotheses( *i, reachable );
  }

  // also loop over recombined hypotheses (arcs)
  const RealG2sArcList *arcList = hypo->GetArcList();
  if (arcList) {
    RealG2sArcList::const_iterator iterArc;
    for (iterArc = arcList->begin(); iterArc != arcList->end(); ++iterArc) {
      const RealG2sHypothesis &arc = **iterArc;
      FindReachableHypotheses( &arc, reachable );
    }
  }
}

void RealG2sManager::CreateDeviantPaths(
  boost::shared_ptr<const RealG2sTrellisPath> basePath,
  RealG2sTrellisDetourQueue &q)
{
  CreateDeviantPaths(basePath, basePath->GetFinalNode(), q);
}

void RealG2sManager::CreateDeviantPaths(
  boost::shared_ptr<const RealG2sTrellisPath> basePath,
  const RealG2sTrellisNode &substitutedNode,
  RealG2sTrellisDetourQueue &queue)
{
  const RealG2sArcList *arcList = substitutedNode.GetHypothesis().GetArcList();
  if (arcList) {
    for (RealG2sArcList::const_iterator iter = arcList->begin();
         iter != arcList->end(); ++iter) {
      const RealG2sHypothesis &replacement = **iter;
      queue.Push(new RealG2sTrellisDetour(basePath, substitutedNode,
                                        replacement));
    }
  }
  // recusively create deviant paths for child nodes
  const RealG2sTrellisNode::NodeChildren &children = substitutedNode.GetChildren();
  RealG2sTrellisNode::NodeChildren::const_iterator iter;
  for (iter = children.begin(); iter != children.end(); ++iter) {
    const RealG2sTrellisNode &child = **iter;
    CreateDeviantPaths(basePath, child, queue);
  }
}

}
} // namespace Moses
