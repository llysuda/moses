// $Id$
// vim:tabstop=2
/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "RealG2sTrellisPath.h"

#include "RealG2sHypothesis.h"
#include "RealG2sTrellisDetour.h"
#include "RealG2sTrellisDetourQueue.h"
#include "RealG2sTrellisNode.h"

namespace Moses
{
namespace RealGraph
{

RealG2sTrellisPath::RealG2sTrellisPath(const RealG2sHypothesis &hypo)
  : m_finalNode(new RealG2sTrellisNode(hypo))
  , m_deviationPoint(NULL)
  , m_scoreBreakdown(hypo.GetScoreBreakdown())
  , m_totalScore(hypo.GetTotalScore())
{
}

RealG2sTrellisPath::RealG2sTrellisPath(const RealG2sTrellisDetour &detour)
  : m_finalNode(new RealG2sTrellisNode(detour, m_deviationPoint))
  , m_scoreBreakdown(detour.GetBasePath().m_scoreBreakdown)
  , m_totalScore(0)
{
  CHECK(m_deviationPoint);
  ScoreComponentCollection scoreChange;
  scoreChange = detour.GetReplacementHypo().GetScoreBreakdown();
  scoreChange.MinusEquals(detour.GetSubstitutedNode().GetHypothesis().GetScoreBreakdown());
  m_scoreBreakdown.PlusEquals(scoreChange);
  m_totalScore = m_scoreBreakdown.GetWeightedScore();
}

RealG2sTrellisPath::~RealG2sTrellisPath()
{
  delete m_finalNode;
}

Phrase RealG2sTrellisPath::GetOutputPhrase() const
{
  Phrase ret = GetFinalNode().GetOutputPhrase();
  return ret;
}

}
}  // namespace Moses
