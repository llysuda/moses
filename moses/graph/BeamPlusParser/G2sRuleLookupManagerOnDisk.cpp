/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "../BeamPlusParser/G2sRuleLookupManagerOnDisk.h"

#include <algorithm>

#include "moses/graph/G2sParser.h"
#include "moses/TranslationModel/RuleTable/PhraseDictionaryOnDisk.h"
#include "moses/StaticData.h"
#include "moses/graph/G2sParserCallback.h"
#include "OnDiskPt/TargetPhraseCollection.h"
#include "moses/graph/MultiDiGraphInput.h"
#include "moses/graph/G2sTranslationOptionColl.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

G2sRuleLookupManagerOnDisk::G2sRuleLookupManagerOnDisk(
  const G2sParser &parser,
  const PhraseDictionaryOnDisk &dictionary,
  OnDiskPt::OnDiskWrapper &dbWrapper,
  const std::vector<FactorType> &inputFactorsVec,
  const std::vector<FactorType> &outputFactorsVec,
  const std::string &filePath)
  : G2sRuleLookupManagerCYKPlus(parser)
  , m_dictionary(dictionary)
  , m_dbWrapper(dbWrapper)
  , m_inputFactorsVec(inputFactorsVec)
  , m_outputFactorsVec(outputFactorsVec)
  , m_filePath(filePath)
{
}

G2sRuleLookupManagerOnDisk::~G2sRuleLookupManagerOnDisk()
{
  std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache;
  for (iterCache = m_cache.begin(); iterCache != m_cache.end(); ++iterCache) {
    delete iterCache->second;
  }
  m_cache.clear();
}

void G2sRuleLookupManagerOnDisk::GetG2sRuleCollection(
		G2sTranslationOptionColl &outColl)
{
	const MultiDiGraph& source =  static_cast<const MultiDiGraph&>(GetParser().GetSource());
	const OnDiskPt::PhraseNode& root = m_dbWrapper.GetRootSourceNode();
	WordsSet range;
	GetG2sRuleCollection(range,0,&root,outColl,source);
}

void G2sRuleLookupManagerOnDisk::GetG2sRuleCollection(
    		WordsSet& , int ,
    		const OnDiskPt::PhraseNode* ,
  			G2sTranslationOptionColl &outColl,
    		const MultiDiGraph& source)
{
  typedef map<OnDiskPt::Word, const OnDiskPt::PhraseNode *> ChildType;
  const StaticData &staticData = StaticData::Instance();
  bool incLabel = staticData.GetIncLabel();
  bool augPhrase = staticData.GetAugPhrase();
  bool useGlue = staticData.GetGapGlue();
  size_t size = source.GetSize();
  size_t tableLimit = m_dictionary.GetTableLimit();
//  int maxSpan = StaticData::Instance().GetMaxPhraseLength();
//  int maxOptSpan = StaticData::Instance().GetMaxOptionSpan();

  Word sourceNonTerm(true);
  sourceNonTerm.SetFactor(0, FactorCollection::Instance().AddFactor("X"));
  OnDiskPt::Word *sourceNonTermDB = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, sourceNonTerm);

  typedef pair<WordsSet, const OnDiskPt::PhraseNode*> PairType;
  vector< pair<Phrase, PairType> > stack;
  WordsSet initSet;
  const OnDiskPt::PhraseNode* root = & m_dbWrapper.GetRootSourceNode();
  stack.push_back(make_pair(Phrase(), PairType(initSet, root)));
  while (stack.size() > 0) {
    Phrase phrase = stack.back().first;
    PairType instance = stack.back().second;
    stack.pop_back();

    const WordsSet& range = instance.first;
    const OnDiskPt::PhraseNode* currNode = instance.second;
    size_t range_size = range.GetNumWordsCovered();
    size_t phraseSize = phrase.GetSize();

    if (currNode != root && phraseSize>0 && phrase.GetWord(phraseSize-1).IsNonTerminal()) {
      if (!useGlue) {
        string structStr = source.get_structure_string(range.GetSortedVec(),incLabel);

        ChildType children = currNode->GetChildren(m_dbWrapper);
        for (ChildType::const_iterator childIter = children.begin();
            childIter != children.end(); ++ childIter) {
          Word labelWord;
          childIter->first.ConvertToMoses(m_outputFactorsVec,m_dbWrapper.GetVocab(),labelWord);
          vector<string> tokens = Moses::Tokenize(labelWord.GetString(0).as_string(),"@");
          if (tokens.size() < 2 || tokens[1] != structStr) continue;
          const OnDiskPt::PhraseNode *ptNode2 = childIter->second;
          const TargetPhraseCollection *targetPhraseCollection = NULL;
          UINT64 tpCollFilePos = ptNode2->GetValue();
          std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache = m_cache.find(tpCollFilePos);
          if (iterCache == m_cache.end()) {
            const OnDiskPt::TargetPhraseCollection *tpcollBerkeleyDb = ptNode2->GetTargetPhraseCollection(tableLimit, m_dbWrapper);
            std::vector<float> weightT = staticData.GetWeights(&m_dictionary);
            targetPhraseCollection
            = tpcollBerkeleyDb->ConvertToMoses(m_inputFactorsVec
                                               ,m_outputFactorsVec
                                               ,m_dictionary
                                               ,weightT
                                               ,m_dbWrapper.GetVocab()
                                               ,sourceNonTermDB);

            delete tpcollBerkeleyDb;
            m_cache[tpCollFilePos] = targetPhraseCollection;
          } else {
            // just get out of cache
            targetPhraseCollection = iterCache->second;
          }

          CHECK(targetPhraseCollection);
          if (!targetPhraseCollection->IsEmpty()) {
            Phrase source = phrase;
            source.AddWord(labelWord);
            AddCompletedRule(*targetPhraseCollection, range, outColl, source);
          }
          delete ptNode2;
        }
      } else {
        string structStr = source.get_structure_string(range.GetSortedVec(),incLabel);
        structStr = "X#X@"+structStr;
        Word labelWord(true);
        labelWord.SetFactor(0, FactorCollection::Instance().AddFactor(structStr));
        OnDiskPt::Word *labelWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, labelWord);
        if (labelWordBerkeleyDb) {
          const OnDiskPt::PhraseNode *ptNode2 = currNode->GetChild(*labelWordBerkeleyDb, m_dbWrapper);
          const TargetPhraseCollection *targetPhraseCollection = NULL;
          if (ptNode2) {
            UINT64 tpCollFilePos = ptNode2->GetValue();
            std::map<UINT64, const TargetPhraseCollection*>::const_iterator iterCache = m_cache.find(tpCollFilePos);
            if (iterCache == m_cache.end()) {
              const OnDiskPt::TargetPhraseCollection *tpcollBerkeleyDb = ptNode2->GetTargetPhraseCollection(tableLimit, m_dbWrapper);
              std::vector<float> weightT = staticData.GetWeights(&m_dictionary);
              targetPhraseCollection
              = tpcollBerkeleyDb->ConvertToMoses(m_inputFactorsVec
                                                 ,m_outputFactorsVec
                                                 ,m_dictionary
                                                 ,weightT
                                                 ,m_dbWrapper.GetVocab()
                                                 ,sourceNonTermDB);

              delete tpcollBerkeleyDb;
              m_cache[tpCollFilePos] = targetPhraseCollection;
            } else {
              // just get out of cache
              targetPhraseCollection = iterCache->second;
            }

            CHECK(targetPhraseCollection);
            if (!targetPhraseCollection->IsEmpty()) {
              Phrase source = phrase;
              source.AddWord(labelWord);
              AddCompletedRule(*targetPhraseCollection, range, outColl, source);
            }
            delete ptNode2;
          } // if (node)
          delete labelWordBerkeleyDb;
        }
      }
     }

    // terminal expansion
    size_t i = 0;
    size_t s = 0;
    if (range_size > 0) {
      i = range.GetMax()+1;
      s = range.GetMin();
    }

    size_t limit = size;

    if (phraseSize == 0 || (phraseSize>0 && !phrase.GetWord(phraseSize-1).IsNonTerminal())) {
      for(; i < limit; i++) {
        const Word& lastWord = source.GetWord(i);
        OnDiskPt::Word *lastWordBerkeleyDb = m_dbWrapper.ConvertFromMoses(m_inputFactorsVec, lastWord);
        if (lastWordBerkeleyDb) {
          const OnDiskPt::PhraseNode *ptNode = currNode->GetChild(*lastWordBerkeleyDb, m_dbWrapper);
          if (ptNode) {
            WordsSet nids(range);
            nids.Add(i, range_size);
            Phrase source = phrase;
            source.AddWord(lastWord);
            stack.push_back(make_pair(source, make_pair(nids, ptNode)));
          }
          delete lastWordBerkeleyDb;
        }
      }
    }

    //	// non-terminal expansion
    if (sourceNonTermDB) {
      const OnDiskPt::PhraseNode *ptNode2 = currNode->GetChild(*sourceNonTermDB, m_dbWrapper);
      if (ptNode2) {
        ChildType children = ptNode2->GetChildren(m_dbWrapper);
        for (ChildType::const_iterator childIter = children.begin();
            childIter != children.end(); ++ childIter) {
          Phrase source = phrase;
          source.AddWord(sourceNonTerm);
          stack.push_back(make_pair(source, make_pair(range, childIter->second)));
        }
        delete ptNode2;
      }
    }
    if (currNode != root)
      delete currNode;
  }
  if (sourceNonTermDB)
    delete sourceNonTermDB;
}

} // namespace Moses
}
