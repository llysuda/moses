/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#pragma once
#ifndef moses_G2sRuleLookupManagerOnDisk_h
#define moses_G2sRuleLookupManagerOnDisk_h

#include "G2sRuleLookupManagerCYKPlus.h"
#include "OnDiskPt/OnDiskWrapper.h"

#include "moses/TranslationModel/RuleTable/PhraseDictionaryOnDisk.h"
#include "moses/graph/G2sParserCallback.h"
#include "moses/InputType.h"

namespace Moses
{
namespace Graph
{
class G2sTranslationOptionColl;
class WordsSet;

//! Implementation of G2sRuleLookupManager for on-disk rule tables.
class G2sRuleLookupManagerOnDisk : public G2sRuleLookupManagerCYKPlus
{
public:
  G2sRuleLookupManagerOnDisk(const G2sParser &parser,
														 const PhraseDictionaryOnDisk &dictionary,
														 OnDiskPt::OnDiskWrapper &dbWrapper,
														 const std::vector<FactorType> &inputFactorsVec,
														 const std::vector<FactorType> &outputFactorsVec,
														 const std::string &filePath);

  ~G2sRuleLookupManagerOnDisk();

  virtual void GetG2sRuleCollection(G2sTranslationOptionColl &outColl);
  void GetG2sRuleCollection(
    		WordsSet& nids, int count,
    		const OnDiskPt::PhraseNode* currNode,
  			G2sTranslationOptionColl &outColl,
    		const MultiDiGraph& source);

private:
  const PhraseDictionaryOnDisk &m_dictionary;
  OnDiskPt::OnDiskWrapper &m_dbWrapper;
  const std::vector<FactorType> &m_inputFactorsVec;
  const std::vector<FactorType> &m_outputFactorsVec;
  const std::string &m_filePath;
  std::map<UINT64, const TargetPhraseCollection*> m_cache;
//  std::list<const OnDiskPt::PhraseNode*> m_sourcePhraseNode;
};

}  // namespace Moses
}

#endif
