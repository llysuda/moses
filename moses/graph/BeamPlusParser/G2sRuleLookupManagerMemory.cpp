/***********************************************************************
  Moses - factored phrase-based language decoder
  Copyright (C) 2011 University of Edinburgh

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "../BeamPlusParser/G2sRuleLookupManagerMemory.h"

#include <iostream>

#include "moses/graph/G2sParser.h"
#include "moses/InputType.h"
#include "moses/graph/G2sTranslationOptionColl.h"
#include "moses/StaticData.h"
#include "moses/NonTerminal.h"
#include "moses/TranslationModel/PhraseDictionaryMemory.h"

using namespace std;
using namespace Moses;

namespace Moses
{
namespace Graph
{

G2sRuleLookupManagerMemory::G2sRuleLookupManagerMemory(
  const G2sParser &parser,
  const PhraseDictionaryMemory &ruleTable)
  : G2sRuleLookupManagerCYKPlus(parser)
  , m_ruleTable(ruleTable)
{
}

G2sRuleLookupManagerMemory::~G2sRuleLookupManagerMemory()
{
}

void G2sRuleLookupManagerMemory::GetG2sRuleCollection(
		G2sTranslationOptionColl &outColl)
{
  const MultiDiGraph& source =  static_cast<const MultiDiGraph&>(GetParser().GetSource());
  const PhraseDictionaryNodeMemory& root = m_ruleTable.GetRootNode();
  WordsSet range;
  GetG2sRuleCollection(range,0,&root,outColl,source);
}

void G2sRuleLookupManagerMemory::GetG2sRuleCollection(
		WordsSet&, int,
		const PhraseDictionaryNodeMemory*,
		G2sTranslationOptionColl &outColl,
		const MultiDiGraph& source) const
{
	bool incLabel = StaticData::Instance().GetIncLabel();
	int size = (int)source.GetSize();
	int maxSpan = StaticData::Instance().GetMaxPhraseLength();

	typedef pair<WordsSet, const PhraseDictionaryNodeMemory*> PairType;
	vector< pair<Phrase, PairType> > stack;
	WordsSet initSet;
	const PhraseDictionaryNodeMemory* root = & m_ruleTable.GetRootNode();
	stack.push_back(make_pair(Phrase(), PairType(initSet, root)));
	while (stack.size() > 0) {
	  Phrase phrase = stack.back().first;
	    PairType instance = stack.back().second;
		stack.pop_back();

		const WordsSet& range = instance.first;
		const PhraseDictionaryNodeMemory* currNode = instance.second;

		size_t range_size = range.GetNumWordsCovered();

		if (currNode != root) {
			string structStr = source.get_structure_string(range.GetSortedVec(),incLabel);
			Word labelWord;
			labelWord.SetFactor(0, FactorCollection::Instance().AddFactor(structStr));
			//TODO add structStr to labelWord;
			const PhraseDictionaryNodeMemory *ptNode2 = currNode->GetChild(labelWord);
			if (ptNode2) {
				const TargetPhraseCollection &targetPhrases = ptNode2->GetTargetPhraseCollection();
				Phrase source = phrase;
				source.AddWord(labelWord);
				AddCompletedRule(targetPhrases, range, outColl, source);
			}
		}

		// terminal expansion
		if (range_size < maxSpan) {
			int i = 0;
			if (range_size > 0)
					i = range.GetMax()+1;

			for(; i < size; i++) {
				const Word& lastWord = source.GetWord(i);
				const PhraseDictionaryNodeMemory *ptNode = currNode->GetChild(lastWord);
				if (ptNode) {
					WordsSet nids(range);
					nids.Add(i, range_size);
					Phrase source = phrase;
					source.AddWord(lastWord);
					stack.push_back(make_pair(source, make_pair(nids, ptNode)));
				}
			}
		}

	//	// non-terminal expansion
		const PhraseDictionaryNodeMemory::NonTerminalMap& nonTermMap = currNode->GetNonTerminalMap();
		PhraseDictionaryNodeMemory::NonTerminalMap::const_iterator p;
		PhraseDictionaryNodeMemory::NonTerminalMap::const_iterator end = nonTermMap.end();
		for (p = nonTermMap.begin(); p != end; ++p) {
		    Phrase source = phrase;
		    source.AddWord(p->first.first);
			stack.push_back(make_pair(source, make_pair(range, &(p->second))));
		}
	}
}


}  // namespace Moses
}
