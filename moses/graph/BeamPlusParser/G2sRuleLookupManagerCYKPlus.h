/***********************************************************************
 Moses - statistical machine translation system
 Copyright (C) 2006-2012 University of Edinburgh

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************/

#pragma once
#ifndef moses_G2sRuleLookupManagerCYKPlus_h
#define moses_G2sRuleLookupManagerCYKPlus_h

#include "moses/graph/G2sRuleLookupManager.h"

namespace Moses
{
class TargetPhraseCollection;

namespace Graph
{
class WordsSet;

/** @todo what is this?
 */
class G2sRuleLookupManagerCYKPlus : public G2sRuleLookupManager
{
public:
  G2sRuleLookupManagerCYKPlus(const G2sParser &parser)
    : G2sRuleLookupManager(parser) {}

protected:
  void AddCompletedRule(
      const TargetPhraseCollection &tpc,
      const WordsSet &range,
      G2sTranslationOptionColl &outColl,
      const Phrase& sourcePhrase) const;
};

}  // namespace Moses
}

#endif
