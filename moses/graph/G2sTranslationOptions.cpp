/***********************************************************************
 Moses - factored phrase-based language decoder
 Copyright (C) 2010 Hieu Hoang

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 ***********************************************************************/

#include "G2sTranslationOptions.h"

#include "G2sHypothesis.h"
#include "G2sTranslationOption.h"

using namespace std;

namespace Moses
{
namespace Graph
{

G2sTranslationOptions::G2sTranslationOptions(const TargetPhraseCollection &targetPhraseColl,
    const WordsSet &wordsRange,
    float score,
    const Phrase& sourcePhrase)
  : m_wordsRange(&wordsRange)
  , m_estimateOfBestScore(score)
  , m_sourcePhrase(sourcePhrase)
{
  string lhs = sourcePhrase.GetWord(sourcePhrase.GetSize()-1).GetString(0).as_string();
  vector<string> tokens = Moses::Tokenize(lhs, "@");
  m_lhs = tokens[0];

  size_t count = 0;
  TargetPhraseCollection::const_iterator iter;
  for (iter = targetPhraseColl.begin(); iter != targetPhraseColl.end(); ++iter) {
    const TargetPhrase *origTP = *iter;

    if (count == 0) {
      size_t size = origTP->GetSize();
      if (size > 1 && origTP->GetWord(size-1).IsNonTerminal())
        m_nextLHS = origTP->GetWord(size-1).GetString(0).as_string();
      else
        m_nextLHS = "XX";
    }

    boost::shared_ptr<G2sTranslationOption> ptr(new G2sTranslationOption(*origTP, *m_wordsRange, &m_sourcePhrase));
    ptr.get()->SetMembers(m_lhs, m_nextLHS);
    m_collection.push_back(ptr);
  }

}

G2sTranslationOptions::~G2sTranslationOptions()
{

}

float G2sTranslationOptions::CalcEstimateOfBestScore(
  const TargetPhraseCollection &tpc)
{
  const TargetPhrase &targetPhrase = **(tpc.begin());
  float estimateOfBestScore = targetPhrase.GetFutureScore();
//  for (G2sStackVec::const_iterator p = stackVec.begin(); p != stackVec.end();
//       ++p) {
//    const G2sHypoList *stack = (*p)->GetStack().cube;
//    assert(stack);
//    assert(!stack->empty());
//    const G2sHypothesis &bestHypo = **(stack->begin());
//    estimateOfBestScore += bestHypo.GetTotalScore();
//  }
  return estimateOfBestScore;
}

void G2sTranslationOptions::Evaluate(const InputType &input)
{
  CollType::iterator iter;
  for (iter = m_collection.begin(); iter != m_collection.end(); ++iter) {
    G2sTranslationOption &transOpt = **iter;
    transOpt.Evaluate(input);
  }

}

}
}
