// $Id$
#ifndef moses_TranslationOptionCollectionConfusionNet_h
#define moses_TranslationOptionCollectionConfusionNet_h

#include "TranslationOptionCollection.h"
#include "InputPath.h"
#include <list>
#include "WordsRange.h"

namespace Moses
{

class ConfusionNet;

/** Holds all translation options, for all spans, of a particular confusion network input
 * Inherited from TranslationOptionCollection.
 */
class TranslationOptionCollectionConfusionNet : public TranslationOptionCollection
{
public:
  typedef std::vector< std::vector<InputPathList> > InputPathMatrix;
  //typedef std::list<WordsRange> RangeList;
  //typedef std::vector< std::vector<InputPathList> > IndexMatrix;

protected:
  bool m_useLegacy;

  InputPathMatrix	m_inputPathMatrix; /*< contains translation options */
  //IndexMatrix m_indexMatrix;

  InputPathList &GetInputPathList(size_t startPos, size_t endPos);
  void CreateTranslationOptionsForRangeNew(const DecodeGraph &decodeStepList
      , size_t startPosition
      , size_t endPosition
      , bool adhereTableLimit
      , size_t graphInd);

  void CheckLegacy();
  void CreateTranslationOptionsForRangeLegacy(const DecodeGraph &decodeStepList
      , size_t startPosition
      , size_t endPosition
      , bool adhereTableLimit
      , size_t graphInd);

public:
  TranslationOptionCollectionConfusionNet(const ConfusionNet &source, size_t maxNoTransOptPerCoverage, float translationOptionThreshold);

  void ProcessUnknownWord();
  void ProcessUnknownWord(size_t sourcePos);
  void ProcessUnknownWord(size_t startPos, size_t endPos);
  void CreateTranslationOptions();
  void CreateTranslationOptions2();
  void CreateTranslationOptionsForRange(const DecodeGraph &decodeStepList
                                        , size_t startPosition
                                        , size_t endPosition
                                        , bool adhereTableLimit
                                        , size_t graphInd);
  bool HasXmlOptionsOverlappingRange(size_t startPosition, size_t endPosition, InputPath& inputPath) const;
  void CreateXmlOptionsForRange(size_t startPos, size_t endPos, InputPath& inputPath);
protected:

};

}
#endif

